import { expect, test } from '@playwright/test'

test.describe('Login', () => {
  test('success login', async ({ page }) => {
    await page.goto('http://localhost:4242/fixtures/reload');
    await page.goto('/');
    await expect(page).toHaveURL('https://localhost:3000/');
    await page.getByRole('link', { name: 'Signin' }).click();
    await expect(page).toHaveURL('https://localhost:3000/login');
    await page.getByLabel('Email').fill('leny@communo.app');
    await page.getByLabel('Password').fill('password');
    await page.getByRole('button', { name: 'Let\'s connect' }).click();
    await expect(page).toHaveURL('https://localhost:3000/pooling');
    await page.getByRole('button', { name: 'close' }).click();
    await page.getByRole('button', { name: 'Leny' }).click();
    await page.getByRole('link', { name: 'My profile' }).click();
    await page.locator('input[type="file"]').click();
    await page.locator('input[type="file"]').setInputFiles('tests/sample/avatar.jpg');
    await expect(page.getByText('Pimp your image 🪄')).toBeVisible();
    await page.getByRole('button', { name: 'Save' }).click();
    await page.getByRole('list').getByRole('button', { name: 'Save' }).click();
    await page.getByText('Copy that 5/5 👍').click();

  })
});
