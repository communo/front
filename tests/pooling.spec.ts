import { expect, test } from '@playwright/test'

test('pooling', async ({ page }) => {

  await page.goto('http://localhost:4242/fixtures/reload');
  await page.goto('/');
  await expect(page).toHaveURL('https://localhost:3000/');

  await page.getByRole('link', { name: 'Signin' }).click();
  await expect(page).toHaveURL('https://localhost:3000/login');
  await page.getByLabel('Email').fill('leny@communo.app');
  await page.getByLabel('Password').fill('password');
  await page.getByRole('button', { name: 'Let\'s connect' }).click();
  await page.getByRole('button', { name: 'close' }).click();
  await page.locator('div').filter({ hasText: /^frGoLenyMy profileMy materialsMy bookingsMy circlesLogout$/ }).getByRole('button', { name: 'Switch to french' }).click();
  await page.getByRole('button', { name: 'Leny' }).click();
  await page.getByRole('link').filter({ hasText: 'Mutualiser' }).click();
  await page.getByRole('link', { name: 'Voir le matériel disponible' }).click();
  await page.getByText('4 résultats').click();
  await page.getByRole('link', { name: 'Voir' }).first().click();
  await page.getByText('Broyeur de branches');
  await page.getByText('Loue broyeur de branches thermique, moteur 15 chevaux démarrage électrique, diam').click();
  await page.getByRole('button', { name: 'Emprunter ce matériel à Cyril' }).click();
  await page.getByRole('button', { name: 'Choose mercredi, 12 avril 2023 as your check-in date. It’s available.' }).click();
  await page.getByRole('button', { name: 'Choose samedi, 15 avril 2023 as your check-out date. It’s available.' }).click();
  await page.getByRole('button', { name: 'C’est parti ?' }).click();
  await page.getByText('Parfait !').click();
  await page.getByText('Un mail a été envoyé à Cyril, il devrait te recontacter prochainement.Si tu n\'ob').click();
  await page.getByRole('button', { name: 'Close' }).click();
  await page.getByRole('button', { name: 'Leny' }).click();
  await page.getByRole('link', { name: 'Mes résas' }).click();
  await page.getByText('du 12 avril 2023 au 15 avril 2023').click();
  await page.getByRole('cell', { name: 'En attente' }).click();
  await page.getByRole('cell').filter({ hasText: 'Annuler' }).getByRole('link').click();
  await page.getByRole('heading', { name: 'Réservation : Broyeur de branches - du 12 au 15 avril 2023' }).click();await page.getByRole('link').filter({ hasText: 'La carte' }).click();
  await page.getByText('18 résultats').click();
  await page.getByPlaceholder('Rechercher un cercle').click();
  await page.getByPlaceholder('Rechercher un cercle').fill('Gétigné');
  await page.getByText('1 résultat').click();
  await page.locator('img').nth(2).click();
  await page.getByText('Gétigné Collectif', { exact: true }).click();
  await page.getByText('Gétigné', { exact: true }).click();
  await page.getByText('Gétigné Collectif, c\'est un collectif citoyen et 6 élu·e·s au conseil municipal ').click();
  await page.getByRole('link', { name: 'Voir la fiche du cercle' }).click();
  await page.getByRole('heading', { name: 'Gétigné Collectif (Gétigné)' }).click();
  await page.getByRole('heading', { name: 'Demandes en attente (2)' }).click();
  await page.locator('div').filter({ hasText: /^JuJean Dubois0 évalutations0 annoncesAccepterRejeter$/ }).getByRole('button', { name: 'Accepter' }).click();
  await page.getByText('Jean Dubois fait désormais partie du cercle 🎉').click();
  await page.getByRole('heading', { name: 'Demandes en attente (1)' }).click();
  await page.getByRole('heading', { name: 'Membres actifs (6)' }).click();
  await page.getByRole('button', { name: 'Rejeter' }).click();
  await page.getByText('Compris. N\'hésitez pas à lui envoyer un petit message pour lui expliquer votre c').click();
  await page.getByRole('heading', { name: 'Membres actifs (6)' }).click();
  await page.getByRole('button', { name: 'Gérer' }).click();
  await page.getByPlaceholder('Quel nom représente bien ce cercle ?').click();
  await page.getByPlaceholder('Quel nom représente bien ce cercle ?').fill('Gétigné Collectif ');
  await page.getByPlaceholder('Quel nom représente bien ce cercle ?').press('Control+ArrowLeft');
  await page.getByPlaceholder('Quel nom représente bien ce cercle ?').press('Control+ArrowLeft');
  await page.getByPlaceholder('Quel nom représente bien ce cercle ?').fill('The Gétigné Collectif ');
  await page.getByPlaceholder('Quel nom représente bien ce cercle ?').press('Control+ArrowRight');
  await page.getByPlaceholder('Quel nom représente bien ce cercle ?').press('Control+ArrowRight');
  await page.getByPlaceholder('Quel nom représente bien ce cercle ?').press('ArrowRight');
  await page.getByPlaceholder('Quel nom représente bien ce cercle ?').fill('The Gétigné Collectif');
  await page.getByRole('button', { name: 'Enregistrer' }).click();
  await page.getByText('OK').click();
  await page.getByRole('link', { name: 'The Gétigné Collectif' }).click();
  await page.getByRole('link', { name: 'The Gétigné Collectif' }).click();
  await page.getByRole('heading', { name: 'The Gétigné Collectif (Gétigné)' }).click();
  await page.getByRole('heading', { name: 'Demandes en attente (0)' }).click();
  await page.getByText('Aucun élément dans cette liste 😵‍💫').click();
  await page.getByRole('link', { name: 'Go' }).click();
  await page.getByPlaceholder('Scie circulaire radiale, Imprimante laser, Tondeuse à barbe, Karaoké années 90...').click();
  await page.getByPlaceholder('Scie circulaire radiale, Imprimante laser, Tondeuse à barbe, Karaoké années 90...').fill('Lorem ipsum');
  await page.getByLabel('Marque').click();
  await page.getByLabel('Marque').fill('Lorem');
  await page.getByLabel('Marque').press('Tab');
  await page.getByPlaceholder('Duo MD10, XD-2000').press('Shift+Tab');
  await page.getByPlaceholder('Duo MD10, XD-2000').click();
  await page.getByLabel('Marque').click();
  await page.getByLabel('Marque').fill('Loremio');
  await page.getByPlaceholder('Duo MD10, XD-2000').click();
  await page.getByPlaceholder('Duo MD10, XD-2000').fill('IPSUMIO');
  await page.getByPlaceholder('Décris ton materiél, donne autant de détails que tu aimerais avoir si ce n´était pas le tien.').click();
  await page.getByPlaceholder('Décris ton materiél, donne autant de détails que tu aimerais avoir si ce n´était pas le tien.').fill('Lorem ipsum dolor sit amet');
  await page.getByRole('button', { name: 'Enregistrer' }).click();
  await page.getByRole('combobox', { name: 'Catégorie' }).selectOption('/material_categories/nettoyage-1');
  await page.getByPlaceholder('ABC-12 43').click();
  await page.getByPlaceholder('ABC-12 43').fill('ref123');
  await page.getByRole('button', { name: 'Enregistrer' }).click();
  await page.getByText('🤩 Et un nouveau matériel sur Communo ! Merci').click();
  await page.getByRole('heading', { name: 'Attrape l\'appareil photo et fais parler l\'artiste qui sommeille en toi !' }).click();
  await page.getByRole('button', { name: 'Ajouter une photo' }).click();
  await page.getByRole('textbox').click();
  await page.setInputFiles("input[type='file']", 'tests/sample/image-1.jpg');
  await page.getByRole('button', { name: 'Suivant' }).click();
  await page.locator('span').filter({ hasText: 'Toute la plateforme' }).locator('span').first().click();
  await page.locator('input[name="circlePricings\\[null\\]\\[value\\]"]').click();
  await page.locator('input[name="circlePricings\\[null\\]\\[value\\]"]').fill('7');
  await page.locator('.css-1m5jnul').first().click();
  await page.locator('input[name="circlePricings\\[null\\]\\[value\\]"]').click();
  await page.locator('.css-16czca > .chakra-switch > .chakra-switch__track > .chakra-switch__thumb').first().click();
  await page.locator('input[name="circlePricings\\[null\\]\\[value\\]"]').click();
  await page.getByRole('button', { name: 'Enregistrer' }).click();
  await page.locator('span').filter({ hasText: 'The Gétigné Collectif' }).locator('span').nth(1).click();
  await page.locator('input[name="circlePricings\\[\\/circles\\/the-getigne-collectif\\]\\[value\\]"]').click();
  await page.locator('input[name="circlePricings\\[\\/circles\\/the-getigne-collectif\\]\\[value\\]"]').fill('3');
  await page.locator('input[name="circlePricings\\[null\\]\\[value\\]"]').click();
  await page.locator('input[name="circlePricings\\[null\\]\\[value\\]"]').fill('5');
  await page.getByTitle('Le matériel n´est pas disponible pour les membres de ce cercle').locator('span').first().click();
  await page.locator('input[name="circlePricings\\[\\/circles\\/le-bois-de-la-roche\\]\\[value\\]"]').click();
  await page.locator('input[name="circlePricings\\[\\/circles\\/le-bois-de-la-roche\\]\\[value\\]"]').fill('4');
  await page.getByRole('button', { name: 'Enregistrer' }).click();
  await page.getByText('OK !').click();
  await page.getByRole('heading', { name: 'Woohoo! Ça m\'a l\'air tout bon ça ! 🎉' }).click();
  /*const messages = [
    "En voiture Simone",
    "C'est parti comme en 40 !",
    "En avant Guingamp !",
    "Prends ton manteau, on s'en va",
    "Jetez-moi ici",
    "C'est tipar gaspard"
  ];
  for (const message of messages) {
    const link = await page.evaluate((message) => {
      const links = document.querySelectorAll('a');
      for (const link of links) {
        if (new RegExp(message).test(link.innerText)) {
          return link;
        }
      }
    }, message);

    if (link) {
      await link.click();
      break;
    } else {
      throw Error('Did not find such a link');
    }
  }*/
  await page.goto('/pooling/lorem-ipsum');
  await page.getByRole('heading', { name: 'Lorem ipsum' }).click();
  await page.getByText('3,00 €/jour (The Gétigné Collectif)').click();

});
