import { expect, test } from '@playwright/test'

test.describe('booking', () => {
  test('book success', async ({ page }) => {
    await page.goto('http://localhost:4242/fixtures/reload');
    const now = new Date('2023-05-07');

    await page.evaluate((now) => {
      class FakeDate extends Date {
        constructor() {
          super();
          if (arguments.length === 0) {
            return now;
          }
          return new Date(...arguments);
        }
      }
      window.Date = FakeDate;
    }, now);
    await page.goto('/');
    await expect(page).toHaveURL('https://localhost:3000/');
    await page.getByRole('link', { name: 'Signin' }).click();
    await expect(page).toHaveURL('https://localhost:3000/login');
    await page.getByLabel('Email').fill('leny@communo.app');
    await page.getByLabel('Password').fill('password');
    await page.getByRole('button', { name: 'Let\'s connect' }).click();
    await expect(page).toHaveURL('https://localhost:3000/');
    await page.getByRole('alert').click();
    await page.getByRole('button', { name: 'Leny' }).click();
    await page.getByRole('link').filter({ hasText: 'Pooling' }).click();
    await page.getByRole('link', { name: 'See pooled material' }).click();
    await page.getByText('4 items found').click();
    await page.getByPlaceholder('What do you need ?').fill('karcher');
    await page.getByText('1 item found').click();
    await page.getByRole('link', { name: 'See Karcher' }).click();
    await page.getByRole('button', { name: 'Borrow this equipment to Louis' }).click();
    await page.getByText('April 2023').click();
    await page.getByRole('button', { name: 'Choose Wednesday, April 19, 2023 as your check-in date. It’s available.' }).click();
    await page.getByRole('button', { name: 'Choose Friday, April 21, 2023 as your check-out date. It’s available.' }).click();
    await page.getByText('Ok, so here is a little summary before validating your request with Louis :').click();
    await page.getByText('from April 19, 2023 to April 21, 2023').click();
    await page.getByRole('button', { name: 'Let\'s go ?' }).click();
    await page.getByText('OK ! The request has been made').click();
    await page.getByText('An email has been sent to Louis, she should contact you shortly.If you don\'t get').click();
    await page.getByRole('button', { name: 'Close' }).click();
    await page.getByRole('button', { name: 'Leny' }).click();
    await page.getByRole('menuitem', { name: 'My bookings' }).getByRole('link', { name: 'My bookings' }).click();
    await page.getByText('from April 19, 2023 to April 21, 2023').click();
    await page.getByRole('cell', { name: 'Pending' }).click();
    await page.getByRole('cell').filter({ hasText: 'Cancel' }).getByRole('link').click();
    await page.getByRole('heading', { name: 'Booking : Karcher - from 19th to April 21, 2023' }).click();
    await page.getByRole('heading', { name: 'Pending' }).click();
    await page.getByText('Hello Louis, Thank you for this announcement, I would be very interested. Is it ').click();
    await page.getByRole('button', { name: 'Cancel' }).click();
    await page.getByRole('heading', { name: 'Canceled' }).click();
    })
});
