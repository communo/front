FROM node:22

WORKDIR /usr/src/app

COPY package*.json ./
COPY yarn.lock ./

RUN corepack enable
RUN corepack yarn install
RUN corepack prepare yarn@4.6.0 --activate

EXPOSE 3000

CMD ["yarn", "start"]
