import { initReactI18next } from 'react-i18next'
import i18n from 'i18next'
import Backend from 'i18next-http-backend'
import LanguageDetector from 'i18next-browser-languagedetector'

const ns = ["messages"];
const supportedLngs = ["en", "fr"];

i18n
    .use(initReactI18next)
    .use(LanguageDetector)
    .use(Backend)
    .init({
        lng: 'en',
        fallbackLng: 'en',
        ns: 'messages',
        interpolation: { escapeValue: false },
        react: { useSuspense: false },
        supportedLngs,
    });

supportedLngs.forEach((lang) => {
    ns.forEach((n) => {
        i18n.addResourceBundle(
            lang,
            n,
            require(`../src/translations/${lang}/${ns}.json`)
        );
    });
});

export default i18n;
