// @ts-ignore
import React from 'react'
import { withThemeByClassName } from '@storybook/addon-themes'
import { Preview } from '@storybook/react'
import i18n from './i18next'
import { ChakraProvider } from '@chakra-ui/react'
import { system } from '../src/theme'
import { ColorModeProvider } from '../src/components/ui/color-mode'
import { withRouter } from 'storybook-addon-remix-react-router'

const preview: Preview = {
    globals: {
        locale: 'en',
        locales: {
            en: 'English',
            fr: 'Français',
        },
    },
    parameters: {
        background: {
            default: 'dark',
        },
        controls: {
            matchers: {
                color: /(background|color)$/i,
                date: /Date$/i,
            },
        },
        i18n,
        // reactRouter: reactRouterParameters({ ... }),
    },
    decorators: [
        (Story) => (
            <React.StrictMode>
                <ChakraProvider value={system}>
                    <ColorModeProvider>
                        <div style={{ padding: '5em' }}>
                            <Story />
                        </div>
                    </ColorModeProvider>
                </ChakraProvider>
            </React.StrictMode>
        ),
        withThemeByClassName({
            defaultTheme: 'light',
            themes: { light: '', dark: 'dark' },
        }),
        withRouter,
    ],
}

export default preview
