# Circle pricing material form

## Context

- **Module**: pooling
- **Views**: material create / update

## Objectives

This form allows material's owner to define precisely for every circle he/she's member of the price and the availability (or not).

| available | Price per day          |
| --------- |------------------------|
| yes/no    | null/int               |

## Use cases

- material is **free** for **all the instance**
    - children circles forms are disabled, meaning it's free and available everywhere
- material is available in **all the instance** but with a **price** (eg. 10€)
    - children circles **availability switches** are disabled but price can be defined (cheaper)
- material is free in **a circle**
    - deeper children circles forms are disabled, it will be free for every children circles
- prices is reduced for **a circle** or **all the instance**
  - every more expensive children circle's prices will be reduced to the price set
- owner is trying to set a price more expensive in a children circle
  - an error message tells "Children circle price cannot be more expensive than parent circle which is {price}€/day"