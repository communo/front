import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T;
export type InputMaybe<T> = T;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Iterable: any;
  MaterialOwnershipType: any;
  Upload: any;
};

export type AbstractMaterialOwnership = Node & {
  __typename?: 'AbstractMaterialOwnership';
  id: Scalars['ID'];
  material?: Maybe<Material>;
  materialMainOwned?: Maybe<Material>;
  ownerIRI: Scalars['String'];
  ownerLabel: Scalars['String'];
};

export type Avatar = Node & {
  __typename?: 'Avatar';
  contentUrl?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  dimensions: Scalars['Iterable'];
  id: Scalars['ID'];
  imageName: Scalars['String'];
  mime?: Maybe<Scalars['String']>;
  originalName?: Maybe<Scalars['String']>;
  size: Scalars['Int'];
  updatedAt?: Maybe<Scalars['String']>;
  user?: Maybe<User>;
};

export type Circle = Node & {
  __typename?: 'Circle';
  /**
   * Not persisted property, used to define what can do current logged user;
   * set by CircleResolver.
   */
  activeUserActions: Scalars['Iterable'];
  address?: Maybe<Scalars['String']>;
  admins?: Maybe<UserCursorConnection>;
  children?: Maybe<CircleCursorConnection>;
  circleOwnerships?: Maybe<CircleMaterialOwnershipCursorConnection>;
  city?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  cover?: Maybe<CircleCover>;
  createdAt?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  distance?: Maybe<Scalars['Float']>;
  explicitLocationLabel?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  indexable: Scalars['Boolean'];
  invitationLinks?: Maybe<Array<Maybe<CircleInvitationLink>>>;
  invitations?: Maybe<InvitationCursorConnection>;
  latitude?: Maybe<Scalars['String']>;
  location?: Maybe<Scalars['String']>;
  logo?: Maybe<CircleLogo>;
  longitude?: Maybe<Scalars['String']>;
  memberships?: Maybe<CircleMembershipCursorConnection>;
  name: Scalars['String'];
  owners?: Maybe<UserCursorConnection>;
  parent?: Maybe<Circle>;
  pendingMemberships?: Maybe<CircleMembershipCursorConnection>;
  postalCode?: Maybe<Scalars['String']>;
  pricings?: Maybe<MaterialPricingCursorConnection>;
  slug: Scalars['String'];
  subscription?: Maybe<Subscription>;
  updatedAt?: Maybe<Scalars['String']>;
  website?: Maybe<Scalars['String']>;
};


export type CircleAdminsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  confirmed?: InputMaybe<Scalars['Boolean']>;
  email?: InputMaybe<Scalars['String']>;
  email_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  first?: InputMaybe<Scalars['Int']>;
  firstname?: InputMaybe<Scalars['String']>;
  last?: InputMaybe<Scalars['Int']>;
  lastname?: InputMaybe<Scalars['String']>;
  postalCode?: InputMaybe<Scalars['String']>;
};


export type CircleChildrenArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  city?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  explicitLocationLabel?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  indexable?: InputMaybe<Scalars['Boolean']>;
  invitations_email?: InputMaybe<Scalars['String']>;
  invitations_email_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  last?: InputMaybe<Scalars['Int']>;
  latitude?: InputMaybe<Array<InputMaybe<CircleFilter_Latitude>>>;
  longitude?: InputMaybe<Array<InputMaybe<CircleFilter_Longitude>>>;
  name?: InputMaybe<Scalars['String']>;
};


export type CircleCircleOwnershipsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};


export type CircleInvitationLinksArgs = {
  enabled?: InputMaybe<Scalars['Boolean']>;
};


export type CircleInvitationsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  circle_id?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  invited?: InputMaybe<Scalars['Boolean']>;
  last?: InputMaybe<Scalars['Int']>;
  postalCode?: InputMaybe<Scalars['String']>;
};


export type CircleMembershipsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  circle_slug?: InputMaybe<Scalars['String']>;
  circle_slug_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  order?: InputMaybe<Array<InputMaybe<CircleMembershipFilter_Order>>>;
  status?: InputMaybe<Scalars['String']>;
  status_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  user_slug?: InputMaybe<Scalars['String']>;
  user_slug_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type CircleOwnersArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  confirmed?: InputMaybe<Scalars['Boolean']>;
  email?: InputMaybe<Scalars['String']>;
  email_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  first?: InputMaybe<Scalars['Int']>;
  firstname?: InputMaybe<Scalars['String']>;
  last?: InputMaybe<Scalars['Int']>;
  lastname?: InputMaybe<Scalars['String']>;
  postalCode?: InputMaybe<Scalars['String']>;
};


export type CirclePendingMembershipsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  circle_slug?: InputMaybe<Scalars['String']>;
  circle_slug_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  order?: InputMaybe<Array<InputMaybe<CircleMembershipFilter_Order>>>;
  status?: InputMaybe<Scalars['String']>;
  status_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  user_slug?: InputMaybe<Scalars['String']>;
  user_slug_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type CirclePricingsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  order?: InputMaybe<Array<InputMaybe<MaterialPricingFilter_Order>>>;
};

export type CircleCover = Node & {
  __typename?: 'CircleCover';
  circle?: Maybe<Circle>;
  contentUrl?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  createdBy?: Maybe<User>;
  dimensions: Scalars['Iterable'];
  id: Scalars['ID'];
  imageName: Scalars['String'];
  mime?: Maybe<Scalars['String']>;
  originalName?: Maybe<Scalars['String']>;
  size: Scalars['Int'];
  updatedAt?: Maybe<Scalars['String']>;
  updatedBy?: Maybe<User>;
};

/** Cursor connection for Circle. */
export type CircleCursorConnection = {
  __typename?: 'CircleCursorConnection';
  edges?: Maybe<Array<Maybe<CircleEdge>>>;
  pageInfo: CirclePageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of Circle. */
export type CircleEdge = {
  __typename?: 'CircleEdge';
  cursor: Scalars['String'];
  node?: Maybe<Circle>;
};

export type CircleFilter_Latitude = {
  between?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
};

export type CircleFilter_Longitude = {
  between?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
};

export type CircleImage = Node & {
  __typename?: 'CircleImage';
  contentUrl?: Maybe<Scalars['String']>;
  createdAt: Scalars['String'];
  createdBy?: Maybe<User>;
  dimensions: Scalars['Iterable'];
  id: Scalars['ID'];
  imageName: Scalars['String'];
  mime?: Maybe<Scalars['String']>;
  originalName?: Maybe<Scalars['String']>;
  size: Scalars['Int'];
  updatedAt: Scalars['String'];
  updatedBy?: Maybe<User>;
};

export type CircleInvitationLink = Node & {
  __typename?: 'CircleInvitationLink';
  _id?: Maybe<Scalars['String']>;
  circle: Circle;
  createdAt?: Maybe<Scalars['String']>;
  enabled: Scalars['Boolean'];
  id: Scalars['ID'];
  numberOfUses: Scalars['Int'];
  updatedAt?: Maybe<Scalars['String']>;
};

export type CircleLogo = Node & {
  __typename?: 'CircleLogo';
  circle?: Maybe<Circle>;
  contentUrl?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  createdBy?: Maybe<User>;
  dimensions: Scalars['Iterable'];
  id: Scalars['ID'];
  imageName: Scalars['String'];
  mime?: Maybe<Scalars['String']>;
  originalName?: Maybe<Scalars['String']>;
  size: Scalars['Int'];
  updatedAt?: Maybe<Scalars['String']>;
  updatedBy?: Maybe<User>;
};

export type CircleMaterialOwnership = Node & {
  __typename?: 'CircleMaterialOwnership';
  circle: Circle;
  id: Scalars['ID'];
  material?: Maybe<Material>;
  materialMainOwned?: Maybe<Material>;
  ownerIRI: Scalars['String'];
  ownerLabel: Scalars['String'];
};

/** Cursor connection for CircleMaterialOwnership. */
export type CircleMaterialOwnershipCursorConnection = {
  __typename?: 'CircleMaterialOwnershipCursorConnection';
  edges?: Maybe<Array<Maybe<CircleMaterialOwnershipEdge>>>;
  pageInfo: CircleMaterialOwnershipPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of CircleMaterialOwnership. */
export type CircleMaterialOwnershipEdge = {
  __typename?: 'CircleMaterialOwnershipEdge';
  cursor: Scalars['String'];
  node?: Maybe<CircleMaterialOwnership>;
};

/** Information about the current page. */
export type CircleMaterialOwnershipPageInfo = {
  __typename?: 'CircleMaterialOwnershipPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

export type CircleMembership = Node & {
  __typename?: 'CircleMembership';
  circle: Circle;
  createdAt?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  message?: Maybe<Scalars['String']>;
  permission: CirclePermissionType;
  permissionMarking: Scalars['String'];
  permissionTransitionAvailables?: Maybe<Scalars['Iterable']>;
  status: Scalars['String'];
  statusTransitionAvailables?: Maybe<Scalars['Iterable']>;
  updatedAt?: Maybe<Scalars['String']>;
  user: User;
};

/** Cursor connection for CircleMembership. */
export type CircleMembershipCursorConnection = {
  __typename?: 'CircleMembershipCursorConnection';
  edges?: Maybe<Array<Maybe<CircleMembershipEdge>>>;
  pageInfo: CircleMembershipPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of CircleMembership. */
export type CircleMembershipEdge = {
  __typename?: 'CircleMembershipEdge';
  cursor: Scalars['String'];
  node?: Maybe<CircleMembership>;
};

export type CircleMembershipFilter_Order = {
  createdAt?: InputMaybe<Scalars['String']>;
  permission?: InputMaybe<Scalars['String']>;
  user_firstname?: InputMaybe<Scalars['String']>;
  user_lastname?: InputMaybe<Scalars['String']>;
};

/** Information about the current page. */
export type CircleMembershipPageInfo = {
  __typename?: 'CircleMembershipPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Information about the current page. */
export type CirclePageInfo = {
  __typename?: 'CirclePageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

export enum CirclePermissionType {
  Administrator = 'Administrator',
  Member = 'Member',
  Owner = 'Owner'
}

export enum CommunicationMode {
  Email = 'EMAIL',
  Phone = 'PHONE'
}

export type Contact = Node & {
  __typename?: 'Contact';
  createdAt?: Maybe<Scalars['String']>;
  email: Scalars['String'];
  id: Scalars['ID'];
  message?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  newsletterOptIn: Scalars['Boolean'];
  updatedAt?: Maybe<Scalars['String']>;
};

export type Discussion = Node & {
  __typename?: 'Discussion';
  createdAt?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  messages?: Maybe<MessageCursorConnection>;
  updatedAt?: Maybe<Scalars['String']>;
  users?: Maybe<UserCursorConnection>;
};


export type DiscussionMessagesArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};


export type DiscussionUsersArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  confirmed?: InputMaybe<Scalars['Boolean']>;
  email?: InputMaybe<Scalars['String']>;
  email_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  first?: InputMaybe<Scalars['Int']>;
  firstname?: InputMaybe<Scalars['String']>;
  last?: InputMaybe<Scalars['Int']>;
  lastname?: InputMaybe<Scalars['String']>;
  postalCode?: InputMaybe<Scalars['String']>;
};

/** Cursor connection for Discussion. */
export type DiscussionCursorConnection = {
  __typename?: 'DiscussionCursorConnection';
  edges?: Maybe<Array<Maybe<DiscussionEdge>>>;
  pageInfo: DiscussionPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of Discussion. */
export type DiscussionEdge = {
  __typename?: 'DiscussionEdge';
  cursor: Scalars['String'];
  node?: Maybe<Discussion>;
};

/** Information about the current page. */
export type DiscussionPageInfo = {
  __typename?: 'DiscussionPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

export type Feature = Node & {
  __typename?: 'Feature';
  createdAt?: Maybe<Scalars['String']>;
  /** currentLocale is a non persisted field configured during postLoad event */
  currentLocale?: Maybe<Scalars['String']>;
  defaultLocale: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
  new: Scalars['Boolean'];
  plan?: Maybe<Plan>;
  updatedAt?: Maybe<Scalars['String']>;
  workInProgress: Scalars['Boolean'];
};

/** request the status of the feature flag, false if not exist */
export type FeatureFlag = Node & {
  __typename?: 'FeatureFlag';
  enabled: Scalars['Boolean'];
  featureName: Scalars['String'];
  id: Scalars['ID'];
};

export type Invitation = Node & {
  __typename?: 'Invitation';
  circle?: Maybe<Circle>;
  createdAt?: Maybe<Scalars['String']>;
  email: Scalars['String'];
  firstname?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  invited: Scalars['Boolean'];
  invitedBy?: Maybe<User>;
  lastname?: Maybe<Scalars['String']>;
  postalCode?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['String']>;
  user?: Maybe<User>;
};

/** Cursor connection for Invitation. */
export type InvitationCursorConnection = {
  __typename?: 'InvitationCursorConnection';
  edges?: Maybe<Array<Maybe<InvitationEdge>>>;
  pageInfo: InvitationPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of Invitation. */
export type InvitationEdge = {
  __typename?: 'InvitationEdge';
  cursor: Scalars['String'];
  node?: Maybe<Invitation>;
};

/** Information about the current page. */
export type InvitationPageInfo = {
  __typename?: 'InvitationPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

export type Material = Node & {
  __typename?: 'Material';
  bestPriceForUser?: Maybe<Scalars['Float']>;
  bookings?: Maybe<MaterialBookingCursorConnection>;
  brand?: Maybe<Scalars['String']>;
  category?: Maybe<MaterialCategory>;
  createdAt?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  distance?: Maybe<Scalars['Float']>;
  forkedFrom?: Maybe<Material>;
  /**
   *
   * When a material is "forked", it is cloned and the original material is set as forkedFrom
   */
  forks?: Maybe<MaterialCursorConnection>;
  id: Scalars['ID'];
  images?: Maybe<MaterialImageCursorConnection>;
  latitude?: Maybe<Scalars['Float']>;
  location?: Maybe<Scalars['String']>;
  longitude?: Maybe<Scalars['Float']>;
  mainOwner?: Maybe<User>;
  mainOwnership?: Maybe<AbstractMaterialOwnership>;
  /** Return strictly mainOwner circle. */
  mainOwnershipCircle?: Maybe<Circle>;
  /** Return strictly main owner user. */
  mainOwnershipUser?: Maybe<User>;
  model?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  /** Return users from mainOwner and other ownerships for both user and circle ownership types. */
  ownerUserList?: Maybe<UserCursorConnection>;
  ownerships?: Maybe<Array<Maybe<AbstractMaterialOwnership>>>;
  price: Scalars['Float'];
  pricings?: Maybe<MaterialPricingCursorConnection>;
  published: Scalars['Boolean'];
  reference?: Maybe<Scalars['String']>;
  slug: Scalars['String'];
  status: Scalars['String'];
  updatedAt?: Maybe<Scalars['String']>;
  userActionAvailables?: Maybe<Scalars['Iterable']>;
  userLists?: Maybe<UserListCursorConnection>;
};


export type MaterialBookingsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  endDate?: InputMaybe<Array<InputMaybe<MaterialBookingFilter_EndDate>>>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  material_slug?: InputMaybe<Scalars['String']>;
  material_slug_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  startDate?: InputMaybe<Array<InputMaybe<MaterialBookingFilter_StartDate>>>;
  status?: InputMaybe<Scalars['String']>;
  status_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  user_slug?: InputMaybe<Scalars['String']>;
  user_slug_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type MaterialForksArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  brand?: InputMaybe<Scalars['String']>;
  category_name?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  model?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  order?: InputMaybe<Array<InputMaybe<MaterialFilter_Order>>>;
  reference?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
  status_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type MaterialImagesArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};


export type MaterialOwnerUserListArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  confirmed?: InputMaybe<Scalars['Boolean']>;
  email?: InputMaybe<Scalars['String']>;
  email_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  first?: InputMaybe<Scalars['Int']>;
  firstname?: InputMaybe<Scalars['String']>;
  last?: InputMaybe<Scalars['Int']>;
  lastname?: InputMaybe<Scalars['String']>;
  postalCode?: InputMaybe<Scalars['String']>;
};


export type MaterialPricingsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  order?: InputMaybe<Array<InputMaybe<MaterialPricingFilter_Order>>>;
};


export type MaterialUserListsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};

export type MaterialBooking = Node & {
  __typename?: 'MaterialBooking';
  comment?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  discussion?: Maybe<Discussion>;
  endDate: Scalars['String'];
  id: Scalars['ID'];
  material?: Maybe<Material>;
  past: Scalars['Boolean'];
  periods?: Maybe<Array<Maybe<MaterialBookingDatePeriod>>>;
  price?: Maybe<Scalars['Float']>;
  ratings?: Maybe<RatingCursorConnection>;
  slug: Scalars['String'];
  startDate: Scalars['String'];
  status: Scalars['String'];
  statusTransitionAvailables?: Maybe<Scalars['Iterable']>;
  terminatedStatuses: Scalars['Iterable'];
  transitions: Scalars['Iterable'];
  updatedAt?: Maybe<Scalars['String']>;
  user?: Maybe<User>;
};


export type MaterialBookingRatingsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};

/** Cursor connection for MaterialBooking. */
export type MaterialBookingCursorConnection = {
  __typename?: 'MaterialBookingCursorConnection';
  edges?: Maybe<Array<Maybe<MaterialBookingEdge>>>;
  pageInfo: MaterialBookingPageInfo;
  totalCount: Scalars['Int'];
};

export type MaterialBookingDatePeriod = Node & {
  __typename?: 'MaterialBookingDatePeriod';
  booking: MaterialBooking;
  endDate: Scalars['String'];
  id: Scalars['ID'];
  price?: Maybe<Scalars['Float']>;
  startDate: Scalars['String'];
};

/** Edge of MaterialBooking. */
export type MaterialBookingEdge = {
  __typename?: 'MaterialBookingEdge';
  cursor: Scalars['String'];
  node?: Maybe<MaterialBooking>;
};

export type MaterialBookingFilter_EndDate = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  strictly_after?: InputMaybe<Scalars['String']>;
  strictly_before?: InputMaybe<Scalars['String']>;
};

export type MaterialBookingFilter_StartDate = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  strictly_after?: InputMaybe<Scalars['String']>;
  strictly_before?: InputMaybe<Scalars['String']>;
};

/** Information about the current page. */
export type MaterialBookingPageInfo = {
  __typename?: 'MaterialBookingPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

export type MaterialCategory = Node & {
  __typename?: 'MaterialCategory';
  children?: Maybe<MaterialCategoryCursorConnection>;
  createdAt?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  materials?: Maybe<MaterialCursorConnection>;
  name: Scalars['String'];
  parent?: Maybe<MaterialCategory>;
  slug: Scalars['String'];
  updatedAt?: Maybe<Scalars['String']>;
};


export type MaterialCategoryChildrenArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};


export type MaterialCategoryMaterialsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  brand?: InputMaybe<Scalars['String']>;
  category_name?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  model?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  order?: InputMaybe<Array<InputMaybe<MaterialFilter_Order>>>;
  reference?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
  status_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

/** Cursor connection for MaterialCategory. */
export type MaterialCategoryCursorConnection = {
  __typename?: 'MaterialCategoryCursorConnection';
  edges?: Maybe<Array<Maybe<MaterialCategoryEdge>>>;
  pageInfo: MaterialCategoryPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of MaterialCategory. */
export type MaterialCategoryEdge = {
  __typename?: 'MaterialCategoryEdge';
  cursor: Scalars['String'];
  node?: Maybe<MaterialCategory>;
};

/** Information about the current page. */
export type MaterialCategoryPageInfo = {
  __typename?: 'MaterialCategoryPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Cursor connection for Material. */
export type MaterialCursorConnection = {
  __typename?: 'MaterialCursorConnection';
  edges?: Maybe<Array<Maybe<MaterialEdge>>>;
  pageInfo: MaterialPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of Material. */
export type MaterialEdge = {
  __typename?: 'MaterialEdge';
  cursor: Scalars['String'];
  node?: Maybe<Material>;
};

export type MaterialFilter_Order = {
  createdAt?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['String']>;
};

export type MaterialImage = Node & {
  __typename?: 'MaterialImage';
  contentUrl?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  dimensions: Scalars['Iterable'];
  id: Scalars['ID'];
  imageName: Scalars['String'];
  material: Material;
  mime?: Maybe<Scalars['String']>;
  originalName?: Maybe<Scalars['String']>;
  size: Scalars['Int'];
  updatedAt?: Maybe<Scalars['String']>;
};

/** Cursor connection for MaterialImage. */
export type MaterialImageCursorConnection = {
  __typename?: 'MaterialImageCursorConnection';
  edges?: Maybe<Array<Maybe<MaterialImageEdge>>>;
  pageInfo: MaterialImagePageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of MaterialImage. */
export type MaterialImageEdge = {
  __typename?: 'MaterialImageEdge';
  cursor: Scalars['String'];
  node?: Maybe<MaterialImage>;
};

/** Information about the current page. */
export type MaterialImagePageInfo = {
  __typename?: 'MaterialImagePageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Information about the current page. */
export type MaterialPageInfo = {
  __typename?: 'MaterialPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

export type MaterialPricing = Node & {
  __typename?: 'MaterialPricing';
  circle?: Maybe<Circle>;
  createdAt?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  material: Material;
  updatedAt?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['Float']>;
};

/** Cursor connection for MaterialPricing. */
export type MaterialPricingCursorConnection = {
  __typename?: 'MaterialPricingCursorConnection';
  edges?: Maybe<Array<Maybe<MaterialPricingEdge>>>;
  pageInfo: MaterialPricingPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of MaterialPricing. */
export type MaterialPricingEdge = {
  __typename?: 'MaterialPricingEdge';
  cursor: Scalars['String'];
  node?: Maybe<MaterialPricing>;
};

export type MaterialPricingFilter_Order = {
  value?: InputMaybe<Scalars['String']>;
};

/** Information about the current page. */
export type MaterialPricingPageInfo = {
  __typename?: 'MaterialPricingPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

export type MaterialSearchAlert = Node & {
  __typename?: 'MaterialSearchAlert';
  _id?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  user?: Maybe<User>;
};

/** Cursor connection for MaterialSearchAlert. */
export type MaterialSearchAlertCursorConnection = {
  __typename?: 'MaterialSearchAlertCursorConnection';
  edges?: Maybe<Array<Maybe<MaterialSearchAlertEdge>>>;
  pageInfo: MaterialSearchAlertPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of MaterialSearchAlert. */
export type MaterialSearchAlertEdge = {
  __typename?: 'MaterialSearchAlertEdge';
  cursor: Scalars['String'];
  node?: Maybe<MaterialSearchAlert>;
};

/** Information about the current page. */
export type MaterialSearchAlertPageInfo = {
  __typename?: 'MaterialSearchAlertPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

export type Message = Node & {
  __typename?: 'Message';
  author?: Maybe<User>;
  content: Scalars['String'];
  createdAt?: Maybe<Scalars['String']>;
  discussion: Discussion;
  id: Scalars['ID'];
  parameters: Scalars['String'];
  updatedAt?: Maybe<Scalars['String']>;
};

/** Cursor connection for Message. */
export type MessageCursorConnection = {
  __typename?: 'MessageCursorConnection';
  edges?: Maybe<Array<Maybe<MessageEdge>>>;
  pageInfo: MessagePageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of Message. */
export type MessageEdge = {
  __typename?: 'MessageEdge';
  cursor: Scalars['String'];
  node?: Maybe<Message>;
};

/** Information about the current page. */
export type MessagePageInfo = {
  __typename?: 'MessagePageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  /** AddMessages a Discussion. */
  addMessageDiscussion?: Maybe<AddMessageDiscussionPayload>;
  /** AddUserTos a UserList. */
  addUserToUserList?: Maybe<AddUserToUserListPayload>;
  /** AskToJoins a CircleMembership. */
  askToJoinCircleMembership?: Maybe<AskToJoinCircleMembershipPayload>;
  /** ChangeDates a MaterialBooking. */
  changeDateMaterialBooking?: Maybe<ChangeDateMaterialBookingPayload>;
  /** ChangePasswords a User. */
  changePasswordUser?: Maybe<ChangePasswordUserPayload>;
  /** ChangePermissions a CircleMembership. */
  changePermissionCircleMembership?: Maybe<ChangePermissionCircleMembershipPayload>;
  /** ChangeStatuss a CircleMembership. */
  changeStatusCircleMembership?: Maybe<ChangeStatusCircleMembershipPayload>;
  /** ChangeStatuss a Material. */
  changeStatusMaterial?: Maybe<ChangeStatusMaterialPayload>;
  /** ChangeStatuss a MaterialBooking. */
  changeStatusMaterialBooking?: Maybe<ChangeStatusMaterialBookingPayload>;
  /** Comments a MaterialBooking. */
  commentMaterialBooking?: Maybe<CommentMaterialBookingPayload>;
  /**
   * Confirms a User.
   * @deprecated we do not confirm email anymore
   */
  confirmUser?: Maybe<ConfirmUserPayload>;
  /** Creates a AbstractMaterialOwnership. */
  createAbstractMaterialOwnership?: Maybe<CreateAbstractMaterialOwnershipPayload>;
  /** Creates a Avatar. */
  createAvatar?: Maybe<CreateAvatarPayload>;
  /** Creates a Circle. */
  createCircle?: Maybe<CreateCirclePayload>;
  /** Creates a CircleCover. */
  createCircleCover?: Maybe<CreateCircleCoverPayload>;
  /** Creates a CircleLogo. */
  createCircleLogo?: Maybe<CreateCircleLogoPayload>;
  /** Creates a CircleMaterialOwnership. */
  createCircleMaterialOwnership?: Maybe<CreateCircleMaterialOwnershipPayload>;
  /** Creates a Contact. */
  createContact?: Maybe<CreateContactPayload>;
  /** Creates a Invitation. */
  createInvitation?: Maybe<CreateInvitationPayload>;
  /** Creates a Material. */
  createMaterial?: Maybe<CreateMaterialPayload>;
  /** Creates a MaterialBooking. */
  createMaterialBooking?: Maybe<CreateMaterialBookingPayload>;
  /** Creates a MaterialCategory. */
  createMaterialCategory?: Maybe<CreateMaterialCategoryPayload>;
  /** Creates a MaterialImage. */
  createMaterialImage?: Maybe<CreateMaterialImagePayload>;
  /** Creates a MaterialPricing. */
  createMaterialPricing?: Maybe<CreateMaterialPricingPayload>;
  /** Creates a MaterialSearchAlert. */
  createMaterialSearchAlert?: Maybe<CreateMaterialSearchAlertPayload>;
  /** Creates a Message. */
  createMessage?: Maybe<CreateMessagePayload>;
  /** Creates a Rating. */
  createRating?: Maybe<CreateRatingPayload>;
  /** Creates a Subscription. */
  createSubscription?: Maybe<CreateSubscriptionPayload>;
  /** Creates a User. */
  createUser?: Maybe<CreateUserPayload>;
  /** Creates a UserList. */
  createUserList?: Maybe<CreateUserListPayload>;
  /** Creates a UserMaterialOwnership. */
  createUserMaterialOwnership?: Maybe<CreateUserMaterialOwnershipPayload>;
  /** Deletes a Circle. */
  deleteCircle?: Maybe<DeleteCirclePayload>;
  /** Deletes a CircleImage. */
  deleteCircleImage?: Maybe<DeleteCircleImagePayload>;
  /** Deletes a CircleMaterialOwnership. */
  deleteCircleMaterialOwnership?: Maybe<DeleteCircleMaterialOwnershipPayload>;
  /** Deletes a Invitation. */
  deleteInvitation?: Maybe<DeleteInvitationPayload>;
  /** Deletes a Material. */
  deleteMaterial?: Maybe<DeleteMaterialPayload>;
  /** Deletes a MaterialBooking. */
  deleteMaterialBooking?: Maybe<DeleteMaterialBookingPayload>;
  /** Deletes a MaterialCategory. */
  deleteMaterialCategory?: Maybe<DeleteMaterialCategoryPayload>;
  /** Deletes a MaterialImage. */
  deleteMaterialImage?: Maybe<DeleteMaterialImagePayload>;
  /** Deletes a MaterialPricing. */
  deleteMaterialPricing?: Maybe<DeleteMaterialPricingPayload>;
  /** Deletes a MaterialSearchAlert. */
  deleteMaterialSearchAlert?: Maybe<DeleteMaterialSearchAlertPayload>;
  /** Deletes a Message. */
  deleteMessage?: Maybe<DeleteMessagePayload>;
  /** Deletes a Rating. */
  deleteRating?: Maybe<DeleteRatingPayload>;
  /** Deletes a User. */
  deleteUser?: Maybe<DeleteUserPayload>;
  /** Deletes a UserList. */
  deleteUserList?: Maybe<DeleteUserListPayload>;
  /** Deletes a UserMaterialOwnership. */
  deleteUserMaterialOwnership?: Maybe<DeleteUserMaterialOwnershipPayload>;
  /** Estimates a MaterialBooking. */
  estimateMaterialBooking?: Maybe<EstimateMaterialBookingPayload>;
  /** Forks a Material. */
  forkMaterial?: Maybe<ForkMaterialPayload>;
  /** Handles a Invitation. */
  handleInvitation?: Maybe<HandleInvitationPayload>;
  /** Joins a CircleMembership. */
  joinCircleMembership?: Maybe<JoinCircleMembershipPayload>;
  /** Logins a User. */
  loginUser?: Maybe<LoginUserPayload>;
  /** Logouts a User. */
  logoutUser?: Maybe<LogoutUserPayload>;
  /** RemoveUserFroms a UserList. */
  removeUserFromUserList?: Maybe<RemoveUserFromUserListPayload>;
  /** SendCircles a Invitation. */
  sendCircleInvitation?: Maybe<SendCircleInvitationPayload>;
  /** Sends a Invitation. */
  sendInvitation?: Maybe<SendInvitationPayload>;
  /** SubscribeNewsletters a Contact. */
  subscribeNewsletterContact?: Maybe<SubscribeNewsletterContactPayload>;
  /** Unavailables a MaterialBooking. */
  unavailableMaterialBooking?: Maybe<UnavailableMaterialBookingPayload>;
  /** Updates a AbstractMaterialOwnership. */
  updateAbstractMaterialOwnership?: Maybe<UpdateAbstractMaterialOwnershipPayload>;
  /** Updates a Avatar. */
  updateAvatar?: Maybe<UpdateAvatarPayload>;
  /** Updates a Circle. */
  updateCircle?: Maybe<UpdateCirclePayload>;
  /** Updates a CircleMaterialOwnership. */
  updateCircleMaterialOwnership?: Maybe<UpdateCircleMaterialOwnershipPayload>;
  /** Updates a Material. */
  updateMaterial?: Maybe<UpdateMaterialPayload>;
  /** Updates a MaterialBooking. */
  updateMaterialBooking?: Maybe<UpdateMaterialBookingPayload>;
  /** Updates a MaterialCategory. */
  updateMaterialCategory?: Maybe<UpdateMaterialCategoryPayload>;
  /** Updates a MaterialImage. */
  updateMaterialImage?: Maybe<UpdateMaterialImagePayload>;
  /** Updates a MaterialPricing. */
  updateMaterialPricing?: Maybe<UpdateMaterialPricingPayload>;
  /** Updates a Message. */
  updateMessage?: Maybe<UpdateMessagePayload>;
  /** Updates a Rating. */
  updateRating?: Maybe<UpdateRatingPayload>;
  /** Updates a Settings. */
  updateSettings?: Maybe<UpdateSettingsPayload>;
  /** Updates a User. */
  updateUser?: Maybe<UpdateUserPayload>;
  /** Updates a UserList. */
  updateUserList?: Maybe<UpdateUserListPayload>;
  /** Updates a UserMaterialOwnership. */
  updateUserMaterialOwnership?: Maybe<UpdateUserMaterialOwnershipPayload>;
};


export type MutationAddMessageDiscussionArgs = {
  input: AddMessageDiscussionInput;
};


export type MutationAddUserToUserListArgs = {
  input: AddUserToUserListInput;
};


export type MutationAskToJoinCircleMembershipArgs = {
  input: AskToJoinCircleMembershipInput;
};


export type MutationChangeDateMaterialBookingArgs = {
  input: ChangeDateMaterialBookingInput;
};


export type MutationChangePasswordUserArgs = {
  input: ChangePasswordUserInput;
};


export type MutationChangePermissionCircleMembershipArgs = {
  input: ChangePermissionCircleMembershipInput;
};


export type MutationChangeStatusCircleMembershipArgs = {
  input: ChangeStatusCircleMembershipInput;
};


export type MutationChangeStatusMaterialArgs = {
  input: ChangeStatusMaterialInput;
};


export type MutationChangeStatusMaterialBookingArgs = {
  input: ChangeStatusMaterialBookingInput;
};


export type MutationCommentMaterialBookingArgs = {
  input: CommentMaterialBookingInput;
};


export type MutationCreateAbstractMaterialOwnershipArgs = {
  input: CreateAbstractMaterialOwnershipInput;
};


export type MutationCreateAvatarArgs = {
  input: CreateAvatarInput;
};


export type MutationCreateCircleArgs = {
  input: CreateCircleInput;
};


export type MutationCreateCircleCoverArgs = {
  input: CreateCircleCoverInput;
};


export type MutationCreateCircleLogoArgs = {
  input: CreateCircleLogoInput;
};


export type MutationCreateCircleMaterialOwnershipArgs = {
  input: CreateCircleMaterialOwnershipInput;
};


export type MutationCreateContactArgs = {
  input: CreateContactInput;
};


export type MutationCreateInvitationArgs = {
  input: CreateInvitationInput;
};


export type MutationCreateMaterialArgs = {
  input: CreateMaterialInput;
};


export type MutationCreateMaterialBookingArgs = {
  input: CreateMaterialBookingInput;
};


export type MutationCreateMaterialCategoryArgs = {
  input: CreateMaterialCategoryInput;
};


export type MutationCreateMaterialImageArgs = {
  input: CreateMaterialImageInput;
};


export type MutationCreateMaterialPricingArgs = {
  input: CreateMaterialPricingInput;
};


export type MutationCreateMaterialSearchAlertArgs = {
  input: CreateMaterialSearchAlertInput;
};


export type MutationCreateMessageArgs = {
  input: CreateMessageInput;
};


export type MutationCreateRatingArgs = {
  input: CreateRatingInput;
};


export type MutationCreateSubscriptionArgs = {
  input: CreateSubscriptionInput;
};


export type MutationCreateUserArgs = {
  input: CreateUserInput;
};


export type MutationCreateUserListArgs = {
  input: CreateUserListInput;
};


export type MutationCreateUserMaterialOwnershipArgs = {
  input: CreateUserMaterialOwnershipInput;
};


export type MutationDeleteCircleArgs = {
  input: DeleteCircleInput;
};


export type MutationDeleteCircleImageArgs = {
  input: DeleteCircleImageInput;
};


export type MutationDeleteCircleMaterialOwnershipArgs = {
  input: DeleteCircleMaterialOwnershipInput;
};


export type MutationDeleteInvitationArgs = {
  input: DeleteInvitationInput;
};


export type MutationDeleteMaterialArgs = {
  input: DeleteMaterialInput;
};


export type MutationDeleteMaterialBookingArgs = {
  input: DeleteMaterialBookingInput;
};


export type MutationDeleteMaterialCategoryArgs = {
  input: DeleteMaterialCategoryInput;
};


export type MutationDeleteMaterialImageArgs = {
  input: DeleteMaterialImageInput;
};


export type MutationDeleteMaterialPricingArgs = {
  input: DeleteMaterialPricingInput;
};


export type MutationDeleteMaterialSearchAlertArgs = {
  input: DeleteMaterialSearchAlertInput;
};


export type MutationDeleteMessageArgs = {
  input: DeleteMessageInput;
};


export type MutationDeleteRatingArgs = {
  input: DeleteRatingInput;
};


export type MutationDeleteUserArgs = {
  input: DeleteUserInput;
};


export type MutationDeleteUserListArgs = {
  input: DeleteUserListInput;
};


export type MutationDeleteUserMaterialOwnershipArgs = {
  input: DeleteUserMaterialOwnershipInput;
};


export type MutationEstimateMaterialBookingArgs = {
  input: EstimateMaterialBookingInput;
};


export type MutationForkMaterialArgs = {
  input: ForkMaterialInput;
};


export type MutationHandleInvitationArgs = {
  input: HandleInvitationInput;
};


export type MutationJoinCircleMembershipArgs = {
  input: JoinCircleMembershipInput;
};


export type MutationLoginUserArgs = {
  input: LoginUserInput;
};


export type MutationLogoutUserArgs = {
  input: LogoutUserInput;
};


export type MutationRemoveUserFromUserListArgs = {
  input: RemoveUserFromUserListInput;
};


export type MutationSendCircleInvitationArgs = {
  input: SendCircleInvitationInput;
};


export type MutationSendInvitationArgs = {
  input: SendInvitationInput;
};


export type MutationSubscribeNewsletterContactArgs = {
  input: SubscribeNewsletterContactInput;
};


export type MutationUnavailableMaterialBookingArgs = {
  input: UnavailableMaterialBookingInput;
};


export type MutationUpdateAbstractMaterialOwnershipArgs = {
  input: UpdateAbstractMaterialOwnershipInput;
};


export type MutationUpdateAvatarArgs = {
  input: UpdateAvatarInput;
};


export type MutationUpdateCircleArgs = {
  input: UpdateCircleInput;
};


export type MutationUpdateCircleMaterialOwnershipArgs = {
  input: UpdateCircleMaterialOwnershipInput;
};


export type MutationUpdateMaterialArgs = {
  input: UpdateMaterialInput;
};


export type MutationUpdateMaterialBookingArgs = {
  input: UpdateMaterialBookingInput;
};


export type MutationUpdateMaterialCategoryArgs = {
  input: UpdateMaterialCategoryInput;
};


export type MutationUpdateMaterialImageArgs = {
  input: UpdateMaterialImageInput;
};


export type MutationUpdateMaterialPricingArgs = {
  input: UpdateMaterialPricingInput;
};


export type MutationUpdateMessageArgs = {
  input: UpdateMessageInput;
};


export type MutationUpdateRatingArgs = {
  input: UpdateRatingInput;
};


export type MutationUpdateSettingsArgs = {
  input: UpdateSettingsInput;
};


export type MutationUpdateUserArgs = {
  input: UpdateUserInput;
};


export type MutationUpdateUserListArgs = {
  input: UpdateUserListInput;
};


export type MutationUpdateUserMaterialOwnershipArgs = {
  input: UpdateUserMaterialOwnershipInput;
};

/** A node, according to the Relay specification. */
export type Node = {
  /** The id of this node. */
  id: Scalars['ID'];
};

export type Plan = Node & {
  __typename?: 'Plan';
  button?: Maybe<Scalars['String']>;
  condition?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  /** currentLocale is a non persisted field configured during postLoad event */
  currentLocale?: Maybe<Scalars['String']>;
  defaultLocale: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  features?: Maybe<Array<Maybe<Feature>>>;
  id: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
  price: Scalars['Float'];
  slug: Scalars['String'];
  updatedAt?: Maybe<Scalars['String']>;
};

export type Query = {
  __typename?: 'Query';
  abstractMaterialOwnerships?: Maybe<Array<Maybe<AbstractMaterialOwnership>>>;
  asOwnerMaterialBookings?: Maybe<MaterialBookingCursorConnection>;
  circle?: Maybe<Circle>;
  circleInvitationLink?: Maybe<CircleInvitationLink>;
  circleMaterialOwnership?: Maybe<CircleMaterialOwnership>;
  circleMaterialOwnerships?: Maybe<CircleMaterialOwnershipCursorConnection>;
  circleMembership?: Maybe<CircleMembership>;
  circleMemberships?: Maybe<CircleMembershipCursorConnection>;
  circles?: Maybe<CircleCursorConnection>;
  discussion?: Maybe<Discussion>;
  discussions?: Maybe<DiscussionCursorConnection>;
  feature?: Maybe<Feature>;
  features?: Maybe<Array<Maybe<Feature>>>;
  /** request the status of the feature flag, false if not exist */
  getFeatureFlag?: Maybe<FeatureFlag>;
  invitation?: Maybe<Invitation>;
  invitations?: Maybe<InvitationCursorConnection>;
  material?: Maybe<Material>;
  materialBooking?: Maybe<MaterialBooking>;
  materialBookingDatePeriod?: Maybe<MaterialBookingDatePeriod>;
  materialBookingDatePeriods?: Maybe<Array<Maybe<MaterialBookingDatePeriod>>>;
  materialBookings?: Maybe<MaterialBookingCursorConnection>;
  materialCategories?: Maybe<MaterialCategoryCursorConnection>;
  materialCategory?: Maybe<MaterialCategory>;
  materialPricing?: Maybe<MaterialPricing>;
  materialPricings?: Maybe<MaterialPricingCursorConnection>;
  materialSearchAlerts?: Maybe<MaterialSearchAlertCursorConnection>;
  materials?: Maybe<MaterialCursorConnection>;
  myCircles?: Maybe<CircleCursorConnection>;
  myMaterials?: Maybe<MaterialCursorConnection>;
  node?: Maybe<Node>;
  ownerMaterials?: Maybe<MaterialCursorConnection>;
  plan?: Maybe<Plan>;
  plans?: Maybe<Array<Maybe<Plan>>>;
  rating?: Maybe<Rating>;
  ratings?: Maybe<RatingCursorConnection>;
  searchMaterials?: Maybe<MaterialCursorConnection>;
  subscription?: Maybe<Subscription>;
  subscriptions?: Maybe<SubscriptionCursorConnection>;
  user?: Maybe<User>;
  userList?: Maybe<UserList>;
  userLists?: Maybe<UserListCursorConnection>;
  userMaterialOwnership?: Maybe<UserMaterialOwnership>;
  userMaterialOwnerships?: Maybe<UserMaterialOwnershipCursorConnection>;
  users?: Maybe<UserCursorConnection>;
};


export type QueryAsOwnerMaterialBookingsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  endDate?: InputMaybe<Array<InputMaybe<MaterialBookingFilter_EndDate>>>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  material_slug?: InputMaybe<Scalars['String']>;
  material_slug_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  startDate?: InputMaybe<Array<InputMaybe<MaterialBookingFilter_StartDate>>>;
  status?: InputMaybe<Scalars['String']>;
  status_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  user_slug?: InputMaybe<Scalars['String']>;
  user_slug_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type QueryCircleArgs = {
  id: Scalars['ID'];
};


export type QueryCircleInvitationLinkArgs = {
  id: Scalars['ID'];
};


export type QueryCircleMaterialOwnershipArgs = {
  id: Scalars['ID'];
};


export type QueryCircleMaterialOwnershipsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};


export type QueryCircleMembershipArgs = {
  id: Scalars['ID'];
};


export type QueryCircleMembershipsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  circle_slug?: InputMaybe<Scalars['String']>;
  circle_slug_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  order?: InputMaybe<Array<InputMaybe<CircleMembershipFilter_Order>>>;
  status?: InputMaybe<Scalars['String']>;
  status_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  user_slug?: InputMaybe<Scalars['String']>;
  user_slug_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type QueryCirclesArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  city?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  explicitLocationLabel?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  indexable?: InputMaybe<Scalars['Boolean']>;
  invitations_email?: InputMaybe<Scalars['String']>;
  invitations_email_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  last?: InputMaybe<Scalars['Int']>;
  latitude?: InputMaybe<Array<InputMaybe<CircleFilter_Latitude>>>;
  longitude?: InputMaybe<Array<InputMaybe<CircleFilter_Longitude>>>;
  name?: InputMaybe<Scalars['String']>;
  searchTerms?: InputMaybe<Scalars['String']>;
};


export type QueryDiscussionArgs = {
  id: Scalars['ID'];
};


export type QueryDiscussionsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};


export type QueryFeatureArgs = {
  id: Scalars['ID'];
};


export type QueryGetFeatureFlagArgs = {
  featureName: Scalars['String'];
};


export type QueryInvitationArgs = {
  id: Scalars['ID'];
};


export type QueryInvitationsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  circle_id?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  invited?: InputMaybe<Scalars['Boolean']>;
  last?: InputMaybe<Scalars['Int']>;
  postalCode?: InputMaybe<Scalars['String']>;
};


export type QueryMaterialArgs = {
  id: Scalars['ID'];
};


export type QueryMaterialBookingArgs = {
  id: Scalars['ID'];
};


export type QueryMaterialBookingDatePeriodArgs = {
  id: Scalars['ID'];
};


export type QueryMaterialBookingsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  endDate?: InputMaybe<Array<InputMaybe<MaterialBookingFilter_EndDate>>>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  material_slug?: InputMaybe<Scalars['String']>;
  material_slug_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  startDate?: InputMaybe<Array<InputMaybe<MaterialBookingFilter_StartDate>>>;
  status?: InputMaybe<Scalars['String']>;
  status_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  user_slug?: InputMaybe<Scalars['String']>;
  user_slug_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type QueryMaterialCategoriesArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};


export type QueryMaterialCategoryArgs = {
  id: Scalars['ID'];
};


export type QueryMaterialPricingArgs = {
  id: Scalars['ID'];
};


export type QueryMaterialPricingsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  order?: InputMaybe<Array<InputMaybe<MaterialPricingFilter_Order>>>;
};


export type QueryMaterialSearchAlertsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  name?: InputMaybe<Scalars['String']>;
};


export type QueryMaterialsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  brand?: InputMaybe<Scalars['String']>;
  category_name?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  model?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  order?: InputMaybe<Array<InputMaybe<MaterialFilter_Order>>>;
  reference?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
  status_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type QueryMyCirclesArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  city?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  explicitLocationLabel?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  indexable?: InputMaybe<Scalars['Boolean']>;
  invitations_email?: InputMaybe<Scalars['String']>;
  invitations_email_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  last?: InputMaybe<Scalars['Int']>;
  latitude?: InputMaybe<Array<InputMaybe<CircleFilter_Latitude>>>;
  longitude?: InputMaybe<Array<InputMaybe<CircleFilter_Longitude>>>;
  name?: InputMaybe<Scalars['String']>;
};


export type QueryMyMaterialsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  brand?: InputMaybe<Scalars['String']>;
  category_name?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  model?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  order?: InputMaybe<Array<InputMaybe<MaterialFilter_Order>>>;
  reference?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
  status_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type QueryNodeArgs = {
  id: Scalars['ID'];
};


export type QueryOwnerMaterialsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  brand?: InputMaybe<Scalars['String']>;
  category_name?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  model?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  order?: InputMaybe<Array<InputMaybe<MaterialFilter_Order>>>;
  owner: Scalars['ID'];
  reference?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
  status_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type QueryPlanArgs = {
  id: Scalars['ID'];
};


export type QueryRatingArgs = {
  id: Scalars['ID'];
};


export type QueryRatingsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};


export type QuerySearchMaterialsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  brand?: InputMaybe<Scalars['String']>;
  category_name?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  model?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  order?: InputMaybe<Array<InputMaybe<MaterialFilter_Order>>>;
  reference?: InputMaybe<Scalars['String']>;
  searchTerms: Scalars['String'];
  status?: InputMaybe<Scalars['String']>;
  status_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type QuerySubscriptionArgs = {
  id: Scalars['ID'];
};


export type QuerySubscriptionsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};


export type QueryUserArgs = {
  id: Scalars['ID'];
};


export type QueryUserListArgs = {
  id: Scalars['ID'];
};


export type QueryUserListsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};


export type QueryUserMaterialOwnershipArgs = {
  id: Scalars['ID'];
};


export type QueryUserMaterialOwnershipsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};


export type QueryUsersArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  confirmed?: InputMaybe<Scalars['Boolean']>;
  email?: InputMaybe<Scalars['String']>;
  email_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  first?: InputMaybe<Scalars['Int']>;
  firstname?: InputMaybe<Scalars['String']>;
  last?: InputMaybe<Scalars['Int']>;
  lastname?: InputMaybe<Scalars['String']>;
  postalCode?: InputMaybe<Scalars['String']>;
};

export type Rating = Node & {
  __typename?: 'Rating';
  author?: Maybe<User>;
  booking: MaterialBooking;
  createdAt?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  updatedAt?: Maybe<Scalars['String']>;
  user?: Maybe<User>;
  value: Scalars['Int'];
};

/** Cursor connection for Rating. */
export type RatingCursorConnection = {
  __typename?: 'RatingCursorConnection';
  edges?: Maybe<Array<Maybe<RatingEdge>>>;
  pageInfo: RatingPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of Rating. */
export type RatingEdge = {
  __typename?: 'RatingEdge';
  cursor: Scalars['String'];
  node?: Maybe<Rating>;
};

/** Information about the current page. */
export type RatingPageInfo = {
  __typename?: 'RatingPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

export type Settings = Node & {
  __typename?: 'Settings';
  createdAt?: Maybe<Scalars['String']>;
  currencySymbol: Scalars['String'];
  id: Scalars['ID'];
  language: Scalars['String'];
  preferedCommunicationMode: CommunicationMode;
  themeMode: ThemeMode;
  updatedAt?: Maybe<Scalars['String']>;
  useAbbreviatedName: Scalars['Boolean'];
  user?: Maybe<User>;
};

export type SettingsNestedInput = {
  createdAt?: InputMaybe<Scalars['String']>;
  currencySymbol: Scalars['String'];
  id?: InputMaybe<Scalars['ID']>;
  language: Scalars['String'];
  preferedCommunicationMode: CommunicationMode;
  themeMode: ThemeMode;
  updatedAt?: InputMaybe<Scalars['String']>;
  useAbbreviatedName: Scalars['Boolean'];
  user?: InputMaybe<Scalars['String']>;
};

export type Subscription = Node & {
  __typename?: 'Subscription';
  active: Scalars['Boolean'];
  circle?: Maybe<Circle>;
  createdAt?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  plan?: Maybe<Plan>;
  price?: Maybe<Scalars['Float']>;
  startDate: Scalars['String'];
  status: SubscriptionStatusType;
  updatedAt?: Maybe<Scalars['String']>;
};

/** Cursor connection for Subscription. */
export type SubscriptionCursorConnection = {
  __typename?: 'SubscriptionCursorConnection';
  edges?: Maybe<Array<Maybe<SubscriptionEdge>>>;
  pageInfo: SubscriptionPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of Subscription. */
export type SubscriptionEdge = {
  __typename?: 'SubscriptionEdge';
  cursor: Scalars['String'];
  node?: Maybe<Subscription>;
};

/** Information about the current page. */
export type SubscriptionPageInfo = {
  __typename?: 'SubscriptionPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

export enum SubscriptionStatusType {
  Active = 'ACTIVE',
  FormalNoticed = 'FORMAL_NOTICED',
  Stopped = 'STOPPED',
  Suspended = 'SUSPENDED',
  Trial = 'TRIAL',
  WaitingPayment = 'WAITING_PAYMENT'
}

export enum ThemeMode {
  Dark = 'DARK',
  Light = 'LIGHT',
  System = 'SYSTEM'
}

export type User = Node & {
  __typename?: 'User';
  avatar?: Maybe<Avatar>;
  averageRatingScore?: Maybe<Scalars['Float']>;
  bio?: Maybe<Scalars['String']>;
  birthDate?: Maybe<Scalars['String']>;
  circleMemberships?: Maybe<CircleMembershipCursorConnection>;
  city?: Maybe<Scalars['String']>;
  confirmationToken?: Maybe<Scalars['String']>;
  confirmed: Scalars['Boolean'];
  country?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  discussions?: Maybe<DiscussionCursorConnection>;
  email: Scalars['String'];
  firstname?: Maybe<Scalars['String']>;
  fullname: Scalars['String'];
  gender?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  lastname: Scalars['String'];
  latitude?: Maybe<Scalars['Float']>;
  /** The geographical location of the user as a POINT (longitude, latitude). */
  location?: Maybe<Scalars['String']>;
  longitude?: Maybe<Scalars['Float']>;
  materialBookings?: Maybe<MaterialBookingCursorConnection>;
  materialSearchAlerts?: Maybe<MaterialSearchAlertCursorConnection>;
  password?: Maybe<Scalars['String']>;
  phoneNumber?: Maybe<Scalars['Iterable']>;
  postalCode?: Maybe<Scalars['String']>;
  ratings?: Maybe<RatingCursorConnection>;
  ratingsSent?: Maybe<RatingCursorConnection>;
  refreshToken?: Maybe<Scalars['String']>;
  roles: Scalars['Iterable'];
  settings?: Maybe<Settings>;
  slug: Scalars['String'];
  streetAddress: Scalars['String'];
  /** //CAUTION: removing comments will break the token generation @TODO needs to be fixed. */
  token?: Maybe<Scalars['String']>;
  tokenExpiresAt?: Maybe<Scalars['Float']>;
  updatedAt?: Maybe<Scalars['String']>;
  /** A visual identifier that represents this user. */
  userIdentifier: Scalars['String'];
  /**
   *
   * The list of users I made (for example, my friends) to easily allow them to access my materials
   */
  userLists?: Maybe<UserListCursorConnection>;
  userOwnerships?: Maybe<UserMaterialOwnershipCursorConnection>;
};


export type UserCircleMembershipsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  circle_slug?: InputMaybe<Scalars['String']>;
  circle_slug_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  order?: InputMaybe<Array<InputMaybe<CircleMembershipFilter_Order>>>;
  status?: InputMaybe<Scalars['String']>;
  status_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  user_slug?: InputMaybe<Scalars['String']>;
  user_slug_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type UserDiscussionsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};


export type UserMaterialBookingsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  endDate?: InputMaybe<Array<InputMaybe<MaterialBookingFilter_EndDate>>>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  material_slug?: InputMaybe<Scalars['String']>;
  material_slug_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  startDate?: InputMaybe<Array<InputMaybe<MaterialBookingFilter_StartDate>>>;
  status?: InputMaybe<Scalars['String']>;
  status_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  user_slug?: InputMaybe<Scalars['String']>;
  user_slug_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type UserMaterialSearchAlertsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  name?: InputMaybe<Scalars['String']>;
  name_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};


export type UserRatingsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};


export type UserRatingsSentArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};


export type UserUserListsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};


export type UserUserOwnershipsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
};

/** Cursor connection for User. */
export type UserCursorConnection = {
  __typename?: 'UserCursorConnection';
  edges?: Maybe<Array<Maybe<UserEdge>>>;
  pageInfo: UserPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of User. */
export type UserEdge = {
  __typename?: 'UserEdge';
  cursor: Scalars['String'];
  node?: Maybe<User>;
};

export type UserList = Node & {
  __typename?: 'UserList';
  id: Scalars['ID'];
  name: Scalars['String'];
  owner?: Maybe<User>;
  slug: Scalars['String'];
  users?: Maybe<UserCursorConnection>;
};


export type UserListUsersArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  confirmed?: InputMaybe<Scalars['Boolean']>;
  email?: InputMaybe<Scalars['String']>;
  email_list?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  first?: InputMaybe<Scalars['Int']>;
  firstname?: InputMaybe<Scalars['String']>;
  last?: InputMaybe<Scalars['Int']>;
  lastname?: InputMaybe<Scalars['String']>;
  postalCode?: InputMaybe<Scalars['String']>;
};

/** Cursor connection for UserList. */
export type UserListCursorConnection = {
  __typename?: 'UserListCursorConnection';
  edges?: Maybe<Array<Maybe<UserListEdge>>>;
  pageInfo: UserListPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of UserList. */
export type UserListEdge = {
  __typename?: 'UserListEdge';
  cursor: Scalars['String'];
  node?: Maybe<UserList>;
};

/** Information about the current page. */
export type UserListPageInfo = {
  __typename?: 'UserListPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

export type UserMaterialOwnership = Node & {
  __typename?: 'UserMaterialOwnership';
  id: Scalars['ID'];
  material?: Maybe<Material>;
  materialMainOwned?: Maybe<Material>;
  ownerIRI: Scalars['String'];
  ownerLabel: Scalars['String'];
  user: User;
};

/** Cursor connection for UserMaterialOwnership. */
export type UserMaterialOwnershipCursorConnection = {
  __typename?: 'UserMaterialOwnershipCursorConnection';
  edges?: Maybe<Array<Maybe<UserMaterialOwnershipEdge>>>;
  pageInfo: UserMaterialOwnershipPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of UserMaterialOwnership. */
export type UserMaterialOwnershipEdge = {
  __typename?: 'UserMaterialOwnershipEdge';
  cursor: Scalars['String'];
  node?: Maybe<UserMaterialOwnership>;
};

/** Information about the current page. */
export type UserMaterialOwnershipPageInfo = {
  __typename?: 'UserMaterialOwnershipPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Information about the current page. */
export type UserPageInfo = {
  __typename?: 'UserPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** AddMessages a Discussion. */
export type AddMessageDiscussionInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  /** On which booking ? */
  id: Scalars['ID'];
  /** Is there a message to attach ? */
  message?: InputMaybe<Scalars['String']>;
  /** What transition do you want to run ? */
  transition?: InputMaybe<Scalars['String']>;
};

/** AddMessages a Discussion. */
export type AddMessageDiscussionPayload = {
  __typename?: 'addMessageDiscussionPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  discussion?: Maybe<Discussion>;
};

/** AddUserTos a UserList. */
export type AddUserToUserListInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  userEmail: Scalars['String'];
};

/** AddUserTos a UserList. */
export type AddUserToUserListPayload = {
  __typename?: 'addUserToUserListPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  userList?: Maybe<UserList>;
};

/** AskToJoins a CircleMembership. */
export type AskToJoinCircleMembershipInput = {
  /** Circle IRI */
  circle?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  /** Is there a message to attach to circle application ? */
  message?: InputMaybe<Scalars['String']>;
};

/** AskToJoins a CircleMembership. */
export type AskToJoinCircleMembershipPayload = {
  __typename?: 'askToJoinCircleMembershipPayload';
  circleMembership?: Maybe<CircleMembership>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** ChangeDates a MaterialBooking. */
export type ChangeDateMaterialBookingInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  endDate?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  startDate?: InputMaybe<Scalars['String']>;
};

/** ChangeDates a MaterialBooking. */
export type ChangeDateMaterialBookingPayload = {
  __typename?: 'changeDateMaterialBookingPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialBooking?: Maybe<MaterialBooking>;
};

/** ChangePasswords a User. */
export type ChangePasswordUserInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  currentPassword: Scalars['String'];
  id: Scalars['ID'];
  newPassword: Scalars['String'];
};

/** ChangePasswords a User. */
export type ChangePasswordUserPayload = {
  __typename?: 'changePasswordUserPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  user?: Maybe<User>;
};

/** ChangePermissions a CircleMembership. */
export type ChangePermissionCircleMembershipInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  /** Is there a message to attach ? */
  message?: InputMaybe<Scalars['String']>;
  /** What transition do you want to run ? */
  transition?: InputMaybe<Scalars['String']>;
};

/** ChangePermissions a CircleMembership. */
export type ChangePermissionCircleMembershipPayload = {
  __typename?: 'changePermissionCircleMembershipPayload';
  circleMembership?: Maybe<CircleMembership>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** ChangeStatuss a CircleMembership. */
export type ChangeStatusCircleMembershipInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  /** Is there a message to attach ? */
  message?: InputMaybe<Scalars['String']>;
  /** What transition do you want to run ? */
  transition?: InputMaybe<Scalars['String']>;
};

/** ChangeStatuss a CircleMembership. */
export type ChangeStatusCircleMembershipPayload = {
  __typename?: 'changeStatusCircleMembershipPayload';
  circleMembership?: Maybe<CircleMembership>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** ChangeStatuss a MaterialBooking. */
export type ChangeStatusMaterialBookingInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  /** Is there a message to attach ? */
  message?: InputMaybe<Scalars['String']>;
  /** What transition do you want to run ? */
  transition?: InputMaybe<Scalars['String']>;
};

/** ChangeStatuss a MaterialBooking. */
export type ChangeStatusMaterialBookingPayload = {
  __typename?: 'changeStatusMaterialBookingPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialBooking?: Maybe<MaterialBooking>;
};

/** ChangeStatuss a Material. */
export type ChangeStatusMaterialInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  transition: Scalars['String'];
};

/** ChangeStatuss a Material. */
export type ChangeStatusMaterialPayload = {
  __typename?: 'changeStatusMaterialPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  material?: Maybe<Material>;
};

/** Comments a MaterialBooking. */
export type CommentMaterialBookingInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  comment: Scalars['String'];
  id: Scalars['ID'];
};

/** Comments a MaterialBooking. */
export type CommentMaterialBookingPayload = {
  __typename?: 'commentMaterialBookingPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialBooking?: Maybe<MaterialBooking>;
};

/** Confirms a User. */
export type ConfirmUserInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  code: Scalars['String'];
  confirmationToken: Scalars['String'];
};

/** Confirms a User. */
export type ConfirmUserPayload = {
  __typename?: 'confirmUserPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  user?: Maybe<User>;
};

/** Creates a AbstractMaterialOwnership. */
export type CreateAbstractMaterialOwnershipInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  material?: InputMaybe<Scalars['String']>;
  materialMainOwned?: InputMaybe<Scalars['String']>;
};

/** Creates a AbstractMaterialOwnership. */
export type CreateAbstractMaterialOwnershipNestedPayload = Node & {
  __typename?: 'createAbstractMaterialOwnershipNestedPayload';
  id: Scalars['ID'];
  material?: Maybe<CreateMaterialNestedPayload>;
  materialMainOwned?: Maybe<CreateMaterialNestedPayload>;
  ownerIRI: Scalars['String'];
  ownerLabel: Scalars['String'];
};

/** Creates a AbstractMaterialOwnership. */
export type CreateAbstractMaterialOwnershipPayload = {
  __typename?: 'createAbstractMaterialOwnershipPayload';
  abstractMaterialOwnership?: Maybe<AbstractMaterialOwnership>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Creates a Avatar. */
export type CreateAvatarInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  /** The file to upload */
  imageFile: Scalars['Upload'];
};

/** Creates a Avatar. */
export type CreateAvatarNestedPayload = Node & {
  __typename?: 'createAvatarNestedPayload';
  contentUrl?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  dimensions: Scalars['Iterable'];
  id: Scalars['ID'];
  imageName: Scalars['String'];
  mime?: Maybe<Scalars['String']>;
  originalName?: Maybe<Scalars['String']>;
  size: Scalars['Int'];
  updatedAt?: Maybe<Scalars['String']>;
  user?: Maybe<CreateUserNestedPayload>;
};

/** Creates a Avatar. */
export type CreateAvatarPayload = {
  __typename?: 'createAvatarPayload';
  avatar?: Maybe<Avatar>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Creates a CircleCover. */
export type CreateCircleCoverInput = {
  circle: Scalars['ID'];
  clientMutationId?: InputMaybe<Scalars['String']>;
  /** The file to upload */
  imageFile: Scalars['Upload'];
};

/** Creates a CircleCover. */
export type CreateCircleCoverNestedPayload = Node & {
  __typename?: 'createCircleCoverNestedPayload';
  circle?: Maybe<CreateCircleNestedPayload>;
  contentUrl?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  createdBy?: Maybe<CreateUserNestedPayload>;
  dimensions: Scalars['Iterable'];
  id: Scalars['ID'];
  imageName: Scalars['String'];
  mime?: Maybe<Scalars['String']>;
  originalName?: Maybe<Scalars['String']>;
  size: Scalars['Int'];
  updatedAt?: Maybe<Scalars['String']>;
  updatedBy?: Maybe<CreateUserNestedPayload>;
};

/** Creates a CircleCover. */
export type CreateCircleCoverPayload = {
  __typename?: 'createCircleCoverPayload';
  circleCover?: Maybe<CircleCover>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Creates a Circle. */
export type CreateCircleInput = {
  address?: InputMaybe<Scalars['String']>;
  city?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  country?: InputMaybe<Scalars['String']>;
  cover?: InputMaybe<Scalars['Upload']>;
  description?: InputMaybe<Scalars['String']>;
  explicitLocationLabel?: InputMaybe<Scalars['String']>;
  indexable: Scalars['Boolean'];
  latitude?: InputMaybe<Scalars['String']>;
  location?: InputMaybe<Scalars['String']>;
  logo?: InputMaybe<Scalars['Upload']>;
  longitude?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  postalCode?: InputMaybe<Scalars['String']>;
  pricings?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  website?: InputMaybe<Scalars['String']>;
};

/** Creates a CircleLogo. */
export type CreateCircleLogoInput = {
  circle: Scalars['ID'];
  clientMutationId?: InputMaybe<Scalars['String']>;
  /** The file to upload */
  imageFile: Scalars['Upload'];
};

/** Creates a CircleLogo. */
export type CreateCircleLogoNestedPayload = Node & {
  __typename?: 'createCircleLogoNestedPayload';
  circle?: Maybe<CreateCircleNestedPayload>;
  contentUrl?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  createdBy?: Maybe<CreateUserNestedPayload>;
  dimensions: Scalars['Iterable'];
  id: Scalars['ID'];
  imageName: Scalars['String'];
  mime?: Maybe<Scalars['String']>;
  originalName?: Maybe<Scalars['String']>;
  size: Scalars['Int'];
  updatedAt?: Maybe<Scalars['String']>;
  updatedBy?: Maybe<CreateUserNestedPayload>;
};

/** Creates a CircleLogo. */
export type CreateCircleLogoPayload = {
  __typename?: 'createCircleLogoPayload';
  circleLogo?: Maybe<CircleLogo>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Creates a CircleMaterialOwnership. */
export type CreateCircleMaterialOwnershipInput = {
  circle: Scalars['String'];
  clientMutationId?: InputMaybe<Scalars['String']>;
  material?: InputMaybe<Scalars['String']>;
  materialMainOwned?: InputMaybe<Scalars['String']>;
};

/** Creates a CircleMaterialOwnership. */
export type CreateCircleMaterialOwnershipNestedPayload = Node & {
  __typename?: 'createCircleMaterialOwnershipNestedPayload';
  circle: CreateCircleNestedPayload;
  id: Scalars['ID'];
  material?: Maybe<CreateMaterialNestedPayload>;
  materialMainOwned?: Maybe<CreateMaterialNestedPayload>;
  ownerIRI: Scalars['String'];
  ownerLabel: Scalars['String'];
};

/** Cursor connection for createCircleMaterialOwnershipNestedPayload. */
export type CreateCircleMaterialOwnershipNestedPayloadCursorConnection = {
  __typename?: 'createCircleMaterialOwnershipNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<CreateCircleMaterialOwnershipNestedPayloadEdge>>>;
  pageInfo: CreateCircleMaterialOwnershipNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of createCircleMaterialOwnershipNestedPayload. */
export type CreateCircleMaterialOwnershipNestedPayloadEdge = {
  __typename?: 'createCircleMaterialOwnershipNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<CreateCircleMaterialOwnershipNestedPayload>;
};

/** Information about the current page. */
export type CreateCircleMaterialOwnershipNestedPayloadPageInfo = {
  __typename?: 'createCircleMaterialOwnershipNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Creates a CircleMaterialOwnership. */
export type CreateCircleMaterialOwnershipPayload = {
  __typename?: 'createCircleMaterialOwnershipPayload';
  circleMaterialOwnership?: Maybe<CircleMaterialOwnership>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Creates a Circle. */
export type CreateCircleNestedPayload = Node & {
  __typename?: 'createCircleNestedPayload';
  /**
   * Not persisted property, used to define what can do current logged user;
   * set by CircleResolver.
   */
  activeUserActions: Scalars['Iterable'];
  address?: Maybe<Scalars['String']>;
  admins?: Maybe<CreateUserNestedPayloadCursorConnection>;
  children?: Maybe<CreateCircleNestedPayloadCursorConnection>;
  circleOwnerships?: Maybe<CreateCircleMaterialOwnershipNestedPayloadCursorConnection>;
  city?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  cover?: Maybe<CreateCircleCoverNestedPayload>;
  createdAt?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  distance?: Maybe<Scalars['Float']>;
  explicitLocationLabel?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  indexable: Scalars['Boolean'];
  invitationLinks?: Maybe<Array<Maybe<CircleInvitationLink>>>;
  invitations?: Maybe<CreateInvitationNestedPayloadCursorConnection>;
  latitude?: Maybe<Scalars['String']>;
  location?: Maybe<Scalars['String']>;
  logo?: Maybe<CreateCircleLogoNestedPayload>;
  longitude?: Maybe<Scalars['String']>;
  memberships?: Maybe<CircleMembershipCursorConnection>;
  name: Scalars['String'];
  owners?: Maybe<CreateUserNestedPayloadCursorConnection>;
  parent?: Maybe<CreateCircleNestedPayload>;
  pendingMemberships?: Maybe<CircleMembershipCursorConnection>;
  postalCode?: Maybe<Scalars['String']>;
  pricings?: Maybe<CreateMaterialPricingNestedPayloadCursorConnection>;
  slug: Scalars['String'];
  subscription?: Maybe<CreateSubscriptionNestedPayload>;
  updatedAt?: Maybe<Scalars['String']>;
  website?: Maybe<Scalars['String']>;
};

/** Cursor connection for createCircleNestedPayload. */
export type CreateCircleNestedPayloadCursorConnection = {
  __typename?: 'createCircleNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<CreateCircleNestedPayloadEdge>>>;
  pageInfo: CreateCircleNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of createCircleNestedPayload. */
export type CreateCircleNestedPayloadEdge = {
  __typename?: 'createCircleNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<CreateCircleNestedPayload>;
};

/** Information about the current page. */
export type CreateCircleNestedPayloadPageInfo = {
  __typename?: 'createCircleNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Creates a Circle. */
export type CreateCirclePayload = {
  __typename?: 'createCirclePayload';
  circle?: Maybe<Circle>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Creates a Contact. */
export type CreateContactInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['String']>;
  email: Scalars['String'];
  message?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  newsletterOptIn: Scalars['Boolean'];
  updatedAt?: InputMaybe<Scalars['String']>;
};

/** Creates a Contact. */
export type CreateContactPayload = {
  __typename?: 'createContactPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  contact?: Maybe<Contact>;
};

/** Creates a Invitation. */
export type CreateInvitationInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  email: Scalars['String'];
  postalCode: Scalars['String'];
};

/** Creates a Invitation. */
export type CreateInvitationNestedPayload = Node & {
  __typename?: 'createInvitationNestedPayload';
  circle?: Maybe<CreateCircleNestedPayload>;
  createdAt?: Maybe<Scalars['String']>;
  email: Scalars['String'];
  firstname?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  invited: Scalars['Boolean'];
  invitedBy?: Maybe<CreateUserNestedPayload>;
  lastname?: Maybe<Scalars['String']>;
  postalCode?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['String']>;
  user?: Maybe<CreateUserNestedPayload>;
};

/** Cursor connection for createInvitationNestedPayload. */
export type CreateInvitationNestedPayloadCursorConnection = {
  __typename?: 'createInvitationNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<CreateInvitationNestedPayloadEdge>>>;
  pageInfo: CreateInvitationNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of createInvitationNestedPayload. */
export type CreateInvitationNestedPayloadEdge = {
  __typename?: 'createInvitationNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<CreateInvitationNestedPayload>;
};

/** Information about the current page. */
export type CreateInvitationNestedPayloadPageInfo = {
  __typename?: 'createInvitationNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Creates a Invitation. */
export type CreateInvitationPayload = {
  __typename?: 'createInvitationPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  invitation?: Maybe<Invitation>;
};

/** Creates a MaterialBooking. */
export type CreateMaterialBookingInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  comment?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['String']>;
  discussion?: InputMaybe<Scalars['String']>;
  endDate: Scalars['String'];
  material?: InputMaybe<Scalars['String']>;
  periods?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  price?: InputMaybe<Scalars['Float']>;
  ratings?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  slug: Scalars['String'];
  startDate: Scalars['String'];
  status: Scalars['String'];
  statusTransitionAvailables?: InputMaybe<Scalars['Iterable']>;
  updatedAt?: InputMaybe<Scalars['String']>;
  user?: InputMaybe<Scalars['String']>;
};

/** Creates a MaterialBooking. */
export type CreateMaterialBookingNestedPayload = Node & {
  __typename?: 'createMaterialBookingNestedPayload';
  comment?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  discussion?: Maybe<Discussion>;
  endDate: Scalars['String'];
  id: Scalars['ID'];
  material?: Maybe<CreateMaterialNestedPayload>;
  past: Scalars['Boolean'];
  periods?: Maybe<Array<Maybe<MaterialBookingDatePeriod>>>;
  price?: Maybe<Scalars['Float']>;
  ratings?: Maybe<CreateRatingNestedPayloadCursorConnection>;
  slug: Scalars['String'];
  startDate: Scalars['String'];
  status: Scalars['String'];
  statusTransitionAvailables?: Maybe<Scalars['Iterable']>;
  terminatedStatuses: Scalars['Iterable'];
  transitions: Scalars['Iterable'];
  updatedAt?: Maybe<Scalars['String']>;
  user?: Maybe<CreateUserNestedPayload>;
};

/** Cursor connection for createMaterialBookingNestedPayload. */
export type CreateMaterialBookingNestedPayloadCursorConnection = {
  __typename?: 'createMaterialBookingNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<CreateMaterialBookingNestedPayloadEdge>>>;
  pageInfo: CreateMaterialBookingNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of createMaterialBookingNestedPayload. */
export type CreateMaterialBookingNestedPayloadEdge = {
  __typename?: 'createMaterialBookingNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<CreateMaterialBookingNestedPayload>;
};

/** Information about the current page. */
export type CreateMaterialBookingNestedPayloadPageInfo = {
  __typename?: 'createMaterialBookingNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Creates a MaterialBooking. */
export type CreateMaterialBookingPayload = {
  __typename?: 'createMaterialBookingPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialBooking?: Maybe<MaterialBooking>;
};

/** Creates a MaterialCategory. */
export type CreateMaterialCategoryInput = {
  children?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['String']>;
  materials?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  name: Scalars['String'];
  parent?: InputMaybe<Scalars['String']>;
  slug: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['String']>;
};

/** Creates a MaterialCategory. */
export type CreateMaterialCategoryPayload = {
  __typename?: 'createMaterialCategoryPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialCategory?: Maybe<MaterialCategory>;
};

/** Creates a MaterialImage. */
export type CreateMaterialImageInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  dimensions?: InputMaybe<Scalars['Iterable']>;
  file: Scalars['Upload'];
  imageName?: InputMaybe<Scalars['String']>;
  material: Scalars['String'];
  mime?: InputMaybe<Scalars['String']>;
  size?: InputMaybe<Scalars['Int']>;
};

/** Creates a MaterialImage. */
export type CreateMaterialImageNestedPayload = Node & {
  __typename?: 'createMaterialImageNestedPayload';
  contentUrl?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  dimensions: Scalars['Iterable'];
  id: Scalars['ID'];
  imageName: Scalars['String'];
  material: CreateMaterialNestedPayload;
  mime?: Maybe<Scalars['String']>;
  originalName?: Maybe<Scalars['String']>;
  size: Scalars['Int'];
  updatedAt?: Maybe<Scalars['String']>;
};

/** Cursor connection for createMaterialImageNestedPayload. */
export type CreateMaterialImageNestedPayloadCursorConnection = {
  __typename?: 'createMaterialImageNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<CreateMaterialImageNestedPayloadEdge>>>;
  pageInfo: CreateMaterialImageNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of createMaterialImageNestedPayload. */
export type CreateMaterialImageNestedPayloadEdge = {
  __typename?: 'createMaterialImageNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<CreateMaterialImageNestedPayload>;
};

/** Information about the current page. */
export type CreateMaterialImageNestedPayloadPageInfo = {
  __typename?: 'createMaterialImageNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Creates a MaterialImage. */
export type CreateMaterialImagePayload = {
  __typename?: 'createMaterialImagePayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialImage?: Maybe<MaterialImage>;
};

/** Creates a Material. */
export type CreateMaterialInput = {
  brand?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  images?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  location?: InputMaybe<Scalars['String']>;
  mainOwner?: InputMaybe<Scalars['String']>;
  mainOwnership?: InputMaybe<Scalars['MaterialOwnershipType']>;
  model?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  ownerships?: InputMaybe<Array<InputMaybe<Scalars['MaterialOwnershipType']>>>;
  price: Scalars['Float'];
  pricings?: InputMaybe<Array<InputMaybe<CreateMaterialPricingNestedInput>>>;
  userLists?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

/** Creates a Material. */
export type CreateMaterialNestedPayload = Node & {
  __typename?: 'createMaterialNestedPayload';
  bestPriceForUser?: Maybe<Scalars['Float']>;
  bookings?: Maybe<CreateMaterialBookingNestedPayloadCursorConnection>;
  brand?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  distance?: Maybe<Scalars['Float']>;
  id: Scalars['ID'];
  images?: Maybe<CreateMaterialImageNestedPayloadCursorConnection>;
  latitude?: Maybe<Scalars['Float']>;
  location?: Maybe<Scalars['String']>;
  longitude?: Maybe<Scalars['Float']>;
  mainOwner?: Maybe<CreateUserNestedPayload>;
  mainOwnership?: Maybe<CreateAbstractMaterialOwnershipNestedPayload>;
  model?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  ownerships?: Maybe<Array<Maybe<CreateAbstractMaterialOwnershipNestedPayload>>>;
  price: Scalars['Float'];
  pricings?: Maybe<CreateMaterialPricingNestedPayloadCursorConnection>;
  slug: Scalars['String'];
  updatedAt?: Maybe<Scalars['String']>;
  userActionAvailables?: Maybe<Scalars['Iterable']>;
  userLists?: Maybe<CreateUserListNestedPayloadCursorConnection>;
};

/** Creates a Material. */
export type CreateMaterialPayload = {
  __typename?: 'createMaterialPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  material?: Maybe<CreateMaterialPayloadData>;
};

/** Creates a Material. */
export type CreateMaterialPayloadData = Node & {
  __typename?: 'createMaterialPayloadData';
  bestPriceForUser?: Maybe<Scalars['Float']>;
  bookings?: Maybe<CreateMaterialBookingNestedPayloadCursorConnection>;
  brand?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  distance?: Maybe<Scalars['Float']>;
  id: Scalars['ID'];
  images?: Maybe<CreateMaterialImageNestedPayloadCursorConnection>;
  latitude?: Maybe<Scalars['Float']>;
  location?: Maybe<Scalars['String']>;
  longitude?: Maybe<Scalars['Float']>;
  mainOwner?: Maybe<CreateUserNestedPayload>;
  mainOwnership?: Maybe<CreateAbstractMaterialOwnershipNestedPayload>;
  model?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  ownerships?: Maybe<Array<Maybe<CreateAbstractMaterialOwnershipNestedPayload>>>;
  price: Scalars['Float'];
  pricings?: Maybe<CreateMaterialPricingNestedPayloadCursorConnection>;
  slug: Scalars['String'];
  updatedAt?: Maybe<Scalars['String']>;
  userActionAvailables?: Maybe<Scalars['Iterable']>;
  userLists?: Maybe<CreateUserListNestedPayloadCursorConnection>;
};

/** Creates a MaterialPricing. */
export type CreateMaterialPricingInput = {
  circle?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['String']>;
  material: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['String']>;
  value?: InputMaybe<Scalars['Float']>;
};

/** Creates a MaterialPricing. */
export type CreateMaterialPricingNestedInput = {
  circle?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  material: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['String']>;
  value?: InputMaybe<Scalars['Float']>;
};

/** Creates a MaterialPricing. */
export type CreateMaterialPricingNestedPayload = Node & {
  __typename?: 'createMaterialPricingNestedPayload';
  circle?: Maybe<CreateCircleNestedPayload>;
  createdAt?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  material: CreateMaterialNestedPayload;
  updatedAt?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['Float']>;
};

/** Cursor connection for createMaterialPricingNestedPayload. */
export type CreateMaterialPricingNestedPayloadCursorConnection = {
  __typename?: 'createMaterialPricingNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<CreateMaterialPricingNestedPayloadEdge>>>;
  pageInfo: CreateMaterialPricingNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of createMaterialPricingNestedPayload. */
export type CreateMaterialPricingNestedPayloadEdge = {
  __typename?: 'createMaterialPricingNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<CreateMaterialPricingNestedPayload>;
};

/** Information about the current page. */
export type CreateMaterialPricingNestedPayloadPageInfo = {
  __typename?: 'createMaterialPricingNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Creates a MaterialPricing. */
export type CreateMaterialPricingPayload = {
  __typename?: 'createMaterialPricingPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialPricing?: Maybe<MaterialPricing>;
};

/** Creates a MaterialSearchAlert. */
export type CreateMaterialSearchAlertInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
};

/** Creates a MaterialSearchAlert. */
export type CreateMaterialSearchAlertNestedPayload = Node & {
  __typename?: 'createMaterialSearchAlertNestedPayload';
  _id?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  user?: Maybe<CreateUserNestedPayload>;
};

/** Cursor connection for createMaterialSearchAlertNestedPayload. */
export type CreateMaterialSearchAlertNestedPayloadCursorConnection = {
  __typename?: 'createMaterialSearchAlertNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<CreateMaterialSearchAlertNestedPayloadEdge>>>;
  pageInfo: CreateMaterialSearchAlertNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of createMaterialSearchAlertNestedPayload. */
export type CreateMaterialSearchAlertNestedPayloadEdge = {
  __typename?: 'createMaterialSearchAlertNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<CreateMaterialSearchAlertNestedPayload>;
};

/** Information about the current page. */
export type CreateMaterialSearchAlertNestedPayloadPageInfo = {
  __typename?: 'createMaterialSearchAlertNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Creates a MaterialSearchAlert. */
export type CreateMaterialSearchAlertPayload = {
  __typename?: 'createMaterialSearchAlertPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialSearchAlert?: Maybe<MaterialSearchAlert>;
};

/** Creates a Message. */
export type CreateMessageInput = {
  author?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  content: Scalars['String'];
  createdAt?: InputMaybe<Scalars['String']>;
  discussion: Scalars['String'];
  parameters: Scalars['String'];
  updatedAt?: InputMaybe<Scalars['String']>;
};

/** Creates a Message. */
export type CreateMessagePayload = {
  __typename?: 'createMessagePayload';
  clientMutationId?: Maybe<Scalars['String']>;
  message?: Maybe<Message>;
};

/** Creates a Rating. */
export type CreateRatingInput = {
  author?: InputMaybe<Scalars['String']>;
  booking: Scalars['String'];
  clientMutationId?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['String']>;
  user?: InputMaybe<Scalars['String']>;
  value: Scalars['Int'];
};

/** Creates a Rating. */
export type CreateRatingNestedPayload = Node & {
  __typename?: 'createRatingNestedPayload';
  author?: Maybe<CreateUserNestedPayload>;
  booking: CreateMaterialBookingNestedPayload;
  createdAt?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  updatedAt?: Maybe<Scalars['String']>;
  user?: Maybe<CreateUserNestedPayload>;
  value: Scalars['Int'];
};

/** Cursor connection for createRatingNestedPayload. */
export type CreateRatingNestedPayloadCursorConnection = {
  __typename?: 'createRatingNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<CreateRatingNestedPayloadEdge>>>;
  pageInfo: CreateRatingNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of createRatingNestedPayload. */
export type CreateRatingNestedPayloadEdge = {
  __typename?: 'createRatingNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<CreateRatingNestedPayload>;
};

/** Information about the current page. */
export type CreateRatingNestedPayloadPageInfo = {
  __typename?: 'createRatingNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Creates a Rating. */
export type CreateRatingPayload = {
  __typename?: 'createRatingPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  rating?: Maybe<Rating>;
};

/** Creates a Subscription. */
export type CreateSubscriptionInput = {
  circle: Scalars['String'];
  clientMutationId?: InputMaybe<Scalars['String']>;
  plan: Scalars['String'];
};

/** Creates a Subscription. */
export type CreateSubscriptionNestedPayload = Node & {
  __typename?: 'createSubscriptionNestedPayload';
  active: Scalars['Boolean'];
  circle?: Maybe<CreateCircleNestedPayload>;
  createdAt?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  plan?: Maybe<Plan>;
  price?: Maybe<Scalars['Float']>;
  startDate: Scalars['String'];
  status: SubscriptionStatusType;
  updatedAt?: Maybe<Scalars['String']>;
};

/** Creates a Subscription. */
export type CreateSubscriptionPayload = {
  __typename?: 'createSubscriptionPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  subscription?: Maybe<Subscription>;
};

/** Creates a User. */
export type CreateUserInput = {
  avatar?: InputMaybe<Scalars['Upload']>;
  bio?: InputMaybe<Scalars['String']>;
  birthDate?: InputMaybe<Scalars['String']>;
  city?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  country?: InputMaybe<Scalars['String']>;
  countryCode?: InputMaybe<Scalars['Int']>;
  email: Scalars['String'];
  firstname?: InputMaybe<Scalars['String']>;
  gender?: InputMaybe<Scalars['String']>;
  lastname: Scalars['String'];
  latitude?: InputMaybe<Scalars['String']>;
  /** The geographical location of the user as a POINT (longitude, latitude). */
  location?: InputMaybe<Scalars['String']>;
  longitude?: InputMaybe<Scalars['String']>;
  nationalNumber?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
  postalCode?: InputMaybe<Scalars['String']>;
  settings?: InputMaybe<SettingsNestedInput>;
  streetAddress: Scalars['String'];
  token?: InputMaybe<Scalars['ID']>;
};

/** Creates a UserList. */
export type CreateUserListInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  users?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

/** Creates a UserList. */
export type CreateUserListNestedPayload = Node & {
  __typename?: 'createUserListNestedPayload';
  id: Scalars['ID'];
  name: Scalars['String'];
  owner?: Maybe<CreateUserNestedPayload>;
  slug: Scalars['String'];
  users?: Maybe<CreateUserNestedPayloadCursorConnection>;
};

/** Cursor connection for createUserListNestedPayload. */
export type CreateUserListNestedPayloadCursorConnection = {
  __typename?: 'createUserListNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<CreateUserListNestedPayloadEdge>>>;
  pageInfo: CreateUserListNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of createUserListNestedPayload. */
export type CreateUserListNestedPayloadEdge = {
  __typename?: 'createUserListNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<CreateUserListNestedPayload>;
};

/** Information about the current page. */
export type CreateUserListNestedPayloadPageInfo = {
  __typename?: 'createUserListNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Creates a UserList. */
export type CreateUserListPayload = {
  __typename?: 'createUserListPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  userList?: Maybe<UserList>;
};

/** Creates a UserMaterialOwnership. */
export type CreateUserMaterialOwnershipInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  material?: InputMaybe<Scalars['String']>;
  materialMainOwned?: InputMaybe<Scalars['String']>;
  user: Scalars['String'];
};

/** Creates a UserMaterialOwnership. */
export type CreateUserMaterialOwnershipNestedPayload = Node & {
  __typename?: 'createUserMaterialOwnershipNestedPayload';
  id: Scalars['ID'];
  material?: Maybe<CreateMaterialNestedPayload>;
  materialMainOwned?: Maybe<CreateMaterialNestedPayload>;
  ownerIRI: Scalars['String'];
  ownerLabel: Scalars['String'];
  user: CreateUserNestedPayload;
};

/** Cursor connection for createUserMaterialOwnershipNestedPayload. */
export type CreateUserMaterialOwnershipNestedPayloadCursorConnection = {
  __typename?: 'createUserMaterialOwnershipNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<CreateUserMaterialOwnershipNestedPayloadEdge>>>;
  pageInfo: CreateUserMaterialOwnershipNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of createUserMaterialOwnershipNestedPayload. */
export type CreateUserMaterialOwnershipNestedPayloadEdge = {
  __typename?: 'createUserMaterialOwnershipNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<CreateUserMaterialOwnershipNestedPayload>;
};

/** Information about the current page. */
export type CreateUserMaterialOwnershipNestedPayloadPageInfo = {
  __typename?: 'createUserMaterialOwnershipNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Creates a UserMaterialOwnership. */
export type CreateUserMaterialOwnershipPayload = {
  __typename?: 'createUserMaterialOwnershipPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  userMaterialOwnership?: Maybe<UserMaterialOwnership>;
};

/** Creates a User. */
export type CreateUserNestedPayload = Node & {
  __typename?: 'createUserNestedPayload';
  avatar?: Maybe<CreateAvatarNestedPayload>;
  averageRatingScore?: Maybe<Scalars['Float']>;
  bio?: Maybe<Scalars['String']>;
  birthDate?: Maybe<Scalars['String']>;
  circleMemberships?: Maybe<CircleMembershipCursorConnection>;
  city?: Maybe<Scalars['String']>;
  confirmationToken?: Maybe<Scalars['String']>;
  confirmed: Scalars['Boolean'];
  country?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  discussions?: Maybe<DiscussionCursorConnection>;
  email: Scalars['String'];
  firstname?: Maybe<Scalars['String']>;
  fullname: Scalars['String'];
  gender?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  lastname: Scalars['String'];
  latitude?: Maybe<Scalars['Float']>;
  /** The geographical location of the user as a POINT (longitude, latitude). */
  location?: Maybe<Scalars['String']>;
  longitude?: Maybe<Scalars['Float']>;
  materialBookings?: Maybe<CreateMaterialBookingNestedPayloadCursorConnection>;
  materialSearchAlerts?: Maybe<CreateMaterialSearchAlertNestedPayloadCursorConnection>;
  password?: Maybe<Scalars['String']>;
  phoneNumber?: Maybe<Scalars['Iterable']>;
  postalCode?: Maybe<Scalars['String']>;
  ratings?: Maybe<CreateRatingNestedPayloadCursorConnection>;
  ratingsSent?: Maybe<CreateRatingNestedPayloadCursorConnection>;
  refreshToken?: Maybe<Scalars['String']>;
  roles: Scalars['Iterable'];
  settings?: Maybe<Settings>;
  slug: Scalars['String'];
  streetAddress: Scalars['String'];
  /** //CAUTION: removing comments will break the token generation @TODO needs to be fixed. */
  token?: Maybe<Scalars['String']>;
  tokenExpiresAt?: Maybe<Scalars['Float']>;
  updatedAt?: Maybe<Scalars['String']>;
  /** A visual identifier that represents this user. */
  userIdentifier: Scalars['String'];
  /**
   *
   * The list of users I made (for example, my friends) to easily allow them to access my materials
   */
  userLists?: Maybe<CreateUserListNestedPayloadCursorConnection>;
  userOwnerships?: Maybe<CreateUserMaterialOwnershipNestedPayloadCursorConnection>;
};

/** Cursor connection for createUserNestedPayload. */
export type CreateUserNestedPayloadCursorConnection = {
  __typename?: 'createUserNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<CreateUserNestedPayloadEdge>>>;
  pageInfo: CreateUserNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of createUserNestedPayload. */
export type CreateUserNestedPayloadEdge = {
  __typename?: 'createUserNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<CreateUserNestedPayload>;
};

/** Information about the current page. */
export type CreateUserNestedPayloadPageInfo = {
  __typename?: 'createUserNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Creates a User. */
export type CreateUserPayload = {
  __typename?: 'createUserPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  user?: Maybe<User>;
};

/** Deletes a CircleImage. */
export type DeleteCircleImageInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Deletes a CircleImage. */
export type DeleteCircleImagePayload = {
  __typename?: 'deleteCircleImagePayload';
  circleImage?: Maybe<CircleImage>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Deletes a Circle. */
export type DeleteCircleInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Deletes a CircleMaterialOwnership. */
export type DeleteCircleMaterialOwnershipInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Deletes a CircleMaterialOwnership. */
export type DeleteCircleMaterialOwnershipPayload = {
  __typename?: 'deleteCircleMaterialOwnershipPayload';
  circleMaterialOwnership?: Maybe<CircleMaterialOwnership>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Deletes a Circle. */
export type DeleteCirclePayload = {
  __typename?: 'deleteCirclePayload';
  circle?: Maybe<Circle>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Deletes a Invitation. */
export type DeleteInvitationInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Deletes a Invitation. */
export type DeleteInvitationPayload = {
  __typename?: 'deleteInvitationPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  invitation?: Maybe<Invitation>;
};

/** Deletes a MaterialBooking. */
export type DeleteMaterialBookingInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Deletes a MaterialBooking. */
export type DeleteMaterialBookingPayload = {
  __typename?: 'deleteMaterialBookingPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialBooking?: Maybe<MaterialBooking>;
};

/** Deletes a MaterialCategory. */
export type DeleteMaterialCategoryInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Deletes a MaterialCategory. */
export type DeleteMaterialCategoryPayload = {
  __typename?: 'deleteMaterialCategoryPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialCategory?: Maybe<MaterialCategory>;
};

/** Deletes a MaterialImage. */
export type DeleteMaterialImageInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Deletes a MaterialImage. */
export type DeleteMaterialImagePayload = {
  __typename?: 'deleteMaterialImagePayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialImage?: Maybe<MaterialImage>;
};

/** Deletes a Material. */
export type DeleteMaterialInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Deletes a Material. */
export type DeleteMaterialPayload = {
  __typename?: 'deleteMaterialPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  material?: Maybe<Material>;
};

/** Deletes a MaterialPricing. */
export type DeleteMaterialPricingInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Deletes a MaterialPricing. */
export type DeleteMaterialPricingPayload = {
  __typename?: 'deleteMaterialPricingPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialPricing?: Maybe<MaterialPricing>;
};

/** Deletes a MaterialSearchAlert. */
export type DeleteMaterialSearchAlertInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Deletes a MaterialSearchAlert. */
export type DeleteMaterialSearchAlertPayload = {
  __typename?: 'deleteMaterialSearchAlertPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialSearchAlert?: Maybe<MaterialSearchAlert>;
};

/** Deletes a Message. */
export type DeleteMessageInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Deletes a Message. */
export type DeleteMessagePayload = {
  __typename?: 'deleteMessagePayload';
  clientMutationId?: Maybe<Scalars['String']>;
  message?: Maybe<Message>;
};

/** Deletes a Rating. */
export type DeleteRatingInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Deletes a Rating. */
export type DeleteRatingPayload = {
  __typename?: 'deleteRatingPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  rating?: Maybe<Rating>;
};

/** Deletes a User. */
export type DeleteUserInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Deletes a UserList. */
export type DeleteUserListInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Deletes a UserList. */
export type DeleteUserListPayload = {
  __typename?: 'deleteUserListPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  userList?: Maybe<UserList>;
};

/** Deletes a UserMaterialOwnership. */
export type DeleteUserMaterialOwnershipInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Deletes a UserMaterialOwnership. */
export type DeleteUserMaterialOwnershipPayload = {
  __typename?: 'deleteUserMaterialOwnershipPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  userMaterialOwnership?: Maybe<UserMaterialOwnership>;
};

/** Deletes a User. */
export type DeleteUserPayload = {
  __typename?: 'deleteUserPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  user?: Maybe<User>;
};

/** Estimates a MaterialBooking. */
export type EstimateMaterialBookingInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  endDate?: InputMaybe<Scalars['String']>;
  materialId?: InputMaybe<Scalars['String']>;
  startDate?: InputMaybe<Scalars['String']>;
};

/** Estimates a MaterialBooking. */
export type EstimateMaterialBookingPayload = {
  __typename?: 'estimateMaterialBookingPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialBooking?: Maybe<MaterialBooking>;
};

/** Forks a Material. */
export type ForkMaterialInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Forks a Material. */
export type ForkMaterialPayload = {
  __typename?: 'forkMaterialPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  material?: Maybe<Material>;
};

/** Handles a Invitation. */
export type HandleInvitationInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  decision: Scalars['String'];
  id: Scalars['ID'];
};

/** Handles a Invitation. */
export type HandleInvitationPayload = {
  __typename?: 'handleInvitationPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  invitation?: Maybe<Invitation>;
};

/** Joins a CircleMembership. */
export type JoinCircleMembershipInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  invitationLinkId?: InputMaybe<Scalars['String']>;
};

/** Joins a CircleMembership. */
export type JoinCircleMembershipPayload = {
  __typename?: 'joinCircleMembershipPayload';
  circleMembership?: Maybe<CircleMembership>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Logins a User. */
export type LoginUserInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  password: Scalars['String'];
  username: Scalars['String'];
};

/** Logins a User. */
export type LoginUserPayload = {
  __typename?: 'loginUserPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  user?: Maybe<User>;
};

/** Logouts a User. */
export type LogoutUserInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Logouts a User. */
export type LogoutUserPayload = {
  __typename?: 'logoutUserPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  user?: Maybe<User>;
};

/** RemoveUserFroms a UserList. */
export type RemoveUserFromUserListInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  userId: Scalars['ID'];
};

/** RemoveUserFroms a UserList. */
export type RemoveUserFromUserListPayload = {
  __typename?: 'removeUserFromUserListPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  userList?: Maybe<UserList>;
};

/** SendCircles a Invitation. */
export type SendCircleInvitationInput = {
  circle?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  email: Scalars['String'];
  firstname: Scalars['String'];
  inviteLink: Scalars['String'];
  lastname: Scalars['String'];
  message: Scalars['String'];
};

/** SendCircles a Invitation. */
export type SendCircleInvitationPayload = {
  __typename?: 'sendCircleInvitationPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  invitation?: Maybe<Invitation>;
};

/** Sends a Invitation. */
export type SendInvitationInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
};

/** Sends a Invitation. */
export type SendInvitationPayload = {
  __typename?: 'sendInvitationPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  invitation?: Maybe<Invitation>;
};

/** SubscribeNewsletters a Contact. */
export type SubscribeNewsletterContactInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  email: Scalars['String'];
};

/** SubscribeNewsletters a Contact. */
export type SubscribeNewsletterContactPayload = {
  __typename?: 'subscribeNewsletterContactPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  contact?: Maybe<Contact>;
};

/** Unavailables a MaterialBooking. */
export type UnavailableMaterialBookingInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  endDate?: InputMaybe<Scalars['String']>;
  materialId?: InputMaybe<Scalars['String']>;
  startDate?: InputMaybe<Scalars['String']>;
};

/** Unavailables a MaterialBooking. */
export type UnavailableMaterialBookingPayload = {
  __typename?: 'unavailableMaterialBookingPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialBooking?: Maybe<MaterialBooking>;
};

/** Updates a AbstractMaterialOwnership. */
export type UpdateAbstractMaterialOwnershipInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  material?: InputMaybe<Scalars['String']>;
  materialMainOwned?: InputMaybe<Scalars['String']>;
};

/** Updates a AbstractMaterialOwnership. */
export type UpdateAbstractMaterialOwnershipNestedPayload = Node & {
  __typename?: 'updateAbstractMaterialOwnershipNestedPayload';
  id: Scalars['ID'];
  material?: Maybe<UpdateMaterialNestedPayload>;
  materialMainOwned?: Maybe<UpdateMaterialNestedPayload>;
  ownerIRI?: Maybe<Scalars['String']>;
  ownerLabel?: Maybe<Scalars['String']>;
};

/** Updates a AbstractMaterialOwnership. */
export type UpdateAbstractMaterialOwnershipPayload = {
  __typename?: 'updateAbstractMaterialOwnershipPayload';
  abstractMaterialOwnership?: Maybe<AbstractMaterialOwnership>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Updates a Avatar. */
export type UpdateAvatarInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  /** The file to upload */
  imageFile: Scalars['Upload'];
};

/** Updates a Avatar. */
export type UpdateAvatarNestedPayload = Node & {
  __typename?: 'updateAvatarNestedPayload';
  contentUrl?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  dimensions?: Maybe<Scalars['Iterable']>;
  id: Scalars['ID'];
  imageName?: Maybe<Scalars['String']>;
  mime?: Maybe<Scalars['String']>;
  originalName?: Maybe<Scalars['String']>;
  size?: Maybe<Scalars['Int']>;
  updatedAt?: Maybe<Scalars['String']>;
  user?: Maybe<UpdateUserNestedPayload>;
};

/** Updates a Avatar. */
export type UpdateAvatarPayload = {
  __typename?: 'updateAvatarPayload';
  avatar?: Maybe<Avatar>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Updates a Circle. */
export type UpdateCircleInput = {
  address?: InputMaybe<Scalars['String']>;
  city?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  country?: InputMaybe<Scalars['String']>;
  cover?: InputMaybe<Scalars['Upload']>;
  description?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  indexable?: InputMaybe<Scalars['Boolean']>;
  latitude?: InputMaybe<Scalars['String']>;
  logo?: InputMaybe<Scalars['Upload']>;
  longitude?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  postalCode?: InputMaybe<Scalars['String']>;
  removeCover?: InputMaybe<Scalars['Boolean']>;
  removeLogo?: InputMaybe<Scalars['Boolean']>;
  website?: InputMaybe<Scalars['String']>;
};

/** Updates a CircleMaterialOwnership. */
export type UpdateCircleMaterialOwnershipInput = {
  circle?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  material?: InputMaybe<Scalars['String']>;
  materialMainOwned?: InputMaybe<Scalars['String']>;
};

/** Updates a CircleMaterialOwnership. */
export type UpdateCircleMaterialOwnershipNestedPayload = Node & {
  __typename?: 'updateCircleMaterialOwnershipNestedPayload';
  circle?: Maybe<UpdateCircleNestedPayload>;
  id: Scalars['ID'];
  material?: Maybe<UpdateMaterialNestedPayload>;
  materialMainOwned?: Maybe<UpdateMaterialNestedPayload>;
  ownerIRI?: Maybe<Scalars['String']>;
  ownerLabel?: Maybe<Scalars['String']>;
};

/** Cursor connection for updateCircleMaterialOwnershipNestedPayload. */
export type UpdateCircleMaterialOwnershipNestedPayloadCursorConnection = {
  __typename?: 'updateCircleMaterialOwnershipNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<UpdateCircleMaterialOwnershipNestedPayloadEdge>>>;
  pageInfo: UpdateCircleMaterialOwnershipNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of updateCircleMaterialOwnershipNestedPayload. */
export type UpdateCircleMaterialOwnershipNestedPayloadEdge = {
  __typename?: 'updateCircleMaterialOwnershipNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<UpdateCircleMaterialOwnershipNestedPayload>;
};

/** Information about the current page. */
export type UpdateCircleMaterialOwnershipNestedPayloadPageInfo = {
  __typename?: 'updateCircleMaterialOwnershipNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Updates a CircleMaterialOwnership. */
export type UpdateCircleMaterialOwnershipPayload = {
  __typename?: 'updateCircleMaterialOwnershipPayload';
  circleMaterialOwnership?: Maybe<CircleMaterialOwnership>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Updates a Circle. */
export type UpdateCircleNestedPayload = Node & {
  __typename?: 'updateCircleNestedPayload';
  /**
   * Not persisted property, used to define what can do current logged user;
   * set by CircleResolver.
   */
  activeUserActions?: Maybe<Scalars['Iterable']>;
  address?: Maybe<Scalars['String']>;
  admins?: Maybe<UpdateUserNestedPayloadCursorConnection>;
  children?: Maybe<UpdateCircleNestedPayloadCursorConnection>;
  circleOwnerships?: Maybe<UpdateCircleMaterialOwnershipNestedPayloadCursorConnection>;
  city?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  cover?: Maybe<CircleCover>;
  createdAt?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  distance?: Maybe<Scalars['Float']>;
  explicitLocationLabel?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  indexable?: Maybe<Scalars['Boolean']>;
  invitationLinks?: Maybe<Array<Maybe<CircleInvitationLink>>>;
  invitations?: Maybe<InvitationCursorConnection>;
  latitude?: Maybe<Scalars['String']>;
  location?: Maybe<Scalars['String']>;
  logo?: Maybe<CircleLogo>;
  longitude?: Maybe<Scalars['String']>;
  memberships?: Maybe<CircleMembershipCursorConnection>;
  name?: Maybe<Scalars['String']>;
  owners?: Maybe<UpdateUserNestedPayloadCursorConnection>;
  parent?: Maybe<UpdateCircleNestedPayload>;
  pendingMemberships?: Maybe<CircleMembershipCursorConnection>;
  postalCode?: Maybe<Scalars['String']>;
  pricings?: Maybe<UpdateMaterialPricingNestedPayloadCursorConnection>;
  slug?: Maybe<Scalars['String']>;
  subscription?: Maybe<Subscription>;
  updatedAt?: Maybe<Scalars['String']>;
  website?: Maybe<Scalars['String']>;
};

/** Cursor connection for updateCircleNestedPayload. */
export type UpdateCircleNestedPayloadCursorConnection = {
  __typename?: 'updateCircleNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<UpdateCircleNestedPayloadEdge>>>;
  pageInfo: UpdateCircleNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of updateCircleNestedPayload. */
export type UpdateCircleNestedPayloadEdge = {
  __typename?: 'updateCircleNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<UpdateCircleNestedPayload>;
};

/** Information about the current page. */
export type UpdateCircleNestedPayloadPageInfo = {
  __typename?: 'updateCircleNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Updates a Circle. */
export type UpdateCirclePayload = {
  __typename?: 'updateCirclePayload';
  circle?: Maybe<Circle>;
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Updates a MaterialBooking. */
export type UpdateMaterialBookingInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  comment?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['String']>;
  discussion?: InputMaybe<Scalars['String']>;
  endDate?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  material?: InputMaybe<Scalars['String']>;
  periods?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  price?: InputMaybe<Scalars['Float']>;
  ratings?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  slug?: InputMaybe<Scalars['String']>;
  startDate?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['String']>;
  statusTransitionAvailables?: InputMaybe<Scalars['Iterable']>;
  updatedAt?: InputMaybe<Scalars['String']>;
  user?: InputMaybe<Scalars['String']>;
};

/** Updates a MaterialBooking. */
export type UpdateMaterialBookingNestedPayload = Node & {
  __typename?: 'updateMaterialBookingNestedPayload';
  comment?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  discussion?: Maybe<Discussion>;
  endDate?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  material?: Maybe<UpdateMaterialNestedPayload>;
  past?: Maybe<Scalars['Boolean']>;
  periods?: Maybe<Array<Maybe<MaterialBookingDatePeriod>>>;
  price?: Maybe<Scalars['Float']>;
  ratings?: Maybe<UpdateRatingNestedPayloadCursorConnection>;
  slug?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
  statusTransitionAvailables?: Maybe<Scalars['Iterable']>;
  terminatedStatuses?: Maybe<Scalars['Iterable']>;
  transitions?: Maybe<Scalars['Iterable']>;
  updatedAt?: Maybe<Scalars['String']>;
  user?: Maybe<UpdateUserNestedPayload>;
};

/** Cursor connection for updateMaterialBookingNestedPayload. */
export type UpdateMaterialBookingNestedPayloadCursorConnection = {
  __typename?: 'updateMaterialBookingNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<UpdateMaterialBookingNestedPayloadEdge>>>;
  pageInfo: UpdateMaterialBookingNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of updateMaterialBookingNestedPayload. */
export type UpdateMaterialBookingNestedPayloadEdge = {
  __typename?: 'updateMaterialBookingNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<UpdateMaterialBookingNestedPayload>;
};

/** Information about the current page. */
export type UpdateMaterialBookingNestedPayloadPageInfo = {
  __typename?: 'updateMaterialBookingNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Updates a MaterialBooking. */
export type UpdateMaterialBookingPayload = {
  __typename?: 'updateMaterialBookingPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialBooking?: Maybe<MaterialBooking>;
};

/** Updates a MaterialCategory. */
export type UpdateMaterialCategoryInput = {
  children?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  materials?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  name?: InputMaybe<Scalars['String']>;
  parent?: InputMaybe<Scalars['String']>;
  slug?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['String']>;
};

/** Updates a MaterialCategory. */
export type UpdateMaterialCategoryPayload = {
  __typename?: 'updateMaterialCategoryPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialCategory?: Maybe<MaterialCategory>;
};

/** Updates a MaterialImage. */
export type UpdateMaterialImageInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  dimensions?: InputMaybe<Scalars['Iterable']>;
  file: Scalars['Upload'];
  id: Scalars['ID'];
  imageName?: InputMaybe<Scalars['String']>;
  mime?: InputMaybe<Scalars['String']>;
  size?: InputMaybe<Scalars['Int']>;
};

/** Updates a MaterialImage. */
export type UpdateMaterialImageNestedPayload = Node & {
  __typename?: 'updateMaterialImageNestedPayload';
  contentUrl?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  dimensions?: Maybe<Scalars['Iterable']>;
  id: Scalars['ID'];
  imageName?: Maybe<Scalars['String']>;
  material?: Maybe<UpdateMaterialNestedPayload>;
  mime?: Maybe<Scalars['String']>;
  originalName?: Maybe<Scalars['String']>;
  size?: Maybe<Scalars['Int']>;
  updatedAt?: Maybe<Scalars['String']>;
};

/** Cursor connection for updateMaterialImageNestedPayload. */
export type UpdateMaterialImageNestedPayloadCursorConnection = {
  __typename?: 'updateMaterialImageNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<UpdateMaterialImageNestedPayloadEdge>>>;
  pageInfo: UpdateMaterialImageNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of updateMaterialImageNestedPayload. */
export type UpdateMaterialImageNestedPayloadEdge = {
  __typename?: 'updateMaterialImageNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<UpdateMaterialImageNestedPayload>;
};

/** Information about the current page. */
export type UpdateMaterialImageNestedPayloadPageInfo = {
  __typename?: 'updateMaterialImageNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Updates a MaterialImage. */
export type UpdateMaterialImagePayload = {
  __typename?: 'updateMaterialImagePayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialImage?: Maybe<MaterialImage>;
};

/** Updates a Material. */
export type UpdateMaterialInput = {
  brand?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  images?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  location?: InputMaybe<Scalars['String']>;
  mainOwner?: InputMaybe<Scalars['String']>;
  mainOwnership?: InputMaybe<Scalars['MaterialOwnershipType']>;
  model?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  ownerships?: InputMaybe<Array<InputMaybe<Scalars['MaterialOwnershipType']>>>;
  price?: InputMaybe<Scalars['Float']>;
  pricings?: InputMaybe<Array<InputMaybe<UpdateMaterialPricingNestedInput>>>;
  userLists?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

/** Updates a Material. */
export type UpdateMaterialNestedPayload = Node & {
  __typename?: 'updateMaterialNestedPayload';
  bestPriceForUser?: Maybe<Scalars['Float']>;
  bookings?: Maybe<UpdateMaterialBookingNestedPayloadCursorConnection>;
  brand?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  distance?: Maybe<Scalars['Float']>;
  id: Scalars['ID'];
  images?: Maybe<UpdateMaterialImageNestedPayloadCursorConnection>;
  latitude?: Maybe<Scalars['Float']>;
  location?: Maybe<Scalars['String']>;
  longitude?: Maybe<Scalars['Float']>;
  mainOwner?: Maybe<UpdateUserNestedPayload>;
  mainOwnership?: Maybe<UpdateAbstractMaterialOwnershipNestedPayload>;
  model?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  ownerships?: Maybe<Array<Maybe<UpdateAbstractMaterialOwnershipNestedPayload>>>;
  price?: Maybe<Scalars['Float']>;
  pricings?: Maybe<UpdateMaterialPricingNestedPayloadCursorConnection>;
  slug?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['String']>;
  userActionAvailables?: Maybe<Scalars['Iterable']>;
  userLists?: Maybe<UpdateUserListNestedPayloadCursorConnection>;
};

/** Updates a Material. */
export type UpdateMaterialPayload = {
  __typename?: 'updateMaterialPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  material?: Maybe<UpdateMaterialPayloadData>;
};

/** Updates a Material. */
export type UpdateMaterialPayloadData = Node & {
  __typename?: 'updateMaterialPayloadData';
  bestPriceForUser?: Maybe<Scalars['Float']>;
  bookings?: Maybe<UpdateMaterialBookingNestedPayloadCursorConnection>;
  brand?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  distance?: Maybe<Scalars['Float']>;
  id: Scalars['ID'];
  images?: Maybe<UpdateMaterialImageNestedPayloadCursorConnection>;
  latitude?: Maybe<Scalars['Float']>;
  location?: Maybe<Scalars['String']>;
  longitude?: Maybe<Scalars['Float']>;
  mainOwner?: Maybe<UpdateUserNestedPayload>;
  mainOwnership?: Maybe<UpdateAbstractMaterialOwnershipNestedPayload>;
  model?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  ownerships?: Maybe<Array<Maybe<UpdateAbstractMaterialOwnershipNestedPayload>>>;
  price?: Maybe<Scalars['Float']>;
  pricings?: Maybe<UpdateMaterialPricingNestedPayloadCursorConnection>;
  slug?: Maybe<Scalars['String']>;
  updatedAt?: Maybe<Scalars['String']>;
  userActionAvailables?: Maybe<Scalars['Iterable']>;
  userLists?: Maybe<UpdateUserListNestedPayloadCursorConnection>;
};

/** Updates a MaterialPricing. */
export type UpdateMaterialPricingInput = {
  circle?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  material?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['String']>;
  value?: InputMaybe<Scalars['Float']>;
};

/** Updates a MaterialPricing. */
export type UpdateMaterialPricingNestedInput = {
  circle?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  material?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['String']>;
  value?: InputMaybe<Scalars['Float']>;
};

/** Updates a MaterialPricing. */
export type UpdateMaterialPricingNestedPayload = Node & {
  __typename?: 'updateMaterialPricingNestedPayload';
  circle?: Maybe<UpdateCircleNestedPayload>;
  createdAt?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  material?: Maybe<UpdateMaterialNestedPayload>;
  updatedAt?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['Float']>;
};

/** Cursor connection for updateMaterialPricingNestedPayload. */
export type UpdateMaterialPricingNestedPayloadCursorConnection = {
  __typename?: 'updateMaterialPricingNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<UpdateMaterialPricingNestedPayloadEdge>>>;
  pageInfo: UpdateMaterialPricingNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of updateMaterialPricingNestedPayload. */
export type UpdateMaterialPricingNestedPayloadEdge = {
  __typename?: 'updateMaterialPricingNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<UpdateMaterialPricingNestedPayload>;
};

/** Information about the current page. */
export type UpdateMaterialPricingNestedPayloadPageInfo = {
  __typename?: 'updateMaterialPricingNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Updates a MaterialPricing. */
export type UpdateMaterialPricingPayload = {
  __typename?: 'updateMaterialPricingPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  materialPricing?: Maybe<MaterialPricing>;
};

/** Updates a Message. */
export type UpdateMessageInput = {
  author?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  content?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['String']>;
  discussion?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  parameters?: InputMaybe<Scalars['String']>;
  updatedAt?: InputMaybe<Scalars['String']>;
};

/** Updates a Message. */
export type UpdateMessagePayload = {
  __typename?: 'updateMessagePayload';
  clientMutationId?: Maybe<Scalars['String']>;
  message?: Maybe<Message>;
};

/** Updates a Rating. */
export type UpdateRatingInput = {
  author?: InputMaybe<Scalars['String']>;
  booking?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  updatedAt?: InputMaybe<Scalars['String']>;
  user?: InputMaybe<Scalars['String']>;
  value?: InputMaybe<Scalars['Int']>;
};

/** Updates a Rating. */
export type UpdateRatingNestedPayload = Node & {
  __typename?: 'updateRatingNestedPayload';
  author?: Maybe<UpdateUserNestedPayload>;
  booking?: Maybe<UpdateMaterialBookingNestedPayload>;
  createdAt?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  updatedAt?: Maybe<Scalars['String']>;
  user?: Maybe<UpdateUserNestedPayload>;
  value?: Maybe<Scalars['Int']>;
};

/** Cursor connection for updateRatingNestedPayload. */
export type UpdateRatingNestedPayloadCursorConnection = {
  __typename?: 'updateRatingNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<UpdateRatingNestedPayloadEdge>>>;
  pageInfo: UpdateRatingNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of updateRatingNestedPayload. */
export type UpdateRatingNestedPayloadEdge = {
  __typename?: 'updateRatingNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<UpdateRatingNestedPayload>;
};

/** Information about the current page. */
export type UpdateRatingNestedPayloadPageInfo = {
  __typename?: 'updateRatingNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Updates a Rating. */
export type UpdateRatingPayload = {
  __typename?: 'updateRatingPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  rating?: Maybe<Rating>;
};

/** Updates a Settings. */
export type UpdateSettingsInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['String']>;
  currencySymbol?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  language?: InputMaybe<Scalars['String']>;
  preferedCommunicationMode?: InputMaybe<CommunicationMode>;
  themeMode?: InputMaybe<ThemeMode>;
  updatedAt?: InputMaybe<Scalars['String']>;
  useAbbreviatedName?: InputMaybe<Scalars['Boolean']>;
  user?: InputMaybe<Scalars['String']>;
};

/** Updates a Settings. */
export type UpdateSettingsNestedInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['String']>;
  currencySymbol?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  language?: InputMaybe<Scalars['String']>;
  preferedCommunicationMode?: InputMaybe<CommunicationMode>;
  themeMode?: InputMaybe<ThemeMode>;
  updatedAt?: InputMaybe<Scalars['String']>;
  useAbbreviatedName?: InputMaybe<Scalars['Boolean']>;
  user?: InputMaybe<Scalars['String']>;
};

/** Updates a Settings. */
export type UpdateSettingsNestedPayload = Node & {
  __typename?: 'updateSettingsNestedPayload';
  createdAt?: Maybe<Scalars['String']>;
  currencySymbol?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  language?: Maybe<Scalars['String']>;
  preferedCommunicationMode?: Maybe<CommunicationMode>;
  themeMode?: Maybe<ThemeMode>;
  updatedAt?: Maybe<Scalars['String']>;
  useAbbreviatedName?: Maybe<Scalars['Boolean']>;
  user?: Maybe<UpdateUserNestedPayload>;
};

/** Updates a Settings. */
export type UpdateSettingsPayload = {
  __typename?: 'updateSettingsPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  settings?: Maybe<Settings>;
};

/** Updates a User. */
export type UpdateUserInput = {
  avatar?: InputMaybe<Scalars['Upload']>;
  bio?: InputMaybe<Scalars['String']>;
  birthDate?: InputMaybe<Scalars['String']>;
  city?: InputMaybe<Scalars['String']>;
  clientMutationId?: InputMaybe<Scalars['String']>;
  country?: InputMaybe<Scalars['String']>;
  countryCode?: InputMaybe<Scalars['Int']>;
  email?: InputMaybe<Scalars['String']>;
  firstname?: InputMaybe<Scalars['String']>;
  gender?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  lastname?: InputMaybe<Scalars['String']>;
  latitude?: InputMaybe<Scalars['String']>;
  /** The geographical location of the user as a POINT (longitude, latitude). */
  location?: InputMaybe<Scalars['String']>;
  longitude?: InputMaybe<Scalars['String']>;
  nationalNumber?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
  postalCode?: InputMaybe<Scalars['String']>;
  removeAvatar?: InputMaybe<Scalars['Boolean']>;
  settings?: InputMaybe<UpdateSettingsNestedInput>;
  streetAddress?: InputMaybe<Scalars['String']>;
};

/** Updates a UserList. */
export type UpdateUserListInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  name?: InputMaybe<Scalars['String']>;
  users?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
};

/** Updates a UserList. */
export type UpdateUserListNestedPayload = Node & {
  __typename?: 'updateUserListNestedPayload';
  id: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
  owner?: Maybe<UpdateUserNestedPayload>;
  slug?: Maybe<Scalars['String']>;
  users?: Maybe<UpdateUserNestedPayloadCursorConnection>;
};

/** Cursor connection for updateUserListNestedPayload. */
export type UpdateUserListNestedPayloadCursorConnection = {
  __typename?: 'updateUserListNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<UpdateUserListNestedPayloadEdge>>>;
  pageInfo: UpdateUserListNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of updateUserListNestedPayload. */
export type UpdateUserListNestedPayloadEdge = {
  __typename?: 'updateUserListNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<UpdateUserListNestedPayload>;
};

/** Information about the current page. */
export type UpdateUserListNestedPayloadPageInfo = {
  __typename?: 'updateUserListNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Updates a UserList. */
export type UpdateUserListPayload = {
  __typename?: 'updateUserListPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  userList?: Maybe<UserList>;
};

/** Updates a UserMaterialOwnership. */
export type UpdateUserMaterialOwnershipInput = {
  clientMutationId?: InputMaybe<Scalars['String']>;
  id: Scalars['ID'];
  material?: InputMaybe<Scalars['String']>;
  materialMainOwned?: InputMaybe<Scalars['String']>;
  user?: InputMaybe<Scalars['String']>;
};

/** Updates a UserMaterialOwnership. */
export type UpdateUserMaterialOwnershipNestedPayload = Node & {
  __typename?: 'updateUserMaterialOwnershipNestedPayload';
  id: Scalars['ID'];
  material?: Maybe<UpdateMaterialNestedPayload>;
  materialMainOwned?: Maybe<UpdateMaterialNestedPayload>;
  ownerIRI?: Maybe<Scalars['String']>;
  ownerLabel?: Maybe<Scalars['String']>;
  user?: Maybe<UpdateUserNestedPayload>;
};

/** Cursor connection for updateUserMaterialOwnershipNestedPayload. */
export type UpdateUserMaterialOwnershipNestedPayloadCursorConnection = {
  __typename?: 'updateUserMaterialOwnershipNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<UpdateUserMaterialOwnershipNestedPayloadEdge>>>;
  pageInfo: UpdateUserMaterialOwnershipNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of updateUserMaterialOwnershipNestedPayload. */
export type UpdateUserMaterialOwnershipNestedPayloadEdge = {
  __typename?: 'updateUserMaterialOwnershipNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<UpdateUserMaterialOwnershipNestedPayload>;
};

/** Information about the current page. */
export type UpdateUserMaterialOwnershipNestedPayloadPageInfo = {
  __typename?: 'updateUserMaterialOwnershipNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Updates a UserMaterialOwnership. */
export type UpdateUserMaterialOwnershipPayload = {
  __typename?: 'updateUserMaterialOwnershipPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  userMaterialOwnership?: Maybe<UserMaterialOwnership>;
};

/** Updates a User. */
export type UpdateUserNestedPayload = Node & {
  __typename?: 'updateUserNestedPayload';
  avatar?: Maybe<UpdateAvatarNestedPayload>;
  averageRatingScore?: Maybe<Scalars['Float']>;
  bio?: Maybe<Scalars['String']>;
  birthDate?: Maybe<Scalars['String']>;
  circleMemberships?: Maybe<CircleMembershipCursorConnection>;
  city?: Maybe<Scalars['String']>;
  confirmationToken?: Maybe<Scalars['String']>;
  confirmed?: Maybe<Scalars['Boolean']>;
  country?: Maybe<Scalars['String']>;
  createdAt?: Maybe<Scalars['String']>;
  discussions?: Maybe<DiscussionCursorConnection>;
  email?: Maybe<Scalars['String']>;
  firstname?: Maybe<Scalars['String']>;
  fullname?: Maybe<Scalars['String']>;
  gender?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  lastname?: Maybe<Scalars['String']>;
  latitude?: Maybe<Scalars['Float']>;
  /** The geographical location of the user as a POINT (longitude, latitude). */
  location?: Maybe<Scalars['String']>;
  longitude?: Maybe<Scalars['Float']>;
  materialBookings?: Maybe<UpdateMaterialBookingNestedPayloadCursorConnection>;
  materialSearchAlerts?: Maybe<MaterialSearchAlertCursorConnection>;
  password?: Maybe<Scalars['String']>;
  phoneNumber?: Maybe<Scalars['Iterable']>;
  postalCode?: Maybe<Scalars['String']>;
  ratings?: Maybe<UpdateRatingNestedPayloadCursorConnection>;
  ratingsSent?: Maybe<UpdateRatingNestedPayloadCursorConnection>;
  refreshToken?: Maybe<Scalars['String']>;
  roles?: Maybe<Scalars['Iterable']>;
  settings?: Maybe<UpdateSettingsNestedPayload>;
  slug?: Maybe<Scalars['String']>;
  streetAddress?: Maybe<Scalars['String']>;
  /** //CAUTION: removing comments will break the token generation @TODO needs to be fixed. */
  token?: Maybe<Scalars['String']>;
  tokenExpiresAt?: Maybe<Scalars['Float']>;
  updatedAt?: Maybe<Scalars['String']>;
  /** A visual identifier that represents this user. */
  userIdentifier?: Maybe<Scalars['String']>;
  /**
   *
   * The list of users I made (for example, my friends) to easily allow them to access my materials
   */
  userLists?: Maybe<UpdateUserListNestedPayloadCursorConnection>;
  userOwnerships?: Maybe<UpdateUserMaterialOwnershipNestedPayloadCursorConnection>;
};

/** Cursor connection for updateUserNestedPayload. */
export type UpdateUserNestedPayloadCursorConnection = {
  __typename?: 'updateUserNestedPayloadCursorConnection';
  edges?: Maybe<Array<Maybe<UpdateUserNestedPayloadEdge>>>;
  pageInfo: UpdateUserNestedPayloadPageInfo;
  totalCount: Scalars['Int'];
};

/** Edge of updateUserNestedPayload. */
export type UpdateUserNestedPayloadEdge = {
  __typename?: 'updateUserNestedPayloadEdge';
  cursor: Scalars['String'];
  node?: Maybe<UpdateUserNestedPayload>;
};

/** Information about the current page. */
export type UpdateUserNestedPayloadPageInfo = {
  __typename?: 'updateUserNestedPayloadPageInfo';
  endCursor?: Maybe<Scalars['String']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startCursor?: Maybe<Scalars['String']>;
};

/** Updates a User. */
export type UpdateUserPayload = {
  __typename?: 'updateUserPayload';
  clientMutationId?: Maybe<Scalars['String']>;
  user?: Maybe<User>;
};

export type AuthUser__UserFragment = { __typename?: 'User', id: string, slug: string, firstname?: string, lastname: string, fullname: string, email: string, phoneNumber?: any, streetAddress: string, city?: string, country?: string, roles: any, gender?: string, token?: string, refreshToken?: string, tokenExpiresAt?: number, averageRatingScore?: number, latitude?: number, longitude?: number, confirmed: boolean, createdAt?: string, settings?: { __typename?: 'Settings', language: string, currencySymbol: string, id: string, themeMode: ThemeMode, useAbbreviatedName: boolean, preferedCommunicationMode: CommunicationMode }, avatar?: { __typename?: 'Avatar', id: string, contentUrl?: string }, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', permission: CirclePermissionType, circle: { __typename?: 'Circle', slug: string, id: string, name: string, description?: string, city?: string, logo?: { __typename?: 'CircleLogo', imageName: string, size: number }, subscription?: { __typename?: 'Subscription', active: boolean, id: string, plan?: { __typename?: 'Plan', slug: string, name?: string } } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number } };

export type AuthUserQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type AuthUserQuery = { __typename?: 'Query', user?: { __typename?: 'User', id: string, slug: string, firstname?: string, lastname: string, fullname: string, email: string, phoneNumber?: any, streetAddress: string, city?: string, country?: string, roles: any, gender?: string, token?: string, refreshToken?: string, tokenExpiresAt?: number, averageRatingScore?: number, latitude?: number, longitude?: number, confirmed: boolean, createdAt?: string, settings?: { __typename?: 'Settings', language: string, currencySymbol: string, id: string, themeMode: ThemeMode, useAbbreviatedName: boolean, preferedCommunicationMode: CommunicationMode }, avatar?: { __typename?: 'Avatar', id: string, contentUrl?: string }, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', permission: CirclePermissionType, circle: { __typename?: 'Circle', slug: string, id: string, name: string, description?: string, city?: string, logo?: { __typename?: 'CircleLogo', imageName: string, size: number }, subscription?: { __typename?: 'Subscription', active: boolean, id: string, plan?: { __typename?: 'Plan', slug: string, name?: string } } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number } } };

export type ChangeStatusMutationVariables = Exact<{
  id: Scalars['ID'];
  transition?: InputMaybe<Scalars['String']>;
}>;


export type ChangeStatusMutation = { __typename?: 'Mutation', changeStatusMaterialBooking?: { __typename?: 'changeStatusMaterialBookingPayload', materialBooking?: { __typename?: 'MaterialBooking', id: string } } };

export type MaterialCategorySelectQueryVariables = Exact<{ [key: string]: never; }>;


export type MaterialCategorySelectQuery = { __typename?: 'Query', materialCategories?: { __typename?: 'MaterialCategoryCursorConnection', edges?: Array<{ __typename?: 'MaterialCategoryEdge', node?: { __typename?: 'MaterialCategory', id: string, name: string, slug: string } }> } };

export type CategorySelect__CategoryFragment = { __typename?: 'MaterialCategory', id: string, name: string, slug: string };

export type Blaze__UserFragment = { __typename?: 'User', roles: any, fullname: string };

export type UserImageQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type UserImageQuery = { __typename?: 'Query', user?: { __typename?: 'User', firstname?: string, lastname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } } };

export type ChatBubble__MessageFragment = { __typename?: 'Message', id: string, content: string, parameters: string, createdAt?: string, updatedAt?: string, author?: { __typename?: 'User', slug: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } } };

export type AvatarFan__UserFragment = { __typename?: 'User', id: string, firstname?: string, lastname: string, avatar?: { __typename?: 'Avatar', id: string, contentUrl?: string } };

export type CommunityCard_CommunityFragment = { __typename?: 'Circle', id: string, slug: string, name: string, city?: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string }, memberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, user: { __typename?: 'User', id: string, roles: any, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } } } }> }, cover?: { __typename?: 'CircleCover', contentUrl?: string } };

export type MaterialCard__MaterialFragment = { __typename?: 'Material', distance?: number, id: string, slug: string, name: string, brand?: string, model?: string, bestPriceForUser?: number, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> }, mainOwnershipUser?: { __typename?: 'User', city?: string, slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, mainOwnershipCircle?: { __typename?: 'Circle', city?: string, slug: string, name: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } };

export type MaterialCard__Material_WithoutOwnershipFragment = { __typename?: 'Material', id: string, slug: string, name: string, brand?: string, model?: string, bestPriceForUser?: number, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> } };

export type MaterialCard__Material_OwnershipFragment = { __typename?: 'Material', mainOwnershipUser?: { __typename?: 'User', city?: string, slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, mainOwnershipCircle?: { __typename?: 'Circle', city?: string, slug: string, name: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } };

export type MaterialOwnerCard_UserFragment = { __typename?: 'User', id: string, slug: string, firstname?: string, lastname: string, city?: string, averageRatingScore?: number, country?: string, fullname: string, birthDate?: string, roles: any, bio?: string, phoneNumber?: any, gender?: string, email: string, avatar?: { __typename?: 'Avatar', contentUrl?: string }, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, circle: { __typename?: 'Circle', name: string, slug: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number }, userOwnerships?: { __typename?: 'UserMaterialOwnershipCursorConnection', totalCount: number }, settings?: { __typename?: 'Settings', preferedCommunicationMode: CommunicationMode } };

export type MaterialOwnerCard_CircleFragment = { __typename?: 'Circle', id: string, slug: string, name: string, city?: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string }, memberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, user: { __typename?: 'User', id: string, roles: any, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } } } }> }, cover?: { __typename?: 'CircleCover', contentUrl?: string } };

export type MaterialHorizontalCard__MaterialFragment = { __typename?: 'Material', distance?: number, id: string, slug: string, name: string, brand?: string, model?: string, bestPriceForUser?: number, mainOwner?: { __typename?: 'User', streetAddress: string, roles: any, fullname: string }, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> }, mainOwnershipUser?: { __typename?: 'User', city?: string, slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, mainOwnershipCircle?: { __typename?: 'Circle', city?: string, slug: string, name: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } };

export type CommunityMemberCardFragment = { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, permissionTransitionAvailables?: any, status: string, statusTransitionAvailables?: any, user: { __typename?: 'User', averageRatingScore?: number, city?: string, country?: string, slug: string, firstname?: string, lastname: string, fullname: string, birthDate?: string, roles: any, bio?: string, id: string, email: string, phoneNumber?: any, gender?: string, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, circle: { __typename?: 'Circle', name: string, slug: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number }, userOwnerships?: { __typename?: 'UserMaterialOwnershipCursorConnection', totalCount: number }, avatar?: { __typename?: 'Avatar', contentUrl?: string }, settings?: { __typename?: 'Settings', preferedCommunicationMode: CommunicationMode } } };

export type UserCard__UserFragment = { __typename?: 'User', averageRatingScore?: number, city?: string, country?: string, slug: string, firstname?: string, lastname: string, fullname: string, birthDate?: string, roles: any, bio?: string, phoneNumber?: any, gender?: string, email: string, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, circle: { __typename?: 'Circle', name: string, slug: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number }, userOwnerships?: { __typename?: 'UserMaterialOwnershipCursorConnection', totalCount: number }, avatar?: { __typename?: 'Avatar', contentUrl?: string }, settings?: { __typename?: 'Settings', preferedCommunicationMode: CommunicationMode } };

export type AskToJoinCircleButtonMutationVariables = Exact<{
  input: AskToJoinCircleMembershipInput;
}>;


export type AskToJoinCircleButtonMutation = { __typename?: 'Mutation', askToJoinCircleMembership?: { __typename?: 'askToJoinCircleMembershipPayload', circleMembership?: { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, user: { __typename?: 'User', id: string }, circle: { __typename?: 'Circle', name: string, activeUserActions: any, description?: string, slug: string, city?: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string }, cover?: { __typename?: 'CircleCover', contentUrl?: string } } } } };

export type JoinCircleButton__CircleFragment = { __typename?: 'Circle', id: string };

export type ChangeMembershipPermissionMembershipFragment = { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, permissionTransitionAvailables?: any, user: { __typename?: 'User', firstname?: string } };

export type ChangeMembershipPermissionButtonMutationVariables = Exact<{
  id: Scalars['ID'];
  transition?: InputMaybe<Scalars['String']>;
}>;


export type ChangeMembershipPermissionButtonMutation = { __typename?: 'Mutation', changePermissionCircleMembership?: { __typename?: 'changePermissionCircleMembershipPayload', circleMembership?: { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, permissionTransitionAvailables?: any, user: { __typename?: 'User', firstname?: string } } } };

export type ChangeMembershipStatusButtonToastFragment = { __typename?: 'User', id: string, firstname?: string, email: string };

export type ChangeMembershipStatusMembershipFragment = { __typename?: 'CircleMembership', id: string, status: string, user: { __typename?: 'User', id: string, firstname?: string, email: string } };

export type ChangeMembershipStatusButtonMutationVariables = Exact<{
  id: Scalars['ID'];
  transition?: InputMaybe<Scalars['String']>;
}>;


export type ChangeMembershipStatusButtonMutation = { __typename?: 'Mutation', changeStatusCircleMembership?: { __typename?: 'changeStatusCircleMembershipPayload', circleMembership?: { __typename?: 'CircleMembership', id: string, status: string, user: { __typename?: 'User', id: string, firstname?: string, email: string } } } };

export type MembershipList__MembershipFragment = { __typename?: 'CircleMembership', id: string, statusTransitionAvailables?: any, permissionTransitionAvailables?: any, permission: CirclePermissionType, status: string, user: { __typename?: 'User', averageRatingScore?: number, city?: string, country?: string, slug: string, firstname?: string, lastname: string, fullname: string, birthDate?: string, roles: any, bio?: string, id: string, email: string, phoneNumber?: any, gender?: string, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, circle: { __typename?: 'Circle', name: string, slug: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number }, userOwnerships?: { __typename?: 'UserMaterialOwnershipCursorConnection', totalCount: number }, avatar?: { __typename?: 'Avatar', contentUrl?: string }, settings?: { __typename?: 'Settings', preferedCommunicationMode: CommunicationMode } } };

export type MembershipListQueryVariables = Exact<{
  slug: Scalars['String'];
  statuses?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
  afterCursor?: InputMaybe<Scalars['String']>;
  beforeCursor?: InputMaybe<Scalars['String']>;
  nbToFetch?: InputMaybe<Scalars['Int']>;
  order?: InputMaybe<Array<InputMaybe<CircleMembershipFilter_Order>> | InputMaybe<CircleMembershipFilter_Order>>;
}>;


export type MembershipListQuery = { __typename?: 'Query', circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, pageInfo: { __typename?: 'CircleMembershipPageInfo', startCursor?: string, endCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean }, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, statusTransitionAvailables?: any, permissionTransitionAvailables?: any, permission: CirclePermissionType, status: string, user: { __typename?: 'User', averageRatingScore?: number, city?: string, country?: string, slug: string, firstname?: string, lastname: string, fullname: string, birthDate?: string, roles: any, bio?: string, id: string, email: string, phoneNumber?: any, gender?: string, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, circle: { __typename?: 'Circle', name: string, slug: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number }, userOwnerships?: { __typename?: 'UserMaterialOwnershipCursorConnection', totalCount: number }, avatar?: { __typename?: 'Avatar', contentUrl?: string }, settings?: { __typename?: 'Settings', preferedCommunicationMode: CommunicationMode } } } }> } };

export type ContactMutationVariables = Exact<{
  input: CreateContactInput;
}>;


export type ContactMutation = { __typename?: 'Mutation', createContact?: { __typename?: 'createContactPayload', contact?: { __typename?: 'Contact', id: string } } };

export type RequestInvitationMutationVariables = Exact<{
  input: CreateInvitationInput;
}>;


export type RequestInvitationMutation = { __typename?: 'Mutation', createInvitation?: { __typename?: 'createInvitationPayload', invitation?: { __typename?: 'Invitation', id: string } } };

export type InvitationFormDialog_CircleFragment = { __typename?: 'Circle', id: string, name: string, slug: string, invitationLinks?: Array<{ __typename?: 'CircleInvitationLink', _id?: string, enabled: boolean, numberOfUses: number }> };

export type InvitationFormDialog_ExistingUserFragment = { __typename?: 'User', id: string, slug: string, email: string, firstname?: string, lastname: string, city?: string, country?: string, avatar?: { __typename?: 'Avatar', contentUrl?: string }, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', circle: { __typename?: 'Circle', id: string, name: string } } }> } };

export type FindExistingUserForEmailQueryVariables = Exact<{
  email: Scalars['String'];
}>;


export type FindExistingUserForEmailQuery = { __typename?: 'Query', users?: { __typename?: 'UserCursorConnection', edges?: Array<{ __typename?: 'UserEdge', node?: { __typename?: 'User', id: string, slug: string, email: string, firstname?: string, lastname: string, city?: string, country?: string, avatar?: { __typename?: 'Avatar', contentUrl?: string }, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', circle: { __typename?: 'Circle', id: string, name: string } } }> } } }> } };

export type SendCommunityInvitationMutationVariables = Exact<{
  input: SendCircleInvitationInput;
}>;


export type SendCommunityInvitationMutation = { __typename?: 'Mutation', sendCircleInvitation?: { __typename?: 'sendCircleInvitationPayload', invitation?: { __typename?: 'Invitation', id: string } } };

export type UpdateUserLocationMutationVariables = Exact<{
  input: UpdateUserInput;
}>;


export type UpdateUserLocationMutation = { __typename?: 'Mutation', updateUser?: { __typename?: 'updateUserPayload', user?: { __typename?: 'User', id: string, location?: string, longitude?: number, latitude?: number, streetAddress: string } } };

export type MyProfileLocationForm_UserFragment = { __typename?: 'User', id: string, latitude?: number, longitude?: number, streetAddress: string, city?: string, postalCode?: string, country?: string };

export type UserAppearenceForm__UserFragment = { __typename?: 'User', id: string, gender?: string, settings?: { __typename?: 'Settings', id: string, themeMode: ThemeMode } };

export type UserPrivacyForm__UserFragment = { __typename?: 'User', id: string, settings?: { __typename?: 'Settings', id: string, useAbbreviatedName: boolean, preferedCommunicationMode: CommunicationMode } };

export type UserForm__UserAvatarFragment = { __typename?: 'Avatar', id: string, contentUrl?: string };

export type UserForm__UserFragment = { __typename?: 'User', firstname?: string, lastname: string, streetAddress: string, email: string, gender?: string, city?: string, country?: string, phoneNumber?: any, settings?: { __typename?: 'Settings', language: string }, avatar?: { __typename?: 'Avatar', id: string, contentUrl?: string } };

export type CreateAvatarReturnFragment = { __typename?: 'Avatar', id: string, contentUrl?: string };

export type CreateAvatarMutationVariables = Exact<{
  input: CreateAvatarInput;
}>;


export type CreateAvatarMutation = { __typename?: 'Mutation', createAvatar?: { __typename?: 'createAvatarPayload', avatar?: { __typename?: 'Avatar', id: string, contentUrl?: string } } };

export type UserListAvatarsQueryVariables = Exact<{
  id: Scalars['ID'];
  afterCursor?: InputMaybe<Scalars['String']>;
  beforeCursor?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
}>;


export type UserListAvatarsQuery = { __typename?: 'Query', userList?: { __typename?: 'UserList', users?: { __typename?: 'UserCursorConnection', totalCount: number, pageInfo: { __typename?: 'UserPageInfo', hasNextPage: boolean, endCursor?: string, hasPreviousPage: boolean, startCursor?: string }, edges?: Array<{ __typename?: 'UserEdge', cursor: string, node?: { __typename?: 'User', id: string, fullname: string, firstname?: string, lastname: string, email: string, phoneNumber?: any, avatar?: { __typename?: 'Avatar', contentUrl?: string } } }> } } };

export type UserListAvatars_UserFragment = { __typename?: 'UserEdge', cursor: string, node?: { __typename?: 'User', id: string, fullname: string, firstname?: string, lastname: string, email: string, phoneNumber?: any, avatar?: { __typename?: 'Avatar', contentUrl?: string } } };

export type CircleForm__CircleFragment = { __typename?: 'Circle', id: string, slug: string, name: string, description?: string, website?: string, explicitLocationLabel?: string, indexable: boolean, latitude?: string, longitude?: string, address?: string, city?: string, postalCode?: string, country?: string, logo?: { __typename?: 'CircleLogo', id: string, contentUrl?: string }, cover?: { __typename?: 'CircleCover', id: string, contentUrl?: string } };

export type EditFormCircleQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type EditFormCircleQuery = { __typename?: 'Query', circle?: { __typename?: 'Circle', id: string, slug: string, name: string, description?: string, website?: string, explicitLocationLabel?: string, indexable: boolean, latitude?: string, longitude?: string, address?: string, city?: string, postalCode?: string, country?: string, logo?: { __typename?: 'CircleLogo', id: string, contentUrl?: string }, cover?: { __typename?: 'CircleCover', id: string, contentUrl?: string } } };

export type NewCommunityMutationVariables = Exact<{
  input: CreateCircleInput;
}>;


export type NewCommunityMutation = { __typename?: 'Mutation', createCircle?: { __typename?: 'createCirclePayload', circle?: { __typename?: 'Circle', id: string, slug: string } } };

export type CircleInformationForm__CircleFragment = { __typename?: 'Circle', name: string, description?: string, website?: string, explicitLocationLabel?: string, indexable: boolean };

export type CircleLocationForm__CircleFragment = { __typename?: 'Circle', latitude?: string, longitude?: string, address?: string, city?: string, postalCode?: string, country?: string };

export type CircleMediaForm__CircleFragment = { __typename?: 'Circle', id: string, name: string, logo?: { __typename?: 'CircleLogo', id: string, contentUrl?: string }, cover?: { __typename?: 'CircleCover', id: string, contentUrl?: string } };

export type AvailabilityPlanningEstimateMutationVariables = Exact<{
  materialId?: InputMaybe<Scalars['String']>;
  startDate?: InputMaybe<Scalars['String']>;
  endDate?: InputMaybe<Scalars['String']>;
}>;


export type AvailabilityPlanningEstimateMutation = { __typename?: 'Mutation', estimateMaterialBooking?: { __typename?: 'estimateMaterialBookingPayload', materialBooking?: { __typename?: 'MaterialBooking', id: string, price?: number, slug: string, status: string, user?: { __typename?: 'User', fullname: string }, material?: { __typename?: 'Material', name: string, slug: string, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> } }, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number }> } } };

export type AvailabilityPlanning__MaterialFragment = { __typename?: 'Material', id: string, mainOwner?: { __typename?: 'User', id: string, firstname?: string }, bookings?: { __typename?: 'MaterialBookingCursorConnection', edges?: Array<{ __typename?: 'MaterialBookingEdge', node?: { __typename?: 'MaterialBooking', status: string, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number }> } }> } };

export type BookingSummary__MaterialBookingFragment = { __typename?: 'MaterialBooking', id: string, price?: number, slug: string, status: string, user?: { __typename?: 'User', fullname: string }, material?: { __typename?: 'Material', name: string, slug: string, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> } }, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number }> };

export type BookingSummaryApplyBookingMutationVariables = Exact<{
  id: Scalars['ID'];
  message?: InputMaybe<Scalars['String']>;
}>;


export type BookingSummaryApplyBookingMutation = { __typename?: 'Mutation', changeStatusMaterialBooking?: { __typename?: 'changeStatusMaterialBookingPayload', materialBooking?: { __typename?: 'MaterialBooking', id: string, price?: number, slug: string, status: string, user?: { __typename?: 'User', fullname: string }, material?: { __typename?: 'Material', name: string, slug: string, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> } }, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number }> } } };

export type ConditionMaterialForm_UserListQueryVariables = Exact<{ [key: string]: never; }>;


export type ConditionMaterialForm_UserListQuery = { __typename?: 'Query', userLists?: { __typename?: 'UserListCursorConnection', edges?: Array<{ __typename?: 'UserListEdge', node?: { __typename?: 'UserList', id: string, slug: string, name: string, users?: { __typename?: 'UserCursorConnection', totalCount: number } } }> } };

export type CreateMaterialImageMutationVariables = Exact<{
  material: Scalars['String'];
  dimensions: Scalars['Iterable'];
  imageName: Scalars['String'];
  size: Scalars['Int'];
  file: Scalars['Upload'];
}>;


export type CreateMaterialImageMutation = { __typename?: 'Mutation', createMaterialImage?: { __typename?: 'createMaterialImagePayload', materialImage?: { __typename?: 'MaterialImage', id: string, contentUrl?: string, material: { __typename?: 'Material', images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', id: string, contentUrl?: string } }> } } } } };

export type DeleteMaterialImageMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type DeleteMaterialImageMutation = { __typename?: 'Mutation', deleteMaterialImage?: { __typename?: 'deleteMaterialImagePayload', clientMutationId?: string } };

export type UpdateMaterialImageMutationVariables = Exact<{
  id: Scalars['ID'];
  dimensions: Scalars['Iterable'];
  imageName: Scalars['String'];
  size: Scalars['Int'];
  file: Scalars['Upload'];
}>;


export type UpdateMaterialImageMutation = { __typename?: 'Mutation', updateMaterialImage?: { __typename?: 'updateMaterialImagePayload', materialImage?: { __typename?: 'MaterialImage', id: string, contentUrl?: string, material: { __typename?: 'Material', images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', id: string, contentUrl?: string } }> } } } } };

export type InformationMaterialForm_CirclesOptionsQueryVariables = Exact<{ [key: string]: never; }>;


export type InformationMaterialForm_CirclesOptionsQuery = { __typename?: 'Query', myCircles?: { __typename?: 'CircleCursorConnection', edges?: Array<{ __typename?: 'CircleEdge', node?: { __typename?: 'Circle', id: string, name: string, subscription?: { __typename?: 'Subscription', id: string, active: boolean, plan?: { __typename?: 'Plan', id: string, slug: string, name?: string, features?: Array<{ __typename?: 'Feature', name?: string, description?: string }> } } } }> } };

export type InformationMaterialForm_CirclesOptionFragment = { __typename?: 'Circle', id: string, name: string, subscription?: { __typename?: 'Subscription', id: string, active: boolean, plan?: { __typename?: 'Plan', id: string, slug: string, name?: string, features?: Array<{ __typename?: 'Feature', name?: string, description?: string }> } } };

export type CreateMaterialMutationVariables = Exact<{
  input: CreateMaterialInput;
}>;


export type CreateMaterialMutation = { __typename?: 'Mutation', createMaterial?: { __typename?: 'createMaterialPayload', material?: { __typename?: 'createMaterialPayloadData', id: string, name: string, slug: string, brand?: string, model?: string } } };

export type InformationMaterialForm__MaterialFragment = { __typename?: 'Material', id: string, slug: string, name: string, brand?: string, model?: string, reference?: string, description?: string, category?: { __typename?: 'MaterialCategory', id: string } };

export type ImagesMaterialForm__MaterialFragment = { __typename?: 'Material', id: string, slug: string, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', id: string, contentUrl?: string } }> } };

export type MaterialConditionForm__MaterialFragment = { __typename?: 'Material', price: number, pricings?: { __typename?: 'MaterialPricingCursorConnection', edges?: Array<{ __typename?: 'MaterialPricingEdge', node?: { __typename?: 'MaterialPricing', id: string, value?: number, circle?: { __typename?: 'Circle', id: string, name: string } } }> }, mainOwnership?: { __typename?: 'AbstractMaterialOwnership', id: string, ownerLabel: string, ownerIRI: string }, ownerships?: Array<{ __typename?: 'AbstractMaterialOwnership', id: string, ownerIRI: string, ownerLabel: string }>, mainOwner?: { __typename?: 'User', settings?: { __typename?: 'Settings', currencySymbol: string }, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, circle: { __typename?: 'Circle', id: string, name: string, subscription?: { __typename?: 'Subscription', id: string, active: boolean, plan?: { __typename?: 'Plan', id: string, slug: string, name?: string, features?: Array<{ __typename?: 'Feature', name?: string, description?: string }> } } } } }> } }, userLists?: { __typename?: 'UserListCursorConnection', edges?: Array<{ __typename?: 'UserListEdge', node?: { __typename?: 'UserList', id: string, name: string } }> } };

export type MaterialConditionForm_CirclePriceItem__UserFragment = { __typename?: 'User', settings?: { __typename?: 'Settings', currencySymbol: string }, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, circle: { __typename?: 'Circle', id: string, name: string, subscription?: { __typename?: 'Subscription', id: string, active: boolean, plan?: { __typename?: 'Plan', id: string, slug: string, name?: string, features?: Array<{ __typename?: 'Feature', name?: string, description?: string }> } } } } }> } };

export type MaterialConditionForm_CirclePriceItem__CircleFragment = { __typename?: 'Circle', id: string, name: string, children?: { __typename?: 'CircleCursorConnection', edges?: Array<{ __typename?: 'CircleEdge', node?: { __typename?: 'Circle', id: string, name: string, children?: { __typename?: 'CircleCursorConnection', edges?: Array<{ __typename?: 'CircleEdge', node?: { __typename?: 'Circle', id: string, name: string } }> } } }> } };

export type MaterialConditionForm_CirclePriceItem__PriceFragment = { __typename?: 'Material', pricings?: { __typename?: 'MaterialPricingCursorConnection', edges?: Array<{ __typename?: 'MaterialPricingEdge', node?: { __typename?: 'MaterialPricing', id: string, value?: number, circle?: { __typename?: 'Circle', id: string, name: string, children?: { __typename?: 'CircleCursorConnection', edges?: Array<{ __typename?: 'CircleEdge', node?: { __typename?: 'Circle', id: string, name: string, children?: { __typename?: 'CircleCursorConnection', edges?: Array<{ __typename?: 'CircleEdge', node?: { __typename?: 'Circle', id: string, name: string } }> } } }> } } } }> } };

export type BookButton_MaterialFragment = { __typename?: 'Material', id: string, mainOwnership?: { __typename?: 'AbstractMaterialOwnership', ownerLabel: string } };

export type ForkMaterialMutationVariables = Exact<{
  input: ForkMaterialInput;
}>;


export type ForkMaterialMutation = { __typename?: 'Mutation', forkMaterial?: { __typename?: 'forkMaterialPayload', material?: { __typename?: 'Material', id: string, slug: string } } };

export type CreateAlertEmptySearchMutationVariables = Exact<{
  name: Scalars['String'];
}>;


export type CreateAlertEmptySearchMutation = { __typename?: 'Mutation', createMaterialSearchAlert?: { __typename?: 'createMaterialSearchAlertPayload', materialSearchAlert?: { __typename?: 'MaterialSearchAlert', _id?: string, id: string, name: string } } };

export type SearchAlertFragment = { __typename?: 'MaterialSearchAlert', _id?: string, id: string, name: string };

export type AlertEmptySearchsQueryVariables = Exact<{
  name: Scalars['String'];
}>;


export type AlertEmptySearchsQuery = { __typename?: 'Query', materialSearchAlerts?: { __typename?: 'MaterialSearchAlertCursorConnection', edges?: Array<{ __typename?: 'MaterialSearchAlertEdge', node?: { __typename?: 'MaterialSearchAlert', _id?: string, id: string, name: string } }> } };

export type DeleteMessageMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type DeleteMessageMutation = { __typename?: 'Mutation', deleteMessage?: { __typename?: 'deleteMessagePayload', clientMutationId?: string } };

export type ContactUserModal__UserFragment = { __typename?: 'User', firstname?: string, fullname: string, phoneNumber?: any, gender?: string, email: string, avatar?: { __typename?: 'Avatar', contentUrl?: string }, settings?: { __typename?: 'Settings', preferedCommunicationMode: CommunicationMode } };

export type ConfirmModal__UserFragment = { __typename?: 'User', firstname?: string, phoneNumber?: any, gender?: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } };

export type SubscribeNewsletterMutationVariables = Exact<{
  input: SubscribeNewsletterContactInput;
}>;


export type SubscribeNewsletterMutation = { __typename?: 'Mutation', subscribeNewsletterContact?: { __typename?: 'subscribeNewsletterContactPayload', contact?: { __typename?: 'Contact', id: string } } };

export type CircleMembershipsTable__CircleMembershipFragment = { __typename?: 'CircleMembership', id: string, status: string, statusTransitionAvailables?: any, user: { __typename?: 'User', gender?: string }, circle: { __typename?: 'Circle', id: string, name: string, slug: string, activeUserActions: any, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } };

export type CircleMembershipsTable__ChangeStatusMutationVariables = Exact<{
  id: Scalars['ID'];
  transition: Scalars['String'];
}>;


export type CircleMembershipsTable__ChangeStatusMutation = { __typename?: 'Mutation', changeStatusCircleMembership?: { __typename?: 'changeStatusCircleMembershipPayload', circleMembership?: { __typename?: 'CircleMembership', id: string, status: string } } };

export type BookingTable__MaterialBookingFragment = { __typename?: 'MaterialBooking', id: string, price?: number, slug: string, status: string, user?: { __typename?: 'User', slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, material?: { __typename?: 'Material', name: string, slug: string, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> }, mainOwnership?: { __typename?: 'AbstractMaterialOwnership', ownerIRI: string } }, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number }> };

export type BookingTable__DatePeriodFragment = { __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number };

export type BookingTable__MaterialBookingCursorConnectionFragment = { __typename?: 'MaterialBookingCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'MaterialBookingEdge', node?: { __typename?: 'MaterialBooking', id: string, price?: number, slug: string, status: string, user?: { __typename?: 'User', slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, material?: { __typename?: 'Material', name: string, slug: string, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> }, mainOwnership?: { __typename?: 'AbstractMaterialOwnership', ownerIRI: string } }, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number }> } }> };

export type MaterialTable_MaterialFragment = { __typename?: 'Material', id: string, name: string, slug: string, status: string, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', id: string, imageName: string, size: number, contentUrl?: string } }> } };

export type PauseMaterialMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type PauseMaterialMutation = { __typename?: 'Mutation', changeStatusMaterial?: { __typename?: 'changeStatusMaterialPayload', material?: { __typename?: 'Material', id: string, status: string } } };

export type PublishMaterialMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type PublishMaterialMutation = { __typename?: 'Mutation', changeStatusMaterial?: { __typename?: 'changeStatusMaterialPayload', material?: { __typename?: 'Material', id: string, status: string } } };

export type UserListTable_UserListFragment = { __typename?: 'UserList', id: string, name: string, slug: string, users?: { __typename?: 'UserCursorConnection', totalCount: number, pageInfo: { __typename?: 'UserPageInfo', endCursor?: string, startCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean }, edges?: Array<{ __typename?: 'UserEdge', cursor: string, node?: { __typename?: 'User', id: string, slug: string, fullname: string, roles: any, avatar?: { __typename?: 'Avatar', contentUrl?: string } } }> } };

export type UserList_UsersConnexFragment = { __typename?: 'UserCursorConnection', totalCount: number, pageInfo: { __typename?: 'UserPageInfo', endCursor?: string, startCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean }, edges?: Array<{ __typename?: 'UserEdge', cursor: string, node?: { __typename?: 'User', id: string, slug: string, fullname: string, roles: any, avatar?: { __typename?: 'Avatar', contentUrl?: string } } }> };

export type UserList_UserFragment = { __typename?: 'User', id: string, slug: string, fullname: string, roles: any, avatar?: { __typename?: 'Avatar', contentUrl?: string } };

export type CircleLogoFragment = { __typename?: 'Circle', name: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } };

export type ProfileUserCoverFragment = { __typename?: 'User', gender?: string, averageRatingScore?: number, city?: string, country?: string, slug: string, firstname?: string, lastname: string, fullname: string, birthDate?: string, roles: any, bio?: string, phoneNumber?: any, email: string, settings?: { __typename?: 'Settings', preferedCommunicationMode: CommunicationMode }, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, circle: { __typename?: 'Circle', name: string, slug: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number }, userOwnerships?: { __typename?: 'UserMaterialOwnershipCursorConnection', totalCount: number }, avatar?: { __typename?: 'Avatar', contentUrl?: string } };

export type CircleList__CircleFragment = { __typename?: 'Circle', id: string, name: string, createdAt?: string, updatedAt?: string, website?: string, explicitLocationLabel?: string, indexable: boolean, logo?: { __typename?: 'CircleLogo', contentUrl?: string } };

export type CircleList_CircleCursorConnectionFragment = { __typename?: 'CircleCursorConnection', totalCount: number, pageInfo: { __typename?: 'CirclePageInfo', endCursor?: string, startCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean } };

export type CirclesQueryVariables = Exact<{
  name?: InputMaybe<Scalars['String']>;
  indexable?: InputMaybe<Scalars['Boolean']>;
  afterCursor?: InputMaybe<Scalars['String']>;
  beforeCursor?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
}>;


export type CirclesQuery = { __typename?: 'Query', circles?: { __typename?: 'CircleCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleEdge', cursor: string, node?: { __typename?: 'Circle', id: string, name: string, createdAt?: string, updatedAt?: string, website?: string, explicitLocationLabel?: string, indexable: boolean, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } }>, pageInfo: { __typename?: 'CirclePageInfo', endCursor?: string, startCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean } } };

export type InvitationList__InvitationFragment = { __typename?: 'Invitation', id: string, email: string, postalCode?: string, invited: boolean, createdAt?: string, updatedAt?: string, user?: { __typename?: 'User', slug: string, firstname?: string, lastname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } } };

export type InvitationList_InvitationCursorConnectionFragment = { __typename?: 'InvitationCursorConnection', totalCount: number, pageInfo: { __typename?: 'InvitationPageInfo', endCursor?: string, startCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean } };

export type InvitationsQueryVariables = Exact<{
  invited?: InputMaybe<Scalars['Boolean']>;
  postalCode?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  afterCursor?: InputMaybe<Scalars['String']>;
  beforeCursor?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
}>;


export type InvitationsQuery = { __typename?: 'Query', invitations?: { __typename?: 'InvitationCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'InvitationEdge', cursor: string, node?: { __typename?: 'Invitation', id: string, email: string, postalCode?: string, invited: boolean, createdAt?: string, updatedAt?: string, user?: { __typename?: 'User', slug: string, firstname?: string, lastname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } } } }>, pageInfo: { __typename?: 'InvitationPageInfo', endCursor?: string, startCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean } } };

export type DeleteInvitationMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type DeleteInvitationMutation = { __typename?: 'Mutation', deleteInvitation?: { __typename?: 'deleteInvitationPayload', invitation?: { __typename?: 'Invitation', id: string } } };

export type SendInvitationMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type SendInvitationMutation = { __typename?: 'Mutation', sendInvitation?: { __typename?: 'sendInvitationPayload', invitation?: { __typename?: 'Invitation', id: string, invited: boolean } } };

export type UserList__UserFragment = { __typename?: 'User', id: string, firstname?: string, lastname: string, email: string, gender?: string, postalCode?: string, city?: string, phoneNumber?: any, confirmed: boolean, createdAt?: string, updatedAt?: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } };

export type UserList_UserCursorConnectionFragment = { __typename?: 'UserCursorConnection', totalCount: number, pageInfo: { __typename?: 'UserPageInfo', endCursor?: string, startCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean } };

export type UsersQueryVariables = Exact<{
  lastname?: InputMaybe<Scalars['String']>;
  firstname?: InputMaybe<Scalars['String']>;
  confirmed?: InputMaybe<Scalars['Boolean']>;
  postalCode?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['String']>;
  afterCursor?: InputMaybe<Scalars['String']>;
  beforeCursor?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
}>;


export type UsersQuery = { __typename?: 'Query', users?: { __typename?: 'UserCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'UserEdge', cursor: string, node?: { __typename?: 'User', id: string, firstname?: string, lastname: string, email: string, gender?: string, postalCode?: string, city?: string, phoneNumber?: any, confirmed: boolean, createdAt?: string, updatedAt?: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } } }>, pageInfo: { __typename?: 'UserPageInfo', endCursor?: string, startCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean } } };

export type StatisticsQueryVariables = Exact<{ [key: string]: never; }>;


export type StatisticsQuery = { __typename?: 'Query', users?: { __typename?: 'UserCursorConnection', totalCount: number }, materials?: { __typename?: 'MaterialCursorConnection', totalCount: number }, materialBookings?: { __typename?: 'MaterialBookingCursorConnection', totalCount: number }, circles?: { __typename?: 'CircleCursorConnection', totalCount: number }, invitations?: { __typename?: 'InvitationCursorConnection', totalCount: number } };

export type PricingView_Plans__FeaturesFragment = { __typename?: 'Feature', id: string, name?: string, description?: string, new: boolean, workInProgress: boolean };

export type PricingView_PlansQueryVariables = Exact<{ [key: string]: never; }>;


export type PricingView_PlansQuery = { __typename?: 'Query', plans?: Array<{ __typename?: 'Plan', id: string, name?: string, slug: string, description?: string, price: number, condition?: string, button?: string, features?: Array<{ __typename?: 'Feature', id: string, name?: string, description?: string, new: boolean, workInProgress: boolean }> }> };

export type UserPreferences__UserFragment = { __typename?: 'User', id: string, gender?: string, settings?: { __typename?: 'Settings', id: string, useAbbreviatedName: boolean, preferedCommunicationMode: CommunicationMode, themeMode: ThemeMode } };

export type UserPreferencesQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type UserPreferencesQuery = { __typename?: 'Query', user?: { __typename?: 'User', id: string, gender?: string, settings?: { __typename?: 'Settings', id: string, useAbbreviatedName: boolean, preferedCommunicationMode: CommunicationMode, themeMode: ThemeMode } } };

export type UpdateUserMutationVariables = Exact<{
  input: UpdateUserInput;
}>;


export type UpdateUserMutation = { __typename?: 'Mutation', updateUser?: { __typename?: 'updateUserPayload', user?: { __typename?: 'User', id: string, slug: string, firstname?: string, lastname: string, fullname: string, email: string, phoneNumber?: any, streetAddress: string, city?: string, country?: string, roles: any, gender?: string, token?: string, refreshToken?: string, tokenExpiresAt?: number, averageRatingScore?: number, latitude?: number, longitude?: number, confirmed: boolean, createdAt?: string, settings?: { __typename?: 'Settings', language: string, currencySymbol: string, id: string, themeMode: ThemeMode, useAbbreviatedName: boolean, preferedCommunicationMode: CommunicationMode }, avatar?: { __typename?: 'Avatar', id: string, contentUrl?: string }, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', permission: CirclePermissionType, circle: { __typename?: 'Circle', slug: string, id: string, name: string, description?: string, city?: string, logo?: { __typename?: 'CircleLogo', imageName: string, size: number }, subscription?: { __typename?: 'Subscription', active: boolean, id: string, plan?: { __typename?: 'Plan', slug: string, name?: string } } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number } } } };

export type UserProfile__UserFragment = { __typename?: 'User', id: string, slug: string, firstname?: string, lastname: string, fullname: string, email: string, phoneNumber?: any, latitude?: number, longitude?: number, streetAddress: string, city?: string, country?: string, roles: any, gender?: string, token?: string, refreshToken?: string, tokenExpiresAt?: number, averageRatingScore?: number, settings?: { __typename?: 'Settings', language: string, currencySymbol: string }, avatar?: { __typename?: 'Avatar', id: string, contentUrl?: string }, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', permission: CirclePermissionType, circle: { __typename?: 'Circle', slug: string, id: string, name: string, description?: string, city?: string, logo?: { __typename?: 'CircleLogo', imageName: string, size: number } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number } };

export type UserProfileQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type UserProfileQuery = { __typename?: 'Query', user?: { __typename?: 'User', id: string, slug: string, firstname?: string, lastname: string, fullname: string, email: string, phoneNumber?: any, latitude?: number, longitude?: number, streetAddress: string, city?: string, country?: string, roles: any, gender?: string, token?: string, refreshToken?: string, tokenExpiresAt?: number, averageRatingScore?: number, settings?: { __typename?: 'Settings', language: string, currencySymbol: string }, avatar?: { __typename?: 'Avatar', id: string, contentUrl?: string }, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', permission: CirclePermissionType, circle: { __typename?: 'Circle', slug: string, id: string, name: string, description?: string, city?: string, logo?: { __typename?: 'CircleLogo', imageName: string, size: number } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number } } };

export type Register__InvitationFragment = { __typename?: 'Invitation', id: string, email: string, postalCode?: string, invited: boolean };

export type InvitationQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type InvitationQuery = { __typename?: 'Query', invitation?: { __typename?: 'Invitation', id: string, email: string, postalCode?: string, invited: boolean } };

export type RegisterWizardHomeViewMutationVariables = Exact<{
  input: CreateUserInput;
}>;


export type RegisterWizardHomeViewMutation = { __typename?: 'Mutation', createUser?: { __typename?: 'createUserPayload', user?: { __typename?: 'User', id: string, firstname?: string, lastname: string, confirmed: boolean, confirmationToken?: string, slug: string, fullname: string, email: string, phoneNumber?: any, streetAddress: string, city?: string, country?: string, roles: any, gender?: string, token?: string, refreshToken?: string, tokenExpiresAt?: number, averageRatingScore?: number, latitude?: number, longitude?: number, createdAt?: string, settings?: { __typename?: 'Settings', language: string, currencySymbol: string, id: string, themeMode: ThemeMode, useAbbreviatedName: boolean, preferedCommunicationMode: CommunicationMode }, avatar?: { __typename?: 'Avatar', id: string, contentUrl?: string }, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', permission: CirclePermissionType, circle: { __typename?: 'Circle', slug: string, id: string, name: string, description?: string, city?: string, logo?: { __typename?: 'CircleLogo', imageName: string, size: number }, subscription?: { __typename?: 'Subscription', active: boolean, id: string, plan?: { __typename?: 'Plan', slug: string, name?: string } } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number } } } };

export type RegisterWizardHomeView_FindUserByEmailQueryVariables = Exact<{
  email?: InputMaybe<Scalars['String']>;
}>;


export type RegisterWizardHomeView_FindUserByEmailQuery = { __typename?: 'Query', users?: { __typename?: 'UserCursorConnection', totalCount: number } };

export type RegisterWizardHomeView_InvitationQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type RegisterWizardHomeView_InvitationQuery = { __typename?: 'Query', invitation?: { __typename?: 'Invitation', id: string, email: string, firstname?: string, lastname?: string, circle?: { __typename?: 'Circle', slug: string } } };

export type CirclesToJoinForEmailQueryVariables = Exact<{
  email?: InputMaybe<Scalars['String']>;
}>;


export type CirclesToJoinForEmailQuery = { __typename?: 'Query', circles?: { __typename?: 'CircleCursorConnection', edges?: Array<{ __typename?: 'CircleEdge', node?: { __typename?: 'Circle', id: string, name: string, slug: string, invitations?: { __typename?: 'InvitationCursorConnection', edges?: Array<{ __typename?: 'InvitationEdge', node?: { __typename?: 'Invitation', id: string } }> } } }> } };

export type HandleCommunityInvitationMutationVariables = Exact<{
  id: Scalars['ID'];
  decision: Scalars['String'];
}>;


export type HandleCommunityInvitationMutation = { __typename?: 'Mutation', handleInvitation?: { __typename?: 'handleInvitationPayload', invitation?: { __typename?: 'Invitation', email: string, circle?: { __typename?: 'Circle', id: string, name: string, slug: string } } } };

export type CirclesMapView_CircleFragment = { __typename?: 'Circle', id: string, slug: string, name: string, description?: string, createdAt?: string, updatedAt?: string, website?: string, explicitLocationLabel?: string, latitude?: string, longitude?: string, location?: string, distance?: number, indexable: boolean, logo?: { __typename?: 'CircleLogo', contentUrl?: string }, cover?: { __typename?: 'CircleCover', contentUrl?: string }, parent?: { __typename?: 'Circle', id: string, name: string, slug: string } };

export type CirclesMapViewQueryVariables = Exact<{
  afterCursor?: InputMaybe<Scalars['String']>;
  beforeCursor?: InputMaybe<Scalars['String']>;
  nbToFetch?: InputMaybe<Scalars['Int']>;
  searchTerm?: InputMaybe<Scalars['String']>;
}>;


export type CirclesMapViewQuery = { __typename?: 'Query', circles?: { __typename?: 'CircleCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleEdge', node?: { __typename?: 'Circle', id: string, slug: string, name: string, description?: string, createdAt?: string, updatedAt?: string, website?: string, explicitLocationLabel?: string, latitude?: string, longitude?: string, location?: string, distance?: number, indexable: boolean, logo?: { __typename?: 'CircleLogo', contentUrl?: string }, cover?: { __typename?: 'CircleCover', contentUrl?: string }, parent?: { __typename?: 'Circle', id: string, name: string, slug: string } } }> } };

export type CircleShowView_CircleQueryVariables = Exact<{
  id: Scalars['ID'];
  statuses?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
}>;


export type CircleShowView_CircleQuery = { __typename?: 'Query', circle?: { __typename?: 'Circle', id: string, name: string, activeUserActions: any, description?: string, website?: string, slug: string, indexable: boolean, address?: string, postalCode?: string, city?: string, explicitLocationLabel?: string, latitude?: string, country?: string, longitude?: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string }, cover?: { __typename?: 'CircleCover', contentUrl?: string }, subscription?: { __typename?: 'Subscription', id: string, status: SubscriptionStatusType, active: boolean, startDate: string, createdAt?: string, updatedAt?: string, plan?: { __typename?: 'Plan', id: string, name?: string } }, memberships?: { __typename?: 'CircleMembershipCursorConnection', pageInfo: { __typename?: 'CircleMembershipPageInfo', startCursor?: string, endCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean }, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', permission: CirclePermissionType, status: string, user: { __typename?: 'User', id: string, firstname?: string, email: string, phoneNumber?: any, fullname: string, slug: string, averageRatingScore?: number, roles: any, avatar?: { __typename?: 'Avatar', contentUrl?: string }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number }, userOwnerships?: { __typename?: 'UserMaterialOwnershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'UserMaterialOwnershipEdge', node?: { __typename?: 'UserMaterialOwnership', material?: { __typename?: 'Material', id: string, slug: string, name: string }, materialMainOwned?: { __typename?: 'Material', id: string, slug: string, name: string } } }> } } } }> }, invitationLinks?: Array<{ __typename?: 'CircleInvitationLink', _id?: string, enabled: boolean, numberOfUses: number }> } };

export type JoinCommunityByInvitationLinkMutationVariables = Exact<{
  invitationLinkId: Scalars['String'];
}>;


export type JoinCommunityByInvitationLinkMutation = { __typename?: 'Mutation', joinCircleMembership?: { __typename?: 'joinCircleMembershipPayload', circleMembership?: { __typename?: 'CircleMembership', id: string, status: string, circle: { __typename?: 'Circle', id: string, slug: string } } } };

export type JoinCommunityInvitationQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type JoinCommunityInvitationQuery = { __typename?: 'Query', circleInvitationLink?: { __typename?: 'CircleInvitationLink', id: string, createdAt?: string, circle: { __typename?: 'Circle', id: string, name: string, description?: string, city?: string, country?: string, slug: string, cover?: { __typename?: 'CircleCover', contentUrl?: string }, logo?: { __typename?: 'CircleLogo', imageName: string } } } };

export type JoinCommunityInvitation__CircleFragment = { __typename?: 'Circle', id: string, name: string, description?: string, city?: string, country?: string, slug: string, cover?: { __typename?: 'CircleCover', contentUrl?: string }, logo?: { __typename?: 'CircleLogo', imageName: string } };

export type UpdateCircleMutationVariables = Exact<{
  input: UpdateCircleInput;
}>;


export type UpdateCircleMutation = { __typename?: 'Mutation', updateCircle?: { __typename?: 'updateCirclePayload', circle?: { __typename?: 'Circle', id: string, name: string, activeUserActions: any, description?: string, website?: string, slug: string, address?: string, postalCode?: string, city?: string, country?: string, latitude?: string, longitude?: string, indexable: boolean, logo?: { __typename?: 'CircleLogo', id: string, contentUrl?: string }, cover?: { __typename?: 'CircleCover', id: string, contentUrl?: string } } } };

export type DeleteCircleMutationVariables = Exact<{
  input: DeleteCircleInput;
}>;


export type DeleteCircleMutation = { __typename?: 'Mutation', deleteCircle?: { __typename?: 'deleteCirclePayload', clientMutationId?: string, circle?: { __typename?: 'Circle', id: string } } };

export type GetUserCirclesQueryVariables = Exact<{
  id: Scalars['ID'];
  afterCursor?: InputMaybe<Scalars['String']>;
  beforeCursor?: InputMaybe<Scalars['String']>;
  nbToFetch?: InputMaybe<Scalars['Int']>;
  statusList?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
}>;


export type GetUserCirclesQuery = { __typename?: 'Query', user?: { __typename?: 'User', circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', cursor: string, node?: { __typename?: 'CircleMembership', id: string, status: string, statusTransitionAvailables?: any, circle: { __typename?: 'Circle', id: string, name: string, slug: string, activeUserActions: any, city?: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string }, memberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, user: { __typename?: 'User', id: string, roles: any, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } } } }> }, cover?: { __typename?: 'CircleCover', contentUrl?: string } }, user: { __typename?: 'User', gender?: string } } }>, pageInfo: { __typename?: 'CircleMembershipPageInfo', startCursor?: string, endCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean } } } };

export type HomeLastMaterialsQueryVariables = Exact<{
  first: Scalars['Int'];
}>;


export type HomeLastMaterialsQuery = { __typename?: 'Query', materials?: { __typename?: 'MaterialCursorConnection', edges?: Array<{ __typename?: 'MaterialEdge', node?: { __typename?: 'Material', description?: string, distance?: number, id: string, slug: string, name: string, brand?: string, model?: string, bestPriceForUser?: number, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> }, mainOwnershipUser?: { __typename?: 'User', city?: string, slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, mainOwnershipCircle?: { __typename?: 'Circle', city?: string, slug: string, name: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } }> } };

export type SearchMaterialFragment = { __typename?: 'Material', distance?: number, id: string, slug: string, name: string, brand?: string, model?: string, bestPriceForUser?: number, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> }, mainOwnershipUser?: { __typename?: 'User', city?: string, slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, mainOwnershipCircle?: { __typename?: 'Circle', city?: string, slug: string, name: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } };

export type SearchMaterialsQueryVariables = Exact<{
  searchTerms: Scalars['String'];
  afterCursor?: InputMaybe<Scalars['String']>;
  beforeCursor?: InputMaybe<Scalars['String']>;
  nbToFetch?: InputMaybe<Scalars['Int']>;
}>;


export type SearchMaterialsQuery = { __typename?: 'Query', searchMaterials?: { __typename?: 'MaterialCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'MaterialEdge', cursor: string, node?: { __typename?: 'Material', distance?: number, id: string, slug: string, name: string, brand?: string, model?: string, bestPriceForUser?: number, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> }, mainOwnershipUser?: { __typename?: 'User', city?: string, slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, mainOwnershipCircle?: { __typename?: 'Circle', city?: string, slug: string, name: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } }>, pageInfo: { __typename?: 'MaterialPageInfo', startCursor?: string, endCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean } }, materialSearchAlerts?: { __typename?: 'MaterialSearchAlertCursorConnection', edges?: Array<{ __typename?: 'MaterialSearchAlertEdge', node?: { __typename?: 'MaterialSearchAlert', _id?: string, id: string, name: string } }> } };

export type DeleteMaterialMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type DeleteMaterialMutation = { __typename?: 'Mutation', deleteMaterial?: { __typename?: 'deleteMaterialPayload', clientMutationId?: string } };

export type MaterialForm__MaterialFragment = { __typename?: 'Material', id: string, slug: string, name: string, brand?: string, model?: string, reference?: string, description?: string, price: number, category?: { __typename?: 'MaterialCategory', id: string }, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', id: string, contentUrl?: string } }> }, pricings?: { __typename?: 'MaterialPricingCursorConnection', edges?: Array<{ __typename?: 'MaterialPricingEdge', node?: { __typename?: 'MaterialPricing', id: string, value?: number, circle?: { __typename?: 'Circle', id: string, name: string } } }> }, mainOwnership?: { __typename?: 'AbstractMaterialOwnership', id: string, ownerLabel: string, ownerIRI: string }, ownerships?: Array<{ __typename?: 'AbstractMaterialOwnership', id: string, ownerIRI: string, ownerLabel: string }>, mainOwner?: { __typename?: 'User', settings?: { __typename?: 'Settings', currencySymbol: string }, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, circle: { __typename?: 'Circle', id: string, name: string, subscription?: { __typename?: 'Subscription', id: string, active: boolean, plan?: { __typename?: 'Plan', id: string, slug: string, name?: string, features?: Array<{ __typename?: 'Feature', name?: string, description?: string }> } } } } }> } }, userLists?: { __typename?: 'UserListCursorConnection', edges?: Array<{ __typename?: 'UserListEdge', node?: { __typename?: 'UserList', id: string, name: string } }> } };

export type EditMaterialQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type EditMaterialQuery = { __typename?: 'Query', material?: { __typename?: 'Material', id: string, slug: string, name: string, brand?: string, model?: string, reference?: string, description?: string, price: number, category?: { __typename?: 'MaterialCategory', id: string }, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', id: string, contentUrl?: string } }> }, pricings?: { __typename?: 'MaterialPricingCursorConnection', edges?: Array<{ __typename?: 'MaterialPricingEdge', node?: { __typename?: 'MaterialPricing', id: string, value?: number, circle?: { __typename?: 'Circle', id: string, name: string } } }> }, mainOwnership?: { __typename?: 'AbstractMaterialOwnership', id: string, ownerLabel: string, ownerIRI: string }, ownerships?: Array<{ __typename?: 'AbstractMaterialOwnership', id: string, ownerIRI: string, ownerLabel: string }>, mainOwner?: { __typename?: 'User', settings?: { __typename?: 'Settings', currencySymbol: string }, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, circle: { __typename?: 'Circle', id: string, name: string, subscription?: { __typename?: 'Subscription', id: string, active: boolean, plan?: { __typename?: 'Plan', id: string, slug: string, name?: string, features?: Array<{ __typename?: 'Feature', name?: string, description?: string }> } } } } }> } }, userLists?: { __typename?: 'UserListCursorConnection', edges?: Array<{ __typename?: 'UserListEdge', node?: { __typename?: 'UserList', id: string, name: string } }> } } };

export type UpdateMaterialMutationVariables = Exact<{
  input: UpdateMaterialInput;
}>;


export type UpdateMaterialMutation = { __typename?: 'Mutation', updateMaterial?: { __typename?: 'updateMaterialPayload', material?: { __typename?: 'updateMaterialPayloadData', id: string, slug?: string, pricings?: { __typename?: 'updateMaterialPricingNestedPayloadCursorConnection', edges?: Array<{ __typename?: 'updateMaterialPricingNestedPayloadEdge', node?: { __typename?: 'updateMaterialPricingNestedPayload', id: string, value?: number, circle?: { __typename?: 'updateCircleNestedPayload', slug?: string } } }> } } } };

export type MyAlertsQueryVariables = Exact<{
  afterCursor?: InputMaybe<Scalars['String']>;
  beforeCursor?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
}>;


export type MyAlertsQuery = { __typename?: 'Query', materialSearchAlerts?: { __typename?: 'MaterialSearchAlertCursorConnection', totalCount: number, pageInfo: { __typename?: 'MaterialSearchAlertPageInfo', startCursor?: string, hasNextPage: boolean, endCursor?: string, hasPreviousPage: boolean }, edges?: Array<{ __typename?: 'MaterialSearchAlertEdge', cursor: string, node?: { __typename?: 'MaterialSearchAlert', name: string, id: string, _id?: string } }> } };

export type DeleteAlertMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type DeleteAlertMutation = { __typename?: 'Mutation', deleteMaterialSearchAlert?: { __typename: 'deleteMaterialSearchAlertPayload' } };

export type MaterialBookingsForApplicantQueryVariables = Exact<{
  slug: Scalars['String'];
  statuses?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
}>;


export type MaterialBookingsForApplicantQuery = { __typename?: 'Query', materialBookings?: { __typename?: 'MaterialBookingCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'MaterialBookingEdge', node?: { __typename?: 'MaterialBooking', id: string, price?: number, slug: string, status: string, user?: { __typename?: 'User', slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, material?: { __typename?: 'Material', name: string, slug: string, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> }, mainOwnership?: { __typename?: 'AbstractMaterialOwnership', ownerIRI: string } }, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number }> } }> } };

export type MaterialBookingsForOwnerQueryVariables = Exact<{
  statuses?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
}>;


export type MaterialBookingsForOwnerQuery = { __typename?: 'Query', asOwnerMaterialBookings?: { __typename?: 'MaterialBookingCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'MaterialBookingEdge', node?: { __typename?: 'MaterialBooking', id: string, price?: number, slug: string, status: string, user?: { __typename?: 'User', slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, material?: { __typename?: 'Material', name: string, slug: string, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> }, mainOwnership?: { __typename?: 'AbstractMaterialOwnership', ownerIRI: string } }, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number }> } }> } };

export type MyBookingQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type MyBookingQuery = { __typename?: 'Query', materialBooking?: { __typename?: 'MaterialBooking', id: string, slug: string, startDate: string, endDate: string, status: string, statusTransitionAvailables?: any, price?: number, discussion?: { __typename?: 'Discussion', id: string, messages?: { __typename?: 'MessageCursorConnection', edges?: Array<{ __typename?: 'MessageEdge', node?: { __typename?: 'Message', id: string, content: string, parameters: string, createdAt?: string, updatedAt?: string, author?: { __typename?: 'User', slug: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } } } }> } }, user?: { __typename?: 'User', id: string, slug: string, firstname?: string, lastname: string, fullname: string, email: string, phoneNumber?: any, city?: string, roles: any, gender?: string, averageRatingScore?: number, avatar?: { __typename?: 'Avatar', contentUrl?: string }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number } }, material?: { __typename?: 'Material', id: string, slug: string, name: string, model?: string, brand?: string, reference?: string, createdAt?: string, updatedAt?: string, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', id: string, imageName: string, size: number, contentUrl?: string } }> }, mainOwner?: { __typename?: 'User', id: string, slug: string, firstname?: string, lastname: string, fullname: string, email: string, phoneNumber?: any, streetAddress: string, city?: string, roles: any, gender?: string, averageRatingScore?: number, avatar?: { __typename?: 'Avatar', contentUrl?: string }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number } }, mainOwnershipUser?: { __typename?: 'User', city?: string, slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, mainOwnershipCircle?: { __typename?: 'Circle', city?: string, slug: string, name: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } }, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number }> } };

export type MyListEditViewQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type MyListEditViewQuery = { __typename?: 'Query', userList?: { __typename?: 'UserList', id: string, name: string, users?: { __typename?: 'UserCursorConnection', totalCount: number } } };

export type UpdateUserListMutationVariables = Exact<{
  input: UpdateUserListInput;
}>;


export type UpdateUserListMutation = { __typename?: 'Mutation', updateUserList?: { __typename?: 'updateUserListPayload', userList?: { __typename?: 'UserList', id: string, slug: string, owner?: { __typename?: 'User', id: string }, users?: { __typename?: 'UserCursorConnection', edges?: Array<{ __typename?: 'UserEdge', node?: { __typename?: 'User', slug: string } }> } } } };

export type MyListEdit_RemoveFromListMutationVariables = Exact<{
  input: RemoveUserFromUserListInput;
}>;


export type MyListEdit_RemoveFromListMutation = { __typename?: 'Mutation', removeUserFromUserList?: { __typename?: 'removeUserFromUserListPayload', userList?: { __typename?: 'UserList', id: string, users?: { __typename?: 'UserCursorConnection', edges?: Array<{ __typename?: 'UserEdge', node?: { __typename?: 'User', id: string, slug: string } }> } } } };

export type MyListEdit_AddFromListMutationVariables = Exact<{
  input: AddUserToUserListInput;
}>;


export type MyListEdit_AddFromListMutation = { __typename?: 'Mutation', addUserToUserList?: { __typename?: 'addUserToUserListPayload', userList?: { __typename?: 'UserList', id: string, users?: { __typename?: 'UserCursorConnection', edges?: Array<{ __typename?: 'UserEdge', node?: { __typename?: 'User', id: string, slug: string } }> } } } };

export type MyListsQueryVariables = Exact<{
  afterCursor?: InputMaybe<Scalars['String']>;
  beforeCursor?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
}>;


export type MyListsQuery = { __typename?: 'Query', userLists?: { __typename?: 'UserListCursorConnection', totalCount: number, pageInfo: { __typename?: 'UserListPageInfo', startCursor?: string, endCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean }, edges?: Array<{ __typename?: 'UserListEdge', node?: { __typename?: 'UserList', id: string, name: string, slug: string, users?: { __typename?: 'UserCursorConnection', totalCount: number, pageInfo: { __typename?: 'UserPageInfo', endCursor?: string, startCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean }, edges?: Array<{ __typename?: 'UserEdge', cursor: string, node?: { __typename?: 'User', id: string, slug: string, fullname: string, roles: any, avatar?: { __typename?: 'Avatar', contentUrl?: string } } }> } } }> } };

export type MyList_DeleteListMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type MyList_DeleteListMutation = { __typename?: 'Mutation', deleteUserList?: { __typename?: 'deleteUserListPayload', clientMutationId?: string } };

export type NewUserListModalMutationVariables = Exact<{
  input: CreateUserListInput;
}>;


export type NewUserListModalMutation = { __typename?: 'Mutation', createUserList?: { __typename?: 'createUserListPayload', userList?: { __typename?: 'UserList', id: string, name: string, slug: string } } };

export type BookingSummaryModal__MaterialBookingFragment = { __typename?: 'MaterialBooking', createdAt?: string, price?: number, status: string, slug: string, user?: { __typename?: 'User', averageRatingScore?: number, city?: string, country?: string, slug: string, firstname?: string, lastname: string, fullname: string, birthDate?: string, roles: any, bio?: string, phoneNumber?: any, gender?: string, email: string, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, circle: { __typename?: 'Circle', name: string, slug: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number }, userOwnerships?: { __typename?: 'UserMaterialOwnershipCursorConnection', totalCount: number }, avatar?: { __typename?: 'Avatar', contentUrl?: string }, settings?: { __typename?: 'Settings', preferedCommunicationMode: CommunicationMode } }, material?: { __typename?: 'Material', distance?: number, id: string, slug: string, name: string, brand?: string, model?: string, bestPriceForUser?: number, mainOwner?: { __typename?: 'User', streetAddress: string, roles: any, fullname: string }, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> }, mainOwnershipUser?: { __typename?: 'User', city?: string, slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, mainOwnershipCircle?: { __typename?: 'Circle', city?: string, slug: string, name: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } }, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string }> };

export type UnavailableModal__MaterialBookingFragment = { __typename?: 'MaterialBooking', id: string, createdAt?: string, price?: number, slug: string, comment?: string, user?: { __typename?: 'User', averageRatingScore?: number, city?: string, country?: string, slug: string, firstname?: string, lastname: string, fullname: string, birthDate?: string, roles: any, bio?: string, phoneNumber?: any, gender?: string, email: string, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, circle: { __typename?: 'Circle', name: string, slug: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number }, userOwnerships?: { __typename?: 'UserMaterialOwnershipCursorConnection', totalCount: number }, avatar?: { __typename?: 'Avatar', contentUrl?: string }, settings?: { __typename?: 'Settings', preferedCommunicationMode: CommunicationMode } }, material?: { __typename?: 'Material', distance?: number, id: string, slug: string, name: string, brand?: string, model?: string, bestPriceForUser?: number, mainOwner?: { __typename?: 'User', streetAddress: string, roles: any, fullname: string }, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> }, mainOwnershipUser?: { __typename?: 'User', city?: string, slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, mainOwnershipCircle?: { __typename?: 'Circle', city?: string, slug: string, name: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } }, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string }> };

export type CommentMaterialBookingMutationVariables = Exact<{
  id: Scalars['ID'];
  comment: Scalars['String'];
}>;


export type CommentMaterialBookingMutation = { __typename?: 'Mutation', commentMaterialBooking?: { __typename?: 'commentMaterialBookingPayload', materialBooking?: { __typename?: 'MaterialBooking', id: string, comment?: string } } };

export type MaterialCalendar_DatePeriodFragment = { __typename?: 'MaterialBookingDatePeriod', startDate: string, endDate: string, price?: number, booking: { __typename?: 'MaterialBooking', id: string, comment?: string, user?: { __typename?: 'User', id: string, fullname: string } } };

export type MaterialCalendar__MaterialBookingFragment = { __typename?: 'MaterialBooking', id: string, createdAt?: string, price?: number, status: string, slug: string, comment?: string, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number, booking: { __typename?: 'MaterialBooking', id: string, comment?: string, user?: { __typename?: 'User', id: string, fullname: string } } }>, user?: { __typename?: 'User', averageRatingScore?: number, city?: string, country?: string, slug: string, firstname?: string, lastname: string, fullname: string, birthDate?: string, roles: any, bio?: string, phoneNumber?: any, gender?: string, email: string, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, circle: { __typename?: 'Circle', name: string, slug: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number }, userOwnerships?: { __typename?: 'UserMaterialOwnershipCursorConnection', totalCount: number }, avatar?: { __typename?: 'Avatar', contentUrl?: string }, settings?: { __typename?: 'Settings', preferedCommunicationMode: CommunicationMode } }, material?: { __typename?: 'Material', distance?: number, id: string, slug: string, name: string, brand?: string, model?: string, bestPriceForUser?: number, mainOwner?: { __typename?: 'User', streetAddress: string, roles: any, fullname: string }, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> }, mainOwnershipUser?: { __typename?: 'User', city?: string, slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, mainOwnershipCircle?: { __typename?: 'Circle', city?: string, slug: string, name: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } };

export type MaterialCalendarQueryVariables = Exact<{
  id: Scalars['ID'];
  statuses?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
}>;


export type MaterialCalendarQuery = { __typename?: 'Query', material?: { __typename?: 'Material', id: string, name: string, bookings?: { __typename?: 'MaterialBookingCursorConnection', edges?: Array<{ __typename?: 'MaterialBookingEdge', node?: { __typename?: 'MaterialBooking', id: string, createdAt?: string, price?: number, status: string, slug: string, comment?: string, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number, booking: { __typename?: 'MaterialBooking', id: string, comment?: string, user?: { __typename?: 'User', id: string, fullname: string } } }>, user?: { __typename?: 'User', averageRatingScore?: number, city?: string, country?: string, slug: string, firstname?: string, lastname: string, fullname: string, birthDate?: string, roles: any, bio?: string, phoneNumber?: any, gender?: string, email: string, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, circle: { __typename?: 'Circle', name: string, slug: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number }, userOwnerships?: { __typename?: 'UserMaterialOwnershipCursorConnection', totalCount: number }, avatar?: { __typename?: 'Avatar', contentUrl?: string }, settings?: { __typename?: 'Settings', preferedCommunicationMode: CommunicationMode } }, material?: { __typename?: 'Material', distance?: number, id: string, slug: string, name: string, brand?: string, model?: string, bestPriceForUser?: number, mainOwner?: { __typename?: 'User', streetAddress: string, roles: any, fullname: string }, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> }, mainOwnershipUser?: { __typename?: 'User', city?: string, slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, mainOwnershipCircle?: { __typename?: 'Circle', city?: string, slug: string, name: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } } }> } } };

export type MaterialBookingCalendarQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type MaterialBookingCalendarQuery = { __typename?: 'Query', materialBooking?: { __typename?: 'MaterialBooking', id: string, createdAt?: string, price?: number, status: string, slug: string, comment?: string, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number, booking: { __typename?: 'MaterialBooking', id: string, comment?: string, user?: { __typename?: 'User', id: string, fullname: string } } }>, user?: { __typename?: 'User', averageRatingScore?: number, city?: string, country?: string, slug: string, firstname?: string, lastname: string, fullname: string, birthDate?: string, roles: any, bio?: string, phoneNumber?: any, gender?: string, email: string, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, circle: { __typename?: 'Circle', name: string, slug: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number }, userOwnerships?: { __typename?: 'UserMaterialOwnershipCursorConnection', totalCount: number }, avatar?: { __typename?: 'Avatar', contentUrl?: string }, settings?: { __typename?: 'Settings', preferedCommunicationMode: CommunicationMode } }, material?: { __typename?: 'Material', distance?: number, id: string, slug: string, name: string, brand?: string, model?: string, bestPriceForUser?: number, mainOwner?: { __typename?: 'User', streetAddress: string, roles: any, fullname: string }, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> }, mainOwnershipUser?: { __typename?: 'User', city?: string, slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, mainOwnershipCircle?: { __typename?: 'Circle', city?: string, slug: string, name: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } } };

export type UnavailableMaterialBookingMutationVariables = Exact<{
  materialId?: InputMaybe<Scalars['String']>;
  startDate?: InputMaybe<Scalars['String']>;
  endDate?: InputMaybe<Scalars['String']>;
}>;


export type UnavailableMaterialBookingMutation = { __typename?: 'Mutation', unavailableMaterialBooking?: { __typename?: 'unavailableMaterialBookingPayload', materialBooking?: { __typename?: 'MaterialBooking', id: string, slug: string, startDate: string, endDate: string, status: string, price?: number, user?: { __typename?: 'User', id: string, slug: string, firstname?: string, lastname: string, fullname: string, email: string, roles: any, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number }>, material?: { __typename?: 'Material', id: string, slug: string, name: string, model?: string, brand?: string, reference?: string, createdAt?: string, updatedAt?: string, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', id: string, imageName: string, size: number, contentUrl?: string } }> } } } } };

export type MyMaterialsQueryVariables = Exact<{
  afterCursor?: InputMaybe<Scalars['String']>;
  beforeCursor?: InputMaybe<Scalars['String']>;
  nbToFetch?: InputMaybe<Scalars['Int']>;
  statusesList?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
}>;


export type MyMaterialsQuery = { __typename?: 'Query', myMaterials?: { __typename?: 'MaterialCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'MaterialEdge', cursor: string, node?: { __typename?: 'Material', id: string, name: string, slug: string, status: string, brand?: string, model?: string, bestPriceForUser?: number, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', id: string, imageName: string, size: number, contentUrl?: string } }> } } }>, pageInfo: { __typename?: 'MaterialPageInfo', startCursor?: string, endCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean } } };

export type MaterialContextualButtonFragment = { __typename?: 'Material', id: string, status: string, slug: string };

export type ShowMaterial__MaterialBookingFragment = { __typename?: 'MaterialBooking', id: string, price?: number, slug: string, status: string, user?: { __typename?: 'User', fullname: string }, material?: { __typename?: 'Material', name: string, slug: string, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> } }, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number }> };

export type ShowMaterial__MaterialPricingFragment = { __typename?: 'MaterialPricing', id: string, value?: number, circle?: { __typename?: 'Circle', id: string, slug: string, name: string, logo?: { __typename?: 'CircleLogo', imageName: string, size: number } } };

export type ShowMaterial__ImagesFragment = { __typename?: 'MaterialImageCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'MaterialImageEdge', cursor: string, node?: { __typename?: 'MaterialImage', id: string, imageName: string, size: number, contentUrl?: string } }>, pageInfo: { __typename?: 'MaterialImagePageInfo', startCursor?: string, endCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean } };

export type ShowMaterialQueryVariables = Exact<{
  id: Scalars['ID'];
  statuses?: InputMaybe<Array<InputMaybe<Scalars['String']>> | InputMaybe<Scalars['String']>>;
}>;


export type ShowMaterialQuery = { __typename?: 'Query', material?: { __typename: 'Material', id: string, name: string, slug: string, description?: string, model?: string, brand?: string, reference?: string, createdAt?: string, updatedAt?: string, userActionAvailables?: any, status: string, bestPriceForUser?: number, mainOwner?: { __typename?: 'User', id: string, averageRatingScore?: number, city?: string, country?: string, slug: string, firstname?: string, lastname: string, fullname: string, birthDate?: string, roles: any, bio?: string, phoneNumber?: any, gender?: string, email: string, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, circle: { __typename?: 'Circle', name: string, slug: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number }, userOwnerships?: { __typename?: 'UserMaterialOwnershipCursorConnection', totalCount: number }, avatar?: { __typename?: 'Avatar', contentUrl?: string }, settings?: { __typename?: 'Settings', preferedCommunicationMode: CommunicationMode } }, mainOwnership?: { __typename?: 'AbstractMaterialOwnership', ownerIRI: string, ownerLabel: string }, ownerships?: Array<{ __typename?: 'AbstractMaterialOwnership', ownerIRI: string }>, category?: { __typename?: 'MaterialCategory', id: string, name: string }, images?: { __typename?: 'MaterialImageCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'MaterialImageEdge', cursor: string, node?: { __typename?: 'MaterialImage', id: string, imageName: string, size: number, contentUrl?: string } }>, pageInfo: { __typename?: 'MaterialImagePageInfo', startCursor?: string, endCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean } }, pricings?: { __typename?: 'MaterialPricingCursorConnection', edges?: Array<{ __typename?: 'MaterialPricingEdge', node?: { __typename?: 'MaterialPricing', id: string, value?: number, circle?: { __typename?: 'Circle', id: string, slug: string, name: string, logo?: { __typename?: 'CircleLogo', imageName: string, size: number } } } }> }, bookings?: { __typename?: 'MaterialBookingCursorConnection', edges?: Array<{ __typename?: 'MaterialBookingEdge', node?: { __typename?: 'MaterialBooking', id: string, price?: number, slug: string, status: string, user?: { __typename?: 'User', fullname: string }, material?: { __typename?: 'Material', name: string, slug: string, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> } }, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number }> } }> } } };

export type ShowMaterial_MainOwnerUserQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ShowMaterial_MainOwnerUserQuery = { __typename?: 'Query', user?: { __typename?: 'User', id: string, slug: string, firstname?: string, lastname: string, city?: string, averageRatingScore?: number, country?: string, fullname: string, birthDate?: string, roles: any, bio?: string, phoneNumber?: any, gender?: string, email: string, avatar?: { __typename?: 'Avatar', contentUrl?: string }, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, circle: { __typename?: 'Circle', name: string, slug: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number }, userOwnerships?: { __typename?: 'UserMaterialOwnershipCursorConnection', totalCount: number }, settings?: { __typename?: 'Settings', preferedCommunicationMode: CommunicationMode } } };

export type ShowMaterial_MainOwnerCircleQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ShowMaterial_MainOwnerCircleQuery = { __typename?: 'Query', circle?: { __typename?: 'Circle', id: string, slug: string, name: string, city?: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string }, memberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, user: { __typename?: 'User', id: string, roles: any, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } } } }> }, cover?: { __typename?: 'CircleCover', contentUrl?: string } } };

export type SubscribeViewQueryVariables = Exact<{
  id: Scalars['ID'];
  first?: InputMaybe<Scalars['Int']>;
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  last?: InputMaybe<Scalars['Int']>;
}>;


export type SubscribeViewQuery = { __typename?: 'Query', plan?: { __typename?: 'Plan', id: string, name?: string, description?: string, price: number, condition?: string, features?: Array<{ __typename?: 'Feature', id: string, name?: string, description?: string }> }, myCircles?: { __typename?: 'CircleCursorConnection', edges?: Array<{ __typename?: 'CircleEdge', cursor: string, node?: { __typename?: 'Circle', id: string, name: string, slug: string, description?: string, city?: string, logo?: { __typename?: 'CircleLogo', imageName: string, size: number, contentUrl?: string }, cover?: { __typename?: 'CircleCover', imageName: string, size: number, contentUrl?: string }, memberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number }, subscription?: { __typename?: 'Subscription', active: boolean, id: string, plan?: { __typename?: 'Plan', id: string, name?: string } } } }>, pageInfo: { __typename?: 'CirclePageInfo', startCursor?: string, endCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean } } };

export type SubscribeView_CreateCircleMutationVariables = Exact<{
  input: CreateCircleInput;
}>;


export type SubscribeView_CreateCircleMutation = { __typename?: 'Mutation', createCircle?: { __typename?: 'createCirclePayload', circle?: { __typename?: 'Circle', id: string, name: string } } };

export type SubscribeView_SubscribeMutationVariables = Exact<{
  input: CreateSubscriptionInput;
}>;


export type SubscribeView_SubscribeMutation = { __typename?: 'Mutation', createSubscription?: { __typename?: 'createSubscriptionPayload', subscription?: { __typename?: 'Subscription', id: string } } };

export type ProfilePoolingHeroFragment = { __typename?: 'User', firstname?: string, lastname: string, gender?: string };

export type ProfileView_MaterialFragment = { __typename?: 'Material', distance?: number, id: string, slug: string, name: string, brand?: string, model?: string, bestPriceForUser?: number, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> }, mainOwnershipUser?: { __typename?: 'User', city?: string, slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, mainOwnershipCircle?: { __typename?: 'Circle', city?: string, slug: string, name: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } };

export type ProfileView_MaterialsQueryVariables = Exact<{
  owner: Scalars['ID'];
  afterCursor?: InputMaybe<Scalars['String']>;
  beforeCursor?: InputMaybe<Scalars['String']>;
  nbToFetch?: InputMaybe<Scalars['Int']>;
}>;


export type ProfileView_MaterialsQuery = { __typename?: 'Query', ownerMaterials?: { __typename?: 'MaterialCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'MaterialEdge', cursor: string, node?: { __typename?: 'Material', distance?: number, id: string, slug: string, name: string, brand?: string, model?: string, bestPriceForUser?: number, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> }, mainOwnershipUser?: { __typename?: 'User', city?: string, slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, mainOwnershipCircle?: { __typename?: 'Circle', city?: string, slug: string, name: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } }>, pageInfo: { __typename?: 'MaterialPageInfo', startCursor?: string, endCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean } } };

export type ProfileView_UserQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ProfileView_UserQuery = { __typename?: 'Query', user?: { __typename?: 'User', gender?: string, averageRatingScore?: number, city?: string, country?: string, slug: string, firstname?: string, lastname: string, fullname: string, birthDate?: string, roles: any, bio?: string, phoneNumber?: any, email: string, settings?: { __typename?: 'Settings', preferedCommunicationMode: CommunicationMode }, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', id: string, permission: CirclePermissionType, circle: { __typename?: 'Circle', name: string, slug: string, logo?: { __typename?: 'CircleLogo', contentUrl?: string } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number }, userOwnerships?: { __typename?: 'UserMaterialOwnershipCursorConnection', totalCount: number }, avatar?: { __typename?: 'Avatar', contentUrl?: string } } };

export type BookingContext__MaterialBookingFragment = { __typename?: 'MaterialBooking', id: string, price?: number, slug: string, status: string, user?: { __typename?: 'User', fullname: string }, material?: { __typename?: 'Material', name: string, slug: string, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', contentUrl?: string } }> } }, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number }> };

export type CreateCircleCoverMutationVariables = Exact<{
  file: Scalars['Upload'];
  circle: Scalars['ID'];
}>;


export type CreateCircleCoverMutation = { __typename?: 'Mutation', createCircleImage?: { __typename?: 'createCircleCoverPayload', circleImage?: { __typename?: 'CircleCover', id: string, contentUrl?: string } } };

export type CreateCircleLogoMutationVariables = Exact<{
  file: Scalars['Upload'];
  circle: Scalars['ID'];
}>;


export type CreateCircleLogoMutation = { __typename?: 'Mutation', createCircleImage?: { __typename?: 'createCircleLogoPayload', circleImage?: { __typename?: 'CircleLogo', id: string, contentUrl?: string } } };

export type DeleteCircleImageMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type DeleteCircleImageMutation = { __typename?: 'Mutation', deleteCircleImage?: { __typename?: 'deleteCircleImagePayload', clientMutationId?: string } };

export type AddMessageDiscussionMutationVariables = Exact<{
  id: Scalars['ID'];
  message?: InputMaybe<Scalars['String']>;
}>;


export type AddMessageDiscussionMutation = { __typename?: 'Mutation', addMessageDiscussion?: { __typename?: 'addMessageDiscussionPayload', discussion?: { __typename?: 'Discussion', id: string, messages?: { __typename?: 'MessageCursorConnection', edges?: Array<{ __typename?: 'MessageEdge', node?: { __typename?: 'Message', id: string, content: string, createdAt?: string, updatedAt?: string, author?: { __typename?: 'User', id: string, slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } } } }> } } } };

export type MaterialBookingQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type MaterialBookingQuery = { __typename?: 'Query', materialBooking?: { __typename?: 'MaterialBooking', id: string, slug: string, startDate: string, endDate: string, status: string, statusTransitionAvailables?: any, price?: number, discussion?: { __typename?: 'Discussion', id: string, messages?: { __typename?: 'MessageCursorConnection', edges?: Array<{ __typename?: 'MessageEdge', node?: { __typename?: 'Message', id: string, content: string, createdAt?: string, author?: { __typename?: 'User', id: string, slug: string, firstname?: string, lastname: string, fullname: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } } } }> } }, user?: { __typename?: 'User', id: string, slug: string, firstname?: string, lastname: string, fullname: string, email: string, phoneNumber?: any, city?: string, roles: any, gender?: string, averageRatingScore?: number, avatar?: { __typename?: 'Avatar', contentUrl?: string }, userOwnerships?: { __typename?: 'UserMaterialOwnershipCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'UserMaterialOwnershipEdge', node?: { __typename?: 'UserMaterialOwnership', material?: { __typename?: 'Material', id: string, slug: string, name: string }, materialMainOwned?: { __typename?: 'Material', id: string, slug: string, name: string } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number } }, material?: { __typename?: 'Material', id: string, slug: string, name: string, model?: string, brand?: string, reference?: string, createdAt?: string, updatedAt?: string, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', id: string, imageName: string, size: number, contentUrl?: string } }> }, mainOwnershipUser?: { __typename?: 'User', id: string, slug: string, firstname?: string, lastname: string, fullname: string, email: string, phoneNumber?: any, streetAddress: string, city?: string, roles: any, gender?: string, averageRatingScore?: number, avatar?: { __typename?: 'Avatar', contentUrl?: string }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number } } }, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', id: string, startDate: string, endDate: string, price?: number }> } };

export type ChangeDateMutationVariables = Exact<{
  id: Scalars['ID'];
  startDate?: InputMaybe<Scalars['String']>;
  endDate?: InputMaybe<Scalars['String']>;
}>;


export type ChangeDateMutation = { __typename?: 'Mutation', changeDateMaterialBooking?: { __typename?: 'changeDateMaterialBookingPayload', materialBooking?: { __typename?: 'MaterialBooking', id: string, startDate: string, endDate: string, periods?: Array<{ __typename?: 'MaterialBookingDatePeriod', startDate: string, endDate: string }> } } };

export type DeleteMaterialBookingMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type DeleteMaterialBookingMutation = { __typename?: 'Mutation', deleteMaterialBooking?: { __typename?: 'deleteMaterialBookingPayload', clientMutationId?: string } };

export type MaterialsQueryVariables = Exact<{
  searchTerms?: InputMaybe<Scalars['String']>;
  afterCursor?: InputMaybe<Scalars['String']>;
  beforeCursor?: InputMaybe<Scalars['String']>;
  nbToFetch?: InputMaybe<Scalars['Int']>;
}>;


export type MaterialsQuery = { __typename?: 'Query', materials?: { __typename?: 'MaterialCursorConnection', totalCount: number, edges?: Array<{ __typename?: 'MaterialEdge', cursor: string, node?: { __typename?: 'Material', id: string, name: string, slug: string, model?: string, brand?: string, reference?: string, createdAt?: string, updatedAt?: string, bestPriceForUser?: number, mainOwner?: { __typename?: 'User', slug: string, firstname?: string, lastname: string, fullname: string, city?: string, avatar?: { __typename?: 'Avatar', contentUrl?: string } }, images?: { __typename?: 'MaterialImageCursorConnection', edges?: Array<{ __typename?: 'MaterialImageEdge', node?: { __typename?: 'MaterialImage', id: string, imageName: string, size: number, contentUrl?: string } }> } } }>, pageInfo: { __typename?: 'MaterialPageInfo', startCursor?: string, endCursor?: string, hasNextPage: boolean, hasPreviousPage: boolean } } };

export type ChangePasswordUserMutationVariables = Exact<{
  id: Scalars['ID'];
  currentPassword: Scalars['String'];
  newPassword: Scalars['String'];
}>;


export type ChangePasswordUserMutation = { __typename?: 'Mutation', changePasswordUser?: { __typename?: 'changePasswordUserPayload', user?: { __typename?: 'User', id: string, updatedAt?: string } } };

export type DeleteUserMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type DeleteUserMutation = { __typename?: 'Mutation', deleteUser?: { __typename?: 'deleteUserPayload', clientMutationId?: string } };

export type LoginMutationVariables = Exact<{
  username: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginMutation = { __typename?: 'Mutation', loginUser?: { __typename?: 'loginUserPayload', user?: { __typename?: 'User', id: string, slug: string, firstname?: string, lastname: string, fullname: string, email: string, phoneNumber?: any, streetAddress: string, city?: string, country?: string, roles: any, gender?: string, token?: string, refreshToken?: string, tokenExpiresAt?: number, averageRatingScore?: number, latitude?: number, longitude?: number, confirmed: boolean, createdAt?: string, settings?: { __typename?: 'Settings', language: string, currencySymbol: string, id: string, themeMode: ThemeMode, useAbbreviatedName: boolean, preferedCommunicationMode: CommunicationMode }, avatar?: { __typename?: 'Avatar', id: string, contentUrl?: string }, circleMemberships?: { __typename?: 'CircleMembershipCursorConnection', edges?: Array<{ __typename?: 'CircleMembershipEdge', node?: { __typename?: 'CircleMembership', permission: CirclePermissionType, circle: { __typename?: 'Circle', slug: string, id: string, name: string, description?: string, city?: string, logo?: { __typename?: 'CircleLogo', imageName: string, size: number }, subscription?: { __typename?: 'Subscription', active: boolean, id: string, plan?: { __typename?: 'Plan', slug: string, name?: string } } } } }> }, ratings?: { __typename?: 'RatingCursorConnection', totalCount: number } } } };

export type PriceText__UserFragment = { __typename?: 'User', settings?: { __typename?: 'Settings', language: string, currencySymbol: string } };

export const AuthUser__UserFragmentDoc = gql`
    fragment AuthUser__User on User {
  id
  slug
  firstname
  lastname
  fullname
  email
  phoneNumber
  settings {
    language
    currencySymbol
  }
  avatar {
    id
    contentUrl
  }
  streetAddress
  city
  country
  roles
  gender
  token
  refreshToken
  tokenExpiresAt
  circleMemberships {
    edges {
      node {
        permission
        circle {
          slug
          id
          id
          name
          description
          logo {
            imageName
            size
          }
          city
          subscription {
            plan {
              slug
              name
            }
            active
            id
          }
        }
      }
    }
  }
  averageRatingScore
  ratings {
    totalCount
  }
  settings {
    id
    themeMode
    useAbbreviatedName
    preferedCommunicationMode
    currencySymbol
  }
  latitude
  longitude
  confirmed
  createdAt
}
    `;
export const CategorySelect__CategoryFragmentDoc = gql`
    fragment CategorySelect__Category on MaterialCategory {
  id
  name
  slug
}
    `;
export const ChatBubble__MessageFragmentDoc = gql`
    fragment ChatBubble__Message on Message {
  id
  content
  parameters
  author {
    slug
    fullname
    avatar {
      contentUrl
    }
  }
  createdAt
  updatedAt
}
    `;
export const AvatarFan__UserFragmentDoc = gql`
    fragment AvatarFan__User on User {
  id
  firstname
  lastname
  avatar {
    id
    contentUrl
  }
}
    `;
export const ContactUserModal__UserFragmentDoc = gql`
    fragment ContactUserModal__User on User {
  firstname
  fullname
  phoneNumber
  avatar {
    contentUrl
  }
  gender
  email
  settings {
    preferedCommunicationMode
  }
}
    `;
export const UserCard__UserFragmentDoc = gql`
    fragment UserCard__User on User {
  ...ContactUserModal__User
  averageRatingScore
  city
  country
  slug
  firstname
  lastname
  fullname
  birthDate
  roles
  bio
  circleMemberships {
    totalCount
    edges {
      node {
        id
        circle {
          name
          slug
          logo {
            contentUrl
          }
        }
        permission
      }
    }
  }
  ratings {
    totalCount
  }
  userOwnerships {
    totalCount
  }
  avatar {
    contentUrl
  }
}
    ${ContactUserModal__UserFragmentDoc}`;
export const MaterialOwnerCard_UserFragmentDoc = gql`
    fragment MaterialOwnerCard_User on User {
  id
  slug
  firstname
  lastname
  avatar {
    contentUrl
  }
  city
  ...UserCard__User
}
    ${UserCard__UserFragmentDoc}`;
export const CommunityCard_CommunityFragmentDoc = gql`
    fragment CommunityCard_Community on Circle {
  id
  slug
  name
  logo {
    contentUrl
  }
  memberships(first: 3, status_list: ["ok"]) {
    edges {
      node {
        id
        user {
          id
          avatar {
            contentUrl
          }
          roles
          fullname
        }
      }
    }
    totalCount
  }
  cover {
    contentUrl
  }
  name
  city
}
    `;
export const MaterialOwnerCard_CircleFragmentDoc = gql`
    fragment MaterialOwnerCard_Circle on Circle {
  ...CommunityCard_Community
}
    ${CommunityCard_CommunityFragmentDoc}`;
export const ChangeMembershipStatusButtonToastFragmentDoc = gql`
    fragment ChangeMembershipStatusButtonToast on User {
  id
  firstname
  email
}
    `;
export const ChangeMembershipStatusMembershipFragmentDoc = gql`
    fragment ChangeMembershipStatusMembership on CircleMembership {
  id
  status
  user {
    ...ChangeMembershipStatusButtonToast
  }
}
    ${ChangeMembershipStatusButtonToastFragmentDoc}`;
export const CommunityMemberCardFragmentDoc = gql`
    fragment CommunityMemberCard on CircleMembership {
  id
  permission
  permissionTransitionAvailables
  status
  statusTransitionAvailables
  user {
    ...UserCard__User
  }
  ...ChangeMembershipStatusMembership
}
    ${UserCard__UserFragmentDoc}
${ChangeMembershipStatusMembershipFragmentDoc}`;
export const JoinCircleButton__CircleFragmentDoc = gql`
    fragment JoinCircleButton__Circle on Circle {
  id
}
    `;
export const ChangeMembershipPermissionMembershipFragmentDoc = gql`
    fragment ChangeMembershipPermissionMembership on CircleMembership {
  id
  permission
  user {
    firstname
  }
  permissionTransitionAvailables
}
    `;
export const MembershipList__MembershipFragmentDoc = gql`
    fragment MembershipList__Membership on CircleMembership {
  id
  statusTransitionAvailables
  permissionTransitionAvailables
  permission
  ...ChangeMembershipStatusMembership
  user {
    ...UserCard__User
  }
}
    ${ChangeMembershipStatusMembershipFragmentDoc}
${UserCard__UserFragmentDoc}`;
export const InvitationFormDialog_CircleFragmentDoc = gql`
    fragment InvitationFormDialog_Circle on Circle {
  id
  name
  slug
  invitationLinks {
    _id
    enabled
    numberOfUses
  }
}
    `;
export const InvitationFormDialog_ExistingUserFragmentDoc = gql`
    fragment InvitationFormDialog_ExistingUser on User {
  id
  slug
  email
  firstname
  lastname
  avatar {
    contentUrl
  }
  city
  country
  circleMemberships {
    edges {
      node {
        circle {
          id
          name
        }
      }
    }
  }
}
    `;
export const MyProfileLocationForm_UserFragmentDoc = gql`
    fragment MyProfileLocationForm_User on User {
  id
  latitude
  longitude
  streetAddress
  city
  postalCode
  country
}
    `;
export const UserForm__UserAvatarFragmentDoc = gql`
    fragment UserForm__UserAvatar on Avatar {
  id
  contentUrl
}
    `;
export const UserForm__UserFragmentDoc = gql`
    fragment UserForm__User on User {
  firstname
  lastname
  streetAddress
  email
  gender
  city
  country
  settings {
    language
  }
  avatar {
    ...UserForm__UserAvatar
  }
  phoneNumber
}
    ${UserForm__UserAvatarFragmentDoc}`;
export const CreateAvatarReturnFragmentDoc = gql`
    fragment CreateAvatarReturn on Avatar {
  id
  contentUrl
}
    `;
export const UserListAvatars_UserFragmentDoc = gql`
    fragment UserListAvatars_User on UserEdge {
  cursor
  node {
    id
    fullname
    firstname
    lastname
    email
    phoneNumber
    avatar {
      contentUrl
    }
  }
}
    `;
export const CircleInformationForm__CircleFragmentDoc = gql`
    fragment CircleInformationForm__Circle on Circle {
  name
  description
  website
  explicitLocationLabel
  indexable
}
    `;
export const CircleLocationForm__CircleFragmentDoc = gql`
    fragment CircleLocationForm__Circle on Circle {
  latitude
  longitude
  address
  city
  postalCode
  country
}
    `;
export const CircleMediaForm__CircleFragmentDoc = gql`
    fragment CircleMediaForm__Circle on Circle {
  id
  name
  logo {
    id
    contentUrl
  }
  cover {
    id
    contentUrl
  }
}
    `;
export const CircleForm__CircleFragmentDoc = gql`
    fragment CircleForm__Circle on Circle {
  id
  slug
  ...CircleInformationForm__Circle
  ...CircleLocationForm__Circle
  ...CircleMediaForm__Circle
}
    ${CircleInformationForm__CircleFragmentDoc}
${CircleLocationForm__CircleFragmentDoc}
${CircleMediaForm__CircleFragmentDoc}`;
export const AvailabilityPlanning__MaterialFragmentDoc = gql`
    fragment AvailabilityPlanning__Material on Material {
  id
  mainOwner {
    id
    firstname
  }
  bookings {
    edges {
      node {
        status
        periods {
          id
          startDate
          endDate
          price
        }
      }
    }
  }
}
    `;
export const BookingContext__MaterialBookingFragmentDoc = gql`
    fragment BookingContext__MaterialBooking on MaterialBooking {
  id
  price
  slug
  status
  user {
    fullname
  }
  material {
    name
    slug
    images {
      edges {
        node {
          contentUrl
        }
      }
    }
  }
  periods {
    id
    startDate
    endDate
    price
  }
}
    `;
export const BookingSummary__MaterialBookingFragmentDoc = gql`
    fragment BookingSummary__MaterialBooking on MaterialBooking {
  ...BookingContext__MaterialBooking
}
    ${BookingContext__MaterialBookingFragmentDoc}`;
export const InformationMaterialForm_CirclesOptionFragmentDoc = gql`
    fragment InformationMaterialForm_circlesOption on Circle {
  id
  name
  subscription {
    id
    plan {
      id
      slug
      name
      features {
        name
        description
      }
    }
    active
  }
}
    `;
export const MaterialConditionForm_CirclePriceItem__CircleFragmentDoc = gql`
    fragment MaterialConditionForm_CirclePriceItem__Circle on Circle {
  id
  name
  children {
    edges {
      node {
        id
        name
        children {
          edges {
            node {
              id
              name
            }
          }
        }
      }
    }
  }
}
    `;
export const MaterialConditionForm_CirclePriceItem__PriceFragmentDoc = gql`
    fragment MaterialConditionForm_CirclePriceItem__Price on Material {
  pricings {
    edges {
      node {
        id
        value
        circle {
          ...MaterialConditionForm_CirclePriceItem__Circle
        }
      }
    }
  }
}
    ${MaterialConditionForm_CirclePriceItem__CircleFragmentDoc}`;
export const BookButton_MaterialFragmentDoc = gql`
    fragment BookButton_Material on Material {
  id
  mainOwnership {
    ownerLabel
  }
}
    `;
export const SearchAlertFragmentDoc = gql`
    fragment SearchAlert on MaterialSearchAlert {
  _id
  id
  name
}
    `;
export const ConfirmModal__UserFragmentDoc = gql`
    fragment ConfirmModal__User on User {
  firstname
  phoneNumber
  avatar {
    contentUrl
  }
  gender
}
    `;
export const CircleMembershipsTable__CircleMembershipFragmentDoc = gql`
    fragment CircleMembershipsTable__CircleMembership on CircleMembership {
  id
  status
  statusTransitionAvailables
  user {
    gender
  }
  circle {
    id
    name
    slug
    activeUserActions
    logo {
      contentUrl
    }
  }
}
    `;
export const BookingTable__DatePeriodFragmentDoc = gql`
    fragment BookingTable__DatePeriod on MaterialBookingDatePeriod {
  id
  startDate
  endDate
  price
}
    `;
export const BookingTable__MaterialBookingFragmentDoc = gql`
    fragment BookingTable__MaterialBooking on MaterialBooking {
  id
  price
  slug
  status
  user {
    slug
    firstname
    lastname
    fullname
    avatar {
      contentUrl
    }
  }
  material {
    name
    slug
    images {
      edges {
        node {
          contentUrl
        }
      }
    }
    mainOwnership {
      ownerIRI
    }
  }
  periods {
    id
    startDate
    endDate
    price
  }
}
    `;
export const BookingTable__MaterialBookingCursorConnectionFragmentDoc = gql`
    fragment BookingTable__MaterialBookingCursorConnection on MaterialBookingCursorConnection {
  edges {
    node {
      ...BookingTable__MaterialBooking
    }
  }
  totalCount
}
    ${BookingTable__MaterialBookingFragmentDoc}`;
export const MaterialTable_MaterialFragmentDoc = gql`
    fragment MaterialTable_Material on Material {
  id
  name
  slug
  status
  images {
    edges {
      node {
        id
        imageName
        size
        contentUrl
      }
    }
  }
}
    `;
export const UserList_UserFragmentDoc = gql`
    fragment UserList_User on User {
  id
  slug
  fullname
  avatar {
    contentUrl
  }
  roles
}
    `;
export const UserList_UsersConnexFragmentDoc = gql`
    fragment UserList_UsersConnex on UserCursorConnection {
  totalCount
  pageInfo {
    endCursor
    startCursor
    hasNextPage
    hasPreviousPage
  }
  edges {
    cursor
    node {
      ...UserList_User
    }
  }
}
    ${UserList_UserFragmentDoc}`;
export const UserListTable_UserListFragmentDoc = gql`
    fragment UserListTable_UserList on UserList {
  id
  name
  slug
  users(first: 10) {
    ...UserList_UsersConnex
  }
}
    ${UserList_UsersConnexFragmentDoc}`;
export const CircleLogoFragmentDoc = gql`
    fragment CircleLogo on Circle {
  logo {
    contentUrl
  }
  name
}
    `;
export const ProfileUserCoverFragmentDoc = gql`
    fragment ProfileUserCover on User {
  ...UserCard__User
  gender
  settings {
    preferedCommunicationMode
  }
}
    ${UserCard__UserFragmentDoc}`;
export const CircleList__CircleFragmentDoc = gql`
    fragment CircleList__Circle on Circle {
  id
  name
  createdAt
  updatedAt
  website
  explicitLocationLabel
  logo {
    contentUrl
  }
  indexable
}
    `;
export const CircleList_CircleCursorConnectionFragmentDoc = gql`
    fragment CircleList_CircleCursorConnection on CircleCursorConnection {
  totalCount
  pageInfo {
    endCursor
    startCursor
    hasNextPage
    hasPreviousPage
  }
}
    `;
export const InvitationList__InvitationFragmentDoc = gql`
    fragment InvitationList__Invitation on Invitation {
  id
  email
  postalCode
  invited
  createdAt
  updatedAt
  user {
    slug
    firstname
    lastname
    avatar {
      contentUrl
    }
  }
}
    `;
export const InvitationList_InvitationCursorConnectionFragmentDoc = gql`
    fragment InvitationList_InvitationCursorConnection on InvitationCursorConnection {
  totalCount
  pageInfo {
    endCursor
    startCursor
    hasNextPage
    hasPreviousPage
  }
}
    `;
export const UserList__UserFragmentDoc = gql`
    fragment UserList__User on User {
  id
  firstname
  lastname
  email
  gender
  postalCode
  city
  phoneNumber
  confirmed
  createdAt
  updatedAt
  confirmed
  avatar {
    contentUrl
  }
}
    `;
export const UserList_UserCursorConnectionFragmentDoc = gql`
    fragment UserList_UserCursorConnection on UserCursorConnection {
  totalCount
  pageInfo {
    endCursor
    startCursor
    hasNextPage
    hasPreviousPage
  }
}
    `;
export const PricingView_Plans__FeaturesFragmentDoc = gql`
    fragment PricingView_Plans__Features on Feature {
  id
  name
  description
  new
  workInProgress
}
    `;
export const UserPrivacyForm__UserFragmentDoc = gql`
    fragment UserPrivacyForm__User on User {
  id
  settings {
    id
    useAbbreviatedName
    preferedCommunicationMode
  }
}
    `;
export const UserAppearenceForm__UserFragmentDoc = gql`
    fragment UserAppearenceForm__User on User {
  id
  settings {
    id
    themeMode
  }
  gender
}
    `;
export const UserPreferences__UserFragmentDoc = gql`
    fragment UserPreferences__User on User {
  ...UserPrivacyForm__User
  ...UserAppearenceForm__User
}
    ${UserPrivacyForm__UserFragmentDoc}
${UserAppearenceForm__UserFragmentDoc}`;
export const UserProfile__UserFragmentDoc = gql`
    fragment UserProfile__User on User {
  id
  slug
  firstname
  lastname
  fullname
  email
  phoneNumber
  latitude
  longitude
  settings {
    language
    currencySymbol
  }
  avatar {
    id
    contentUrl
  }
  streetAddress
  city
  country
  roles
  gender
  token
  refreshToken
  tokenExpiresAt
  circleMemberships {
    edges {
      node {
        permission
        circle {
          slug
          id
          id
          name
          description
          logo {
            imageName
            size
          }
          city
        }
      }
    }
  }
  averageRatingScore
  ratings {
    totalCount
  }
}
    `;
export const Register__InvitationFragmentDoc = gql`
    fragment Register__Invitation on Invitation {
  id
  email
  postalCode
  invited
}
    `;
export const CirclesMapView_CircleFragmentDoc = gql`
    fragment CirclesMapView_Circle on Circle {
  id
  slug
  name
  description
  createdAt
  updatedAt
  website
  explicitLocationLabel
  latitude
  longitude
  location
  distance
  logo {
    contentUrl
  }
  cover {
    contentUrl
  }
  parent {
    id
    name
    slug
  }
  indexable
}
    `;
export const JoinCommunityInvitation__CircleFragmentDoc = gql`
    fragment JoinCommunityInvitation__Circle on Circle {
  id
  name
  description
  city
  country
  slug
  cover {
    contentUrl
  }
  logo {
    imageName
  }
}
    `;
export const MaterialCard__Material_WithoutOwnershipFragmentDoc = gql`
    fragment MaterialCard__Material_WithoutOwnership on Material {
  id
  slug
  name
  brand
  model
  images {
    edges {
      node {
        contentUrl
      }
    }
  }
  bestPriceForUser
}
    `;
export const MaterialCard__Material_OwnershipFragmentDoc = gql`
    fragment MaterialCard__Material_Ownership on Material {
  mainOwnershipUser {
    city
    slug
    firstname
    lastname
    fullname
    avatar {
      contentUrl
    }
  }
  mainOwnershipCircle {
    city
    slug
    name
    logo {
      contentUrl
    }
  }
}
    `;
export const MaterialCard__MaterialFragmentDoc = gql`
    fragment MaterialCard__Material on Material {
  distance
  ...MaterialCard__Material_WithoutOwnership
  ...MaterialCard__Material_Ownership
}
    ${MaterialCard__Material_WithoutOwnershipFragmentDoc}
${MaterialCard__Material_OwnershipFragmentDoc}`;
export const SearchMaterialFragmentDoc = gql`
    fragment SearchMaterial on Material {
  ...MaterialCard__Material
}
    ${MaterialCard__MaterialFragmentDoc}`;
export const InformationMaterialForm__MaterialFragmentDoc = gql`
    fragment InformationMaterialForm__Material on Material {
  id
  slug
  name
  brand
  brand
  model
  reference
  description
  category {
    id
  }
}
    `;
export const ImagesMaterialForm__MaterialFragmentDoc = gql`
    fragment ImagesMaterialForm__Material on Material {
  id
  slug
  images {
    edges {
      node {
        id
        contentUrl
      }
    }
  }
}
    `;
export const MaterialConditionForm_CirclePriceItem__UserFragmentDoc = gql`
    fragment MaterialConditionForm_CirclePriceItem__User on User {
  settings {
    currencySymbol
  }
  circleMemberships {
    edges {
      node {
        id
        circle {
          id
          name
          subscription {
            id
            plan {
              id
              slug
              name
              features {
                name
                description
              }
            }
            active
          }
        }
      }
    }
  }
}
    `;
export const MaterialConditionForm__MaterialFragmentDoc = gql`
    fragment MaterialConditionForm__Material on Material {
  pricings {
    edges {
      node {
        id
        value
        circle {
          id
          name
        }
      }
    }
  }
  price
  mainOwnership {
    id
    ownerLabel
    ownerIRI
  }
  ownerships {
    id
    ownerIRI
    ownerLabel
  }
  mainOwner {
    ...MaterialConditionForm_CirclePriceItem__User
  }
  userLists(first: 1000) {
    edges {
      node {
        id
        name
      }
    }
  }
}
    ${MaterialConditionForm_CirclePriceItem__UserFragmentDoc}`;
export const MaterialForm__MaterialFragmentDoc = gql`
    fragment MaterialForm__Material on Material {
  ...InformationMaterialForm__Material
  ...ImagesMaterialForm__Material
  ...MaterialConditionForm__Material
}
    ${InformationMaterialForm__MaterialFragmentDoc}
${ImagesMaterialForm__MaterialFragmentDoc}
${MaterialConditionForm__MaterialFragmentDoc}`;
export const Blaze__UserFragmentDoc = gql`
    fragment Blaze__User on User {
  roles
  fullname
}
    `;
export const MaterialHorizontalCard__MaterialFragmentDoc = gql`
    fragment MaterialHorizontalCard__Material on Material {
  ...MaterialCard__Material
  mainOwner {
    ...Blaze__User
    streetAddress
  }
}
    ${MaterialCard__MaterialFragmentDoc}
${Blaze__UserFragmentDoc}`;
export const BookingSummaryModal__MaterialBookingFragmentDoc = gql`
    fragment BookingSummaryModal__MaterialBooking on MaterialBooking {
  createdAt
  user {
    ...UserCard__User
  }
  price
  status
  slug
  material {
    ...MaterialCard__Material
    ...MaterialHorizontalCard__Material
  }
  periods {
    id
    startDate
    endDate
  }
}
    ${UserCard__UserFragmentDoc}
${MaterialCard__MaterialFragmentDoc}
${MaterialHorizontalCard__MaterialFragmentDoc}`;
export const UnavailableModal__MaterialBookingFragmentDoc = gql`
    fragment UnavailableModal__MaterialBooking on MaterialBooking {
  id
  createdAt
  user {
    ...UserCard__User
  }
  price
  slug
  comment
  material {
    ...MaterialCard__Material
    ...MaterialHorizontalCard__Material
  }
  periods {
    id
    startDate
    endDate
  }
}
    ${UserCard__UserFragmentDoc}
${MaterialCard__MaterialFragmentDoc}
${MaterialHorizontalCard__MaterialFragmentDoc}`;
export const MaterialCalendar_DatePeriodFragmentDoc = gql`
    fragment MaterialCalendar_DatePeriod on MaterialBookingDatePeriod {
  startDate
  endDate
  booking {
    id
    user {
      id
      fullname
    }
    comment
  }
  price
}
    `;
export const MaterialCalendar__MaterialBookingFragmentDoc = gql`
    fragment MaterialCalendar__MaterialBooking on MaterialBooking {
  id
  ...BookingSummaryModal__MaterialBooking
  ...UnavailableModal__MaterialBooking
  periods {
    ...MaterialCalendar_DatePeriod
  }
}
    ${BookingSummaryModal__MaterialBookingFragmentDoc}
${UnavailableModal__MaterialBookingFragmentDoc}
${MaterialCalendar_DatePeriodFragmentDoc}`;
export const MaterialContextualButtonFragmentDoc = gql`
    fragment MaterialContextualButton on Material {
  id
  status
  slug
}
    `;
export const ShowMaterial__MaterialBookingFragmentDoc = gql`
    fragment ShowMaterial__MaterialBooking on MaterialBooking {
  ...BookingContext__MaterialBooking
}
    ${BookingContext__MaterialBookingFragmentDoc}`;
export const ShowMaterial__MaterialPricingFragmentDoc = gql`
    fragment ShowMaterial__MaterialPricing on MaterialPricing {
  id
  value
  circle {
    id
    slug
    name
    logo {
      imageName
      size
    }
  }
}
    `;
export const ShowMaterial__ImagesFragmentDoc = gql`
    fragment ShowMaterial__Images on MaterialImageCursorConnection {
  edges {
    node {
      id
      imageName
      size
      contentUrl
    }
    cursor
  }
  pageInfo {
    startCursor
    endCursor
    hasNextPage
    hasPreviousPage
  }
  totalCount
}
    `;
export const ProfilePoolingHeroFragmentDoc = gql`
    fragment ProfilePoolingHero on User {
  firstname
  lastname
  gender
}
    `;
export const ProfileView_MaterialFragmentDoc = gql`
    fragment ProfileView_Material on Material {
  ...MaterialCard__Material
}
    ${MaterialCard__MaterialFragmentDoc}`;
export const PriceText__UserFragmentDoc = gql`
    fragment PriceText__User on User {
  settings {
    language
    currencySymbol
  }
}
    `;
export const AuthUserDocument = gql`
    query authUser($id: ID!) {
  user(id: $id) {
    ...AuthUser__User
  }
}
    ${AuthUser__UserFragmentDoc}`;

/**
 * __useAuthUserQuery__
 *
 * To run a query within a React component, call `useAuthUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useAuthUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAuthUserQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useAuthUserQuery(baseOptions: Apollo.QueryHookOptions<AuthUserQuery, AuthUserQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<AuthUserQuery, AuthUserQueryVariables>(AuthUserDocument, options);
      }
export function useAuthUserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<AuthUserQuery, AuthUserQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<AuthUserQuery, AuthUserQueryVariables>(AuthUserDocument, options);
        }
export type AuthUserQueryHookResult = ReturnType<typeof useAuthUserQuery>;
export type AuthUserLazyQueryHookResult = ReturnType<typeof useAuthUserLazyQuery>;
export type AuthUserQueryResult = Apollo.QueryResult<AuthUserQuery, AuthUserQueryVariables>;
export const ChangeStatusDocument = gql`
    mutation changeStatus($id: ID!, $transition: String) {
  changeStatusMaterialBooking(input: {id: $id, transition: $transition}) {
    materialBooking {
      id
    }
  }
}
    `;
export type ChangeStatusMutationFn = Apollo.MutationFunction<ChangeStatusMutation, ChangeStatusMutationVariables>;

/**
 * __useChangeStatusMutation__
 *
 * To run a mutation, you first call `useChangeStatusMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeStatusMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeStatusMutation, { data, loading, error }] = useChangeStatusMutation({
 *   variables: {
 *      id: // value for 'id'
 *      transition: // value for 'transition'
 *   },
 * });
 */
export function useChangeStatusMutation(baseOptions?: Apollo.MutationHookOptions<ChangeStatusMutation, ChangeStatusMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeStatusMutation, ChangeStatusMutationVariables>(ChangeStatusDocument, options);
      }
export type ChangeStatusMutationHookResult = ReturnType<typeof useChangeStatusMutation>;
export type ChangeStatusMutationResult = Apollo.MutationResult<ChangeStatusMutation>;
export type ChangeStatusMutationOptions = Apollo.BaseMutationOptions<ChangeStatusMutation, ChangeStatusMutationVariables>;
export const MaterialCategorySelectDocument = gql`
    query MaterialCategorySelect {
  materialCategories {
    edges {
      node {
        ...CategorySelect__Category
      }
    }
  }
}
    ${CategorySelect__CategoryFragmentDoc}`;

/**
 * __useMaterialCategorySelectQuery__
 *
 * To run a query within a React component, call `useMaterialCategorySelectQuery` and pass it any options that fit your needs.
 * When your component renders, `useMaterialCategorySelectQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMaterialCategorySelectQuery({
 *   variables: {
 *   },
 * });
 */
export function useMaterialCategorySelectQuery(baseOptions?: Apollo.QueryHookOptions<MaterialCategorySelectQuery, MaterialCategorySelectQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MaterialCategorySelectQuery, MaterialCategorySelectQueryVariables>(MaterialCategorySelectDocument, options);
      }
export function useMaterialCategorySelectLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MaterialCategorySelectQuery, MaterialCategorySelectQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MaterialCategorySelectQuery, MaterialCategorySelectQueryVariables>(MaterialCategorySelectDocument, options);
        }
export type MaterialCategorySelectQueryHookResult = ReturnType<typeof useMaterialCategorySelectQuery>;
export type MaterialCategorySelectLazyQueryHookResult = ReturnType<typeof useMaterialCategorySelectLazyQuery>;
export type MaterialCategorySelectQueryResult = Apollo.QueryResult<MaterialCategorySelectQuery, MaterialCategorySelectQueryVariables>;
export const UserImageDocument = gql`
    query UserImage($id: ID!) {
  user(id: $id) {
    avatar {
      contentUrl
    }
    firstname
    lastname
  }
}
    `;

/**
 * __useUserImageQuery__
 *
 * To run a query within a React component, call `useUserImageQuery` and pass it any options that fit your needs.
 * When your component renders, `useUserImageQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUserImageQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useUserImageQuery(baseOptions: Apollo.QueryHookOptions<UserImageQuery, UserImageQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<UserImageQuery, UserImageQueryVariables>(UserImageDocument, options);
      }
export function useUserImageLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<UserImageQuery, UserImageQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<UserImageQuery, UserImageQueryVariables>(UserImageDocument, options);
        }
export type UserImageQueryHookResult = ReturnType<typeof useUserImageQuery>;
export type UserImageLazyQueryHookResult = ReturnType<typeof useUserImageLazyQuery>;
export type UserImageQueryResult = Apollo.QueryResult<UserImageQuery, UserImageQueryVariables>;
export const AskToJoinCircleButtonDocument = gql`
    mutation askToJoinCircleButton($input: askToJoinCircleMembershipInput!) {
  askToJoinCircleMembership(input: $input) {
    circleMembership {
      id
      user {
        id
      }
      permission
      circle {
        name
        activeUserActions
        description
        slug
        city
        logo {
          contentUrl
        }
        cover {
          contentUrl
        }
      }
    }
  }
}
    `;
export type AskToJoinCircleButtonMutationFn = Apollo.MutationFunction<AskToJoinCircleButtonMutation, AskToJoinCircleButtonMutationVariables>;

/**
 * __useAskToJoinCircleButtonMutation__
 *
 * To run a mutation, you first call `useAskToJoinCircleButtonMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAskToJoinCircleButtonMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [askToJoinCircleButtonMutation, { data, loading, error }] = useAskToJoinCircleButtonMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useAskToJoinCircleButtonMutation(baseOptions?: Apollo.MutationHookOptions<AskToJoinCircleButtonMutation, AskToJoinCircleButtonMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AskToJoinCircleButtonMutation, AskToJoinCircleButtonMutationVariables>(AskToJoinCircleButtonDocument, options);
      }
export type AskToJoinCircleButtonMutationHookResult = ReturnType<typeof useAskToJoinCircleButtonMutation>;
export type AskToJoinCircleButtonMutationResult = Apollo.MutationResult<AskToJoinCircleButtonMutation>;
export type AskToJoinCircleButtonMutationOptions = Apollo.BaseMutationOptions<AskToJoinCircleButtonMutation, AskToJoinCircleButtonMutationVariables>;
export const ChangeMembershipPermissionButtonDocument = gql`
    mutation ChangeMembershipPermissionButton($id: ID!, $transition: String) {
  changePermissionCircleMembership(input: {id: $id, transition: $transition}) {
    circleMembership {
      ...ChangeMembershipPermissionMembership
    }
  }
}
    ${ChangeMembershipPermissionMembershipFragmentDoc}`;
export type ChangeMembershipPermissionButtonMutationFn = Apollo.MutationFunction<ChangeMembershipPermissionButtonMutation, ChangeMembershipPermissionButtonMutationVariables>;

/**
 * __useChangeMembershipPermissionButtonMutation__
 *
 * To run a mutation, you first call `useChangeMembershipPermissionButtonMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeMembershipPermissionButtonMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeMembershipPermissionButtonMutation, { data, loading, error }] = useChangeMembershipPermissionButtonMutation({
 *   variables: {
 *      id: // value for 'id'
 *      transition: // value for 'transition'
 *   },
 * });
 */
export function useChangeMembershipPermissionButtonMutation(baseOptions?: Apollo.MutationHookOptions<ChangeMembershipPermissionButtonMutation, ChangeMembershipPermissionButtonMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeMembershipPermissionButtonMutation, ChangeMembershipPermissionButtonMutationVariables>(ChangeMembershipPermissionButtonDocument, options);
      }
export type ChangeMembershipPermissionButtonMutationHookResult = ReturnType<typeof useChangeMembershipPermissionButtonMutation>;
export type ChangeMembershipPermissionButtonMutationResult = Apollo.MutationResult<ChangeMembershipPermissionButtonMutation>;
export type ChangeMembershipPermissionButtonMutationOptions = Apollo.BaseMutationOptions<ChangeMembershipPermissionButtonMutation, ChangeMembershipPermissionButtonMutationVariables>;
export const ChangeMembershipStatusButtonDocument = gql`
    mutation ChangeMembershipStatusButton($id: ID!, $transition: String) {
  changeStatusCircleMembership(input: {id: $id, transition: $transition}) {
    circleMembership {
      ...ChangeMembershipStatusMembership
    }
  }
}
    ${ChangeMembershipStatusMembershipFragmentDoc}`;
export type ChangeMembershipStatusButtonMutationFn = Apollo.MutationFunction<ChangeMembershipStatusButtonMutation, ChangeMembershipStatusButtonMutationVariables>;

/**
 * __useChangeMembershipStatusButtonMutation__
 *
 * To run a mutation, you first call `useChangeMembershipStatusButtonMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeMembershipStatusButtonMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeMembershipStatusButtonMutation, { data, loading, error }] = useChangeMembershipStatusButtonMutation({
 *   variables: {
 *      id: // value for 'id'
 *      transition: // value for 'transition'
 *   },
 * });
 */
export function useChangeMembershipStatusButtonMutation(baseOptions?: Apollo.MutationHookOptions<ChangeMembershipStatusButtonMutation, ChangeMembershipStatusButtonMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeMembershipStatusButtonMutation, ChangeMembershipStatusButtonMutationVariables>(ChangeMembershipStatusButtonDocument, options);
      }
export type ChangeMembershipStatusButtonMutationHookResult = ReturnType<typeof useChangeMembershipStatusButtonMutation>;
export type ChangeMembershipStatusButtonMutationResult = Apollo.MutationResult<ChangeMembershipStatusButtonMutation>;
export type ChangeMembershipStatusButtonMutationOptions = Apollo.BaseMutationOptions<ChangeMembershipStatusButtonMutation, ChangeMembershipStatusButtonMutationVariables>;
export const MembershipListDocument = gql`
    query membershipList($slug: String!, $statuses: [String], $afterCursor: String, $beforeCursor: String, $nbToFetch: Int, $order: [CircleMembershipFilter_order]) {
  circleMemberships(
    circle_slug: $slug
    status_list: $statuses
    order: $order
    first: $nbToFetch
    after: $afterCursor
    before: $beforeCursor
  ) {
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
    totalCount
    edges {
      node {
        ...MembershipList__Membership
      }
    }
  }
}
    ${MembershipList__MembershipFragmentDoc}`;

/**
 * __useMembershipListQuery__
 *
 * To run a query within a React component, call `useMembershipListQuery` and pass it any options that fit your needs.
 * When your component renders, `useMembershipListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMembershipListQuery({
 *   variables: {
 *      slug: // value for 'slug'
 *      statuses: // value for 'statuses'
 *      afterCursor: // value for 'afterCursor'
 *      beforeCursor: // value for 'beforeCursor'
 *      nbToFetch: // value for 'nbToFetch'
 *      order: // value for 'order'
 *   },
 * });
 */
export function useMembershipListQuery(baseOptions: Apollo.QueryHookOptions<MembershipListQuery, MembershipListQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MembershipListQuery, MembershipListQueryVariables>(MembershipListDocument, options);
      }
export function useMembershipListLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MembershipListQuery, MembershipListQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MembershipListQuery, MembershipListQueryVariables>(MembershipListDocument, options);
        }
export type MembershipListQueryHookResult = ReturnType<typeof useMembershipListQuery>;
export type MembershipListLazyQueryHookResult = ReturnType<typeof useMembershipListLazyQuery>;
export type MembershipListQueryResult = Apollo.QueryResult<MembershipListQuery, MembershipListQueryVariables>;
export const ContactDocument = gql`
    mutation contact($input: createContactInput!) {
  createContact(input: $input) {
    contact {
      id
    }
  }
}
    `;
export type ContactMutationFn = Apollo.MutationFunction<ContactMutation, ContactMutationVariables>;

/**
 * __useContactMutation__
 *
 * To run a mutation, you first call `useContactMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useContactMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [contactMutation, { data, loading, error }] = useContactMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useContactMutation(baseOptions?: Apollo.MutationHookOptions<ContactMutation, ContactMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ContactMutation, ContactMutationVariables>(ContactDocument, options);
      }
export type ContactMutationHookResult = ReturnType<typeof useContactMutation>;
export type ContactMutationResult = Apollo.MutationResult<ContactMutation>;
export type ContactMutationOptions = Apollo.BaseMutationOptions<ContactMutation, ContactMutationVariables>;
export const RequestInvitationDocument = gql`
    mutation requestInvitation($input: createInvitationInput!) {
  createInvitation(input: $input) {
    invitation {
      id
    }
  }
}
    `;
export type RequestInvitationMutationFn = Apollo.MutationFunction<RequestInvitationMutation, RequestInvitationMutationVariables>;

/**
 * __useRequestInvitationMutation__
 *
 * To run a mutation, you first call `useRequestInvitationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRequestInvitationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [requestInvitationMutation, { data, loading, error }] = useRequestInvitationMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useRequestInvitationMutation(baseOptions?: Apollo.MutationHookOptions<RequestInvitationMutation, RequestInvitationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RequestInvitationMutation, RequestInvitationMutationVariables>(RequestInvitationDocument, options);
      }
export type RequestInvitationMutationHookResult = ReturnType<typeof useRequestInvitationMutation>;
export type RequestInvitationMutationResult = Apollo.MutationResult<RequestInvitationMutation>;
export type RequestInvitationMutationOptions = Apollo.BaseMutationOptions<RequestInvitationMutation, RequestInvitationMutationVariables>;
export const FindExistingUserForEmailDocument = gql`
    query findExistingUserForEmail($email: String!) {
  users(email: $email) {
    edges {
      node {
        ...InvitationFormDialog_ExistingUser
      }
    }
  }
}
    ${InvitationFormDialog_ExistingUserFragmentDoc}`;

/**
 * __useFindExistingUserForEmailQuery__
 *
 * To run a query within a React component, call `useFindExistingUserForEmailQuery` and pass it any options that fit your needs.
 * When your component renders, `useFindExistingUserForEmailQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFindExistingUserForEmailQuery({
 *   variables: {
 *      email: // value for 'email'
 *   },
 * });
 */
export function useFindExistingUserForEmailQuery(baseOptions: Apollo.QueryHookOptions<FindExistingUserForEmailQuery, FindExistingUserForEmailQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FindExistingUserForEmailQuery, FindExistingUserForEmailQueryVariables>(FindExistingUserForEmailDocument, options);
      }
export function useFindExistingUserForEmailLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FindExistingUserForEmailQuery, FindExistingUserForEmailQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FindExistingUserForEmailQuery, FindExistingUserForEmailQueryVariables>(FindExistingUserForEmailDocument, options);
        }
export type FindExistingUserForEmailQueryHookResult = ReturnType<typeof useFindExistingUserForEmailQuery>;
export type FindExistingUserForEmailLazyQueryHookResult = ReturnType<typeof useFindExistingUserForEmailLazyQuery>;
export type FindExistingUserForEmailQueryResult = Apollo.QueryResult<FindExistingUserForEmailQuery, FindExistingUserForEmailQueryVariables>;
export const SendCommunityInvitationDocument = gql`
    mutation SendCommunityInvitation($input: sendCircleInvitationInput!) {
  sendCircleInvitation(input: $input) {
    invitation {
      id
    }
  }
}
    `;
export type SendCommunityInvitationMutationFn = Apollo.MutationFunction<SendCommunityInvitationMutation, SendCommunityInvitationMutationVariables>;

/**
 * __useSendCommunityInvitationMutation__
 *
 * To run a mutation, you first call `useSendCommunityInvitationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSendCommunityInvitationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [sendCommunityInvitationMutation, { data, loading, error }] = useSendCommunityInvitationMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useSendCommunityInvitationMutation(baseOptions?: Apollo.MutationHookOptions<SendCommunityInvitationMutation, SendCommunityInvitationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SendCommunityInvitationMutation, SendCommunityInvitationMutationVariables>(SendCommunityInvitationDocument, options);
      }
export type SendCommunityInvitationMutationHookResult = ReturnType<typeof useSendCommunityInvitationMutation>;
export type SendCommunityInvitationMutationResult = Apollo.MutationResult<SendCommunityInvitationMutation>;
export type SendCommunityInvitationMutationOptions = Apollo.BaseMutationOptions<SendCommunityInvitationMutation, SendCommunityInvitationMutationVariables>;
export const UpdateUserLocationDocument = gql`
    mutation UpdateUserLocation($input: updateUserInput!) {
  updateUser(input: $input) {
    user {
      id
      location
      longitude
      latitude
      streetAddress
    }
  }
}
    `;
export type UpdateUserLocationMutationFn = Apollo.MutationFunction<UpdateUserLocationMutation, UpdateUserLocationMutationVariables>;

/**
 * __useUpdateUserLocationMutation__
 *
 * To run a mutation, you first call `useUpdateUserLocationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserLocationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUserLocationMutation, { data, loading, error }] = useUpdateUserLocationMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateUserLocationMutation(baseOptions?: Apollo.MutationHookOptions<UpdateUserLocationMutation, UpdateUserLocationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateUserLocationMutation, UpdateUserLocationMutationVariables>(UpdateUserLocationDocument, options);
      }
export type UpdateUserLocationMutationHookResult = ReturnType<typeof useUpdateUserLocationMutation>;
export type UpdateUserLocationMutationResult = Apollo.MutationResult<UpdateUserLocationMutation>;
export type UpdateUserLocationMutationOptions = Apollo.BaseMutationOptions<UpdateUserLocationMutation, UpdateUserLocationMutationVariables>;
export const CreateAvatarDocument = gql`
    mutation createAvatar($input: createAvatarInput!) {
  createAvatar(input: $input) {
    avatar {
      ...CreateAvatarReturn
    }
  }
}
    ${CreateAvatarReturnFragmentDoc}`;
export type CreateAvatarMutationFn = Apollo.MutationFunction<CreateAvatarMutation, CreateAvatarMutationVariables>;

/**
 * __useCreateAvatarMutation__
 *
 * To run a mutation, you first call `useCreateAvatarMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateAvatarMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createAvatarMutation, { data, loading, error }] = useCreateAvatarMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateAvatarMutation(baseOptions?: Apollo.MutationHookOptions<CreateAvatarMutation, CreateAvatarMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateAvatarMutation, CreateAvatarMutationVariables>(CreateAvatarDocument, options);
      }
export type CreateAvatarMutationHookResult = ReturnType<typeof useCreateAvatarMutation>;
export type CreateAvatarMutationResult = Apollo.MutationResult<CreateAvatarMutation>;
export type CreateAvatarMutationOptions = Apollo.BaseMutationOptions<CreateAvatarMutation, CreateAvatarMutationVariables>;
export const UserListAvatarsDocument = gql`
    query UserListAvatars($id: ID!, $afterCursor: String, $beforeCursor: String, $first: Int) {
  userList(id: $id) {
    users(first: $first, after: $afterCursor, before: $beforeCursor) {
      pageInfo {
        hasNextPage
        endCursor
        hasPreviousPage
        startCursor
      }
      totalCount
      edges {
        ...UserListAvatars_User
      }
    }
  }
}
    ${UserListAvatars_UserFragmentDoc}`;

/**
 * __useUserListAvatarsQuery__
 *
 * To run a query within a React component, call `useUserListAvatarsQuery` and pass it any options that fit your needs.
 * When your component renders, `useUserListAvatarsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUserListAvatarsQuery({
 *   variables: {
 *      id: // value for 'id'
 *      afterCursor: // value for 'afterCursor'
 *      beforeCursor: // value for 'beforeCursor'
 *      first: // value for 'first'
 *   },
 * });
 */
export function useUserListAvatarsQuery(baseOptions: Apollo.QueryHookOptions<UserListAvatarsQuery, UserListAvatarsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<UserListAvatarsQuery, UserListAvatarsQueryVariables>(UserListAvatarsDocument, options);
      }
export function useUserListAvatarsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<UserListAvatarsQuery, UserListAvatarsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<UserListAvatarsQuery, UserListAvatarsQueryVariables>(UserListAvatarsDocument, options);
        }
export type UserListAvatarsQueryHookResult = ReturnType<typeof useUserListAvatarsQuery>;
export type UserListAvatarsLazyQueryHookResult = ReturnType<typeof useUserListAvatarsLazyQuery>;
export type UserListAvatarsQueryResult = Apollo.QueryResult<UserListAvatarsQuery, UserListAvatarsQueryVariables>;
export const EditFormCircleDocument = gql`
    query editFormCircle($id: ID!) {
  circle(id: $id) {
    ...CircleForm__Circle
  }
}
    ${CircleForm__CircleFragmentDoc}`;

/**
 * __useEditFormCircleQuery__
 *
 * To run a query within a React component, call `useEditFormCircleQuery` and pass it any options that fit your needs.
 * When your component renders, `useEditFormCircleQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useEditFormCircleQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useEditFormCircleQuery(baseOptions: Apollo.QueryHookOptions<EditFormCircleQuery, EditFormCircleQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<EditFormCircleQuery, EditFormCircleQueryVariables>(EditFormCircleDocument, options);
      }
export function useEditFormCircleLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<EditFormCircleQuery, EditFormCircleQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<EditFormCircleQuery, EditFormCircleQueryVariables>(EditFormCircleDocument, options);
        }
export type EditFormCircleQueryHookResult = ReturnType<typeof useEditFormCircleQuery>;
export type EditFormCircleLazyQueryHookResult = ReturnType<typeof useEditFormCircleLazyQuery>;
export type EditFormCircleQueryResult = Apollo.QueryResult<EditFormCircleQuery, EditFormCircleQueryVariables>;
export const NewCommunityDocument = gql`
    mutation NewCommunity($input: createCircleInput!) {
  createCircle(input: $input) {
    circle {
      id
      slug
    }
  }
}
    `;
export type NewCommunityMutationFn = Apollo.MutationFunction<NewCommunityMutation, NewCommunityMutationVariables>;

/**
 * __useNewCommunityMutation__
 *
 * To run a mutation, you first call `useNewCommunityMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useNewCommunityMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [newCommunityMutation, { data, loading, error }] = useNewCommunityMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useNewCommunityMutation(baseOptions?: Apollo.MutationHookOptions<NewCommunityMutation, NewCommunityMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<NewCommunityMutation, NewCommunityMutationVariables>(NewCommunityDocument, options);
      }
export type NewCommunityMutationHookResult = ReturnType<typeof useNewCommunityMutation>;
export type NewCommunityMutationResult = Apollo.MutationResult<NewCommunityMutation>;
export type NewCommunityMutationOptions = Apollo.BaseMutationOptions<NewCommunityMutation, NewCommunityMutationVariables>;
export const AvailabilityPlanningEstimateDocument = gql`
    mutation availabilityPlanningEstimate($materialId: String, $startDate: String, $endDate: String) {
  estimateMaterialBooking(
    input: {materialId: $materialId, startDate: $startDate, endDate: $endDate}
  ) {
    materialBooking {
      ...BookingContext__MaterialBooking
    }
  }
}
    ${BookingContext__MaterialBookingFragmentDoc}`;
export type AvailabilityPlanningEstimateMutationFn = Apollo.MutationFunction<AvailabilityPlanningEstimateMutation, AvailabilityPlanningEstimateMutationVariables>;

/**
 * __useAvailabilityPlanningEstimateMutation__
 *
 * To run a mutation, you first call `useAvailabilityPlanningEstimateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAvailabilityPlanningEstimateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [availabilityPlanningEstimateMutation, { data, loading, error }] = useAvailabilityPlanningEstimateMutation({
 *   variables: {
 *      materialId: // value for 'materialId'
 *      startDate: // value for 'startDate'
 *      endDate: // value for 'endDate'
 *   },
 * });
 */
export function useAvailabilityPlanningEstimateMutation(baseOptions?: Apollo.MutationHookOptions<AvailabilityPlanningEstimateMutation, AvailabilityPlanningEstimateMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AvailabilityPlanningEstimateMutation, AvailabilityPlanningEstimateMutationVariables>(AvailabilityPlanningEstimateDocument, options);
      }
export type AvailabilityPlanningEstimateMutationHookResult = ReturnType<typeof useAvailabilityPlanningEstimateMutation>;
export type AvailabilityPlanningEstimateMutationResult = Apollo.MutationResult<AvailabilityPlanningEstimateMutation>;
export type AvailabilityPlanningEstimateMutationOptions = Apollo.BaseMutationOptions<AvailabilityPlanningEstimateMutation, AvailabilityPlanningEstimateMutationVariables>;
export const BookingSummaryApplyBookingDocument = gql`
    mutation BookingSummaryApplyBooking($id: ID!, $message: String) {
  changeStatusMaterialBooking(
    input: {id: $id, transition: "apply", message: $message}
  ) {
    materialBooking {
      ...BookingContext__MaterialBooking
    }
  }
}
    ${BookingContext__MaterialBookingFragmentDoc}`;
export type BookingSummaryApplyBookingMutationFn = Apollo.MutationFunction<BookingSummaryApplyBookingMutation, BookingSummaryApplyBookingMutationVariables>;

/**
 * __useBookingSummaryApplyBookingMutation__
 *
 * To run a mutation, you first call `useBookingSummaryApplyBookingMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useBookingSummaryApplyBookingMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [bookingSummaryApplyBookingMutation, { data, loading, error }] = useBookingSummaryApplyBookingMutation({
 *   variables: {
 *      id: // value for 'id'
 *      message: // value for 'message'
 *   },
 * });
 */
export function useBookingSummaryApplyBookingMutation(baseOptions?: Apollo.MutationHookOptions<BookingSummaryApplyBookingMutation, BookingSummaryApplyBookingMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<BookingSummaryApplyBookingMutation, BookingSummaryApplyBookingMutationVariables>(BookingSummaryApplyBookingDocument, options);
      }
export type BookingSummaryApplyBookingMutationHookResult = ReturnType<typeof useBookingSummaryApplyBookingMutation>;
export type BookingSummaryApplyBookingMutationResult = Apollo.MutationResult<BookingSummaryApplyBookingMutation>;
export type BookingSummaryApplyBookingMutationOptions = Apollo.BaseMutationOptions<BookingSummaryApplyBookingMutation, BookingSummaryApplyBookingMutationVariables>;
export const ConditionMaterialForm_UserListDocument = gql`
    query ConditionMaterialForm_UserList {
  userLists {
    edges {
      node {
        id
        slug
        name
        users {
          totalCount
        }
      }
    }
  }
}
    `;

/**
 * __useConditionMaterialForm_UserListQuery__
 *
 * To run a query within a React component, call `useConditionMaterialForm_UserListQuery` and pass it any options that fit your needs.
 * When your component renders, `useConditionMaterialForm_UserListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useConditionMaterialForm_UserListQuery({
 *   variables: {
 *   },
 * });
 */
export function useConditionMaterialForm_UserListQuery(baseOptions?: Apollo.QueryHookOptions<ConditionMaterialForm_UserListQuery, ConditionMaterialForm_UserListQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ConditionMaterialForm_UserListQuery, ConditionMaterialForm_UserListQueryVariables>(ConditionMaterialForm_UserListDocument, options);
      }
export function useConditionMaterialForm_UserListLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ConditionMaterialForm_UserListQuery, ConditionMaterialForm_UserListQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ConditionMaterialForm_UserListQuery, ConditionMaterialForm_UserListQueryVariables>(ConditionMaterialForm_UserListDocument, options);
        }
export type ConditionMaterialForm_UserListQueryHookResult = ReturnType<typeof useConditionMaterialForm_UserListQuery>;
export type ConditionMaterialForm_UserListLazyQueryHookResult = ReturnType<typeof useConditionMaterialForm_UserListLazyQuery>;
export type ConditionMaterialForm_UserListQueryResult = Apollo.QueryResult<ConditionMaterialForm_UserListQuery, ConditionMaterialForm_UserListQueryVariables>;
export const CreateMaterialImageDocument = gql`
    mutation createMaterialImage($material: String!, $dimensions: Iterable!, $imageName: String!, $size: Int!, $file: Upload!) {
  createMaterialImage(
    input: {imageName: $imageName, dimensions: $dimensions, size: $size, material: $material, file: $file}
  ) {
    materialImage {
      id
      contentUrl
      material {
        images {
          edges {
            node {
              id
              contentUrl
            }
          }
        }
      }
    }
  }
}
    `;
export type CreateMaterialImageMutationFn = Apollo.MutationFunction<CreateMaterialImageMutation, CreateMaterialImageMutationVariables>;

/**
 * __useCreateMaterialImageMutation__
 *
 * To run a mutation, you first call `useCreateMaterialImageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateMaterialImageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createMaterialImageMutation, { data, loading, error }] = useCreateMaterialImageMutation({
 *   variables: {
 *      material: // value for 'material'
 *      dimensions: // value for 'dimensions'
 *      imageName: // value for 'imageName'
 *      size: // value for 'size'
 *      file: // value for 'file'
 *   },
 * });
 */
export function useCreateMaterialImageMutation(baseOptions?: Apollo.MutationHookOptions<CreateMaterialImageMutation, CreateMaterialImageMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateMaterialImageMutation, CreateMaterialImageMutationVariables>(CreateMaterialImageDocument, options);
      }
export type CreateMaterialImageMutationHookResult = ReturnType<typeof useCreateMaterialImageMutation>;
export type CreateMaterialImageMutationResult = Apollo.MutationResult<CreateMaterialImageMutation>;
export type CreateMaterialImageMutationOptions = Apollo.BaseMutationOptions<CreateMaterialImageMutation, CreateMaterialImageMutationVariables>;
export const DeleteMaterialImageDocument = gql`
    mutation deleteMaterialImage($id: ID!) {
  deleteMaterialImage(input: {id: $id}) {
    clientMutationId
  }
}
    `;
export type DeleteMaterialImageMutationFn = Apollo.MutationFunction<DeleteMaterialImageMutation, DeleteMaterialImageMutationVariables>;

/**
 * __useDeleteMaterialImageMutation__
 *
 * To run a mutation, you first call `useDeleteMaterialImageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteMaterialImageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteMaterialImageMutation, { data, loading, error }] = useDeleteMaterialImageMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteMaterialImageMutation(baseOptions?: Apollo.MutationHookOptions<DeleteMaterialImageMutation, DeleteMaterialImageMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteMaterialImageMutation, DeleteMaterialImageMutationVariables>(DeleteMaterialImageDocument, options);
      }
export type DeleteMaterialImageMutationHookResult = ReturnType<typeof useDeleteMaterialImageMutation>;
export type DeleteMaterialImageMutationResult = Apollo.MutationResult<DeleteMaterialImageMutation>;
export type DeleteMaterialImageMutationOptions = Apollo.BaseMutationOptions<DeleteMaterialImageMutation, DeleteMaterialImageMutationVariables>;
export const UpdateMaterialImageDocument = gql`
    mutation updateMaterialImage($id: ID!, $dimensions: Iterable!, $imageName: String!, $size: Int!, $file: Upload!) {
  updateMaterialImage(
    input: {id: $id, imageName: $imageName, dimensions: $dimensions, size: $size, file: $file}
  ) {
    materialImage {
      id
      contentUrl
      material {
        images {
          edges {
            node {
              id
              contentUrl
            }
          }
        }
      }
    }
  }
}
    `;
export type UpdateMaterialImageMutationFn = Apollo.MutationFunction<UpdateMaterialImageMutation, UpdateMaterialImageMutationVariables>;

/**
 * __useUpdateMaterialImageMutation__
 *
 * To run a mutation, you first call `useUpdateMaterialImageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateMaterialImageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateMaterialImageMutation, { data, loading, error }] = useUpdateMaterialImageMutation({
 *   variables: {
 *      id: // value for 'id'
 *      dimensions: // value for 'dimensions'
 *      imageName: // value for 'imageName'
 *      size: // value for 'size'
 *      file: // value for 'file'
 *   },
 * });
 */
export function useUpdateMaterialImageMutation(baseOptions?: Apollo.MutationHookOptions<UpdateMaterialImageMutation, UpdateMaterialImageMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateMaterialImageMutation, UpdateMaterialImageMutationVariables>(UpdateMaterialImageDocument, options);
      }
export type UpdateMaterialImageMutationHookResult = ReturnType<typeof useUpdateMaterialImageMutation>;
export type UpdateMaterialImageMutationResult = Apollo.MutationResult<UpdateMaterialImageMutation>;
export type UpdateMaterialImageMutationOptions = Apollo.BaseMutationOptions<UpdateMaterialImageMutation, UpdateMaterialImageMutationVariables>;
export const InformationMaterialForm_CirclesOptionsDocument = gql`
    query InformationMaterialForm_circlesOptions {
  myCircles {
    edges {
      node {
        ...InformationMaterialForm_circlesOption
      }
    }
  }
}
    ${InformationMaterialForm_CirclesOptionFragmentDoc}`;

/**
 * __useInformationMaterialForm_CirclesOptionsQuery__
 *
 * To run a query within a React component, call `useInformationMaterialForm_CirclesOptionsQuery` and pass it any options that fit your needs.
 * When your component renders, `useInformationMaterialForm_CirclesOptionsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useInformationMaterialForm_CirclesOptionsQuery({
 *   variables: {
 *   },
 * });
 */
export function useInformationMaterialForm_CirclesOptionsQuery(baseOptions?: Apollo.QueryHookOptions<InformationMaterialForm_CirclesOptionsQuery, InformationMaterialForm_CirclesOptionsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<InformationMaterialForm_CirclesOptionsQuery, InformationMaterialForm_CirclesOptionsQueryVariables>(InformationMaterialForm_CirclesOptionsDocument, options);
      }
export function useInformationMaterialForm_CirclesOptionsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<InformationMaterialForm_CirclesOptionsQuery, InformationMaterialForm_CirclesOptionsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<InformationMaterialForm_CirclesOptionsQuery, InformationMaterialForm_CirclesOptionsQueryVariables>(InformationMaterialForm_CirclesOptionsDocument, options);
        }
export type InformationMaterialForm_CirclesOptionsQueryHookResult = ReturnType<typeof useInformationMaterialForm_CirclesOptionsQuery>;
export type InformationMaterialForm_CirclesOptionsLazyQueryHookResult = ReturnType<typeof useInformationMaterialForm_CirclesOptionsLazyQuery>;
export type InformationMaterialForm_CirclesOptionsQueryResult = Apollo.QueryResult<InformationMaterialForm_CirclesOptionsQuery, InformationMaterialForm_CirclesOptionsQueryVariables>;
export const CreateMaterialDocument = gql`
    mutation createMaterial($input: createMaterialInput!) {
  createMaterial(input: $input) {
    material {
      id
      name
      slug
      brand
      model
    }
  }
}
    `;
export type CreateMaterialMutationFn = Apollo.MutationFunction<CreateMaterialMutation, CreateMaterialMutationVariables>;

/**
 * __useCreateMaterialMutation__
 *
 * To run a mutation, you first call `useCreateMaterialMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateMaterialMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createMaterialMutation, { data, loading, error }] = useCreateMaterialMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateMaterialMutation(baseOptions?: Apollo.MutationHookOptions<CreateMaterialMutation, CreateMaterialMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateMaterialMutation, CreateMaterialMutationVariables>(CreateMaterialDocument, options);
      }
export type CreateMaterialMutationHookResult = ReturnType<typeof useCreateMaterialMutation>;
export type CreateMaterialMutationResult = Apollo.MutationResult<CreateMaterialMutation>;
export type CreateMaterialMutationOptions = Apollo.BaseMutationOptions<CreateMaterialMutation, CreateMaterialMutationVariables>;
export const ForkMaterialDocument = gql`
    mutation forkMaterial($input: forkMaterialInput!) {
  forkMaterial(input: $input) {
    material {
      id
      slug
    }
  }
}
    `;
export type ForkMaterialMutationFn = Apollo.MutationFunction<ForkMaterialMutation, ForkMaterialMutationVariables>;

/**
 * __useForkMaterialMutation__
 *
 * To run a mutation, you first call `useForkMaterialMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useForkMaterialMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [forkMaterialMutation, { data, loading, error }] = useForkMaterialMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useForkMaterialMutation(baseOptions?: Apollo.MutationHookOptions<ForkMaterialMutation, ForkMaterialMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ForkMaterialMutation, ForkMaterialMutationVariables>(ForkMaterialDocument, options);
      }
export type ForkMaterialMutationHookResult = ReturnType<typeof useForkMaterialMutation>;
export type ForkMaterialMutationResult = Apollo.MutationResult<ForkMaterialMutation>;
export type ForkMaterialMutationOptions = Apollo.BaseMutationOptions<ForkMaterialMutation, ForkMaterialMutationVariables>;
export const CreateAlertEmptySearchDocument = gql`
    mutation CreateAlertEmptySearch($name: String!) {
  createMaterialSearchAlert(input: {name: $name}) {
    materialSearchAlert {
      ...SearchAlert
    }
  }
}
    ${SearchAlertFragmentDoc}`;
export type CreateAlertEmptySearchMutationFn = Apollo.MutationFunction<CreateAlertEmptySearchMutation, CreateAlertEmptySearchMutationVariables>;

/**
 * __useCreateAlertEmptySearchMutation__
 *
 * To run a mutation, you first call `useCreateAlertEmptySearchMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateAlertEmptySearchMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createAlertEmptySearchMutation, { data, loading, error }] = useCreateAlertEmptySearchMutation({
 *   variables: {
 *      name: // value for 'name'
 *   },
 * });
 */
export function useCreateAlertEmptySearchMutation(baseOptions?: Apollo.MutationHookOptions<CreateAlertEmptySearchMutation, CreateAlertEmptySearchMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateAlertEmptySearchMutation, CreateAlertEmptySearchMutationVariables>(CreateAlertEmptySearchDocument, options);
      }
export type CreateAlertEmptySearchMutationHookResult = ReturnType<typeof useCreateAlertEmptySearchMutation>;
export type CreateAlertEmptySearchMutationResult = Apollo.MutationResult<CreateAlertEmptySearchMutation>;
export type CreateAlertEmptySearchMutationOptions = Apollo.BaseMutationOptions<CreateAlertEmptySearchMutation, CreateAlertEmptySearchMutationVariables>;
export const AlertEmptySearchsDocument = gql`
    query AlertEmptySearchs($name: String!) {
  materialSearchAlerts(name: $name) {
    edges {
      node {
        ...SearchAlert
      }
    }
  }
}
    ${SearchAlertFragmentDoc}`;

/**
 * __useAlertEmptySearchsQuery__
 *
 * To run a query within a React component, call `useAlertEmptySearchsQuery` and pass it any options that fit your needs.
 * When your component renders, `useAlertEmptySearchsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAlertEmptySearchsQuery({
 *   variables: {
 *      name: // value for 'name'
 *   },
 * });
 */
export function useAlertEmptySearchsQuery(baseOptions: Apollo.QueryHookOptions<AlertEmptySearchsQuery, AlertEmptySearchsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<AlertEmptySearchsQuery, AlertEmptySearchsQueryVariables>(AlertEmptySearchsDocument, options);
      }
export function useAlertEmptySearchsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<AlertEmptySearchsQuery, AlertEmptySearchsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<AlertEmptySearchsQuery, AlertEmptySearchsQueryVariables>(AlertEmptySearchsDocument, options);
        }
export type AlertEmptySearchsQueryHookResult = ReturnType<typeof useAlertEmptySearchsQuery>;
export type AlertEmptySearchsLazyQueryHookResult = ReturnType<typeof useAlertEmptySearchsLazyQuery>;
export type AlertEmptySearchsQueryResult = Apollo.QueryResult<AlertEmptySearchsQuery, AlertEmptySearchsQueryVariables>;
export const DeleteMessageDocument = gql`
    mutation deleteMessage($id: ID!) {
  deleteMessage(input: {id: $id}) {
    clientMutationId
  }
}
    `;
export type DeleteMessageMutationFn = Apollo.MutationFunction<DeleteMessageMutation, DeleteMessageMutationVariables>;

/**
 * __useDeleteMessageMutation__
 *
 * To run a mutation, you first call `useDeleteMessageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteMessageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteMessageMutation, { data, loading, error }] = useDeleteMessageMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteMessageMutation(baseOptions?: Apollo.MutationHookOptions<DeleteMessageMutation, DeleteMessageMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteMessageMutation, DeleteMessageMutationVariables>(DeleteMessageDocument, options);
      }
export type DeleteMessageMutationHookResult = ReturnType<typeof useDeleteMessageMutation>;
export type DeleteMessageMutationResult = Apollo.MutationResult<DeleteMessageMutation>;
export type DeleteMessageMutationOptions = Apollo.BaseMutationOptions<DeleteMessageMutation, DeleteMessageMutationVariables>;
export const SubscribeNewsletterDocument = gql`
    mutation subscribeNewsletter($input: subscribeNewsletterContactInput!) {
  subscribeNewsletterContact(input: $input) {
    contact {
      id
    }
  }
}
    `;
export type SubscribeNewsletterMutationFn = Apollo.MutationFunction<SubscribeNewsletterMutation, SubscribeNewsletterMutationVariables>;

/**
 * __useSubscribeNewsletterMutation__
 *
 * To run a mutation, you first call `useSubscribeNewsletterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSubscribeNewsletterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [subscribeNewsletterMutation, { data, loading, error }] = useSubscribeNewsletterMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useSubscribeNewsletterMutation(baseOptions?: Apollo.MutationHookOptions<SubscribeNewsletterMutation, SubscribeNewsletterMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SubscribeNewsletterMutation, SubscribeNewsletterMutationVariables>(SubscribeNewsletterDocument, options);
      }
export type SubscribeNewsletterMutationHookResult = ReturnType<typeof useSubscribeNewsletterMutation>;
export type SubscribeNewsletterMutationResult = Apollo.MutationResult<SubscribeNewsletterMutation>;
export type SubscribeNewsletterMutationOptions = Apollo.BaseMutationOptions<SubscribeNewsletterMutation, SubscribeNewsletterMutationVariables>;
export const CircleMembershipsTable__ChangeStatusDocument = gql`
    mutation CircleMembershipsTable__ChangeStatus($id: ID!, $transition: String!) {
  changeStatusCircleMembership(input: {id: $id, transition: $transition}) {
    circleMembership {
      id
      status
    }
  }
}
    `;
export type CircleMembershipsTable__ChangeStatusMutationFn = Apollo.MutationFunction<CircleMembershipsTable__ChangeStatusMutation, CircleMembershipsTable__ChangeStatusMutationVariables>;

/**
 * __useCircleMembershipsTable__ChangeStatusMutation__
 *
 * To run a mutation, you first call `useCircleMembershipsTable__ChangeStatusMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCircleMembershipsTable__ChangeStatusMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [circleMembershipsTableChangeStatusMutation, { data, loading, error }] = useCircleMembershipsTable__ChangeStatusMutation({
 *   variables: {
 *      id: // value for 'id'
 *      transition: // value for 'transition'
 *   },
 * });
 */
export function useCircleMembershipsTable__ChangeStatusMutation(baseOptions?: Apollo.MutationHookOptions<CircleMembershipsTable__ChangeStatusMutation, CircleMembershipsTable__ChangeStatusMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CircleMembershipsTable__ChangeStatusMutation, CircleMembershipsTable__ChangeStatusMutationVariables>(CircleMembershipsTable__ChangeStatusDocument, options);
      }
export type CircleMembershipsTable__ChangeStatusMutationHookResult = ReturnType<typeof useCircleMembershipsTable__ChangeStatusMutation>;
export type CircleMembershipsTable__ChangeStatusMutationResult = Apollo.MutationResult<CircleMembershipsTable__ChangeStatusMutation>;
export type CircleMembershipsTable__ChangeStatusMutationOptions = Apollo.BaseMutationOptions<CircleMembershipsTable__ChangeStatusMutation, CircleMembershipsTable__ChangeStatusMutationVariables>;
export const PauseMaterialDocument = gql`
    mutation pauseMaterial($id: ID!) {
  changeStatusMaterial(input: {id: $id, transition: "pause"}) {
    material {
      id
      status
    }
  }
}
    `;
export type PauseMaterialMutationFn = Apollo.MutationFunction<PauseMaterialMutation, PauseMaterialMutationVariables>;

/**
 * __usePauseMaterialMutation__
 *
 * To run a mutation, you first call `usePauseMaterialMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `usePauseMaterialMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [pauseMaterialMutation, { data, loading, error }] = usePauseMaterialMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function usePauseMaterialMutation(baseOptions?: Apollo.MutationHookOptions<PauseMaterialMutation, PauseMaterialMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<PauseMaterialMutation, PauseMaterialMutationVariables>(PauseMaterialDocument, options);
      }
export type PauseMaterialMutationHookResult = ReturnType<typeof usePauseMaterialMutation>;
export type PauseMaterialMutationResult = Apollo.MutationResult<PauseMaterialMutation>;
export type PauseMaterialMutationOptions = Apollo.BaseMutationOptions<PauseMaterialMutation, PauseMaterialMutationVariables>;
export const PublishMaterialDocument = gql`
    mutation publishMaterial($id: ID!) {
  changeStatusMaterial(input: {id: $id, transition: "publish"}) {
    material {
      id
      status
    }
  }
}
    `;
export type PublishMaterialMutationFn = Apollo.MutationFunction<PublishMaterialMutation, PublishMaterialMutationVariables>;

/**
 * __usePublishMaterialMutation__
 *
 * To run a mutation, you first call `usePublishMaterialMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `usePublishMaterialMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [publishMaterialMutation, { data, loading, error }] = usePublishMaterialMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function usePublishMaterialMutation(baseOptions?: Apollo.MutationHookOptions<PublishMaterialMutation, PublishMaterialMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<PublishMaterialMutation, PublishMaterialMutationVariables>(PublishMaterialDocument, options);
      }
export type PublishMaterialMutationHookResult = ReturnType<typeof usePublishMaterialMutation>;
export type PublishMaterialMutationResult = Apollo.MutationResult<PublishMaterialMutation>;
export type PublishMaterialMutationOptions = Apollo.BaseMutationOptions<PublishMaterialMutation, PublishMaterialMutationVariables>;
export const CirclesDocument = gql`
    query circles($name: String, $indexable: Boolean, $afterCursor: String, $beforeCursor: String, $first: Int) {
  circles(
    name: $name
    indexable: $indexable
    after: $afterCursor
    before: $beforeCursor
    first: $first
  ) {
    edges {
      node {
        ...CircleList__Circle
      }
      cursor
    }
    ...CircleList_CircleCursorConnection
  }
}
    ${CircleList__CircleFragmentDoc}
${CircleList_CircleCursorConnectionFragmentDoc}`;

/**
 * __useCirclesQuery__
 *
 * To run a query within a React component, call `useCirclesQuery` and pass it any options that fit your needs.
 * When your component renders, `useCirclesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCirclesQuery({
 *   variables: {
 *      name: // value for 'name'
 *      indexable: // value for 'indexable'
 *      afterCursor: // value for 'afterCursor'
 *      beforeCursor: // value for 'beforeCursor'
 *      first: // value for 'first'
 *   },
 * });
 */
export function useCirclesQuery(baseOptions?: Apollo.QueryHookOptions<CirclesQuery, CirclesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CirclesQuery, CirclesQueryVariables>(CirclesDocument, options);
      }
export function useCirclesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CirclesQuery, CirclesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CirclesQuery, CirclesQueryVariables>(CirclesDocument, options);
        }
export type CirclesQueryHookResult = ReturnType<typeof useCirclesQuery>;
export type CirclesLazyQueryHookResult = ReturnType<typeof useCirclesLazyQuery>;
export type CirclesQueryResult = Apollo.QueryResult<CirclesQuery, CirclesQueryVariables>;
export const InvitationsDocument = gql`
    query invitations($invited: Boolean, $postalCode: String, $email: String, $afterCursor: String, $beforeCursor: String, $first: Int) {
  invitations(
    invited: $invited
    postalCode: $postalCode
    email: $email
    after: $afterCursor
    before: $beforeCursor
    first: $first
  ) {
    edges {
      node {
        ...InvitationList__Invitation
      }
      cursor
    }
    ...InvitationList_InvitationCursorConnection
  }
}
    ${InvitationList__InvitationFragmentDoc}
${InvitationList_InvitationCursorConnectionFragmentDoc}`;

/**
 * __useInvitationsQuery__
 *
 * To run a query within a React component, call `useInvitationsQuery` and pass it any options that fit your needs.
 * When your component renders, `useInvitationsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useInvitationsQuery({
 *   variables: {
 *      invited: // value for 'invited'
 *      postalCode: // value for 'postalCode'
 *      email: // value for 'email'
 *      afterCursor: // value for 'afterCursor'
 *      beforeCursor: // value for 'beforeCursor'
 *      first: // value for 'first'
 *   },
 * });
 */
export function useInvitationsQuery(baseOptions?: Apollo.QueryHookOptions<InvitationsQuery, InvitationsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<InvitationsQuery, InvitationsQueryVariables>(InvitationsDocument, options);
      }
export function useInvitationsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<InvitationsQuery, InvitationsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<InvitationsQuery, InvitationsQueryVariables>(InvitationsDocument, options);
        }
export type InvitationsQueryHookResult = ReturnType<typeof useInvitationsQuery>;
export type InvitationsLazyQueryHookResult = ReturnType<typeof useInvitationsLazyQuery>;
export type InvitationsQueryResult = Apollo.QueryResult<InvitationsQuery, InvitationsQueryVariables>;
export const DeleteInvitationDocument = gql`
    mutation deleteInvitation($id: ID!) {
  deleteInvitation(input: {id: $id}) {
    invitation {
      id
    }
  }
}
    `;
export type DeleteInvitationMutationFn = Apollo.MutationFunction<DeleteInvitationMutation, DeleteInvitationMutationVariables>;

/**
 * __useDeleteInvitationMutation__
 *
 * To run a mutation, you first call `useDeleteInvitationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteInvitationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteInvitationMutation, { data, loading, error }] = useDeleteInvitationMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteInvitationMutation(baseOptions?: Apollo.MutationHookOptions<DeleteInvitationMutation, DeleteInvitationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteInvitationMutation, DeleteInvitationMutationVariables>(DeleteInvitationDocument, options);
      }
export type DeleteInvitationMutationHookResult = ReturnType<typeof useDeleteInvitationMutation>;
export type DeleteInvitationMutationResult = Apollo.MutationResult<DeleteInvitationMutation>;
export type DeleteInvitationMutationOptions = Apollo.BaseMutationOptions<DeleteInvitationMutation, DeleteInvitationMutationVariables>;
export const SendInvitationDocument = gql`
    mutation sendInvitation($id: ID!) {
  sendInvitation(input: {id: $id}) {
    invitation {
      id
      invited
    }
  }
}
    `;
export type SendInvitationMutationFn = Apollo.MutationFunction<SendInvitationMutation, SendInvitationMutationVariables>;

/**
 * __useSendInvitationMutation__
 *
 * To run a mutation, you first call `useSendInvitationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSendInvitationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [sendInvitationMutation, { data, loading, error }] = useSendInvitationMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useSendInvitationMutation(baseOptions?: Apollo.MutationHookOptions<SendInvitationMutation, SendInvitationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SendInvitationMutation, SendInvitationMutationVariables>(SendInvitationDocument, options);
      }
export type SendInvitationMutationHookResult = ReturnType<typeof useSendInvitationMutation>;
export type SendInvitationMutationResult = Apollo.MutationResult<SendInvitationMutation>;
export type SendInvitationMutationOptions = Apollo.BaseMutationOptions<SendInvitationMutation, SendInvitationMutationVariables>;
export const UsersDocument = gql`
    query users($lastname: String, $firstname: String, $confirmed: Boolean, $postalCode: String, $email: String, $afterCursor: String, $beforeCursor: String, $first: Int) {
  users(
    firstname: $firstname
    lastname: $lastname
    confirmed: $confirmed
    postalCode: $postalCode
    email: $email
    after: $afterCursor
    before: $beforeCursor
    first: $first
  ) {
    edges {
      node {
        ...UserList__User
      }
      cursor
    }
    ...UserList_UserCursorConnection
  }
}
    ${UserList__UserFragmentDoc}
${UserList_UserCursorConnectionFragmentDoc}`;

/**
 * __useUsersQuery__
 *
 * To run a query within a React component, call `useUsersQuery` and pass it any options that fit your needs.
 * When your component renders, `useUsersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUsersQuery({
 *   variables: {
 *      lastname: // value for 'lastname'
 *      firstname: // value for 'firstname'
 *      confirmed: // value for 'confirmed'
 *      postalCode: // value for 'postalCode'
 *      email: // value for 'email'
 *      afterCursor: // value for 'afterCursor'
 *      beforeCursor: // value for 'beforeCursor'
 *      first: // value for 'first'
 *   },
 * });
 */
export function useUsersQuery(baseOptions?: Apollo.QueryHookOptions<UsersQuery, UsersQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<UsersQuery, UsersQueryVariables>(UsersDocument, options);
      }
export function useUsersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<UsersQuery, UsersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<UsersQuery, UsersQueryVariables>(UsersDocument, options);
        }
export type UsersQueryHookResult = ReturnType<typeof useUsersQuery>;
export type UsersLazyQueryHookResult = ReturnType<typeof useUsersLazyQuery>;
export type UsersQueryResult = Apollo.QueryResult<UsersQuery, UsersQueryVariables>;
export const StatisticsDocument = gql`
    query statistics {
  users {
    totalCount
  }
  materials {
    totalCount
  }
  materialBookings {
    totalCount
  }
  circles {
    totalCount
  }
  invitations {
    totalCount
  }
}
    `;

/**
 * __useStatisticsQuery__
 *
 * To run a query within a React component, call `useStatisticsQuery` and pass it any options that fit your needs.
 * When your component renders, `useStatisticsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useStatisticsQuery({
 *   variables: {
 *   },
 * });
 */
export function useStatisticsQuery(baseOptions?: Apollo.QueryHookOptions<StatisticsQuery, StatisticsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<StatisticsQuery, StatisticsQueryVariables>(StatisticsDocument, options);
      }
export function useStatisticsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<StatisticsQuery, StatisticsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<StatisticsQuery, StatisticsQueryVariables>(StatisticsDocument, options);
        }
export type StatisticsQueryHookResult = ReturnType<typeof useStatisticsQuery>;
export type StatisticsLazyQueryHookResult = ReturnType<typeof useStatisticsLazyQuery>;
export type StatisticsQueryResult = Apollo.QueryResult<StatisticsQuery, StatisticsQueryVariables>;
export const PricingView_PlansDocument = gql`
    query PricingView_Plans {
  plans {
    id
    name
    slug
    description
    price
    condition
    button
    features {
      ...PricingView_Plans__Features
    }
  }
}
    ${PricingView_Plans__FeaturesFragmentDoc}`;

/**
 * __usePricingView_PlansQuery__
 *
 * To run a query within a React component, call `usePricingView_PlansQuery` and pass it any options that fit your needs.
 * When your component renders, `usePricingView_PlansQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePricingView_PlansQuery({
 *   variables: {
 *   },
 * });
 */
export function usePricingView_PlansQuery(baseOptions?: Apollo.QueryHookOptions<PricingView_PlansQuery, PricingView_PlansQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<PricingView_PlansQuery, PricingView_PlansQueryVariables>(PricingView_PlansDocument, options);
      }
export function usePricingView_PlansLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<PricingView_PlansQuery, PricingView_PlansQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<PricingView_PlansQuery, PricingView_PlansQueryVariables>(PricingView_PlansDocument, options);
        }
export type PricingView_PlansQueryHookResult = ReturnType<typeof usePricingView_PlansQuery>;
export type PricingView_PlansLazyQueryHookResult = ReturnType<typeof usePricingView_PlansLazyQuery>;
export type PricingView_PlansQueryResult = Apollo.QueryResult<PricingView_PlansQuery, PricingView_PlansQueryVariables>;
export const UserPreferencesDocument = gql`
    query userPreferences($id: ID!) {
  user(id: $id) {
    ...UserPreferences__User
  }
}
    ${UserPreferences__UserFragmentDoc}`;

/**
 * __useUserPreferencesQuery__
 *
 * To run a query within a React component, call `useUserPreferencesQuery` and pass it any options that fit your needs.
 * When your component renders, `useUserPreferencesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUserPreferencesQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useUserPreferencesQuery(baseOptions: Apollo.QueryHookOptions<UserPreferencesQuery, UserPreferencesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<UserPreferencesQuery, UserPreferencesQueryVariables>(UserPreferencesDocument, options);
      }
export function useUserPreferencesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<UserPreferencesQuery, UserPreferencesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<UserPreferencesQuery, UserPreferencesQueryVariables>(UserPreferencesDocument, options);
        }
export type UserPreferencesQueryHookResult = ReturnType<typeof useUserPreferencesQuery>;
export type UserPreferencesLazyQueryHookResult = ReturnType<typeof useUserPreferencesLazyQuery>;
export type UserPreferencesQueryResult = Apollo.QueryResult<UserPreferencesQuery, UserPreferencesQueryVariables>;
export const UpdateUserDocument = gql`
    mutation updateUser($input: updateUserInput!) {
  updateUser(input: $input) {
    user {
      ...AuthUser__User
    }
  }
}
    ${AuthUser__UserFragmentDoc}`;
export type UpdateUserMutationFn = Apollo.MutationFunction<UpdateUserMutation, UpdateUserMutationVariables>;

/**
 * __useUpdateUserMutation__
 *
 * To run a mutation, you first call `useUpdateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUserMutation, { data, loading, error }] = useUpdateUserMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateUserMutation(baseOptions?: Apollo.MutationHookOptions<UpdateUserMutation, UpdateUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateUserMutation, UpdateUserMutationVariables>(UpdateUserDocument, options);
      }
export type UpdateUserMutationHookResult = ReturnType<typeof useUpdateUserMutation>;
export type UpdateUserMutationResult = Apollo.MutationResult<UpdateUserMutation>;
export type UpdateUserMutationOptions = Apollo.BaseMutationOptions<UpdateUserMutation, UpdateUserMutationVariables>;
export const UserProfileDocument = gql`
    query userProfile($id: ID!) {
  user(id: $id) {
    ...UserProfile__User
  }
}
    ${UserProfile__UserFragmentDoc}`;

/**
 * __useUserProfileQuery__
 *
 * To run a query within a React component, call `useUserProfileQuery` and pass it any options that fit your needs.
 * When your component renders, `useUserProfileQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useUserProfileQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useUserProfileQuery(baseOptions: Apollo.QueryHookOptions<UserProfileQuery, UserProfileQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<UserProfileQuery, UserProfileQueryVariables>(UserProfileDocument, options);
      }
export function useUserProfileLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<UserProfileQuery, UserProfileQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<UserProfileQuery, UserProfileQueryVariables>(UserProfileDocument, options);
        }
export type UserProfileQueryHookResult = ReturnType<typeof useUserProfileQuery>;
export type UserProfileLazyQueryHookResult = ReturnType<typeof useUserProfileLazyQuery>;
export type UserProfileQueryResult = Apollo.QueryResult<UserProfileQuery, UserProfileQueryVariables>;
export const InvitationDocument = gql`
    query invitation($id: ID!) {
  invitation(id: $id) {
    ...Register__Invitation
  }
}
    ${Register__InvitationFragmentDoc}`;

/**
 * __useInvitationQuery__
 *
 * To run a query within a React component, call `useInvitationQuery` and pass it any options that fit your needs.
 * When your component renders, `useInvitationQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useInvitationQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useInvitationQuery(baseOptions: Apollo.QueryHookOptions<InvitationQuery, InvitationQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<InvitationQuery, InvitationQueryVariables>(InvitationDocument, options);
      }
export function useInvitationLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<InvitationQuery, InvitationQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<InvitationQuery, InvitationQueryVariables>(InvitationDocument, options);
        }
export type InvitationQueryHookResult = ReturnType<typeof useInvitationQuery>;
export type InvitationLazyQueryHookResult = ReturnType<typeof useInvitationLazyQuery>;
export type InvitationQueryResult = Apollo.QueryResult<InvitationQuery, InvitationQueryVariables>;
export const RegisterWizardHomeViewDocument = gql`
    mutation RegisterWizardHomeView($input: createUserInput!) {
  createUser(input: $input) {
    user {
      id
      firstname
      lastname
      confirmed
      confirmationToken
      ...AuthUser__User
    }
  }
}
    ${AuthUser__UserFragmentDoc}`;
export type RegisterWizardHomeViewMutationFn = Apollo.MutationFunction<RegisterWizardHomeViewMutation, RegisterWizardHomeViewMutationVariables>;

/**
 * __useRegisterWizardHomeViewMutation__
 *
 * To run a mutation, you first call `useRegisterWizardHomeViewMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterWizardHomeViewMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerWizardHomeViewMutation, { data, loading, error }] = useRegisterWizardHomeViewMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useRegisterWizardHomeViewMutation(baseOptions?: Apollo.MutationHookOptions<RegisterWizardHomeViewMutation, RegisterWizardHomeViewMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RegisterWizardHomeViewMutation, RegisterWizardHomeViewMutationVariables>(RegisterWizardHomeViewDocument, options);
      }
export type RegisterWizardHomeViewMutationHookResult = ReturnType<typeof useRegisterWizardHomeViewMutation>;
export type RegisterWizardHomeViewMutationResult = Apollo.MutationResult<RegisterWizardHomeViewMutation>;
export type RegisterWizardHomeViewMutationOptions = Apollo.BaseMutationOptions<RegisterWizardHomeViewMutation, RegisterWizardHomeViewMutationVariables>;
export const RegisterWizardHomeView_FindUserByEmailDocument = gql`
    query RegisterWizardHomeView_findUserByEmail($email: String) {
  users(email: $email) {
    totalCount
  }
}
    `;

/**
 * __useRegisterWizardHomeView_FindUserByEmailQuery__
 *
 * To run a query within a React component, call `useRegisterWizardHomeView_FindUserByEmailQuery` and pass it any options that fit your needs.
 * When your component renders, `useRegisterWizardHomeView_FindUserByEmailQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useRegisterWizardHomeView_FindUserByEmailQuery({
 *   variables: {
 *      email: // value for 'email'
 *   },
 * });
 */
export function useRegisterWizardHomeView_FindUserByEmailQuery(baseOptions?: Apollo.QueryHookOptions<RegisterWizardHomeView_FindUserByEmailQuery, RegisterWizardHomeView_FindUserByEmailQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<RegisterWizardHomeView_FindUserByEmailQuery, RegisterWizardHomeView_FindUserByEmailQueryVariables>(RegisterWizardHomeView_FindUserByEmailDocument, options);
      }
export function useRegisterWizardHomeView_FindUserByEmailLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<RegisterWizardHomeView_FindUserByEmailQuery, RegisterWizardHomeView_FindUserByEmailQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<RegisterWizardHomeView_FindUserByEmailQuery, RegisterWizardHomeView_FindUserByEmailQueryVariables>(RegisterWizardHomeView_FindUserByEmailDocument, options);
        }
export type RegisterWizardHomeView_FindUserByEmailQueryHookResult = ReturnType<typeof useRegisterWizardHomeView_FindUserByEmailQuery>;
export type RegisterWizardHomeView_FindUserByEmailLazyQueryHookResult = ReturnType<typeof useRegisterWizardHomeView_FindUserByEmailLazyQuery>;
export type RegisterWizardHomeView_FindUserByEmailQueryResult = Apollo.QueryResult<RegisterWizardHomeView_FindUserByEmailQuery, RegisterWizardHomeView_FindUserByEmailQueryVariables>;
export const RegisterWizardHomeView_InvitationDocument = gql`
    query RegisterWizardHomeView_Invitation($id: ID!) {
  invitation(id: $id) {
    id
    email
    firstname
    lastname
    circle {
      slug
    }
  }
}
    `;

/**
 * __useRegisterWizardHomeView_InvitationQuery__
 *
 * To run a query within a React component, call `useRegisterWizardHomeView_InvitationQuery` and pass it any options that fit your needs.
 * When your component renders, `useRegisterWizardHomeView_InvitationQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useRegisterWizardHomeView_InvitationQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useRegisterWizardHomeView_InvitationQuery(baseOptions: Apollo.QueryHookOptions<RegisterWizardHomeView_InvitationQuery, RegisterWizardHomeView_InvitationQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<RegisterWizardHomeView_InvitationQuery, RegisterWizardHomeView_InvitationQueryVariables>(RegisterWizardHomeView_InvitationDocument, options);
      }
export function useRegisterWizardHomeView_InvitationLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<RegisterWizardHomeView_InvitationQuery, RegisterWizardHomeView_InvitationQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<RegisterWizardHomeView_InvitationQuery, RegisterWizardHomeView_InvitationQueryVariables>(RegisterWizardHomeView_InvitationDocument, options);
        }
export type RegisterWizardHomeView_InvitationQueryHookResult = ReturnType<typeof useRegisterWizardHomeView_InvitationQuery>;
export type RegisterWizardHomeView_InvitationLazyQueryHookResult = ReturnType<typeof useRegisterWizardHomeView_InvitationLazyQuery>;
export type RegisterWizardHomeView_InvitationQueryResult = Apollo.QueryResult<RegisterWizardHomeView_InvitationQuery, RegisterWizardHomeView_InvitationQueryVariables>;
export const CirclesToJoinForEmailDocument = gql`
    query CirclesToJoinForEmail($email: String) {
  circles(invitations_email: $email) {
    edges {
      node {
        id
        name
        slug
        invitations(email: $email) {
          edges {
            node {
              id
            }
          }
        }
      }
    }
  }
}
    `;

/**
 * __useCirclesToJoinForEmailQuery__
 *
 * To run a query within a React component, call `useCirclesToJoinForEmailQuery` and pass it any options that fit your needs.
 * When your component renders, `useCirclesToJoinForEmailQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCirclesToJoinForEmailQuery({
 *   variables: {
 *      email: // value for 'email'
 *   },
 * });
 */
export function useCirclesToJoinForEmailQuery(baseOptions?: Apollo.QueryHookOptions<CirclesToJoinForEmailQuery, CirclesToJoinForEmailQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CirclesToJoinForEmailQuery, CirclesToJoinForEmailQueryVariables>(CirclesToJoinForEmailDocument, options);
      }
export function useCirclesToJoinForEmailLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CirclesToJoinForEmailQuery, CirclesToJoinForEmailQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CirclesToJoinForEmailQuery, CirclesToJoinForEmailQueryVariables>(CirclesToJoinForEmailDocument, options);
        }
export type CirclesToJoinForEmailQueryHookResult = ReturnType<typeof useCirclesToJoinForEmailQuery>;
export type CirclesToJoinForEmailLazyQueryHookResult = ReturnType<typeof useCirclesToJoinForEmailLazyQuery>;
export type CirclesToJoinForEmailQueryResult = Apollo.QueryResult<CirclesToJoinForEmailQuery, CirclesToJoinForEmailQueryVariables>;
export const HandleCommunityInvitationDocument = gql`
    mutation HandleCommunityInvitation($id: ID!, $decision: String!) {
  handleInvitation(input: {id: $id, decision: $decision}) {
    invitation {
      email
      circle {
        id
        name
        slug
      }
    }
  }
}
    `;
export type HandleCommunityInvitationMutationFn = Apollo.MutationFunction<HandleCommunityInvitationMutation, HandleCommunityInvitationMutationVariables>;

/**
 * __useHandleCommunityInvitationMutation__
 *
 * To run a mutation, you first call `useHandleCommunityInvitationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useHandleCommunityInvitationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [handleCommunityInvitationMutation, { data, loading, error }] = useHandleCommunityInvitationMutation({
 *   variables: {
 *      id: // value for 'id'
 *      decision: // value for 'decision'
 *   },
 * });
 */
export function useHandleCommunityInvitationMutation(baseOptions?: Apollo.MutationHookOptions<HandleCommunityInvitationMutation, HandleCommunityInvitationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<HandleCommunityInvitationMutation, HandleCommunityInvitationMutationVariables>(HandleCommunityInvitationDocument, options);
      }
export type HandleCommunityInvitationMutationHookResult = ReturnType<typeof useHandleCommunityInvitationMutation>;
export type HandleCommunityInvitationMutationResult = Apollo.MutationResult<HandleCommunityInvitationMutation>;
export type HandleCommunityInvitationMutationOptions = Apollo.BaseMutationOptions<HandleCommunityInvitationMutation, HandleCommunityInvitationMutationVariables>;
export const CirclesMapViewDocument = gql`
    query CirclesMapView($afterCursor: String, $beforeCursor: String, $nbToFetch: Int, $searchTerm: String) {
  circles(
    first: $nbToFetch
    after: $afterCursor
    before: $beforeCursor
    searchTerms: $searchTerm
  ) {
    totalCount
    edges {
      node {
        ...CirclesMapView_Circle
      }
    }
  }
}
    ${CirclesMapView_CircleFragmentDoc}`;

/**
 * __useCirclesMapViewQuery__
 *
 * To run a query within a React component, call `useCirclesMapViewQuery` and pass it any options that fit your needs.
 * When your component renders, `useCirclesMapViewQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCirclesMapViewQuery({
 *   variables: {
 *      afterCursor: // value for 'afterCursor'
 *      beforeCursor: // value for 'beforeCursor'
 *      nbToFetch: // value for 'nbToFetch'
 *      searchTerm: // value for 'searchTerm'
 *   },
 * });
 */
export function useCirclesMapViewQuery(baseOptions?: Apollo.QueryHookOptions<CirclesMapViewQuery, CirclesMapViewQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CirclesMapViewQuery, CirclesMapViewQueryVariables>(CirclesMapViewDocument, options);
      }
export function useCirclesMapViewLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CirclesMapViewQuery, CirclesMapViewQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CirclesMapViewQuery, CirclesMapViewQueryVariables>(CirclesMapViewDocument, options);
        }
export type CirclesMapViewQueryHookResult = ReturnType<typeof useCirclesMapViewQuery>;
export type CirclesMapViewLazyQueryHookResult = ReturnType<typeof useCirclesMapViewLazyQuery>;
export type CirclesMapViewQueryResult = Apollo.QueryResult<CirclesMapViewQuery, CirclesMapViewQueryVariables>;
export const CircleShowView_CircleDocument = gql`
    query CircleShowView_circle($id: ID!, $statuses: [String]) {
  circle(id: $id) {
    id
    name
    activeUserActions
    description
    website
    slug
    indexable
    address
    postalCode
    city
    explicitLocationLabel
    latitude
    country
    address
    longitude
    logo {
      contentUrl
    }
    cover {
      contentUrl
    }
    ...InvitationFormDialog_Circle
    subscription {
      id
      status
      active
      plan {
        id
        name
      }
      startDate
      createdAt
      updatedAt
    }
    memberships(
      status_list: $statuses
      order: {permission: "ASC", user_firstname: "ASC", user_lastname: "ASC"}
    ) {
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
      edges {
        node {
          permission
          status
          user {
            id
            firstname
            email
            phoneNumber
            fullname
            avatar {
              contentUrl
            }
            slug
            averageRatingScore
            ratings {
              totalCount
            }
            roles
            userOwnerships {
              edges {
                node {
                  material {
                    id
                    slug
                    name
                  }
                  materialMainOwned {
                    id
                    slug
                    name
                  }
                }
              }
              totalCount
            }
          }
        }
      }
    }
  }
}
    ${InvitationFormDialog_CircleFragmentDoc}`;

/**
 * __useCircleShowView_CircleQuery__
 *
 * To run a query within a React component, call `useCircleShowView_CircleQuery` and pass it any options that fit your needs.
 * When your component renders, `useCircleShowView_CircleQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCircleShowView_CircleQuery({
 *   variables: {
 *      id: // value for 'id'
 *      statuses: // value for 'statuses'
 *   },
 * });
 */
export function useCircleShowView_CircleQuery(baseOptions: Apollo.QueryHookOptions<CircleShowView_CircleQuery, CircleShowView_CircleQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CircleShowView_CircleQuery, CircleShowView_CircleQueryVariables>(CircleShowView_CircleDocument, options);
      }
export function useCircleShowView_CircleLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CircleShowView_CircleQuery, CircleShowView_CircleQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CircleShowView_CircleQuery, CircleShowView_CircleQueryVariables>(CircleShowView_CircleDocument, options);
        }
export type CircleShowView_CircleQueryHookResult = ReturnType<typeof useCircleShowView_CircleQuery>;
export type CircleShowView_CircleLazyQueryHookResult = ReturnType<typeof useCircleShowView_CircleLazyQuery>;
export type CircleShowView_CircleQueryResult = Apollo.QueryResult<CircleShowView_CircleQuery, CircleShowView_CircleQueryVariables>;
export const JoinCommunityByInvitationLinkDocument = gql`
    mutation JoinCommunityByInvitationLink($invitationLinkId: String!) {
  joinCircleMembership(input: {invitationLinkId: $invitationLinkId}) {
    circleMembership {
      id
      status
      circle {
        id
        slug
      }
    }
  }
}
    `;
export type JoinCommunityByInvitationLinkMutationFn = Apollo.MutationFunction<JoinCommunityByInvitationLinkMutation, JoinCommunityByInvitationLinkMutationVariables>;

/**
 * __useJoinCommunityByInvitationLinkMutation__
 *
 * To run a mutation, you first call `useJoinCommunityByInvitationLinkMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useJoinCommunityByInvitationLinkMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [joinCommunityByInvitationLinkMutation, { data, loading, error }] = useJoinCommunityByInvitationLinkMutation({
 *   variables: {
 *      invitationLinkId: // value for 'invitationLinkId'
 *   },
 * });
 */
export function useJoinCommunityByInvitationLinkMutation(baseOptions?: Apollo.MutationHookOptions<JoinCommunityByInvitationLinkMutation, JoinCommunityByInvitationLinkMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<JoinCommunityByInvitationLinkMutation, JoinCommunityByInvitationLinkMutationVariables>(JoinCommunityByInvitationLinkDocument, options);
      }
export type JoinCommunityByInvitationLinkMutationHookResult = ReturnType<typeof useJoinCommunityByInvitationLinkMutation>;
export type JoinCommunityByInvitationLinkMutationResult = Apollo.MutationResult<JoinCommunityByInvitationLinkMutation>;
export type JoinCommunityByInvitationLinkMutationOptions = Apollo.BaseMutationOptions<JoinCommunityByInvitationLinkMutation, JoinCommunityByInvitationLinkMutationVariables>;
export const JoinCommunityInvitationDocument = gql`
    query JoinCommunityInvitation($id: ID!) {
  circleInvitationLink(id: $id) {
    id
    circle {
      ...JoinCommunityInvitation__Circle
    }
    createdAt
  }
}
    ${JoinCommunityInvitation__CircleFragmentDoc}`;

/**
 * __useJoinCommunityInvitationQuery__
 *
 * To run a query within a React component, call `useJoinCommunityInvitationQuery` and pass it any options that fit your needs.
 * When your component renders, `useJoinCommunityInvitationQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useJoinCommunityInvitationQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useJoinCommunityInvitationQuery(baseOptions: Apollo.QueryHookOptions<JoinCommunityInvitationQuery, JoinCommunityInvitationQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<JoinCommunityInvitationQuery, JoinCommunityInvitationQueryVariables>(JoinCommunityInvitationDocument, options);
      }
export function useJoinCommunityInvitationLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<JoinCommunityInvitationQuery, JoinCommunityInvitationQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<JoinCommunityInvitationQuery, JoinCommunityInvitationQueryVariables>(JoinCommunityInvitationDocument, options);
        }
export type JoinCommunityInvitationQueryHookResult = ReturnType<typeof useJoinCommunityInvitationQuery>;
export type JoinCommunityInvitationLazyQueryHookResult = ReturnType<typeof useJoinCommunityInvitationLazyQuery>;
export type JoinCommunityInvitationQueryResult = Apollo.QueryResult<JoinCommunityInvitationQuery, JoinCommunityInvitationQueryVariables>;
export const UpdateCircleDocument = gql`
    mutation updateCircle($input: updateCircleInput!) {
  updateCircle(input: $input) {
    circle {
      id
      name
      activeUserActions
      description
      website
      slug
      address
      postalCode
      city
      country
      latitude
      longitude
      indexable
      logo {
        id
        contentUrl
      }
      cover {
        id
        contentUrl
      }
    }
  }
}
    `;
export type UpdateCircleMutationFn = Apollo.MutationFunction<UpdateCircleMutation, UpdateCircleMutationVariables>;

/**
 * __useUpdateCircleMutation__
 *
 * To run a mutation, you first call `useUpdateCircleMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateCircleMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateCircleMutation, { data, loading, error }] = useUpdateCircleMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateCircleMutation(baseOptions?: Apollo.MutationHookOptions<UpdateCircleMutation, UpdateCircleMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateCircleMutation, UpdateCircleMutationVariables>(UpdateCircleDocument, options);
      }
export type UpdateCircleMutationHookResult = ReturnType<typeof useUpdateCircleMutation>;
export type UpdateCircleMutationResult = Apollo.MutationResult<UpdateCircleMutation>;
export type UpdateCircleMutationOptions = Apollo.BaseMutationOptions<UpdateCircleMutation, UpdateCircleMutationVariables>;
export const DeleteCircleDocument = gql`
    mutation deleteCircle($input: deleteCircleInput!) {
  deleteCircle(input: $input) {
    clientMutationId
    circle {
      id
    }
  }
}
    `;
export type DeleteCircleMutationFn = Apollo.MutationFunction<DeleteCircleMutation, DeleteCircleMutationVariables>;

/**
 * __useDeleteCircleMutation__
 *
 * To run a mutation, you first call `useDeleteCircleMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteCircleMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteCircleMutation, { data, loading, error }] = useDeleteCircleMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteCircleMutation(baseOptions?: Apollo.MutationHookOptions<DeleteCircleMutation, DeleteCircleMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteCircleMutation, DeleteCircleMutationVariables>(DeleteCircleDocument, options);
      }
export type DeleteCircleMutationHookResult = ReturnType<typeof useDeleteCircleMutation>;
export type DeleteCircleMutationResult = Apollo.MutationResult<DeleteCircleMutation>;
export type DeleteCircleMutationOptions = Apollo.BaseMutationOptions<DeleteCircleMutation, DeleteCircleMutationVariables>;
export const GetUserCirclesDocument = gql`
    query getUserCircles($id: ID!, $afterCursor: String, $beforeCursor: String, $nbToFetch: Int, $statusList: [String]) {
  user(id: $id) {
    circleMemberships(
      first: $nbToFetch
      after: $afterCursor
      before: $beforeCursor
      status_list: $statusList
    ) {
      edges {
        node {
          ...CircleMembershipsTable__CircleMembership
          circle {
            ...CommunityCard_Community
          }
        }
        cursor
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
      totalCount
    }
  }
}
    ${CircleMembershipsTable__CircleMembershipFragmentDoc}
${CommunityCard_CommunityFragmentDoc}`;

/**
 * __useGetUserCirclesQuery__
 *
 * To run a query within a React component, call `useGetUserCirclesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserCirclesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetUserCirclesQuery({
 *   variables: {
 *      id: // value for 'id'
 *      afterCursor: // value for 'afterCursor'
 *      beforeCursor: // value for 'beforeCursor'
 *      nbToFetch: // value for 'nbToFetch'
 *      statusList: // value for 'statusList'
 *   },
 * });
 */
export function useGetUserCirclesQuery(baseOptions: Apollo.QueryHookOptions<GetUserCirclesQuery, GetUserCirclesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetUserCirclesQuery, GetUserCirclesQueryVariables>(GetUserCirclesDocument, options);
      }
export function useGetUserCirclesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetUserCirclesQuery, GetUserCirclesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetUserCirclesQuery, GetUserCirclesQueryVariables>(GetUserCirclesDocument, options);
        }
export type GetUserCirclesQueryHookResult = ReturnType<typeof useGetUserCirclesQuery>;
export type GetUserCirclesLazyQueryHookResult = ReturnType<typeof useGetUserCirclesLazyQuery>;
export type GetUserCirclesQueryResult = Apollo.QueryResult<GetUserCirclesQuery, GetUserCirclesQueryVariables>;
export const HomeLastMaterialsDocument = gql`
    query HomeLastMaterials($first: Int!) {
  materials(first: $first, order: [{createdAt: "desc"}]) {
    edges {
      node {
        ...MaterialCard__Material
        description
      }
    }
  }
}
    ${MaterialCard__MaterialFragmentDoc}`;

/**
 * __useHomeLastMaterialsQuery__
 *
 * To run a query within a React component, call `useHomeLastMaterialsQuery` and pass it any options that fit your needs.
 * When your component renders, `useHomeLastMaterialsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useHomeLastMaterialsQuery({
 *   variables: {
 *      first: // value for 'first'
 *   },
 * });
 */
export function useHomeLastMaterialsQuery(baseOptions: Apollo.QueryHookOptions<HomeLastMaterialsQuery, HomeLastMaterialsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<HomeLastMaterialsQuery, HomeLastMaterialsQueryVariables>(HomeLastMaterialsDocument, options);
      }
export function useHomeLastMaterialsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<HomeLastMaterialsQuery, HomeLastMaterialsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<HomeLastMaterialsQuery, HomeLastMaterialsQueryVariables>(HomeLastMaterialsDocument, options);
        }
export type HomeLastMaterialsQueryHookResult = ReturnType<typeof useHomeLastMaterialsQuery>;
export type HomeLastMaterialsLazyQueryHookResult = ReturnType<typeof useHomeLastMaterialsLazyQuery>;
export type HomeLastMaterialsQueryResult = Apollo.QueryResult<HomeLastMaterialsQuery, HomeLastMaterialsQueryVariables>;
export const SearchMaterialsDocument = gql`
    query searchMaterials($searchTerms: String!, $afterCursor: String, $beforeCursor: String, $nbToFetch: Int) {
  searchMaterials(
    first: $nbToFetch
    after: $afterCursor
    searchTerms: $searchTerms
    before: $beforeCursor
    order: [{updatedAt: "DESC"}, {name: "ASC"}]
  ) {
    totalCount
    edges {
      cursor
      node {
        ...SearchMaterial
      }
    }
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
  materialSearchAlerts(name: $searchTerms) {
    edges {
      node {
        ...SearchAlert
      }
    }
  }
}
    ${SearchMaterialFragmentDoc}
${SearchAlertFragmentDoc}`;

/**
 * __useSearchMaterialsQuery__
 *
 * To run a query within a React component, call `useSearchMaterialsQuery` and pass it any options that fit your needs.
 * When your component renders, `useSearchMaterialsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSearchMaterialsQuery({
 *   variables: {
 *      searchTerms: // value for 'searchTerms'
 *      afterCursor: // value for 'afterCursor'
 *      beforeCursor: // value for 'beforeCursor'
 *      nbToFetch: // value for 'nbToFetch'
 *   },
 * });
 */
export function useSearchMaterialsQuery(baseOptions: Apollo.QueryHookOptions<SearchMaterialsQuery, SearchMaterialsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SearchMaterialsQuery, SearchMaterialsQueryVariables>(SearchMaterialsDocument, options);
      }
export function useSearchMaterialsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SearchMaterialsQuery, SearchMaterialsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SearchMaterialsQuery, SearchMaterialsQueryVariables>(SearchMaterialsDocument, options);
        }
export type SearchMaterialsQueryHookResult = ReturnType<typeof useSearchMaterialsQuery>;
export type SearchMaterialsLazyQueryHookResult = ReturnType<typeof useSearchMaterialsLazyQuery>;
export type SearchMaterialsQueryResult = Apollo.QueryResult<SearchMaterialsQuery, SearchMaterialsQueryVariables>;
export const DeleteMaterialDocument = gql`
    mutation deleteMaterial($id: ID!) {
  deleteMaterial(input: {id: $id}) {
    clientMutationId
  }
}
    `;
export type DeleteMaterialMutationFn = Apollo.MutationFunction<DeleteMaterialMutation, DeleteMaterialMutationVariables>;

/**
 * __useDeleteMaterialMutation__
 *
 * To run a mutation, you first call `useDeleteMaterialMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteMaterialMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteMaterialMutation, { data, loading, error }] = useDeleteMaterialMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteMaterialMutation(baseOptions?: Apollo.MutationHookOptions<DeleteMaterialMutation, DeleteMaterialMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteMaterialMutation, DeleteMaterialMutationVariables>(DeleteMaterialDocument, options);
      }
export type DeleteMaterialMutationHookResult = ReturnType<typeof useDeleteMaterialMutation>;
export type DeleteMaterialMutationResult = Apollo.MutationResult<DeleteMaterialMutation>;
export type DeleteMaterialMutationOptions = Apollo.BaseMutationOptions<DeleteMaterialMutation, DeleteMaterialMutationVariables>;
export const EditMaterialDocument = gql`
    query editMaterial($id: ID!) {
  material(id: $id) {
    ...MaterialForm__Material
  }
}
    ${MaterialForm__MaterialFragmentDoc}`;

/**
 * __useEditMaterialQuery__
 *
 * To run a query within a React component, call `useEditMaterialQuery` and pass it any options that fit your needs.
 * When your component renders, `useEditMaterialQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useEditMaterialQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useEditMaterialQuery(baseOptions: Apollo.QueryHookOptions<EditMaterialQuery, EditMaterialQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<EditMaterialQuery, EditMaterialQueryVariables>(EditMaterialDocument, options);
      }
export function useEditMaterialLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<EditMaterialQuery, EditMaterialQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<EditMaterialQuery, EditMaterialQueryVariables>(EditMaterialDocument, options);
        }
export type EditMaterialQueryHookResult = ReturnType<typeof useEditMaterialQuery>;
export type EditMaterialLazyQueryHookResult = ReturnType<typeof useEditMaterialLazyQuery>;
export type EditMaterialQueryResult = Apollo.QueryResult<EditMaterialQuery, EditMaterialQueryVariables>;
export const UpdateMaterialDocument = gql`
    mutation updateMaterial($input: updateMaterialInput!) {
  updateMaterial(input: $input) {
    material {
      id
      slug
      pricings {
        edges {
          node {
            id
            value
            circle {
              slug
            }
          }
        }
      }
    }
  }
}
    `;
export type UpdateMaterialMutationFn = Apollo.MutationFunction<UpdateMaterialMutation, UpdateMaterialMutationVariables>;

/**
 * __useUpdateMaterialMutation__
 *
 * To run a mutation, you first call `useUpdateMaterialMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateMaterialMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateMaterialMutation, { data, loading, error }] = useUpdateMaterialMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateMaterialMutation(baseOptions?: Apollo.MutationHookOptions<UpdateMaterialMutation, UpdateMaterialMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateMaterialMutation, UpdateMaterialMutationVariables>(UpdateMaterialDocument, options);
      }
export type UpdateMaterialMutationHookResult = ReturnType<typeof useUpdateMaterialMutation>;
export type UpdateMaterialMutationResult = Apollo.MutationResult<UpdateMaterialMutation>;
export type UpdateMaterialMutationOptions = Apollo.BaseMutationOptions<UpdateMaterialMutation, UpdateMaterialMutationVariables>;
export const MyAlertsDocument = gql`
    query MyAlerts($afterCursor: String, $beforeCursor: String, $first: Int) {
  materialSearchAlerts(after: $afterCursor, before: $beforeCursor, first: $first) {
    pageInfo {
      startCursor
      hasNextPage
      endCursor
      hasPreviousPage
    }
    totalCount
    edges {
      cursor
      node {
        name
        id
        _id
      }
    }
  }
}
    `;

/**
 * __useMyAlertsQuery__
 *
 * To run a query within a React component, call `useMyAlertsQuery` and pass it any options that fit your needs.
 * When your component renders, `useMyAlertsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMyAlertsQuery({
 *   variables: {
 *      afterCursor: // value for 'afterCursor'
 *      beforeCursor: // value for 'beforeCursor'
 *      first: // value for 'first'
 *   },
 * });
 */
export function useMyAlertsQuery(baseOptions?: Apollo.QueryHookOptions<MyAlertsQuery, MyAlertsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MyAlertsQuery, MyAlertsQueryVariables>(MyAlertsDocument, options);
      }
export function useMyAlertsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MyAlertsQuery, MyAlertsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MyAlertsQuery, MyAlertsQueryVariables>(MyAlertsDocument, options);
        }
export type MyAlertsQueryHookResult = ReturnType<typeof useMyAlertsQuery>;
export type MyAlertsLazyQueryHookResult = ReturnType<typeof useMyAlertsLazyQuery>;
export type MyAlertsQueryResult = Apollo.QueryResult<MyAlertsQuery, MyAlertsQueryVariables>;
export const DeleteAlertDocument = gql`
    mutation DeleteAlert($id: ID!) {
  deleteMaterialSearchAlert(input: {id: $id}) {
    __typename
  }
}
    `;
export type DeleteAlertMutationFn = Apollo.MutationFunction<DeleteAlertMutation, DeleteAlertMutationVariables>;

/**
 * __useDeleteAlertMutation__
 *
 * To run a mutation, you first call `useDeleteAlertMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteAlertMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteAlertMutation, { data, loading, error }] = useDeleteAlertMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteAlertMutation(baseOptions?: Apollo.MutationHookOptions<DeleteAlertMutation, DeleteAlertMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteAlertMutation, DeleteAlertMutationVariables>(DeleteAlertDocument, options);
      }
export type DeleteAlertMutationHookResult = ReturnType<typeof useDeleteAlertMutation>;
export type DeleteAlertMutationResult = Apollo.MutationResult<DeleteAlertMutation>;
export type DeleteAlertMutationOptions = Apollo.BaseMutationOptions<DeleteAlertMutation, DeleteAlertMutationVariables>;
export const MaterialBookingsForApplicantDocument = gql`
    query materialBookingsForApplicant($slug: String!, $statuses: [String]) {
  materialBookings(user_slug: $slug, status_list: $statuses) {
    ...BookingTable__MaterialBookingCursorConnection
  }
}
    ${BookingTable__MaterialBookingCursorConnectionFragmentDoc}`;

/**
 * __useMaterialBookingsForApplicantQuery__
 *
 * To run a query within a React component, call `useMaterialBookingsForApplicantQuery` and pass it any options that fit your needs.
 * When your component renders, `useMaterialBookingsForApplicantQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMaterialBookingsForApplicantQuery({
 *   variables: {
 *      slug: // value for 'slug'
 *      statuses: // value for 'statuses'
 *   },
 * });
 */
export function useMaterialBookingsForApplicantQuery(baseOptions: Apollo.QueryHookOptions<MaterialBookingsForApplicantQuery, MaterialBookingsForApplicantQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MaterialBookingsForApplicantQuery, MaterialBookingsForApplicantQueryVariables>(MaterialBookingsForApplicantDocument, options);
      }
export function useMaterialBookingsForApplicantLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MaterialBookingsForApplicantQuery, MaterialBookingsForApplicantQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MaterialBookingsForApplicantQuery, MaterialBookingsForApplicantQueryVariables>(MaterialBookingsForApplicantDocument, options);
        }
export type MaterialBookingsForApplicantQueryHookResult = ReturnType<typeof useMaterialBookingsForApplicantQuery>;
export type MaterialBookingsForApplicantLazyQueryHookResult = ReturnType<typeof useMaterialBookingsForApplicantLazyQuery>;
export type MaterialBookingsForApplicantQueryResult = Apollo.QueryResult<MaterialBookingsForApplicantQuery, MaterialBookingsForApplicantQueryVariables>;
export const MaterialBookingsForOwnerDocument = gql`
    query materialBookingsForOwner($statuses: [String]) {
  asOwnerMaterialBookings(status_list: $statuses) {
    ...BookingTable__MaterialBookingCursorConnection
  }
}
    ${BookingTable__MaterialBookingCursorConnectionFragmentDoc}`;

/**
 * __useMaterialBookingsForOwnerQuery__
 *
 * To run a query within a React component, call `useMaterialBookingsForOwnerQuery` and pass it any options that fit your needs.
 * When your component renders, `useMaterialBookingsForOwnerQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMaterialBookingsForOwnerQuery({
 *   variables: {
 *      statuses: // value for 'statuses'
 *   },
 * });
 */
export function useMaterialBookingsForOwnerQuery(baseOptions?: Apollo.QueryHookOptions<MaterialBookingsForOwnerQuery, MaterialBookingsForOwnerQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MaterialBookingsForOwnerQuery, MaterialBookingsForOwnerQueryVariables>(MaterialBookingsForOwnerDocument, options);
      }
export function useMaterialBookingsForOwnerLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MaterialBookingsForOwnerQuery, MaterialBookingsForOwnerQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MaterialBookingsForOwnerQuery, MaterialBookingsForOwnerQueryVariables>(MaterialBookingsForOwnerDocument, options);
        }
export type MaterialBookingsForOwnerQueryHookResult = ReturnType<typeof useMaterialBookingsForOwnerQuery>;
export type MaterialBookingsForOwnerLazyQueryHookResult = ReturnType<typeof useMaterialBookingsForOwnerLazyQuery>;
export type MaterialBookingsForOwnerQueryResult = Apollo.QueryResult<MaterialBookingsForOwnerQuery, MaterialBookingsForOwnerQueryVariables>;
export const MyBookingDocument = gql`
    query myBooking($id: ID!) {
  materialBooking(id: $id) {
    id
    slug
    startDate
    endDate
    status
    statusTransitionAvailables
    price
    discussion {
      id
      messages {
        edges {
          node {
            ...ChatBubble__Message
          }
        }
      }
    }
    user {
      id
      slug
      firstname
      lastname
      fullname
      email
      phoneNumber
      avatar {
        contentUrl
      }
      city
      roles
      gender
      averageRatingScore
      ratings {
        totalCount
      }
    }
    material {
      id
      slug
      name
      model
      brand
      reference
      createdAt
      updatedAt
      images {
        edges {
          node {
            id
            imageName
            size
            contentUrl
          }
        }
      }
      mainOwner {
        id
        slug
        firstname
        lastname
        fullname
        email
        phoneNumber
        avatar {
          contentUrl
        }
        streetAddress
        city
        roles
        gender
        averageRatingScore
        ratings {
          totalCount
        }
      }
      mainOwnershipUser {
        city
        slug
        firstname
        lastname
        fullname
        avatar {
          contentUrl
        }
      }
      mainOwnershipCircle {
        city
        slug
        name
        logo {
          contentUrl
        }
      }
    }
    periods {
      id
      startDate
      endDate
      price
    }
  }
}
    ${ChatBubble__MessageFragmentDoc}`;

/**
 * __useMyBookingQuery__
 *
 * To run a query within a React component, call `useMyBookingQuery` and pass it any options that fit your needs.
 * When your component renders, `useMyBookingQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMyBookingQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useMyBookingQuery(baseOptions: Apollo.QueryHookOptions<MyBookingQuery, MyBookingQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MyBookingQuery, MyBookingQueryVariables>(MyBookingDocument, options);
      }
export function useMyBookingLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MyBookingQuery, MyBookingQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MyBookingQuery, MyBookingQueryVariables>(MyBookingDocument, options);
        }
export type MyBookingQueryHookResult = ReturnType<typeof useMyBookingQuery>;
export type MyBookingLazyQueryHookResult = ReturnType<typeof useMyBookingLazyQuery>;
export type MyBookingQueryResult = Apollo.QueryResult<MyBookingQuery, MyBookingQueryVariables>;
export const MyListEditViewDocument = gql`
    query MyListEditView($id: ID!) {
  userList(id: $id) {
    id
    name
    users {
      totalCount
    }
  }
}
    `;

/**
 * __useMyListEditViewQuery__
 *
 * To run a query within a React component, call `useMyListEditViewQuery` and pass it any options that fit your needs.
 * When your component renders, `useMyListEditViewQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMyListEditViewQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useMyListEditViewQuery(baseOptions: Apollo.QueryHookOptions<MyListEditViewQuery, MyListEditViewQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MyListEditViewQuery, MyListEditViewQueryVariables>(MyListEditViewDocument, options);
      }
export function useMyListEditViewLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MyListEditViewQuery, MyListEditViewQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MyListEditViewQuery, MyListEditViewQueryVariables>(MyListEditViewDocument, options);
        }
export type MyListEditViewQueryHookResult = ReturnType<typeof useMyListEditViewQuery>;
export type MyListEditViewLazyQueryHookResult = ReturnType<typeof useMyListEditViewLazyQuery>;
export type MyListEditViewQueryResult = Apollo.QueryResult<MyListEditViewQuery, MyListEditViewQueryVariables>;
export const UpdateUserListDocument = gql`
    mutation UpdateUserList($input: updateUserListInput!) {
  updateUserList(input: $input) {
    userList {
      id
      slug
      owner {
        id
      }
      users {
        edges {
          node {
            slug
          }
        }
      }
    }
  }
}
    `;
export type UpdateUserListMutationFn = Apollo.MutationFunction<UpdateUserListMutation, UpdateUserListMutationVariables>;

/**
 * __useUpdateUserListMutation__
 *
 * To run a mutation, you first call `useUpdateUserListMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserListMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUserListMutation, { data, loading, error }] = useUpdateUserListMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateUserListMutation(baseOptions?: Apollo.MutationHookOptions<UpdateUserListMutation, UpdateUserListMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateUserListMutation, UpdateUserListMutationVariables>(UpdateUserListDocument, options);
      }
export type UpdateUserListMutationHookResult = ReturnType<typeof useUpdateUserListMutation>;
export type UpdateUserListMutationResult = Apollo.MutationResult<UpdateUserListMutation>;
export type UpdateUserListMutationOptions = Apollo.BaseMutationOptions<UpdateUserListMutation, UpdateUserListMutationVariables>;
export const MyListEdit_RemoveFromListDocument = gql`
    mutation MyListEdit_removeFromList($input: removeUserFromUserListInput!) {
  removeUserFromUserList(input: $input) {
    userList {
      id
      users {
        edges {
          node {
            id
            slug
          }
        }
      }
    }
  }
}
    `;
export type MyListEdit_RemoveFromListMutationFn = Apollo.MutationFunction<MyListEdit_RemoveFromListMutation, MyListEdit_RemoveFromListMutationVariables>;

/**
 * __useMyListEdit_RemoveFromListMutation__
 *
 * To run a mutation, you first call `useMyListEdit_RemoveFromListMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useMyListEdit_RemoveFromListMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [myListEditRemoveFromListMutation, { data, loading, error }] = useMyListEdit_RemoveFromListMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useMyListEdit_RemoveFromListMutation(baseOptions?: Apollo.MutationHookOptions<MyListEdit_RemoveFromListMutation, MyListEdit_RemoveFromListMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<MyListEdit_RemoveFromListMutation, MyListEdit_RemoveFromListMutationVariables>(MyListEdit_RemoveFromListDocument, options);
      }
export type MyListEdit_RemoveFromListMutationHookResult = ReturnType<typeof useMyListEdit_RemoveFromListMutation>;
export type MyListEdit_RemoveFromListMutationResult = Apollo.MutationResult<MyListEdit_RemoveFromListMutation>;
export type MyListEdit_RemoveFromListMutationOptions = Apollo.BaseMutationOptions<MyListEdit_RemoveFromListMutation, MyListEdit_RemoveFromListMutationVariables>;
export const MyListEdit_AddFromListDocument = gql`
    mutation MyListEdit_addFromList($input: addUserToUserListInput!) {
  addUserToUserList(input: $input) {
    userList {
      id
      users {
        edges {
          node {
            id
            slug
          }
        }
      }
    }
  }
}
    `;
export type MyListEdit_AddFromListMutationFn = Apollo.MutationFunction<MyListEdit_AddFromListMutation, MyListEdit_AddFromListMutationVariables>;

/**
 * __useMyListEdit_AddFromListMutation__
 *
 * To run a mutation, you first call `useMyListEdit_AddFromListMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useMyListEdit_AddFromListMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [myListEditAddFromListMutation, { data, loading, error }] = useMyListEdit_AddFromListMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useMyListEdit_AddFromListMutation(baseOptions?: Apollo.MutationHookOptions<MyListEdit_AddFromListMutation, MyListEdit_AddFromListMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<MyListEdit_AddFromListMutation, MyListEdit_AddFromListMutationVariables>(MyListEdit_AddFromListDocument, options);
      }
export type MyListEdit_AddFromListMutationHookResult = ReturnType<typeof useMyListEdit_AddFromListMutation>;
export type MyListEdit_AddFromListMutationResult = Apollo.MutationResult<MyListEdit_AddFromListMutation>;
export type MyListEdit_AddFromListMutationOptions = Apollo.BaseMutationOptions<MyListEdit_AddFromListMutation, MyListEdit_AddFromListMutationVariables>;
export const MyListsDocument = gql`
    query MyLists($afterCursor: String, $beforeCursor: String, $first: Int) {
  userLists(after: $afterCursor, before: $beforeCursor, first: $first) {
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
    totalCount
    edges {
      node {
        ...UserListTable_UserList
      }
    }
  }
}
    ${UserListTable_UserListFragmentDoc}`;

/**
 * __useMyListsQuery__
 *
 * To run a query within a React component, call `useMyListsQuery` and pass it any options that fit your needs.
 * When your component renders, `useMyListsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMyListsQuery({
 *   variables: {
 *      afterCursor: // value for 'afterCursor'
 *      beforeCursor: // value for 'beforeCursor'
 *      first: // value for 'first'
 *   },
 * });
 */
export function useMyListsQuery(baseOptions?: Apollo.QueryHookOptions<MyListsQuery, MyListsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MyListsQuery, MyListsQueryVariables>(MyListsDocument, options);
      }
export function useMyListsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MyListsQuery, MyListsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MyListsQuery, MyListsQueryVariables>(MyListsDocument, options);
        }
export type MyListsQueryHookResult = ReturnType<typeof useMyListsQuery>;
export type MyListsLazyQueryHookResult = ReturnType<typeof useMyListsLazyQuery>;
export type MyListsQueryResult = Apollo.QueryResult<MyListsQuery, MyListsQueryVariables>;
export const MyList_DeleteListDocument = gql`
    mutation MyList_DeleteList($id: ID!) {
  deleteUserList(input: {id: $id}) {
    clientMutationId
  }
}
    `;
export type MyList_DeleteListMutationFn = Apollo.MutationFunction<MyList_DeleteListMutation, MyList_DeleteListMutationVariables>;

/**
 * __useMyList_DeleteListMutation__
 *
 * To run a mutation, you first call `useMyList_DeleteListMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useMyList_DeleteListMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [myListDeleteListMutation, { data, loading, error }] = useMyList_DeleteListMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useMyList_DeleteListMutation(baseOptions?: Apollo.MutationHookOptions<MyList_DeleteListMutation, MyList_DeleteListMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<MyList_DeleteListMutation, MyList_DeleteListMutationVariables>(MyList_DeleteListDocument, options);
      }
export type MyList_DeleteListMutationHookResult = ReturnType<typeof useMyList_DeleteListMutation>;
export type MyList_DeleteListMutationResult = Apollo.MutationResult<MyList_DeleteListMutation>;
export type MyList_DeleteListMutationOptions = Apollo.BaseMutationOptions<MyList_DeleteListMutation, MyList_DeleteListMutationVariables>;
export const NewUserListModalDocument = gql`
    mutation NewUserListModal($input: createUserListInput!) {
  createUserList(input: $input) {
    userList {
      id
      name
      slug
    }
  }
}
    `;
export type NewUserListModalMutationFn = Apollo.MutationFunction<NewUserListModalMutation, NewUserListModalMutationVariables>;

/**
 * __useNewUserListModalMutation__
 *
 * To run a mutation, you first call `useNewUserListModalMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useNewUserListModalMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [newUserListModalMutation, { data, loading, error }] = useNewUserListModalMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useNewUserListModalMutation(baseOptions?: Apollo.MutationHookOptions<NewUserListModalMutation, NewUserListModalMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<NewUserListModalMutation, NewUserListModalMutationVariables>(NewUserListModalDocument, options);
      }
export type NewUserListModalMutationHookResult = ReturnType<typeof useNewUserListModalMutation>;
export type NewUserListModalMutationResult = Apollo.MutationResult<NewUserListModalMutation>;
export type NewUserListModalMutationOptions = Apollo.BaseMutationOptions<NewUserListModalMutation, NewUserListModalMutationVariables>;
export const CommentMaterialBookingDocument = gql`
    mutation commentMaterialBooking($id: ID!, $comment: String!) {
  commentMaterialBooking(input: {id: $id, comment: $comment}) {
    materialBooking {
      id
      comment
    }
  }
}
    `;
export type CommentMaterialBookingMutationFn = Apollo.MutationFunction<CommentMaterialBookingMutation, CommentMaterialBookingMutationVariables>;

/**
 * __useCommentMaterialBookingMutation__
 *
 * To run a mutation, you first call `useCommentMaterialBookingMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCommentMaterialBookingMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [commentMaterialBookingMutation, { data, loading, error }] = useCommentMaterialBookingMutation({
 *   variables: {
 *      id: // value for 'id'
 *      comment: // value for 'comment'
 *   },
 * });
 */
export function useCommentMaterialBookingMutation(baseOptions?: Apollo.MutationHookOptions<CommentMaterialBookingMutation, CommentMaterialBookingMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CommentMaterialBookingMutation, CommentMaterialBookingMutationVariables>(CommentMaterialBookingDocument, options);
      }
export type CommentMaterialBookingMutationHookResult = ReturnType<typeof useCommentMaterialBookingMutation>;
export type CommentMaterialBookingMutationResult = Apollo.MutationResult<CommentMaterialBookingMutation>;
export type CommentMaterialBookingMutationOptions = Apollo.BaseMutationOptions<CommentMaterialBookingMutation, CommentMaterialBookingMutationVariables>;
export const MaterialCalendarDocument = gql`
    query materialCalendar($id: ID!, $statuses: [String]) {
  material(id: $id) {
    id
    name
    bookings(status_list: $statuses) {
      edges {
        node {
          ...MaterialCalendar__MaterialBooking
        }
      }
    }
  }
}
    ${MaterialCalendar__MaterialBookingFragmentDoc}`;

/**
 * __useMaterialCalendarQuery__
 *
 * To run a query within a React component, call `useMaterialCalendarQuery` and pass it any options that fit your needs.
 * When your component renders, `useMaterialCalendarQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMaterialCalendarQuery({
 *   variables: {
 *      id: // value for 'id'
 *      statuses: // value for 'statuses'
 *   },
 * });
 */
export function useMaterialCalendarQuery(baseOptions: Apollo.QueryHookOptions<MaterialCalendarQuery, MaterialCalendarQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MaterialCalendarQuery, MaterialCalendarQueryVariables>(MaterialCalendarDocument, options);
      }
export function useMaterialCalendarLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MaterialCalendarQuery, MaterialCalendarQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MaterialCalendarQuery, MaterialCalendarQueryVariables>(MaterialCalendarDocument, options);
        }
export type MaterialCalendarQueryHookResult = ReturnType<typeof useMaterialCalendarQuery>;
export type MaterialCalendarLazyQueryHookResult = ReturnType<typeof useMaterialCalendarLazyQuery>;
export type MaterialCalendarQueryResult = Apollo.QueryResult<MaterialCalendarQuery, MaterialCalendarQueryVariables>;
export const MaterialBookingCalendarDocument = gql`
    query materialBookingCalendar($id: ID!) {
  materialBooking(id: $id) {
    ...MaterialCalendar__MaterialBooking
  }
}
    ${MaterialCalendar__MaterialBookingFragmentDoc}`;

/**
 * __useMaterialBookingCalendarQuery__
 *
 * To run a query within a React component, call `useMaterialBookingCalendarQuery` and pass it any options that fit your needs.
 * When your component renders, `useMaterialBookingCalendarQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMaterialBookingCalendarQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useMaterialBookingCalendarQuery(baseOptions: Apollo.QueryHookOptions<MaterialBookingCalendarQuery, MaterialBookingCalendarQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MaterialBookingCalendarQuery, MaterialBookingCalendarQueryVariables>(MaterialBookingCalendarDocument, options);
      }
export function useMaterialBookingCalendarLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MaterialBookingCalendarQuery, MaterialBookingCalendarQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MaterialBookingCalendarQuery, MaterialBookingCalendarQueryVariables>(MaterialBookingCalendarDocument, options);
        }
export type MaterialBookingCalendarQueryHookResult = ReturnType<typeof useMaterialBookingCalendarQuery>;
export type MaterialBookingCalendarLazyQueryHookResult = ReturnType<typeof useMaterialBookingCalendarLazyQuery>;
export type MaterialBookingCalendarQueryResult = Apollo.QueryResult<MaterialBookingCalendarQuery, MaterialBookingCalendarQueryVariables>;
export const UnavailableMaterialBookingDocument = gql`
    mutation unavailableMaterialBooking($materialId: String, $startDate: String, $endDate: String) {
  unavailableMaterialBooking(
    input: {materialId: $materialId, startDate: $startDate, endDate: $endDate}
  ) {
    materialBooking {
      id
      slug
      startDate
      endDate
      status
      price
      user {
        id
        slug
        firstname
        lastname
        fullname
        email
        roles
        avatar {
          contentUrl
        }
      }
      periods {
        id
        startDate
        endDate
        price
      }
      material {
        id
        slug
        name
        model
        brand
        reference
        createdAt
        updatedAt
        images {
          edges {
            node {
              id
              imageName
              size
              contentUrl
            }
          }
        }
      }
    }
  }
}
    `;
export type UnavailableMaterialBookingMutationFn = Apollo.MutationFunction<UnavailableMaterialBookingMutation, UnavailableMaterialBookingMutationVariables>;

/**
 * __useUnavailableMaterialBookingMutation__
 *
 * To run a mutation, you first call `useUnavailableMaterialBookingMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUnavailableMaterialBookingMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [unavailableMaterialBookingMutation, { data, loading, error }] = useUnavailableMaterialBookingMutation({
 *   variables: {
 *      materialId: // value for 'materialId'
 *      startDate: // value for 'startDate'
 *      endDate: // value for 'endDate'
 *   },
 * });
 */
export function useUnavailableMaterialBookingMutation(baseOptions?: Apollo.MutationHookOptions<UnavailableMaterialBookingMutation, UnavailableMaterialBookingMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UnavailableMaterialBookingMutation, UnavailableMaterialBookingMutationVariables>(UnavailableMaterialBookingDocument, options);
      }
export type UnavailableMaterialBookingMutationHookResult = ReturnType<typeof useUnavailableMaterialBookingMutation>;
export type UnavailableMaterialBookingMutationResult = Apollo.MutationResult<UnavailableMaterialBookingMutation>;
export type UnavailableMaterialBookingMutationOptions = Apollo.BaseMutationOptions<UnavailableMaterialBookingMutation, UnavailableMaterialBookingMutationVariables>;
export const MyMaterialsDocument = gql`
    query myMaterials($afterCursor: String, $beforeCursor: String, $nbToFetch: Int, $statusesList: [String]) {
  myMaterials(
    first: $nbToFetch
    after: $afterCursor
    before: $beforeCursor
    order: [{updatedAt: "DESC"}, {name: "ASC"}]
    status_list: $statusesList
  ) {
    totalCount
    edges {
      cursor
      node {
        ...MaterialTable_Material
        ...MaterialCard__Material_WithoutOwnership
      }
    }
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
    ${MaterialTable_MaterialFragmentDoc}
${MaterialCard__Material_WithoutOwnershipFragmentDoc}`;

/**
 * __useMyMaterialsQuery__
 *
 * To run a query within a React component, call `useMyMaterialsQuery` and pass it any options that fit your needs.
 * When your component renders, `useMyMaterialsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMyMaterialsQuery({
 *   variables: {
 *      afterCursor: // value for 'afterCursor'
 *      beforeCursor: // value for 'beforeCursor'
 *      nbToFetch: // value for 'nbToFetch'
 *      statusesList: // value for 'statusesList'
 *   },
 * });
 */
export function useMyMaterialsQuery(baseOptions?: Apollo.QueryHookOptions<MyMaterialsQuery, MyMaterialsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MyMaterialsQuery, MyMaterialsQueryVariables>(MyMaterialsDocument, options);
      }
export function useMyMaterialsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MyMaterialsQuery, MyMaterialsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MyMaterialsQuery, MyMaterialsQueryVariables>(MyMaterialsDocument, options);
        }
export type MyMaterialsQueryHookResult = ReturnType<typeof useMyMaterialsQuery>;
export type MyMaterialsLazyQueryHookResult = ReturnType<typeof useMyMaterialsLazyQuery>;
export type MyMaterialsQueryResult = Apollo.QueryResult<MyMaterialsQuery, MyMaterialsQueryVariables>;
export const ShowMaterialDocument = gql`
    query showMaterial($id: ID!, $statuses: [String]) {
  material(id: $id) {
    __typename
    id
    name
    slug
    description
    model
    brand
    reference
    createdAt
    updatedAt
    userActionAvailables
    mainOwner {
      ...UserCard__User
      ...ConfirmModal__User
      ...ContactUserModal__User
      id
    }
    status
    mainOwnership {
      ownerIRI
    }
    ownerships {
      ownerIRI
    }
    ...BookButton_Material
    category {
      id
      name
    }
    images {
      ...ShowMaterial__Images
    }
    bestPriceForUser
    pricings {
      edges {
        node {
          ...ShowMaterial__MaterialPricing
        }
      }
    }
    bookings(status_list: $statuses) {
      edges {
        node {
          ...BookingContext__MaterialBooking
        }
      }
    }
  }
}
    ${UserCard__UserFragmentDoc}
${ConfirmModal__UserFragmentDoc}
${ContactUserModal__UserFragmentDoc}
${BookButton_MaterialFragmentDoc}
${ShowMaterial__ImagesFragmentDoc}
${ShowMaterial__MaterialPricingFragmentDoc}
${BookingContext__MaterialBookingFragmentDoc}`;

/**
 * __useShowMaterialQuery__
 *
 * To run a query within a React component, call `useShowMaterialQuery` and pass it any options that fit your needs.
 * When your component renders, `useShowMaterialQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useShowMaterialQuery({
 *   variables: {
 *      id: // value for 'id'
 *      statuses: // value for 'statuses'
 *   },
 * });
 */
export function useShowMaterialQuery(baseOptions: Apollo.QueryHookOptions<ShowMaterialQuery, ShowMaterialQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ShowMaterialQuery, ShowMaterialQueryVariables>(ShowMaterialDocument, options);
      }
export function useShowMaterialLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ShowMaterialQuery, ShowMaterialQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ShowMaterialQuery, ShowMaterialQueryVariables>(ShowMaterialDocument, options);
        }
export type ShowMaterialQueryHookResult = ReturnType<typeof useShowMaterialQuery>;
export type ShowMaterialLazyQueryHookResult = ReturnType<typeof useShowMaterialLazyQuery>;
export type ShowMaterialQueryResult = Apollo.QueryResult<ShowMaterialQuery, ShowMaterialQueryVariables>;
export const ShowMaterial_MainOwnerUserDocument = gql`
    query ShowMaterial_MainOwnerUser($id: ID!) {
  user(id: $id) {
    ...MaterialOwnerCard_User
  }
}
    ${MaterialOwnerCard_UserFragmentDoc}`;

/**
 * __useShowMaterial_MainOwnerUserQuery__
 *
 * To run a query within a React component, call `useShowMaterial_MainOwnerUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useShowMaterial_MainOwnerUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useShowMaterial_MainOwnerUserQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useShowMaterial_MainOwnerUserQuery(baseOptions: Apollo.QueryHookOptions<ShowMaterial_MainOwnerUserQuery, ShowMaterial_MainOwnerUserQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ShowMaterial_MainOwnerUserQuery, ShowMaterial_MainOwnerUserQueryVariables>(ShowMaterial_MainOwnerUserDocument, options);
      }
export function useShowMaterial_MainOwnerUserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ShowMaterial_MainOwnerUserQuery, ShowMaterial_MainOwnerUserQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ShowMaterial_MainOwnerUserQuery, ShowMaterial_MainOwnerUserQueryVariables>(ShowMaterial_MainOwnerUserDocument, options);
        }
export type ShowMaterial_MainOwnerUserQueryHookResult = ReturnType<typeof useShowMaterial_MainOwnerUserQuery>;
export type ShowMaterial_MainOwnerUserLazyQueryHookResult = ReturnType<typeof useShowMaterial_MainOwnerUserLazyQuery>;
export type ShowMaterial_MainOwnerUserQueryResult = Apollo.QueryResult<ShowMaterial_MainOwnerUserQuery, ShowMaterial_MainOwnerUserQueryVariables>;
export const ShowMaterial_MainOwnerCircleDocument = gql`
    query ShowMaterial_MainOwnerCircle($id: ID!) {
  circle(id: $id) {
    ...MaterialOwnerCard_Circle
  }
}
    ${MaterialOwnerCard_CircleFragmentDoc}`;

/**
 * __useShowMaterial_MainOwnerCircleQuery__
 *
 * To run a query within a React component, call `useShowMaterial_MainOwnerCircleQuery` and pass it any options that fit your needs.
 * When your component renders, `useShowMaterial_MainOwnerCircleQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useShowMaterial_MainOwnerCircleQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useShowMaterial_MainOwnerCircleQuery(baseOptions: Apollo.QueryHookOptions<ShowMaterial_MainOwnerCircleQuery, ShowMaterial_MainOwnerCircleQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ShowMaterial_MainOwnerCircleQuery, ShowMaterial_MainOwnerCircleQueryVariables>(ShowMaterial_MainOwnerCircleDocument, options);
      }
export function useShowMaterial_MainOwnerCircleLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ShowMaterial_MainOwnerCircleQuery, ShowMaterial_MainOwnerCircleQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ShowMaterial_MainOwnerCircleQuery, ShowMaterial_MainOwnerCircleQueryVariables>(ShowMaterial_MainOwnerCircleDocument, options);
        }
export type ShowMaterial_MainOwnerCircleQueryHookResult = ReturnType<typeof useShowMaterial_MainOwnerCircleQuery>;
export type ShowMaterial_MainOwnerCircleLazyQueryHookResult = ReturnType<typeof useShowMaterial_MainOwnerCircleLazyQuery>;
export type ShowMaterial_MainOwnerCircleQueryResult = Apollo.QueryResult<ShowMaterial_MainOwnerCircleQuery, ShowMaterial_MainOwnerCircleQueryVariables>;
export const SubscribeViewDocument = gql`
    query SubscribeView($id: ID!, $first: Int, $after: String, $before: String, $last: Int) {
  plan(id: $id) {
    id
    name
    description
    price
    condition
    features {
      id
      name
      description
    }
  }
  myCircles(first: $first, after: $after, before: $before, last: $last) {
    edges {
      cursor
      node {
        id
        name
        slug
        description
        logo {
          imageName
          size
          contentUrl
        }
        cover {
          imageName
          size
          contentUrl
        }
        city
        memberships {
          totalCount
        }
        subscription {
          plan {
            id
            name
          }
          active
          id
        }
      }
    }
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
    `;

/**
 * __useSubscribeViewQuery__
 *
 * To run a query within a React component, call `useSubscribeViewQuery` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeViewQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeViewQuery({
 *   variables: {
 *      id: // value for 'id'
 *      first: // value for 'first'
 *      after: // value for 'after'
 *      before: // value for 'before'
 *      last: // value for 'last'
 *   },
 * });
 */
export function useSubscribeViewQuery(baseOptions: Apollo.QueryHookOptions<SubscribeViewQuery, SubscribeViewQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SubscribeViewQuery, SubscribeViewQueryVariables>(SubscribeViewDocument, options);
      }
export function useSubscribeViewLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SubscribeViewQuery, SubscribeViewQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SubscribeViewQuery, SubscribeViewQueryVariables>(SubscribeViewDocument, options);
        }
export type SubscribeViewQueryHookResult = ReturnType<typeof useSubscribeViewQuery>;
export type SubscribeViewLazyQueryHookResult = ReturnType<typeof useSubscribeViewLazyQuery>;
export type SubscribeViewQueryResult = Apollo.QueryResult<SubscribeViewQuery, SubscribeViewQueryVariables>;
export const SubscribeView_CreateCircleDocument = gql`
    mutation SubscribeView_CreateCircle($input: createCircleInput!) {
  createCircle(input: $input) {
    circle {
      id
      name
    }
  }
}
    `;
export type SubscribeView_CreateCircleMutationFn = Apollo.MutationFunction<SubscribeView_CreateCircleMutation, SubscribeView_CreateCircleMutationVariables>;

/**
 * __useSubscribeView_CreateCircleMutation__
 *
 * To run a mutation, you first call `useSubscribeView_CreateCircleMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSubscribeView_CreateCircleMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [subscribeViewCreateCircleMutation, { data, loading, error }] = useSubscribeView_CreateCircleMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useSubscribeView_CreateCircleMutation(baseOptions?: Apollo.MutationHookOptions<SubscribeView_CreateCircleMutation, SubscribeView_CreateCircleMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SubscribeView_CreateCircleMutation, SubscribeView_CreateCircleMutationVariables>(SubscribeView_CreateCircleDocument, options);
      }
export type SubscribeView_CreateCircleMutationHookResult = ReturnType<typeof useSubscribeView_CreateCircleMutation>;
export type SubscribeView_CreateCircleMutationResult = Apollo.MutationResult<SubscribeView_CreateCircleMutation>;
export type SubscribeView_CreateCircleMutationOptions = Apollo.BaseMutationOptions<SubscribeView_CreateCircleMutation, SubscribeView_CreateCircleMutationVariables>;
export const SubscribeView_SubscribeDocument = gql`
    mutation SubscribeView_Subscribe($input: createSubscriptionInput!) {
  createSubscription(input: $input) {
    subscription {
      id
    }
  }
}
    `;
export type SubscribeView_SubscribeMutationFn = Apollo.MutationFunction<SubscribeView_SubscribeMutation, SubscribeView_SubscribeMutationVariables>;

/**
 * __useSubscribeView_SubscribeMutation__
 *
 * To run a mutation, you first call `useSubscribeView_SubscribeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSubscribeView_SubscribeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [subscribeViewSubscribeMutation, { data, loading, error }] = useSubscribeView_SubscribeMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useSubscribeView_SubscribeMutation(baseOptions?: Apollo.MutationHookOptions<SubscribeView_SubscribeMutation, SubscribeView_SubscribeMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SubscribeView_SubscribeMutation, SubscribeView_SubscribeMutationVariables>(SubscribeView_SubscribeDocument, options);
      }
export type SubscribeView_SubscribeMutationHookResult = ReturnType<typeof useSubscribeView_SubscribeMutation>;
export type SubscribeView_SubscribeMutationResult = Apollo.MutationResult<SubscribeView_SubscribeMutation>;
export type SubscribeView_SubscribeMutationOptions = Apollo.BaseMutationOptions<SubscribeView_SubscribeMutation, SubscribeView_SubscribeMutationVariables>;
export const ProfileView_MaterialsDocument = gql`
    query ProfileView_Materials($owner: ID!, $afterCursor: String, $beforeCursor: String, $nbToFetch: Int) {
  ownerMaterials(
    first: $nbToFetch
    after: $afterCursor
    before: $beforeCursor
    order: [{updatedAt: "DESC"}, {name: "ASC"}]
    owner: $owner
  ) {
    totalCount
    edges {
      cursor
      node {
        ...SearchMaterial
      }
    }
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
    ${SearchMaterialFragmentDoc}`;

/**
 * __useProfileView_MaterialsQuery__
 *
 * To run a query within a React component, call `useProfileView_MaterialsQuery` and pass it any options that fit your needs.
 * When your component renders, `useProfileView_MaterialsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useProfileView_MaterialsQuery({
 *   variables: {
 *      owner: // value for 'owner'
 *      afterCursor: // value for 'afterCursor'
 *      beforeCursor: // value for 'beforeCursor'
 *      nbToFetch: // value for 'nbToFetch'
 *   },
 * });
 */
export function useProfileView_MaterialsQuery(baseOptions: Apollo.QueryHookOptions<ProfileView_MaterialsQuery, ProfileView_MaterialsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ProfileView_MaterialsQuery, ProfileView_MaterialsQueryVariables>(ProfileView_MaterialsDocument, options);
      }
export function useProfileView_MaterialsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ProfileView_MaterialsQuery, ProfileView_MaterialsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ProfileView_MaterialsQuery, ProfileView_MaterialsQueryVariables>(ProfileView_MaterialsDocument, options);
        }
export type ProfileView_MaterialsQueryHookResult = ReturnType<typeof useProfileView_MaterialsQuery>;
export type ProfileView_MaterialsLazyQueryHookResult = ReturnType<typeof useProfileView_MaterialsLazyQuery>;
export type ProfileView_MaterialsQueryResult = Apollo.QueryResult<ProfileView_MaterialsQuery, ProfileView_MaterialsQueryVariables>;
export const ProfileView_UserDocument = gql`
    query ProfileView_User($id: ID!) {
  user(id: $id) {
    ...ProfileUserCover
  }
}
    ${ProfileUserCoverFragmentDoc}`;

/**
 * __useProfileView_UserQuery__
 *
 * To run a query within a React component, call `useProfileView_UserQuery` and pass it any options that fit your needs.
 * When your component renders, `useProfileView_UserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useProfileView_UserQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useProfileView_UserQuery(baseOptions: Apollo.QueryHookOptions<ProfileView_UserQuery, ProfileView_UserQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ProfileView_UserQuery, ProfileView_UserQueryVariables>(ProfileView_UserDocument, options);
      }
export function useProfileView_UserLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ProfileView_UserQuery, ProfileView_UserQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ProfileView_UserQuery, ProfileView_UserQueryVariables>(ProfileView_UserDocument, options);
        }
export type ProfileView_UserQueryHookResult = ReturnType<typeof useProfileView_UserQuery>;
export type ProfileView_UserLazyQueryHookResult = ReturnType<typeof useProfileView_UserLazyQuery>;
export type ProfileView_UserQueryResult = Apollo.QueryResult<ProfileView_UserQuery, ProfileView_UserQueryVariables>;
export const CreateCircleCoverDocument = gql`
    mutation createCircleCover($file: Upload!, $circle: ID!) {
  createCircleImage: createCircleCover(input: {imageFile: $file, circle: $circle}) {
    circleImage: circleCover {
      id
      contentUrl
    }
  }
}
    `;
export type CreateCircleCoverMutationFn = Apollo.MutationFunction<CreateCircleCoverMutation, CreateCircleCoverMutationVariables>;

/**
 * __useCreateCircleCoverMutation__
 *
 * To run a mutation, you first call `useCreateCircleCoverMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateCircleCoverMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createCircleCoverMutation, { data, loading, error }] = useCreateCircleCoverMutation({
 *   variables: {
 *      file: // value for 'file'
 *      circle: // value for 'circle'
 *   },
 * });
 */
export function useCreateCircleCoverMutation(baseOptions?: Apollo.MutationHookOptions<CreateCircleCoverMutation, CreateCircleCoverMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateCircleCoverMutation, CreateCircleCoverMutationVariables>(CreateCircleCoverDocument, options);
      }
export type CreateCircleCoverMutationHookResult = ReturnType<typeof useCreateCircleCoverMutation>;
export type CreateCircleCoverMutationResult = Apollo.MutationResult<CreateCircleCoverMutation>;
export type CreateCircleCoverMutationOptions = Apollo.BaseMutationOptions<CreateCircleCoverMutation, CreateCircleCoverMutationVariables>;
export const CreateCircleLogoDocument = gql`
    mutation createCircleLogo($file: Upload!, $circle: ID!) {
  createCircleImage: createCircleLogo(input: {imageFile: $file, circle: $circle}) {
    circleImage: circleLogo {
      id
      contentUrl
    }
  }
}
    `;
export type CreateCircleLogoMutationFn = Apollo.MutationFunction<CreateCircleLogoMutation, CreateCircleLogoMutationVariables>;

/**
 * __useCreateCircleLogoMutation__
 *
 * To run a mutation, you first call `useCreateCircleLogoMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateCircleLogoMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createCircleLogoMutation, { data, loading, error }] = useCreateCircleLogoMutation({
 *   variables: {
 *      file: // value for 'file'
 *      circle: // value for 'circle'
 *   },
 * });
 */
export function useCreateCircleLogoMutation(baseOptions?: Apollo.MutationHookOptions<CreateCircleLogoMutation, CreateCircleLogoMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateCircleLogoMutation, CreateCircleLogoMutationVariables>(CreateCircleLogoDocument, options);
      }
export type CreateCircleLogoMutationHookResult = ReturnType<typeof useCreateCircleLogoMutation>;
export type CreateCircleLogoMutationResult = Apollo.MutationResult<CreateCircleLogoMutation>;
export type CreateCircleLogoMutationOptions = Apollo.BaseMutationOptions<CreateCircleLogoMutation, CreateCircleLogoMutationVariables>;
export const DeleteCircleImageDocument = gql`
    mutation deleteCircleImage($id: ID!) {
  deleteCircleImage(input: {id: $id}) {
    clientMutationId
  }
}
    `;
export type DeleteCircleImageMutationFn = Apollo.MutationFunction<DeleteCircleImageMutation, DeleteCircleImageMutationVariables>;

/**
 * __useDeleteCircleImageMutation__
 *
 * To run a mutation, you first call `useDeleteCircleImageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteCircleImageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteCircleImageMutation, { data, loading, error }] = useDeleteCircleImageMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteCircleImageMutation(baseOptions?: Apollo.MutationHookOptions<DeleteCircleImageMutation, DeleteCircleImageMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteCircleImageMutation, DeleteCircleImageMutationVariables>(DeleteCircleImageDocument, options);
      }
export type DeleteCircleImageMutationHookResult = ReturnType<typeof useDeleteCircleImageMutation>;
export type DeleteCircleImageMutationResult = Apollo.MutationResult<DeleteCircleImageMutation>;
export type DeleteCircleImageMutationOptions = Apollo.BaseMutationOptions<DeleteCircleImageMutation, DeleteCircleImageMutationVariables>;
export const AddMessageDiscussionDocument = gql`
    mutation addMessageDiscussion($id: ID!, $message: String) {
  addMessageDiscussion(input: {id: $id, message: $message}) {
    discussion {
      id
      messages {
        edges {
          node {
            id
            content
            author {
              id
              slug
              firstname
              lastname
              fullname
              avatar {
                contentUrl
              }
            }
            createdAt
            updatedAt
          }
        }
      }
    }
  }
}
    `;
export type AddMessageDiscussionMutationFn = Apollo.MutationFunction<AddMessageDiscussionMutation, AddMessageDiscussionMutationVariables>;

/**
 * __useAddMessageDiscussionMutation__
 *
 * To run a mutation, you first call `useAddMessageDiscussionMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddMessageDiscussionMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addMessageDiscussionMutation, { data, loading, error }] = useAddMessageDiscussionMutation({
 *   variables: {
 *      id: // value for 'id'
 *      message: // value for 'message'
 *   },
 * });
 */
export function useAddMessageDiscussionMutation(baseOptions?: Apollo.MutationHookOptions<AddMessageDiscussionMutation, AddMessageDiscussionMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddMessageDiscussionMutation, AddMessageDiscussionMutationVariables>(AddMessageDiscussionDocument, options);
      }
export type AddMessageDiscussionMutationHookResult = ReturnType<typeof useAddMessageDiscussionMutation>;
export type AddMessageDiscussionMutationResult = Apollo.MutationResult<AddMessageDiscussionMutation>;
export type AddMessageDiscussionMutationOptions = Apollo.BaseMutationOptions<AddMessageDiscussionMutation, AddMessageDiscussionMutationVariables>;
export const MaterialBookingDocument = gql`
    query materialBooking($id: ID!) {
  materialBooking(id: $id) {
    id
    slug
    startDate
    endDate
    status
    statusTransitionAvailables
    price
    discussion {
      id
      messages {
        edges {
          node {
            id
            content
            author {
              id
              slug
              firstname
              lastname
              fullname
              avatar {
                contentUrl
              }
            }
            createdAt
          }
        }
      }
    }
    user {
      id
      slug
      firstname
      lastname
      fullname
      email
      phoneNumber
      avatar {
        contentUrl
      }
      city
      roles
      gender
      userOwnerships {
        edges {
          node {
            material {
              id
              slug
              name
            }
            materialMainOwned {
              id
              slug
              name
            }
          }
        }
        totalCount
      }
      averageRatingScore
      ratings {
        totalCount
      }
    }
    material {
      id
      slug
      name
      model
      brand
      reference
      createdAt
      updatedAt
      images {
        edges {
          node {
            id
            imageName
            size
            contentUrl
          }
        }
      }
      mainOwnershipUser {
        id
        slug
        firstname
        lastname
        fullname
        email
        phoneNumber
        avatar {
          contentUrl
        }
        streetAddress
        city
        roles
        gender
        averageRatingScore
        ratings {
          totalCount
        }
      }
    }
    periods {
      id
      startDate
      endDate
      price
    }
  }
}
    `;

/**
 * __useMaterialBookingQuery__
 *
 * To run a query within a React component, call `useMaterialBookingQuery` and pass it any options that fit your needs.
 * When your component renders, `useMaterialBookingQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMaterialBookingQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useMaterialBookingQuery(baseOptions: Apollo.QueryHookOptions<MaterialBookingQuery, MaterialBookingQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MaterialBookingQuery, MaterialBookingQueryVariables>(MaterialBookingDocument, options);
      }
export function useMaterialBookingLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MaterialBookingQuery, MaterialBookingQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MaterialBookingQuery, MaterialBookingQueryVariables>(MaterialBookingDocument, options);
        }
export type MaterialBookingQueryHookResult = ReturnType<typeof useMaterialBookingQuery>;
export type MaterialBookingLazyQueryHookResult = ReturnType<typeof useMaterialBookingLazyQuery>;
export type MaterialBookingQueryResult = Apollo.QueryResult<MaterialBookingQuery, MaterialBookingQueryVariables>;
export const ChangeDateDocument = gql`
    mutation changeDate($id: ID!, $startDate: String, $endDate: String) {
  changeDateMaterialBooking(
    input: {id: $id, startDate: $startDate, endDate: $endDate}
  ) {
    materialBooking {
      id
      startDate
      endDate
      periods {
        startDate
        endDate
      }
    }
  }
}
    `;
export type ChangeDateMutationFn = Apollo.MutationFunction<ChangeDateMutation, ChangeDateMutationVariables>;

/**
 * __useChangeDateMutation__
 *
 * To run a mutation, you first call `useChangeDateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeDateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeDateMutation, { data, loading, error }] = useChangeDateMutation({
 *   variables: {
 *      id: // value for 'id'
 *      startDate: // value for 'startDate'
 *      endDate: // value for 'endDate'
 *   },
 * });
 */
export function useChangeDateMutation(baseOptions?: Apollo.MutationHookOptions<ChangeDateMutation, ChangeDateMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeDateMutation, ChangeDateMutationVariables>(ChangeDateDocument, options);
      }
export type ChangeDateMutationHookResult = ReturnType<typeof useChangeDateMutation>;
export type ChangeDateMutationResult = Apollo.MutationResult<ChangeDateMutation>;
export type ChangeDateMutationOptions = Apollo.BaseMutationOptions<ChangeDateMutation, ChangeDateMutationVariables>;
export const DeleteMaterialBookingDocument = gql`
    mutation deleteMaterialBooking($id: ID!) {
  deleteMaterialBooking(input: {id: $id}) {
    clientMutationId
  }
}
    `;
export type DeleteMaterialBookingMutationFn = Apollo.MutationFunction<DeleteMaterialBookingMutation, DeleteMaterialBookingMutationVariables>;

/**
 * __useDeleteMaterialBookingMutation__
 *
 * To run a mutation, you first call `useDeleteMaterialBookingMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteMaterialBookingMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteMaterialBookingMutation, { data, loading, error }] = useDeleteMaterialBookingMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteMaterialBookingMutation(baseOptions?: Apollo.MutationHookOptions<DeleteMaterialBookingMutation, DeleteMaterialBookingMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteMaterialBookingMutation, DeleteMaterialBookingMutationVariables>(DeleteMaterialBookingDocument, options);
      }
export type DeleteMaterialBookingMutationHookResult = ReturnType<typeof useDeleteMaterialBookingMutation>;
export type DeleteMaterialBookingMutationResult = Apollo.MutationResult<DeleteMaterialBookingMutation>;
export type DeleteMaterialBookingMutationOptions = Apollo.BaseMutationOptions<DeleteMaterialBookingMutation, DeleteMaterialBookingMutationVariables>;
export const MaterialsDocument = gql`
    query materials($searchTerms: String, $afterCursor: String, $beforeCursor: String, $nbToFetch: Int) {
  materials(
    first: $nbToFetch
    after: $afterCursor
    before: $beforeCursor
    name: $searchTerms
    order: [{updatedAt: "DESC"}, {name: "ASC"}]
  ) {
    totalCount
    edges {
      cursor
      node {
        id
        name
        slug
        model
        brand
        mainOwner {
          slug
          firstname
          lastname
          fullname
          city
          avatar {
            contentUrl
          }
        }
        reference
        createdAt
        updatedAt
        images {
          edges {
            node {
              id
              imageName
              size
              contentUrl
            }
          }
        }
        bestPriceForUser
      }
    }
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
    `;

/**
 * __useMaterialsQuery__
 *
 * To run a query within a React component, call `useMaterialsQuery` and pass it any options that fit your needs.
 * When your component renders, `useMaterialsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMaterialsQuery({
 *   variables: {
 *      searchTerms: // value for 'searchTerms'
 *      afterCursor: // value for 'afterCursor'
 *      beforeCursor: // value for 'beforeCursor'
 *      nbToFetch: // value for 'nbToFetch'
 *   },
 * });
 */
export function useMaterialsQuery(baseOptions?: Apollo.QueryHookOptions<MaterialsQuery, MaterialsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MaterialsQuery, MaterialsQueryVariables>(MaterialsDocument, options);
      }
export function useMaterialsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MaterialsQuery, MaterialsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MaterialsQuery, MaterialsQueryVariables>(MaterialsDocument, options);
        }
export type MaterialsQueryHookResult = ReturnType<typeof useMaterialsQuery>;
export type MaterialsLazyQueryHookResult = ReturnType<typeof useMaterialsLazyQuery>;
export type MaterialsQueryResult = Apollo.QueryResult<MaterialsQuery, MaterialsQueryVariables>;
export const ChangePasswordUserDocument = gql`
    mutation changePasswordUser($id: ID!, $currentPassword: String!, $newPassword: String!) {
  changePasswordUser(
    input: {id: $id, currentPassword: $currentPassword, newPassword: $newPassword}
  ) {
    user {
      id
      updatedAt
    }
  }
}
    `;
export type ChangePasswordUserMutationFn = Apollo.MutationFunction<ChangePasswordUserMutation, ChangePasswordUserMutationVariables>;

/**
 * __useChangePasswordUserMutation__
 *
 * To run a mutation, you first call `useChangePasswordUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangePasswordUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changePasswordUserMutation, { data, loading, error }] = useChangePasswordUserMutation({
 *   variables: {
 *      id: // value for 'id'
 *      currentPassword: // value for 'currentPassword'
 *      newPassword: // value for 'newPassword'
 *   },
 * });
 */
export function useChangePasswordUserMutation(baseOptions?: Apollo.MutationHookOptions<ChangePasswordUserMutation, ChangePasswordUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangePasswordUserMutation, ChangePasswordUserMutationVariables>(ChangePasswordUserDocument, options);
      }
export type ChangePasswordUserMutationHookResult = ReturnType<typeof useChangePasswordUserMutation>;
export type ChangePasswordUserMutationResult = Apollo.MutationResult<ChangePasswordUserMutation>;
export type ChangePasswordUserMutationOptions = Apollo.BaseMutationOptions<ChangePasswordUserMutation, ChangePasswordUserMutationVariables>;
export const DeleteUserDocument = gql`
    mutation DeleteUser($id: ID!) {
  deleteUser(input: {id: $id}) {
    clientMutationId
  }
}
    `;
export type DeleteUserMutationFn = Apollo.MutationFunction<DeleteUserMutation, DeleteUserMutationVariables>;

/**
 * __useDeleteUserMutation__
 *
 * To run a mutation, you first call `useDeleteUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteUserMutation, { data, loading, error }] = useDeleteUserMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteUserMutation(baseOptions?: Apollo.MutationHookOptions<DeleteUserMutation, DeleteUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteUserMutation, DeleteUserMutationVariables>(DeleteUserDocument, options);
      }
export type DeleteUserMutationHookResult = ReturnType<typeof useDeleteUserMutation>;
export type DeleteUserMutationResult = Apollo.MutationResult<DeleteUserMutation>;
export type DeleteUserMutationOptions = Apollo.BaseMutationOptions<DeleteUserMutation, DeleteUserMutationVariables>;
export const LoginDocument = gql`
    mutation login($username: String!, $password: String!) {
  loginUser(input: {username: $username, password: $password}) {
    user {
      ...AuthUser__User
    }
  }
}
    ${AuthUser__UserFragmentDoc}`;
export type LoginMutationFn = Apollo.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      username: // value for 'username'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(baseOptions?: Apollo.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, options);
      }
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = Apollo.MutationResult<LoginMutation>;
export type LoginMutationOptions = Apollo.BaseMutationOptions<LoginMutation, LoginMutationVariables>;