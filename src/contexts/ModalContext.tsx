import React, {
    createContext,
    ReactNode,
    useContext,
    useMemo,
    useState,
} from 'react'

type ModalContextProps = {
    modal: ReactNode | undefined
    setModal: React.Dispatch<React.SetStateAction<ReactNode | undefined>>
}

const ModalContext = createContext<ModalContextProps | undefined>(undefined)

type Props = {
    children: ReactNode
}

export const ModalProvider: React.FC<Props> = ({ children }) => {
    const [modal, setModal] = useState<ReactNode | undefined>()

    return useMemo(
        () => (
            <ModalContext.Provider value={{ modal, setModal }}>
                {children}
            </ModalContext.Provider>
        ),
        [modal, setModal, children]
    )
}

export const useModal = (): ModalContextProps => {
    const context = useContext(ModalContext)
    if (!context) {
        throw new Error('useModal must be used within an ModalProvider')
    }
    return context
}
