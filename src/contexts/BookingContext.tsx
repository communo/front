import React, { Dispatch, SetStateAction } from 'react'
import { BookingContext__MaterialBookingFragment } from '../graphql/api/generated'

interface BookingContextInterface {
    booking: BookingContext__MaterialBookingFragment | undefined
    setBooking: Dispatch<
        SetStateAction<BookingContext__MaterialBookingFragment | undefined>
    >
}

export const BookingContext =
    React.createContext<BookingContextInterface | null>(null)
