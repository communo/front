import React from 'react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { Pig } from '@components/atoms/Illustrations/Pig'
import { Hippopotame } from '@components/atoms/Illustrations/Hippopotame'
import { Drill } from '@components/atoms/Illustrations/Drill'
import { Stamp } from '@components/atoms/Illustrations/Stamp'
import { StatsWithCard } from '@components/molecules/Stats/StatsWithCard/StatsWithCard'
import { TeamWithLargeImage } from '@components/organisms/team/TeamWithLargeImage'
import { MaiaMaterIcon } from '@components/molecules/Feature/Sponsor/MaiaMaterLogo'
import { Box, Flex, Heading, Image } from '@chakra-ui/react'
import { useColorModeValue } from '@components/ui/color-mode'

export const TeamView = () => {
    const { t } = useTranslation()

    return (
        <>
            <Helmet>
                <title>{t('home.title')}</title>
            </Helmet>
            <TeamWithLargeImage />
            <Box bg={useColorModeValue('orange.50', 'orange.fg')} p={10}>
                <Flex
                    maxW="7xl"
                    m={'0 auto'}
                    textAlign={'center'}
                    direction={'column'}
                    align={'center'}
                    gap={5}
                >
                    <MaiaMaterIcon height="100px" width="100px" />
                    <Heading
                        as={'h2'}
                        size="4xl"
                        fontWeight="extrabold"
                        letterSpacing="tight"
                        lineHeight="normal"
                    >
                        {t('team.incubatedBy', {
                            name: 'Maïa Mater / La cantine',
                        })}
                    </Heading>
                    <Image
                        rounded="2xl"
                        src="/images/sponsors/maia-mater-promo5.jpg"
                        alt="Maïa Mater"
                    />
                </Flex>
            </Box>

            <StatsWithCard
                heading={t('why.stats.title')}
                subHeading={t('why.stats.subTitle')}
                stats={[
                    {
                        title: t('why.stats.annualBudget.title'),
                        description: t('why.stats.annualBudget.description'),
                        icon: <Pig height="10px" />,
                        value: t('why.stats.annualBudget.value'),
                        source: {
                            name: t('why.stats.annualBudget.source.name'),
                            link: t('why.stats.annualBudget.source.link'),
                        },
                    },
                    {
                        title: t('why.stats.trailerFootprint.title'),
                        description: t(
                            'why.stats.trailerFootprint.description'
                        ),
                        icon: <Hippopotame height="14px" />,
                        value: t('why.stats.trailerFootprint.value'),
                        source: {
                            name: t('why.stats.trailerFootprint.source.name'),
                            link: t('why.stats.trailerFootprint.source.link'),
                        },
                    },
                    {
                        title: t('why.stats.drillUse.title'),
                        description: t('why.stats.drillUse.description'),
                        icon: <Drill height="14px" />,
                        value: t('why.stats.drillUse.value'),
                        source: {
                            name: t('why.stats.drillUse.source.name'),
                            link: t('why.stats.drillUse.source.link'),
                        },
                    },
                    {
                        title: t('why.stats.electronicItemsCount.title'),
                        description: t(
                            'why.stats.electronicItemsCount.description'
                        ),
                        icon: <Stamp height="14px" />,
                        value: t('why.stats.electronicItemsCount.value'),
                        source: {
                            name: t(
                                'why.stats.electronicItemsCount.source.name'
                            ),
                            link: t(
                                'why.stats.electronicItemsCount.source.link'
                            ),
                        },
                    },
                ]}
            />
            <iframe
                src="https://archives.qqf.fr/uploads/infographiesHTML/5bacebefb6dbd/index.html"
                width={'100%'}
                style={{ height: '300vh', margin: '0 auto' }}
            />
        </>
    )
}
