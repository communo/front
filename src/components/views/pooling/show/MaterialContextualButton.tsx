import React from 'react'
import {
    MaterialContextualButtonFragment,
    MyMaterialsDocument,
    ShowMaterialDocument,
    useDeleteMaterialMutation,
    usePauseMaterialMutation,
    usePublishMaterialMutation,
} from '@_/graphql/api/generated'
import { Box, useDisclosure } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { generatePath, Link, useNavigate } from 'react-router'
import { toast } from 'react-toastify'
import { BsCalendar3WeekFill } from 'react-icons/bs'
import { PoolingRoutes } from '@_/routes/_hooks/routes.enum'
import {
    ThreeDotsMenu,
    ThreeDotsMenuItem,
} from '@components/atoms/Menu/ThreeDotsMenu'
import { Button } from '@components/ui/button'
import { FaPause, FaPlay, FaTrashAlt } from 'react-icons/fa'
import { DangerOverlay } from '@components/atoms/Modal/Overlay/DangerOverlay'
import { useAuth } from '@_/auth/useAuth'
import {
    DialogBody,
    DialogContent,
    DialogFooter,
    DialogHeader,
    DialogRoot,
} from '@components/ui/dialog'
import { useCookies } from 'react-cookie'
import { useColorModeValue } from '@components/ui/color-mode'

export const MaterialContextualButton = ({
    material,
    additionalButtons,
}: {
    material: MaterialContextualButtonFragment
    additionalButtons?: ThreeDotsMenuItem[]
}) => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const [pauseMaterial] = usePauseMaterialMutation()
    const [publishMaterial] = usePublishMaterialMutation()
    const auth = useAuth()
    const cancelRef = React.useRef<HTMLButtonElement>(null)
    const {
        open: isDeleteModalOpen,
        onOpen: onDeleteModalOpen,
        onClose: onDeleteModalClose,
    } = useDisclosure()
    const [cookies] = useCookies()
    const [deleteMaterial] = useDeleteMaterialMutation()
    const refetchQueries = [
        {
            query: ShowMaterialDocument,
            variables: {
                id: material.id,
                statusesList: cookies.myMaterialFilters?.statusesList ?? [],
            },
        },
        {
            query: MyMaterialsDocument,
        },
    ]
    const onDelete = (_material: { id: string }) => {
        deleteMaterial({
            variables: {
                id: _material.id,
            },
            refetchQueries,
        })
        toast.success(t('pooling.my_materials.actions.delete.success'))
        navigate(PoolingRoutes.MyMaterials)
    }

    const pauseButton = {
        key: `btn-pause${material.slug}`,
        component: (
            <Button
                variant="ghost"
                onClick={() =>
                    pauseMaterial({
                        variables: {
                            id: material.id,
                        },
                        refetchQueries,
                        onCompleted: () => {
                            toast.success(
                                t('pooling.my_materials.actions.pause.success')
                            )
                        },
                    })
                }
            >
                <FaPause />
                <Box ml={2}>
                    {t('pooling.my_materials.index.table.actions.pause.label')}
                </Box>
            </Button>
        ),
    }

    const publishButton = {
        key: `btn-publish${material.slug}`,
        component: (
            <Button
                variant="ghost"
                onClick={() =>
                    publishMaterial({
                        variables: {
                            id: material.id,
                        },
                        refetchQueries,
                        onCompleted: () => {
                            toast.success(
                                t(
                                    'pooling.my_materials.actions.publish.success'
                                )
                            )
                        },
                    })
                }
            >
                <FaPlay />
                <Box ml={2}>
                    {t(
                        'pooling.my_materials.index.table.actions.publish.label'
                    )}
                </Box>
            </Button>
        ),
    }

    const deleteButton = {
        key: `btn-delete${material.slug}`,
        component: (
            <Button onClick={onDeleteModalOpen} variant="ghost">
                <FaTrashAlt />
                <Box ml={2}>
                    {t('pooling.my_materials.index.table.actions.delete.label')}
                </Box>
            </Button>
        ),
    }

    const seeCalendarButton = {
        key: `btn-see${material.slug}`,
        component: (
            <Button asChild variant="ghost">
                <Link
                    to={generatePath(PoolingRoutes.Calendar, {
                        slug: material.slug,
                    })}
                >
                    <BsCalendar3WeekFill />
                    <Box ml={2}>
                        {t(
                            'pooling.my_materials.index.table.actions.calendar.label'
                        )}
                    </Box>
                </Link>
            </Button>
        ),
    }
    const dialogContentProps = {
        bg: useColorModeValue('blue.50', 'blue.900'),
    }

    return (
        <>
            <ThreeDotsMenu
                ariaLabel="Actions"
                buttons={[
                    ...(additionalButtons ?? []),
                    ...(material.status === 'published' ? [pauseButton] : []),
                    ...(material.status === 'paused' ? [publishButton] : []),
                    seeCalendarButton,
                    deleteButton,
                ]}
            />
            <DialogRoot
                lazyMount
                open={isDeleteModalOpen}
                onValueChange={onDeleteModalOpen}
                leastDestructiveRef={cancelRef}
                placement={'center'}
            >
                <DangerOverlay />
                <DialogContent {...dialogContentProps}>
                    <DialogHeader fontSize="lg" fontWeight="bold">
                        {t('pooling.form.delete.confirm.title', {
                            context: auth.user?.gender,
                        })}
                    </DialogHeader>
                    <DialogBody>
                        {t('pooling.form.delete.confirm.body')}
                    </DialogBody>

                    <DialogFooter>
                        <Button ref={cancelRef} onClick={onDeleteModalClose}>
                            {t('global.cancel')}
                        </Button>
                        <Button
                            colorPalette="red"
                            onClick={() => onDelete(material)}
                            ml={3}
                        >
                            {t('pooling.form.delete.confirm.button')}
                        </Button>
                    </DialogFooter>
                </DialogContent>
            </DialogRoot>
        </>
    )
}
