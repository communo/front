import * as React from 'react'
import { useEffect, useState } from 'react'
import {
    Box,
    BreadcrumbLink,
    Button,
    Container,
    Flex,
    GridItem,
    Heading,
    HStack,
    SimpleGrid,
    Text,
    useDisclosure,
    VStack,
} from '@chakra-ui/react'
import {
    generatePath,
    Link,
    useLocation,
    useNavigate,
    useParams,
} from 'react-router'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { FiHome } from 'react-icons/fi'
import 'react-medium-image-zoom/dist/styles.css'
import { AvailabilityPlanning } from '@components/molecules/Form/Pooling/Material/Booking/AvailabilityPlanning'
import { BookingContext } from '@_/contexts/BookingContext'
import { useAuth } from '@_/auth/useAuth'
import { Pictures } from '@components/molecules/Material/Pictures'
import { BookButton } from '@components/molecules/Material/BookButton'
import { Loader } from '@components/atoms/Loader/Loader'
import { priceText } from '@utils/priceText'
import { ApplyConfirmedModal } from '@components/molecules/Modal/Pooling/MaterialBooking/ApplyConfirmedModal'
import { ForkButton } from '@components/molecules/Material/ForkButton'
import {
    ShowMaterial__MaterialBookingFragment,
    useShowMaterialQuery,
} from '@_/graphql/api/generated'
import { MaterialOwnerCard } from '@components/molecules/Cards/MaterialCard/MaterialOwnerCard'
import { PoolingRoutes, UserRoutes } from '@_/routes/_hooks/routes.enum'
import { toast } from 'react-toastify'
import { MaterialContextualButton } from '@components/views/pooling/show/MaterialContextualButton'
import { useLinkify } from '@_/hooks/useLinkify'
import {
    BreadcrumbCurrentLink,
    BreadcrumbRoot,
} from '@components/ui/breadcrumb'

const MaterialDescription = ({ description }: { description: string }) => {
    return <Box whiteSpace="pre-line">{useLinkify(description)}</Box>
}

export const MaterialShowView = () => {
    const { t } = useTranslation()
    const [step, setStep] = useState<'initial' | 'choosePeriod' | 'pending'>(
        'initial'
    )
    const [booking, setBooking] = useState<
        ShowMaterial__MaterialBookingFragment | undefined
    >()
    const params = useParams()
    const auth = useAuth()

    const { data, loading, error } = useShowMaterialQuery({
        variables: {
            id: generatePath(PoolingRoutes.IRI, { slug: params.slug! }),
        },
    })

    const {
        open: isValidationOpen,
        onOpen: onValidationOpen,
        onClose: onValidationClose,
    } = useDisclosure()
    useEffect(() => {
        if (booking && booking.status === 'pending') {
            onValidationOpen()
        }
    }, [booking, onValidationOpen])
    const { pathname } = useLocation()
    const navigate = useNavigate()
    if (loading) return <Loader />
    if (error) return <Box>{error.message}</Box>
    const material = data?.material

    return (
        (material && ( // eslint-disable-next-line react/jsx-no-constructed-context-values
            <BookingContext.Provider value={{ booking, setBooking }}>
                <Helmet>
                    <title>
                        {material.name} {material.brand} {material.model}{' '}
                        {t('meta.title.suffix')}
                    </title>
                </Helmet>
                <Container maxW="7xl" minW="300px">
                    <BreadcrumbRoot py={2} color="yellow.600">
                        <BreadcrumbLink asChild>
                            <Link to="/">
                                <FiHome />
                            </Link>
                        </BreadcrumbLink>
                        <BreadcrumbLink asChild>
                            <Link to="/pooling">{t('menu.pooling')}</Link>
                        </BreadcrumbLink>
                        <BreadcrumbCurrentLink>
                            {material.name}
                        </BreadcrumbCurrentLink>
                    </BreadcrumbRoot>

                    <Flex direction="column">
                        <SimpleGrid
                            display={{
                                base: 'block',
                                md: 'grid',
                            }}
                            order={{ base: 2, md: 1 }}
                            columns={
                                step === 'initial' || step === 'pending'
                                    ? { base: 1, md: 5 }
                                    : { base: 1, md: 6 }
                            }
                            gap={{ base: 8, md: 10 }}
                            pt={{ base: 9, md: 15 }}
                        >
                            <GridItem
                                rowSpan={{ base: 1, md: 2 }}
                                colSpan={
                                    step === 'initial' || step === 'pending'
                                        ? { base: 1, md: 3 }
                                        : { base: 1, md: 2 }
                                }
                                display={
                                    step === 'choosePeriod'
                                        ? { base: 'none', md: 'grid' }
                                        : 'inherit'
                                }
                            >
                                <Pictures
                                    material={material}
                                    imageConnection={material.images}
                                    borderRadius={
                                        step === 'choosePeriod'
                                            ? '10px 10px 0 0 '
                                            : undefined
                                    }
                                />
                                {step === 'choosePeriod' &&
                                    material.mainOwnership?.ownerIRI && (
                                        <MaterialOwnerCard
                                            mainOwnerId={
                                                material.mainOwnership.ownerIRI
                                            }
                                        />
                                    )}
                            </GridItem>
                            <GridItem
                                rowSpan={2}
                                colSpan={
                                    step === 'initial' || step === 'pending'
                                        ? 2
                                        : 4
                                }
                            >
                                {step === 'choosePeriod' && (
                                    <Flex height="100%">
                                        <AvailabilityPlanning
                                            material={material!}
                                            onBack={() => setStep('initial')}
                                            setStep={setStep}
                                        />
                                    </Flex>
                                )}
                                {(step === 'initial' || step === 'pending') && (
                                    <VStack
                                        display={{
                                            base: 'none',
                                            md: 'block',
                                        }}
                                    >
                                        <Box w="100%">
                                            {material.mainOwnership
                                                ?.ownerIRI && (
                                                <MaterialOwnerCard
                                                    mainOwnerId={
                                                        material.mainOwnership
                                                            .ownerIRI
                                                    }
                                                />
                                            )}
                                        </Box>
                                        <Flex width="100%" my={5} gap={5}>
                                            {(!auth.user && (
                                                <BookButton
                                                    material={material}
                                                    onClick={() => {
                                                        toast(
                                                            t(
                                                                'global.please.login'
                                                            )
                                                        )
                                                        navigate(
                                                            UserRoutes.Login,
                                                            {
                                                                state: {
                                                                    from: pathname,
                                                                },
                                                            }
                                                        )
                                                    }}
                                                />
                                            )) ||
                                                (material.userActionAvailables.includes(
                                                    'borrow'
                                                ) && (
                                                    <>
                                                        <BookButton
                                                            material={material}
                                                            onClick={() =>
                                                                setStep(
                                                                    'choosePeriod'
                                                                )
                                                            }
                                                        />
                                                        <ForkButton
                                                            material={material}
                                                        />
                                                    </>
                                                ))}
                                        </Flex>
                                        {material.userActionAvailables.includes(
                                            'edit'
                                        ) && (
                                            <HStack>
                                                <Button
                                                    asChild
                                                    flex={1}
                                                    width="full"
                                                    fontSize="sm"
                                                    colorPalette="blue"
                                                    _hover={{
                                                        transform:
                                                            'translateY(2px)',
                                                        boxShadow: 'lg',
                                                    }}
                                                >
                                                    <Link
                                                        to={generatePath(
                                                            PoolingRoutes.EditMyMaterial,
                                                            {
                                                                slug: material.slug,
                                                            }
                                                        )}
                                                    >
                                                        {t(
                                                            'pooling.my_materials.index.table.actions.edit.label'
                                                        )}
                                                    </Link>
                                                </Button>
                                                <MaterialContextualButton
                                                    material={material}
                                                />
                                            </HStack>
                                        )}
                                    </VStack>
                                )}
                            </GridItem>
                        </SimpleGrid>
                        <Heading
                            order={{ base: 1, md: 1 }}
                            lineHeight={1.1}
                            fontWeight={600}
                            fontSize={{
                                base: 'xl',
                                sm: '2xl',
                                lg: '2xl',
                            }}
                            mt="40px"
                            as="h1"
                        >
                            {material.name}
                        </Heading>
                        <Box order={{ base: 3, md: 1 }}>
                            {material.bestPriceForUser !== undefined && (
                                    <Text
                                        colorPalette="gray"
                                        fontWeight={700}
                                        as="span"
                                        mr={2}
                                        fontSize="lg"
                                    >
                                        {t('pooling.show.pricing.label')}
                                    </Text>
                                ) && (
                                    <Text
                                        colorPalette="gray"
                                        fontWeight={300}
                                        as="span"
                                        borderBottomWidth={2}
                                        borderBottomStyle="solid"
                                        borderBottomColor="red"
                                        mr={3}
                                        fontSize="lg"
                                    >
                                        {priceText(
                                            material.bestPriceForUser,
                                            auth.user,
                                            t
                                        )}
                                    </Text>
                                )}
                        </Box>
                        <Box
                            order={{ base: 4, md: 1 }}
                            whiteSpace="pre-line"
                            mt={5}
                            mb={85}
                        >
                            <strong>
                                {material.brand} {material.model}
                            </strong>
                            <br />
                            {material.description && (
                                <MaterialDescription
                                    description={material.description}
                                />
                            )}
                        </Box>
                        <Box
                            order={{ base: 5, md: 1 }}
                            display={{
                                base: 'block',
                                md: 'none',
                            }}
                        >
                            <VStack>
                                <Box width="100%">
                                    {(material.mainOwner?.slug ===
                                        auth.user?.slug && (
                                        <HStack>
                                            <Button
                                                asChild
                                                flex={1}
                                                width="full"
                                                fontSize="sm"
                                                colorPalette="blue"
                                                _hover={{
                                                    transform:
                                                        'translateY(2px)',
                                                    boxShadow: 'lg',
                                                }}
                                            >
                                                <Link
                                                    to={generatePath(
                                                        PoolingRoutes.EditMyMaterial,
                                                        {
                                                            slug: material.slug,
                                                        }
                                                    )}
                                                >
                                                    {t(
                                                        'pooling.my_materials.index.table.actions.edit.label'
                                                    )}
                                                </Link>
                                            </Button>
                                            <MaterialContextualButton
                                                material={material}
                                            />
                                        </HStack>
                                    )) || (
                                        <BookButton
                                            material={material}
                                            onClick={() =>
                                                setStep('choosePeriod')
                                            }
                                        />
                                    )}
                                </Box>
                                {material.mainOwnership?.ownerIRI && (
                                    <MaterialOwnerCard
                                        mainOwnerId={
                                            material.mainOwnership.ownerIRI
                                        }
                                        borderRadius={
                                            step === 'choosePeriod'
                                                ? '0 0 10px 10px'
                                                : undefined
                                        }
                                    />
                                )}
                            </VStack>
                        </Box>
                    </Flex>
                </Container>
                {booking && (
                    <ApplyConfirmedModal
                        bookingLink={`/my/bookings/${booking.slug}`}
                        material={material}
                        onClose={onValidationClose}
                        isOpen={isValidationOpen}
                    />
                )}
            </BookingContext.Provider>
        )) || <Loader />
    )
}
