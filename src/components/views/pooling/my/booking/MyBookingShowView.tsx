import * as React from 'react'
import { useCallback, useMemo } from 'react'
import { useApolloClient } from '@apollo/client'
import { Link, useParams } from 'react-router'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { FiHome } from 'react-icons/fi'
import { FieldValues, useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import { Loader } from '@components/atoms/Loader/Loader'
import { useAuth } from '@_/auth/useAuth'
import { BookingStatusActionButton } from '@components/atoms/Button/material/booking/BookingStatusActionButton'
import { BookingPriceStat } from '@components/atoms/Stat/Material/Booking/BookingPriceStat'
import { H2 } from '@components/atoms/Heading'
import { Messages } from '@components/molecules/Message/Messages'
import { datePeriodText } from '@utils/datePeriodText'
import {
    useAddMessageDiscussionMutation,
    useMyBookingQuery,
} from '@_/graphql/api/generated'
import { useColorModeValue } from '@components/ui/color-mode'
import {
    BreadcrumbCurrentLink,
    BreadcrumbLink,
    BreadcrumbRoot,
} from '@components/ui/breadcrumb'
import {
    AspectRatio,
    Box,
    Card,
    Center,
    Container,
    Flex,
    Grid,
    GridItem,
    Heading,
    HStack,
    Image,
    Input,
    Kbd,
    Spinner,
    Stack,
    Text,
    Textarea,
} from '@chakra-ui/react'
import { Avatar } from '@_/components/ui/avatar'
import { materialmainPicture } from '@utils/image'
import { Tag } from '@components/ui/tag'
import { LuArrowRight } from 'react-icons/lu'
import { FaPaperPlane } from 'react-icons/fa'
import { ExpandingOnFocusButton } from '@components/atoms/Button/ExpandingOnFocusButton'

export const MyBookingShowView = () => {
    const { t, i18n } = useTranslation()
    const auth = useAuth()
    const params = useParams()
    const bgAccent = useColorModeValue('gray.100', 'gray.800')
    const client = useApolloClient()

    const { error, data: materialBookingData } = useMyBookingQuery({
        variables: {
            id: `/material_bookings/${params.slug}`,
        },
    })

    const [addMessage] = useAddMessageDiscussionMutation({
        update() {
            client.resetStore()
        },
    })
    const { handleSubmit, register, resetField } = useForm<FieldValues>()

    const onSubmit = (data: FieldValues) => {
        addMessage({
            variables: {
                id: data.id,
                message: data.message,
            },
        }).then((r) => {
            if (!r.data || r.data?.addMessageDiscussion?.discussion === null) {
                toast.error(t(''))
                return
            }
            resetField('message')
        })
    }

    const booking = materialBookingData?.materialBooking

    const user =
        auth.user?.id === materialBookingData?.materialBooking?.id
            ? materialBookingData?.materialBooking?.material?.mainOwner
            : materialBookingData?.materialBooking?.user
    const datePeriod = booking
        ? datePeriodText(booking.startDate, booking.endDate, t, i18n.language)
        : ''
    const picture = booking?.material
        ? materialmainPicture(booking.material)
        : undefined
    const owner = booking?.material?.mainOwnershipUser
    const bookingPriceStat = useMemo(() => {
        return (
            (booking && booking.periods && (
                <BookingPriceStat
                    price={booking.price || 0}
                    periods={booking.periods.map(
                        ({ id, startDate, endDate }) => {
                            return {
                                id,
                                startDate,
                                endDate,
                            }
                        }
                    )}
                />
            )) || <Spinner />
        )
    }, [booking])
    const handleKeyDown = useCallback(
        (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
            if (e.ctrlKey && e.key === 'Enter') {
                e.preventDefault()
                handleSubmit(onSubmit)()
            }
        },
        [handleSubmit, onSubmit]
    )

    return (
        (error && <p>Error </p>) ||
        (booking && booking.material && (
            <>
                <Helmet>
                    <title>
                        {t('pooling.booking.show.meta.title', {
                            booking,
                            datePeriod,
                            user,
                        })}
                        {t('meta.title.suffix')}
                    </title>
                </Helmet>
                <Container>
                    <BreadcrumbRoot py={2} color="yellow.600">
                        <BreadcrumbLink asChild>
                            <Link to="/">
                                <FiHome />
                            </Link>
                        </BreadcrumbLink>
                        <BreadcrumbLink asChild>
                            <Link to="/my/bookings/requests">
                                {t('menu.my.bookings')}
                            </Link>
                        </BreadcrumbLink>{' '}
                        <BreadcrumbCurrentLink>
                            {t('menu.my.booking', {
                                booking,
                                datePeriod,
                                user,
                            })}
                        </BreadcrumbCurrentLink>
                    </BreadcrumbRoot>
                </Container>

                <Container>
                    <Grid templateColumns="repeat(3, 1fr)" gap="6" my={10}>
                        <GridItem colSpan={{ base: 3, sm: 1 }}>
                            <Flex direction="column" gap={10}>
                                <Box
                                    gap={5}
                                    px={8}
                                    py={5}
                                    borderRadius="2xl"
                                    bg={bgAccent}
                                >
                                    <H2 m={0}>
                                        {t('pooling.booking.status', {
                                            context: booking.status,
                                        })}
                                    </H2>
                                    {booking.statusTransitionAvailables.length >
                                        0 && (
                                        <Box mt={3}>
                                            <BookingStatusActionButton
                                                id={booking.id}
                                            />
                                        </Box>
                                    )}
                                </Box>
                                <Card.Root
                                    overflow="hidden"
                                    variant="elevated"
                                    boxShadow="lg"
                                    bg={bgAccent}
                                    rounded="2xl"
                                >
                                    {picture && (
                                        <Card.Header p="0">
                                            <AspectRatio
                                                ratio={16 / 9}
                                                w="full"
                                            >
                                                <Center
                                                    w="full"
                                                    h="full"
                                                    bg="bg.muted"
                                                    color="fg.subtle"
                                                >
                                                    <Image src={picture} />
                                                </Center>
                                            </AspectRatio>
                                        </Card.Header>
                                    )}
                                    <Card.Body gap={{ base: '5', md: '6' }}>
                                        <Stack gap="3" flex="1">
                                            <Stack>
                                                <Text
                                                    textStyle="sm"
                                                    fontWeight="medium"
                                                    color="yellow.400"
                                                >
                                                    {t(
                                                        'pooling.booking.show.title',
                                                        {
                                                            datePeriod,
                                                            user,
                                                        }
                                                    )}
                                                </Text>
                                                <Heading textStyle="xl">
                                                    {booking.material.name}
                                                </Heading>
                                            </Stack>
                                        </Stack>
                                    </Card.Body>
                                    <Card.Footer>
                                        <Flex direction="column" gap={3}>
                                            <Flex
                                                gap="3"
                                                align="center"
                                                justify="center"
                                            >
                                                {booking.user && (
                                                    <HStack gap="3">
                                                        <Avatar
                                                            src={
                                                                booking.user
                                                                    .avatar
                                                                    ?.contentUrl
                                                            }
                                                        />
                                                        <Box textStyle="sm">
                                                            <Text fontWeight="medium">
                                                                {
                                                                    booking.user
                                                                        .fullname
                                                                }
                                                            </Text>
                                                        </Box>
                                                    </HStack>
                                                )}
                                                <Tag
                                                    variant="solid"
                                                    colorPalette="yellow"
                                                >
                                                    <HStack>
                                                        {t(
                                                            'pooling.booking.show.asksTo'
                                                        )}
                                                        <LuArrowRight />
                                                    </HStack>
                                                </Tag>
                                                <HStack gap="3">
                                                    <Avatar
                                                        src={
                                                            owner?.avatar
                                                                ?.contentUrl
                                                        }
                                                    />
                                                    <Box textStyle="sm">
                                                        <Text fontWeight="medium">
                                                            {owner?.fullname}
                                                        </Text>
                                                    </Box>
                                                </HStack>
                                            </Flex>
                                        </Flex>
                                    </Card.Footer>
                                </Card.Root>

                                <Flex direction="column" gap={10} w={'full'}>
                                    {booking.periods && (
                                        <Box w={'full'}>{bookingPriceStat}</Box>
                                    )}
                                </Flex>
                            </Flex>
                        </GridItem>
                        <GridItem colSpan={{ base: 3, sm: 2 }}>
                            <Flex direction="column" gap={10}>
                                <Card.Root
                                    rounded="3xl"
                                    p="3"
                                    shadow="xl"
                                    bg={bgAccent}
                                >
                                    <Card.Body>
                                        <Flex
                                            direction="column"
                                            w="100%"
                                            maxH={700}
                                        >
                                            {booking.discussion?.messages
                                                ?.edges && (
                                                <Messages
                                                    messages={booking.discussion.messages.edges.map(
                                                        (edge) => edge.node!
                                                    )}
                                                />
                                            )}
                                            <form
                                                onSubmit={handleSubmit(
                                                    onSubmit
                                                )}
                                            >
                                                <Input
                                                    type="hidden"
                                                    {...register('id')}
                                                    value={
                                                        booking.discussion?.id
                                                    }
                                                />
                                                <Box w="full">
                                                    <Box
                                                        borderWidth="1px"
                                                        rounded="l2"
                                                        _focusWithin={{
                                                            borderColor:
                                                                'border.emphasized',
                                                        }}
                                                    >
                                                        <Textarea
                                                            px="4"
                                                            py="4"
                                                            unstyled
                                                            bg="transparent"
                                                            outline="none"
                                                            width="full"
                                                            resize="none"
                                                            placeholder={t(
                                                                'discussion.form.message.placeholder'
                                                            )}
                                                            {...register(
                                                                'message'
                                                            )}
                                                            onKeyDown={
                                                                handleKeyDown
                                                            }
                                                        />
                                                        <Flex
                                                            px="2"
                                                            py="2"
                                                            gap={3}
                                                            justify="end"
                                                            alignItems="center"
                                                        >
                                                            <ExpandingOnFocusButton
                                                                type={'submit'}
                                                                colorPalette="yellow"
                                                                label={t(
                                                                    'global.send'
                                                                )}
                                                                icon={
                                                                    <FaPaperPlane />
                                                                }
                                                            />
                                                        </Flex>
                                                        <Flex
                                                            px="2"
                                                            pb="2"
                                                            justify="end"
                                                            alignItems="center"
                                                            gap={2}
                                                        >
                                                            <Kbd fontSize="xs">
                                                                {t(
                                                                    'discussion.form.message.sendShortcut'
                                                                )}
                                                            </Kbd>
                                                            <Text
                                                                mb={1}
                                                                color="gray.500"
                                                                fontSize="xs"
                                                            >
                                                                {t(
                                                                    'discussion.form.message.toSend'
                                                                )}
                                                            </Text>
                                                        </Flex>
                                                    </Box>
                                                </Box>
                                            </form>
                                        </Flex>
                                    </Card.Body>
                                </Card.Root>
                            </Flex>
                        </GridItem>
                    </Grid>
                </Container>
            </>
        )) || <Loader />
    )
}
