import { PrimaryButton } from '@components/atoms/Button'
import { PoolingRoutes } from '@_/routes/_hooks/routes.enum'
import { CtaVertical } from '@components/molecules/CTA/CtaVertical'
import { Link } from 'react-router'
import { LuPlus } from 'react-icons/lu'
import { useTranslation } from 'react-i18next'

export const MyMaterialsEmptyState = () => {
    const { t } = useTranslation()

    return (
        <CtaVertical
            title={t('pooling.my_materials.index.empty.title')}
            description={t('pooling.my_materials.index.empty.description')}
            emphasisColor="blue.400"
            buttons={[
                {
                    key: 'newMaterial',
                    component: (
                        <PrimaryButton asChild size={'xs'}>
                            <Link to={PoolingRoutes.New}>
                                <LuPlus />
                                {t(
                                    'pooling.my_materials.index.actions.new.label'
                                )}
                            </Link>
                        </PrimaryButton>
                    ),
                },
            ]}
        />
    )
}
