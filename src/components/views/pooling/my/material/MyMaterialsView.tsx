import {
    Card,
    Container,
    Flex,
    Heading,
    SimpleGrid,
    Spinner,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import React, { useEffect, useState } from 'react'
import { Loader } from '@components/atoms/Loader/Loader'
import 'moment/locale/fr'
import { RainbowButton } from '@components/atoms/Button'
import { Button } from '@components/ui/button'
import { MaterialTable } from '@components/molecules/Table/Material/MaterialTable'
import { useMyMaterialsQuery } from '@_/graphql/api/generated'
import { useViewMode, ViewMode } from '@_/hooks/useViewMode'
import { MaterialCard } from '@components/molecules/Cards/MaterialCard/MaterialCard'
import { ToggleViewTableAndCardMode } from '@components/atoms/Button/ToggleViewTableAndCardMode'
import { inUserSpace } from '@components/templates/AccountManagement/inUserSpace'
import { TabNavigationLayout } from '@components/layout/TabNavigationLayout'
import {
    MyMaterialsTabs,
    useMyMaterialsTabs,
} from '@components/views/pooling/my/material/useMyMaterialsTabs'
import { PoolingRoutes } from '@_/routes/_hooks/routes.enum'
import { LuPlus } from 'react-icons/lu'
import { Switch } from '@components/ui/switch'
import { useCookies } from 'react-cookie'
import { Link } from 'react-router'
import { useColorModeValue } from '@components/ui/color-mode'
import { MyMaterialsEmptyState } from '@components/views/pooling/my/material/MyMaterialsEmptyState'

type MyMaterialFilters = {
    statusesList: string[]
}

const MyMaterials = () => {
    const { t } = useTranslation()

    const [cookies, setCookie] = useCookies()
    const [afterCursor, setAfterCursor] = useState<string>()
    const [beforeCursor, setBeforeCursor] = useState<string>()
    const [viewMode, setViewMode] = useViewMode<ViewMode.Table | ViewMode.Card>(
        ViewMode.Card
    )
    const nbToFetch = 15
    const [showPausedMaterials, setShowPausedMaterials] = useState(
        cookies.myMaterialFilters?.statusesList?.includes('paused') ?? false
    )
    const [myMaterialFilters, setMyMaterialFilters] =
        useState<MyMaterialFilters>({
            statusesList: ['published'],
        })

    const tabs = useMyMaterialsTabs()
    useEffect(() => {
        const statusesList = showPausedMaterials
            ? ['paused', 'published']
            : ['published']

        if (myMaterialFilters.statusesList !== statusesList) {
            setMyMaterialFilters((prevFilters) => ({
                ...prevFilters,
                statusesList,
            }))
            setCookie('myMaterialFilters', {
                statusesList,
                path: '/',
            })
        }
    }, [showPausedMaterials, setCookie])

    const variables = {
        afterCursor,
        beforeCursor,
        nbToFetch,
        statusesList: myMaterialFilters?.statusesList,
    }
    const { loading, error, data } = useMyMaterialsQuery({
        variables,
        fetchPolicy: 'no-cache',
    })

    const pageInfo = data?.myMaterials?.pageInfo
    const totalCount = data?.myMaterials?.totalCount
    const materials = data?.myMaterials?.edges?.map((edge) => edge.node!)
    const cardBgColor = useColorModeValue('gray.50', 'gray.800')

    const actions = [
        {
            key: 'showPausedMaterials',
            component: (
                <Switch
                    colorPalette={'yellow'}
                    checked={showPausedMaterials}
                    onCheckedChange={(e: { checked: boolean }) => {
                        setShowPausedMaterials(e.checked)
                    }}
                >
                    {t('pooling.my_materials.index.actions.showPaused')}
                </Switch>
            ),
        },
        {
            key: 'toggleViewMode',
            component: (
                <ToggleViewTableAndCardMode
                    viewMode={viewMode}
                    onClick={setViewMode}
                />
            ),
        },
        {
            key: 'addMaterialButton',
            component: (
                <RainbowButton asChild size={'xs'}>
                    <Link to={PoolingRoutes.New}>
                        <>
                            <LuPlus />
                            {t('pooling.my_materials.index.actions.new.label')}
                        </>
                    </Link>
                </RainbowButton>
            ),
        },
    ]

    return (
        <>
            <Helmet>
                <title>
                    {t('pooling.my_materials.index.title')}{' '}
                    {t('meta.title.suffix')}
                </title>
            </Helmet>
            <Container>
                <Heading size="3xl" py="6">
                    {t('pooling.my_materials.index.title')}
                </Heading>

                <Card.Root mb="20" rounded="2xl" bgColor={cardBgColor}>
                    <Card.Body gap={10}>
                        <TabNavigationLayout
                            {...{
                                tabs,
                                activeTab:
                                    MyMaterialsTabs.MY_PERSONAL_MATERIALS,
                                actions,
                            }}
                        >
                            {(loading && <Loader />) ||
                                (error && <div>Error :(</div>) ||
                                (materials && (
                                    <>
                                        {viewMode === 'table' ? (
                                            <MaterialTable
                                                materials={materials}
                                                emptyState={
                                                    <MyMaterialsEmptyState />
                                                }
                                            />
                                        ) : (
                                            (materials.length > 0 && (
                                                <SimpleGrid
                                                    columns={{
                                                        base: 1,
                                                        md: 4,
                                                        lg: 5,
                                                    }}
                                                    gap={10}
                                                >
                                                    {materials.map(
                                                        (material) => (
                                                            <MaterialCard
                                                                material={
                                                                    material
                                                                }
                                                                key={
                                                                    material.id
                                                                }
                                                            />
                                                        )
                                                    )}
                                                </SimpleGrid>
                                            )) || <MyMaterialsEmptyState />
                                        )}
                                        {pageInfo && pageInfo.hasNextPage && (
                                            <Flex justifyContent="center">
                                                <Button
                                                    disabled={loading}
                                                    onClick={async () => {
                                                        setAfterCursor(
                                                            pageInfo?.endCursor
                                                        )
                                                        setBeforeCursor(
                                                            undefined
                                                        )
                                                    }}
                                                >
                                                    {t('global.next')}{' '}
                                                    {loading && <Spinner />}
                                                </Button>
                                            </Flex>
                                        )}
                                        {pageInfo &&
                                            pageInfo.hasPreviousPage &&
                                            nbToFetch < (totalCount || 0) && (
                                                <Flex justifyContent="center">
                                                    <Button
                                                        disabled={loading}
                                                        onClick={async () => {
                                                            setBeforeCursor(
                                                                pageInfo?.startCursor
                                                            )
                                                            setAfterCursor(
                                                                undefined
                                                            )
                                                        }}
                                                    >
                                                        {t('global.previous')}{' '}
                                                        {loading && <Spinner />}
                                                    </Button>
                                                </Flex>
                                            )}
                                    </>
                                ))}
                        </TabNavigationLayout>
                    </Card.Body>
                </Card.Root>
            </Container>
        </>
    )
}

export const MyMaterialsView = inUserSpace(MyMaterials)
