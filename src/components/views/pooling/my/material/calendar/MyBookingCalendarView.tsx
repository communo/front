import React, { useState } from 'react'
import { Container } from '@chakra-ui/react'
import {
    BreadcrumbCurrentLink,
    BreadcrumbLink,
    BreadcrumbRoot,
} from '@components/ui/breadcrumb'
import { useTranslation } from 'react-i18next'
import { generatePath, Link, useParams } from 'react-router'
import { FiHome } from 'react-icons/fi'
import moment, { locale } from 'moment'
import { Loader } from '@components/atoms/Loader/Loader'
import { H1 } from '@components/atoms/Heading'
import 'moment/locale/fr'
import { moneyFormat } from '@utils/money'
import { datePeriodText } from '@utils/datePeriodText'
import { useAuth } from '@_/auth/useAuth'
import {
    MaterialCalendar__MaterialBookingFragment,
    MaterialCalendar_DatePeriodFragment,
    useMaterialBookingCalendarLazyQuery,
    useMaterialCalendarQuery,
} from '@_/graphql/api/generated'
import { useCalendarOperations } from '@_/hooks/Material/Booking/useCalendarOperations'
import {
    BookingCalendar,
    CalendarEventInfo,
} from '@components/molecules/Calendar/BookingCalendar'
import { PoolingRoutes, StaticPagesRoutes } from '@_/routes/_hooks/routes.enum'
import { UnavailableModal } from './UnavailableModal'
import { BookingSummaryModal } from './BookingSummaryModal'

export const MyBookingCalendarView = () => {
    const { t, i18n } = useTranslation()
    locale(i18n.language)
    const params = useParams()
    const auth = useAuth()

    const { error, data } = useMaterialCalendarQuery({
        variables: {
            id: `/materials/${params.slug}`,
        },
    })
    const [isOpenBookingManagerModal, setBookingManagerModalState] =
        useState(false)
    const [isOpenUnavailableModal, setUnavailableModalState] = useState(false)
    const [selectedBooking, setSelectedBooking] = useState<
        MaterialCalendar__MaterialBookingFragment | undefined
    >(undefined)

    const material = data?.material

    const generateEvent = (
        materialBookingPeriod: MaterialCalendar_DatePeriodFragment
    ) => {
        const { price, booking, startDate, endDate } = materialBookingPeriod
        const { user } = booking

        const isUnavailableEvent = user?.id === auth.user?.id
        let event = {
            groupId: booking.id,
            comment: booking.comment ? `(${booking.comment})` : '',
            start: new Date(startDate),
            end: new Date(moment(endDate).add(1, 'd').format()),
            allDay: true,
            userName: user?.fullname,
            userId: user?.id,
            datePeriod: datePeriodText(startDate, endDate, t, i18n.language),
            price: price && moneyFormat(price, i18n.language),
            backgroundColor: '#ffde59',
            borderColor: '#ffde59',
            textColor: '#000',
            modal: () => setBookingManagerModalState(true),
            editable: false,
        }

        if (isUnavailableEvent) {
            event = {
                ...event,
                modal: () => setUnavailableModalState(true),
                backgroundColor: '#ffde59',
                borderColor: 'gray',
                textColor: 'black',
                editable: true,
            }
        }
        return event
    }
    const events =
        material?.bookings?.edges?.flatMap((edge) =>
            edge.node?.periods?.map((period) => generateEvent(period))
        ) ?? []

    const [queryBookingLazyQuery] = useMaterialBookingCalendarLazyQuery()

    const { changeDate, deleteUnavailable, unavailable } =
        useCalendarOperations()

    const handleEventMove = async (eventInfo: CalendarEventInfo) => {
        return changeDate({
            variables: {
                id: eventInfo.event.groupId,
                startDate: moment(eventInfo.event.start).format('Y/MM/DD'),
                endDate: moment(eventInfo.event.end)
                    .subtract(1, 'second')
                    .format('Y/MM/DD'),
            },
        })
    }

    const eventTitle = ({
        userId,
        userName,
        datePeriod,
        price,
        comment = '',
    }: Record<string, unknown>) => {
        if (userId === auth.user?.id) {
            return `${t(
                'pooling.booking.calendar.unavailable.eventTitle'
            )} ${comment}`
        }
        return `${userName} · ${datePeriod} · ${price}`
    }

    return (
        (error && <p>Error </p>) ||
        (material && (
            <Container maxW="7xl">
                <BreadcrumbRoot py={2} color="yellow.600">
                    <BreadcrumbLink asChild>
                        <Link to={StaticPagesRoutes.Home}>
                            <FiHome />
                        </Link>
                    </BreadcrumbLink>
                    <BreadcrumbLink asChild>
                        <Link to={PoolingRoutes.Search}>
                            {t('menu.my.materials')}
                        </Link>
                    </BreadcrumbLink>
                    <BreadcrumbLink asChild>
                        <Link
                            to={generatePath(PoolingRoutes.Show, {
                                slug: params.slug!,
                            })}
                        >
                            {material.name}
                        </Link>
                    </BreadcrumbLink>
                    <BreadcrumbCurrentLink>
                        {t('menu.my.material', {
                            material,
                        })}
                    </BreadcrumbCurrentLink>
                </BreadcrumbRoot>
                <H1>
                    {t('pooling.my_materials.calendar.heading', {
                        name: material.name,
                    })}
                </H1>
                <BookingCalendar
                    // @ts-ignore-next-line
                    events={events}
                    handleEventMove={handleEventMove}
                    eventTitle={eventTitle}
                    queryBookingLazyQuery={queryBookingLazyQuery}
                    unavailable={unavailable}
                    materialId={material?.id}
                    setSelectedBooking={setSelectedBooking}
                />
                {selectedBooking && (
                    <>
                        <BookingSummaryModal
                            isOpen={isOpenBookingManagerModal}
                            close={() => setBookingManagerModalState(false)}
                            booking={selectedBooking}
                        />

                        <UnavailableModal
                            isOpen={isOpenUnavailableModal}
                            close={() => setUnavailableModalState(false)}
                            booking={selectedBooking}
                            onDelete={deleteUnavailable}
                        />
                    </>
                )}
            </Container>
        )) || <Loader />
    )
}
