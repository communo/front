import React, { useEffect } from 'react'
import { Box, HStack, Textarea } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import moment, { locale } from 'moment'
import { useForm } from 'react-hook-form'
import { FaTrash } from 'react-icons/fa'
import ReactMarkdown from 'react-markdown'
import rehypeRaw from 'rehype-raw'
import { DangerButton } from '@components/atoms/Button'
import { Button } from '@components/ui/button'
import 'moment/locale/fr'
import {
    CommentMaterialBookingInput,
    useCommentMaterialBookingMutation,
} from '@_/graphql/api/generated'
import {
    DialogBackdrop,
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogFooter,
    DialogHeader,
    DialogRoot,
} from '@components/ui/dialog'
import { H3 } from '@components/atoms/Heading'
import { Props } from './types'

export const UnavailableModal = ({
    isOpen,
    close,
    onDelete,
    booking,
}: Props) => {
    const { t, i18n } = useTranslation()
    locale(i18n.language)

    const { handleSubmit, register, setValue } =
        useForm<CommentMaterialBookingInput>({
            defaultValues: {
                id: booking.id,
                comment: booking.comment,
            },
        })

    useEffect(() => {
        setValue('id', booking.id)
        setValue('comment', booking.comment ?? '')
    }, [booking, setValue])
    const [comment] = useCommentMaterialBookingMutation()
    const onSubmit = (variables: CommentMaterialBookingInput) => {
        comment({
            variables,
            onCompleted: (data) => {
                if (data?.commentMaterialBooking?.materialBooking) {
                    close()
                }
            },
        })
    }

    return (
        <DialogRoot
            lazyMount
            open={isOpen}
            onOpenChange={() => close()}
            placement={'center'}
        >
            <DialogBackdrop />
            <DialogContent bgColor={'gray.700'}>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <DialogHeader>
                        <H3>
                            {t(
                                'pooling.booking.calendar.unavailableModal.title',
                                {
                                    booking,
                                    dateOfCreation: moment(
                                        booking.createdAt
                                    ).format('L'),
                                    user: booking.user,
                                }
                            )}
                        </H3>
                    </DialogHeader>
                    <DialogCloseTrigger title={t('modal.close')} />
                    <DialogBody>
                        <Box>
                            <ReactMarkdown rehypePlugins={[rehypeRaw]}>
                                {t(
                                    'pooling.booking.calendar.unavailableModal.content.description'
                                )}
                            </ReactMarkdown>
                            <Textarea {...register('comment')} rows={5} />
                        </Box>
                    </DialogBody>
                    <DialogFooter>
                        <HStack>
                            <Button type="submit">{t('global.ok')}</Button>
                            <DangerButton
                                onClick={() =>
                                    onDelete({
                                        variables: {
                                            id: booking.id,
                                        },
                                    }).then(() => {
                                        close()
                                    })
                                }
                            >
                                <FaTrash />
                                {t('global.delete')}
                            </DangerButton>
                        </HStack>
                    </DialogFooter>
                </form>
            </DialogContent>
        </DialogRoot>
    )
}
