import * as React from 'react'
import { useState } from 'react'
import {
    Box,
    Card,
    Container,
    Flex,
    Heading,
    IconButton,
    SimpleGrid,
    Spinner,
    useDisclosure,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { PrimaryButton } from '@components/atoms/Button'
import { Button } from '@components/ui/button'
import { Loader } from '@components/atoms/Loader/Loader'
import {
    MyAlertsDocument,
    useDeleteAlertMutation,
    useMyAlertsQuery,
} from '@_/graphql/api/generated'
import { SearchAlertCard } from '@components/molecules/Cards/SearchAlertCard/SearchAlertCard'
import { FaTrash } from 'react-icons/fa'
import { toast } from 'react-toastify'
import { CtaVertical } from '@components/molecules/CTA/CtaVertical'
import { PoolingRoutes } from '@_/routes/_hooks/routes.enum'
import { inUserSpace } from '@components/templates/AccountManagement/inUserSpace'
import { Link } from 'react-router'
import { ToggleViewTableAndCardMode } from '@components/atoms/Button/ToggleViewTableAndCardMode'
import { TabNavigationLayout } from '@components/layout/TabNavigationLayout'
import { useViewMode, ViewMode } from '@_/hooks/useViewMode'
import { Table } from '@components/molecules/Table/Table'
import {
    SearchAlertRow,
    useSearchAlertTableColumns,
} from '@components/molecules/Table/Material/Search/useSearchAlertTableColumns'
import { useColorModeValue } from '@components/ui/color-mode'

export const MyAlerts = () => {
    const { t } = useTranslation()
    const [afterCursor, setAfterCursor] = useState<string>()
    const [beforeCursor, setBeforeCursor] = useState<string>()
    const nbToFetch = 9 // 3 x 3 cards
    const cardBgColor = useColorModeValue('gray.50', 'gray.800')
    const [viewMode, setViewMode] = useViewMode<ViewMode.Table | ViewMode.Card>(
        ViewMode.Card
    )
    const variables = {
        afterCursor,
        beforeCursor,
        first: nbToFetch,
    }
    const {
        open: deleting,
        onOpen: setDeleting,
        onClose: unsetDeleting,
    } = useDisclosure()
    const { data, loading } = useMyAlertsQuery({
        variables,
    })
    const [deleteAlert] = useDeleteAlertMutation()
    const onDelete = async (id: string) => {
        setDeleting()
        deleteAlert({
            variables: {
                id,
            },
            refetchQueries: [
                {
                    query: MyAlertsDocument,
                    variables,
                },
            ],
            onCompleted: () => {
                unsetDeleting()
                toast.success(
                    t('pooling.my_alerts.index.actions.delete.success')
                )
            },
        })
    }
    const deleteIconButton = (id: string) => (
        <IconButton
            aria-label={t('global.delete')}
            disabled={deleting}
            onClick={() => onDelete(id)}
        >
            <FaTrash />
        </IconButton>
    )

    const columns = useSearchAlertTableColumns()
    const rows =
        data?.materialSearchAlerts?.edges?.map((edge) => {
            return {
                ...edge.node,
                actions: (
                    <Flex
                        alignItems="center"
                        gap={3}
                        justifyContent="flex-end"
                        flexWrap="wrap"
                    >
                        <Button
                            disabled={deleting}
                            onClick={() => onDelete(edge.node!.id)}
                        >
                            {t('global.delete')}
                        </Button>
                    </Flex>
                ),
            }
        }) || ([] as SearchAlertRow[])

    if (!loading && data?.materialSearchAlerts?.edges) {
        const alerts = data?.materialSearchAlerts?.edges
            ?.map((edge) => edge.node)
            .filter(Boolean)
        const { pageInfo, totalCount } = data.materialSearchAlerts
        return (
            <>
                <Helmet>
                    <title>
                        {t('pooling.my_alerts.meta.title')}
                        {t('meta.title.suffix')}
                    </title>
                </Helmet>

                <Container>
                    <Heading size="3xl" py="6">
                        {t('pooling.my_alerts.index.title')}
                    </Heading>

                    <Card.Root mb="20" rounded="2xl" bgColor={cardBgColor}>
                        <Card.Body gap={10}>
                            <TabNavigationLayout
                                {...{
                                    tabs: [],
                                    actions: [
                                        {
                                            key: 'toggleViewMode',
                                            component: (
                                                <ToggleViewTableAndCardMode
                                                    viewMode={viewMode}
                                                    onClick={setViewMode}
                                                />
                                            ),
                                        },
                                    ],
                                }}
                            >
                                <Box w="100%">
                                    {(loading && <Loader />) ||
                                        (data.materialSearchAlerts?.edges
                                            ?.length > 0 && (
                                            <>
                                                {viewMode === ViewMode.Table ? (
                                                    <Table<SearchAlertRow>
                                                        columns={columns}
                                                        data={rows}
                                                        enableSorting={false}
                                                    ></Table>
                                                ) : (
                                                    <SimpleGrid
                                                        mt={50}
                                                        columns={{
                                                            base: 1,
                                                            lg: 2,
                                                            '2xl': 3,
                                                        }}
                                                        gap={4}
                                                    >
                                                        {alerts.map(
                                                            (alert) =>
                                                                alert && (
                                                                    <SearchAlertCard
                                                                        key={
                                                                            alert.id
                                                                        }
                                                                        alert={
                                                                            alert
                                                                        }
                                                                        button={deleteIconButton(
                                                                            alert.id
                                                                        )}
                                                                    />
                                                                )
                                                        )}
                                                    </SimpleGrid>
                                                )}
                                            </>
                                        ))}

                                    {(loading && <Loader />) ||
                                        (alerts?.length > 0 && (
                                            <>
                                                <Box my={10}>
                                                    {pageInfo &&
                                                        pageInfo.hasNextPage && (
                                                            <Flex justifyContent="center">
                                                                <Button
                                                                    disabled={
                                                                        loading
                                                                    }
                                                                    onClick={async () => {
                                                                        setAfterCursor(
                                                                            pageInfo?.endCursor
                                                                        )
                                                                        setBeforeCursor(
                                                                            undefined
                                                                        )
                                                                    }}
                                                                >
                                                                    {t(
                                                                        'global.next'
                                                                    )}{' '}
                                                                    {loading && (
                                                                        <Spinner />
                                                                    )}
                                                                </Button>
                                                            </Flex>
                                                        )}
                                                    {pageInfo &&
                                                        pageInfo.hasPreviousPage &&
                                                        nbToFetch <
                                                            (totalCount ||
                                                                0) && (
                                                            <Flex justifyContent="center">
                                                                <Button
                                                                    disabled={
                                                                        loading
                                                                    }
                                                                    onClick={async () => {
                                                                        setBeforeCursor(
                                                                            pageInfo?.startCursor
                                                                        )
                                                                        setAfterCursor(
                                                                            undefined
                                                                        )
                                                                    }}
                                                                >
                                                                    {t(
                                                                        'global.previous'
                                                                    )}{' '}
                                                                    {loading && (
                                                                        <Spinner />
                                                                    )}
                                                                </Button>
                                                            </Flex>
                                                        )}
                                                </Box>
                                            </>
                                        )) ||
                                        (alerts?.length === 0 && (
                                            <CtaVertical
                                                title={t(
                                                    'pooling.my_alerts.index.empty.title'
                                                )}
                                                description={t(
                                                    'pooling.my_alerts.index.empty.description'
                                                )}
                                                emphasisColor="orange.400"
                                                buttons={[
                                                    {
                                                        key: 'searchBtn',
                                                        component: (
                                                            <PrimaryButton
                                                                asChild
                                                            >
                                                                <Link
                                                                    to={
                                                                        PoolingRoutes.Search
                                                                    }
                                                                >
                                                                    {t(
                                                                        'pooling.my_alerts.index.empty.button'
                                                                    )}
                                                                </Link>
                                                            </PrimaryButton>
                                                        ),
                                                    },
                                                ]}
                                            />
                                        ))}
                                </Box>
                            </TabNavigationLayout>
                        </Card.Body>
                    </Card.Root>
                </Container>
            </>
        )
    }
    return <Loader />
}

export const MyAlertsView = inUserSpace(MyAlerts)
