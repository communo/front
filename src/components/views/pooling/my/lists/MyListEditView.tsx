import * as React from 'react'
import { Fragment } from 'react'
import { Box, Flex, Heading, Text } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { Loader } from '@components/atoms/Loader/Loader'
import {
    useMyListEditViewQuery,
    useUpdateUserListMutation,
} from '@_/graphql/api/generated'
import { generatePath, Link, useNavigate, useParams } from 'react-router'
import { FaChevronRight } from 'react-icons/fa'
import { UserRoutes } from '@_/routes/_hooks/routes.enum'
import {
    EditUserListForm,
    UserListFormValues,
} from '@components/molecules/Form/AccountManagement/UserList/EditUserListForm'
import { toast } from 'react-toastify'
import { inUserSpace } from '@components/templates/AccountManagement/inUserSpace'

const MyListEdit = () => {
    const { slug } = useParams<{ slug: string }>()
    const { t } = useTranslation()
    const variables = {
        id: `/user_lists/${slug}`,
    }
    const { data, loading } = useMyListEditViewQuery({
        variables,
    })
    const [updateUserList] = useUpdateUserListMutation()
    const navigate = useNavigate()
    if (loading) {
        return <Loader />
    }

    const handleSubmit = (value: UserListFormValues) => {
        updateUserList({
            variables: {
                input: {
                    id: `/user_lists/${slug}`,
                    name: value.name,
                },
            },
        }).then((r) => {
            if (r.data?.updateUserList?.userList) {
                toast.success(t('pooling.my_lists.edit.toast.success'))
            } else {
                toast.error(t('pooling.my_lists.edit.toast.error'))
            }
            if (
                r.data?.updateUserList?.userList?.slug &&
                slug !== r.data?.updateUserList?.userList?.slug
            ) {
                navigate(
                    generatePath(UserRoutes.EditMyList, {
                        slug: r.data?.updateUserList?.userList?.slug,
                    })
                )
            }
        })
    }

    if (data?.userList) {
        return (
            <>
                <Helmet>
                    <title>
                        {t('pooling.my_list.meta.title', {
                            name: data.userList.name,
                        })}
                        {t('meta.title.suffix')}
                    </title>
                </Helmet>

                <Box textAlign="center" fontSize="xl" w="100%">
                    <Heading my={4}>
                        <Flex alignItems="center" gap={3}>
                            <Link to={UserRoutes.MyLists}>
                                {t('pooling.my_lists.index.title')}
                            </Link>
                            <FaChevronRight />
                            <Text color={'yellow.400'}>
                                {data.userList.name}
                            </Text>
                        </Flex>
                    </Heading>

                    <EditUserListForm
                        userList={data.userList}
                        variables={variables}
                        onSubmit={(value) => handleSubmit(value)}
                    />
                </Box>
            </>
        )
    }
    return <Loader />
}

export const MyListEditView = inUserSpace(MyListEdit)
