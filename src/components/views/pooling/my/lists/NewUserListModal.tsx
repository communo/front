import { Input } from '@chakra-ui/react'
import {
    DialogActionTrigger,
    DialogBackdrop,
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogFooter,
    DialogHeader,
    DialogRoot,
    DialogTrigger,
} from '@components/ui/dialog'
import {
    ConditionMaterialForm_UserListDocument,
    CreateUserListPayload,
    useNewUserListModalMutation,
} from '@_/graphql/api/generated'
import {
    schema,
    UserListFormValues,
} from '@components/molecules/Form/AccountManagement/UserList/EditUserListForm'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { RequiredAsterisk } from '@components/atoms/Form/RequiredAsterisk'
import { PrimaryButton } from '@components/atoms/Button'
import { useTranslation } from 'react-i18next'
import { toast } from 'react-toastify'
import { EmphasisText } from '@components/atoms/Text/EmphasisText'
import { useAuth } from '@_/auth/useAuth'
import { generatePath, useNavigate } from 'react-router'
import { UserRoutes } from '@_/routes/_hooks/routes.enum'
import { randomTrans } from '@utils/random'
import { Field } from '@components/ui/field'
import { Alert } from '@components/ui/alert'
import { Button } from '@components/ui/button'

type NewUserListModalProps = {
    afterSubmit?: (item: CreateUserListPayload['userList']) => void
    label?: string
}

export const NewUserListModal = ({
    afterSubmit,
    label,
}: NewUserListModalProps) => {
    const [createUserListMutation] = useNewUserListModalMutation()
    const { t } = useTranslation()

    const {
        handleSubmit,
        register,
        formState: { isSubmitting, errors },
    } = useForm<UserListFormValues>({
        resolver: yupResolver(schema),
    })
    const auth = useAuth()
    const navigate = useNavigate()

    const onSubmit = (values: UserListFormValues) => {
        createUserListMutation({
            variables: {
                input: {
                    name: values.name,
                },
            },
            refetchQueries: [{ query: ConditionMaterialForm_UserListDocument }],
        }).then((r) => {
            if (r.data?.createUserList?.userList) {
                if (afterSubmit) {
                    afterSubmit(r.data?.createUserList?.userList)
                    return
                }
                toast.success(
                    randomTrans(t, 'pooling.my_lists.form.new.toast.success', 3)
                )
                navigate(
                    generatePath(UserRoutes.EditMyList, {
                        slug: r.data.createUserList.userList.slug,
                    })
                )
            }
        })
    }
    return (
        <DialogRoot blockScrollOnMount={false} placement="center">
            <DialogBackdrop />
            <DialogTrigger asChild>
                <Button variant="ghost">
                    {label || t('pooling.form.userList.createNew')}
                </Button>
            </DialogTrigger>
            <DialogContent>
                <DialogHeader>
                    {t('pooling.my_lists.index.actions.new')}
                </DialogHeader>
                <DialogCloseTrigger />
                <DialogBody p={25} display={'flex'} flexDir={'column'} gap={5}>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <Field
                            id="name"
                            invalid={errors.name !== undefined}
                            label={
                                <>
                                    {t('pooling.my_lists.form.name.label')}{' '}
                                    <RequiredAsterisk />
                                </>
                            }
                        >
                            <Input
                                type="text"
                                placeholder={t(
                                    'pooling.my_lists.form.name.placeholder'
                                )}
                                required={true}
                                {...register('name', {
                                    required: t('form.required').toString(),
                                })}
                            />
                        </Field>
                    </form>
                    <Alert status="info">
                        <EmphasisText color={'yellow.400'} mb={5}>
                            {t('pooling.my_lists.form.new.content', {
                                context: auth.user?.gender,
                            })}
                        </EmphasisText>
                    </Alert>
                </DialogBody>

                <DialogFooter>
                    <DialogActionTrigger asChild>
                        <PrimaryButton
                            fontFamily="heading"
                            w={'full'}
                            disabled={isSubmitting}
                            type="button"
                            onClick={handleSubmit(onSubmit)}
                        >
                            {t('global.create')}
                        </PrimaryButton>
                    </DialogActionTrigger>
                </DialogFooter>
            </DialogContent>
        </DialogRoot>
    )
}
