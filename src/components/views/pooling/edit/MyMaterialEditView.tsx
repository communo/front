import React, { useEffect, useState } from 'react'
import { BreadcrumbLink, Container } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { generatePath, Link, useParams } from 'react-router'
import { FiHome } from 'react-icons/fi'
import { useAuth } from '@_/auth/useAuth'
import { Unauthorized403 } from '@components/molecules/Error/Unauthorized403'
import { Loader } from '@components/atoms/Loader/Loader'
import { EditMaterialForm } from '@components/molecules/Form/Pooling/Material/EditMaterialForm'
import { useEditMaterialQuery } from '@_/graphql/api/generated'
import { isGranted } from '@utils/isGranted'
import {
    BreadcrumbCurrentLink,
    BreadcrumbRoot,
} from '@components/ui/breadcrumb'
import { MaterialFormSteps } from '@components/molecules/Form/Pooling/Material/types'
import { PoolingRoutes } from '@_/routes/_hooks/routes.enum'

export const MyMaterialEditView = () => {
    const { t } = useTranslation()
    const auth = useAuth()
    const { slug, step = MaterialFormSteps.Informations } = useParams<{
        slug: string
        step: MaterialFormSteps
    }>()
    const id = generatePath(PoolingRoutes.IRI, { slug: slug! })
    // const [material, setMaterial] = useState<EditMaterialQuery['material']>()
    const [canEdit, setCanEdit] = useState<boolean>()
    useEffect(() => {
        if (auth.user?.slug) {
            isGranted(auth.user?.slug, 'edit', id).then((decision) => {
                setCanEdit(decision)
            })
        }
    }, [auth.user?.slug, id])
    const { loading, data } = useEditMaterialQuery({
        variables: {
            id,
        },
    })
    const material = data?.material
    if (canEdit === false) {
        return <Unauthorized403 />
    }
    if (loading || !material || canEdit === undefined) {
        return <Loader />
    }

    return (
        <>
            <Helmet>
                <title>
                    {t('pooling.edit.title', { material })} {'< Communo'}
                </title>
            </Helmet>
            {material && step && canEdit === true && (
                <Container maxW="7xl">
                    <BreadcrumbRoot py={2} color="yellow.600">
                        <BreadcrumbLink asChild>
                            <Link to="/">
                                <FiHome />
                            </Link>
                        </BreadcrumbLink>
                        <BreadcrumbLink asChild>
                            <Link to="/pooling">{t('menu.pooling')}</Link>
                        </BreadcrumbLink>
                        <BreadcrumbLink asChild>
                            <Link to={`/pooling/${material.slug}`}>
                                {material.name}
                            </Link>
                        </BreadcrumbLink>
                        <BreadcrumbCurrentLink>
                            {t('pooling.edit.breadcrumb')}
                        </BreadcrumbCurrentLink>
                    </BreadcrumbRoot>
                    <EditMaterialForm {...{ step, material }} />
                </Container>
            )}
        </>
    )
}
