import React from 'react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { Container } from '@chakra-ui/react'
import { NewMaterialForm } from '@components/molecules/Form/Pooling/Material/NewMaterialForm'
import { withUser } from '@_/components/templates/AccountManagement/withUser'

export const MaterialNew = () => {
    const { t } = useTranslation()

    return (
        <>
            <Helmet>
                <title>
                    {t('pooling.new.title')} {'< Communo'}
                </title>
            </Helmet>
            <Container maxW="3xl">
                <NewMaterialForm />
            </Container>
        </>
    )
}

export const MaterialNewView = withUser(MaterialNew)
