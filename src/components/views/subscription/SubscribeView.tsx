import React, { useEffect, useState } from 'react'
import {
    Box,
    Button,
    Container,
    Flex,
    Heading,
    IconButton,
    Link,
    Separator,
    SimpleGrid,
    Text,
    useDisclosure,
    VStack,
} from '@chakra-ui/react'
import { motion } from 'framer-motion'
import { useTranslation } from 'react-i18next'
import { generatePath, useNavigate, useParams } from 'react-router'
import { FaArrowCircleLeft, FaArrowCircleRight } from 'react-icons/fa'
import {
    useSubscribeView_CreateCircleMutation,
    useSubscribeView_SubscribeMutation,
    useSubscribeViewQuery,
} from '@_/graphql/api/generated'
import { CircleLogo } from '@components/molecules/User/Circle/CircleLogo'
import { CommunitiesRoutes } from '@_/routes/_hooks/routes.enum'
import { SubscriptionCongratulationModal } from '@components/molecules/Modal/SubscriptionCongratulationModal/SubscribtionCongratulationModal'
import { useColorModeValue } from '@components/ui/color-mode'
import { Tooltip } from '@components/ui/tooltip'
import { NumberInputField, NumberInputRoot } from '@components/ui/number-input'
import { Alert } from '@components/ui/alert'
import {
    DialogBackdrop,
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogHeader,
    DialogRoot,
} from '@components/ui/dialog'
import { CreateCircleForm } from './CreateCircleForm'

const MotionBox = motion(Box)

export const SubscribeView = () => {
    const params = useParams()
    const { t } = useTranslation()
    const { open, onOpen, onClose } = useDisclosure()
    const [selectedCircle, setSelectedCircle] = useState('')
    const [numUsers, setNumUsers] = useState(0)
    const [createCircle, { loading: creatingCircle }] =
        useSubscribeView_CreateCircleMutation()
    const [first, setFirst] = useState<number | undefined>(3)
    const [after, setAfter] = useState<string>()
    const [last, setLast] = useState<number>()
    const [before, setBefore] = useState<string>()

    const textColor = useColorModeValue('gray.900', 'gray.100')
    const bgColor = useColorModeValue('white', 'gray.800')
    const borderColor = useColorModeValue('gray.200', 'gray.700')
    const cardBgColor = useColorModeValue('gray.100', 'gray.700')
    const cardBorderColor = useColorModeValue('gray.300', 'gray.600')
    const cardSelectedBgColor = useColorModeValue('yellow.500', 'gray.900')
    const cardSelectedColor = useColorModeValue('white', 'white')
    const cardInactiveBgColor = useColorModeValue('gray.400', 'gray.600')
    const cardInactiveTextColor = useColorModeValue('gray.700', 'gray.400')
    const coverPlaceholderColor = useColorModeValue('gray.200', 'gray.600')

    const {
        open: isSubscriptionCongratsModalOpen,
        onOpen: openSubscriptionCongratsModal,
    } = useDisclosure()

    const { data, loading, error, refetch } = useSubscribeViewQuery({
        variables: {
            id: `/plans/${params.slug}`,
            first,
            after,
            last,
            before,
        },
    })
    const [subscribe] = useSubscribeView_SubscribeMutation()
    const navigate = useNavigate()

    useEffect(() => {
        if (!loading && data?.myCircles?.edges?.length === 0) {
            setSelectedCircle('')
        }
    }, [loading, data])

    useEffect(() => {
        if (selectedCircle) {
            const selectedCircleData = data?.myCircles?.edges?.find(
                (circle) => circle.node?.id === selectedCircle
            )?.node
            setNumUsers(selectedCircleData?.memberships?.totalCount ?? 0)
        }
    }, [selectedCircle, data])

    if (loading) return <Text>{t('subscription.subscribe.loading')}</Text>
    if (error)
        return (
            <Text>
                {t('subscription.subscribe.error', { error: error.message })}
            </Text>
        )

    const plan = data?.plan
    const circles = data?.myCircles?.edges
    const pageInfo = data?.myCircles?.pageInfo

    const selectedCircleData = circles?.find(
        (circle) => circle.node?.id === selectedCircle
    )?.node

    const costPerUser = plan?.price || 0
    const totalCost = numUsers * costPerUser

    const handleCircleChange = (circleId: string) => {
        setSelectedCircle(circleId)
    }

    const handleSubscribe = () => {
        subscribe({
            variables: {
                input: {
                    circle: selectedCircle,
                    plan: plan?.id!,
                },
            },
        })
        openSubscriptionCongratsModal()
    }

    const handleCreateCircle = async (name: string) => {
        const result = await createCircle({
            variables: {
                input: {
                    indexable: false,
                    name,
                },
            },
        })
        const newCircleId = result.data?.createCircle?.circle?.id
        await refetch()
        if (newCircleId) {
            setSelectedCircle(newCircleId)
            setNumUsers(0) // Reset numUsers for the newly created circle
        }
    }

    const handleNextPage = () => {
        if (pageInfo?.hasNextPage) {
            setFirst(6)
            setAfter(pageInfo.endCursor)
            setLast(undefined)
            setBefore(undefined)
        }
    }

    const handlePreviousPage = () => {
        if (pageInfo?.hasPreviousPage) {
            setFirst(undefined)
            setAfter(undefined)
            setLast(6)
            setBefore(pageInfo.startCursor)
        }
    }

    return (
        <Container maxW="container.lg" mt={'90px'} mb={50}>
            <MotionBox
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                transition={{ duration: 0.5 }}
                p={10}
                borderWidth={1}
                borderRadius="3xl"
                boxShadow="lg"
                bg={bgColor}
                borderColor={borderColor}
                color={textColor}
            >
                <VStack gap={6} align="stretch">
                    <Heading as="h1" size="2xl" textAlign="center">
                        {t('subscription.subscribe.title', {
                            planName: plan?.name,
                        })}
                    </Heading>
                    <Text fontSize="lg" textAlign="center">
                        {plan?.description}
                    </Text>
                    <Separator />
                    <Flex
                        justify="space-between"
                        direction={'column'}
                        align="center"
                        mt={6}
                        mb={4}
                    >
                        <Heading as="h2" size="lg">
                            {t('subscription.subscribe.selectCircle')}
                        </Heading>
                        <Link onClick={onOpen} color="blue.500">
                            {t('subscription.subscribe.createCircle')}
                        </Link>
                    </Flex>
                    {circles && circles.length === 0 && (
                        <Box>
                            <Text textAlign="center" mb={4}>
                                {t('subscription.subscribe.noCircles')}
                            </Text>
                            <CreateCircleForm
                                onCreate={handleCreateCircle}
                                loading={creatingCircle}
                            />
                        </Box>
                    )}
                    {circles && circles.length > 0 && (
                        <>
                            <SimpleGrid
                                columns={{ base: 1, md: 2, lg: 3 }}
                                gap={4}
                            >
                                {circles.map((circle) => {
                                    const isSelected =
                                        circle.node?.id === selectedCircle
                                    const coverUrl =
                                        circle.node?.cover?.contentUrl
                                    const hasActiveSubscription =
                                        circle.node?.subscription?.active &&
                                        circle.node?.subscription?.plan
                                            ?.name === plan?.name

                                    // eslint-disable-next-line no-nested-ternary
                                    const bg = isSelected
                                        ? cardSelectedBgColor
                                        : hasActiveSubscription
                                          ? cardInactiveBgColor
                                          : cardBgColor
                                    const border = isSelected
                                        ? cardSelectedColor
                                        : cardBorderColor
                                    // eslint-disable-next-line no-nested-ternary
                                    const color = isSelected
                                        ? cardSelectedColor
                                        : hasActiveSubscription
                                          ? cardInactiveTextColor
                                          : textColor

                                    const cardContent = (
                                        <MotionBox
                                            key={circle.node?.id}
                                            borderWidth={2}
                                            borderRadius="lg"
                                            bg={bg}
                                            borderColor={border}
                                            color={color}
                                            onClick={() =>
                                                !hasActiveSubscription &&
                                                handleCircleChange(
                                                    circle.node?.id!
                                                )
                                            }
                                            onKeyPress={(e) => {
                                                if (
                                                    e.key === 'Enter' &&
                                                    !hasActiveSubscription
                                                ) {
                                                    handleCircleChange(
                                                        circle.node?.id!
                                                    )
                                                }
                                            }}
                                            tabIndex={0}
                                            cursor={
                                                hasActiveSubscription
                                                    ? 'not-allowed'
                                                    : 'pointer'
                                            }
                                            whileHover={
                                                !hasActiveSubscription
                                                    ? { scale: 1.05 }
                                                    : {}
                                            }
                                            whileTap={
                                                !hasActiveSubscription
                                                    ? { scale: 0.95 }
                                                    : {}
                                            }
                                            transform={
                                                isSelected
                                                    ? 'scale(1.05)'
                                                    : 'none'
                                            }
                                            position="relative"
                                            opacity={
                                                hasActiveSubscription ? 0.6 : 1
                                            }
                                        >
                                            <Box
                                                position="relative"
                                                borderTopRadius="lg"
                                                overflow="hidden"
                                            >
                                                <Box
                                                    bgImage={
                                                        coverUrl
                                                            ? `url(${coverUrl})`
                                                            : coverPlaceholderColor
                                                    }
                                                    bgSize="cover"
                                                    bgPos="center"
                                                    h="100px"
                                                />
                                                <Flex
                                                    direction="column"
                                                    align="center"
                                                    position="absolute"
                                                    top="50%"
                                                    left="50%"
                                                    transform="translate(-50%, -50%)"
                                                    my={2}
                                                >
                                                    <CircleLogo
                                                        circle={circle.node!}
                                                        size={'xl'}
                                                    />
                                                </Flex>
                                            </Box>
                                            <Flex
                                                direction="column"
                                                align="center"
                                                gap={2}
                                                p={4}
                                            >
                                                <Text
                                                    fontSize="lg"
                                                    fontWeight="bold"
                                                >
                                                    {circle.node?.name}
                                                </Text>
                                                {hasActiveSubscription && (
                                                    <Text
                                                        fontSize="sm"
                                                        color={
                                                            cardInactiveTextColor
                                                        }
                                                    >
                                                        {t(
                                                            'subscription.subscribe.active',
                                                            {
                                                                plan: circle
                                                                    .node
                                                                    ?.subscription
                                                                    ?.plan
                                                                    ?.name,
                                                            }
                                                        )}
                                                    </Text>
                                                )}
                                            </Flex>
                                        </MotionBox>
                                    )

                                    return hasActiveSubscription ? (
                                        <Tooltip
                                            key={circle.node?.id}
                                            label={t(
                                                'subscription.subscribe.tooltip',
                                                {
                                                    plan: circle.node
                                                        ?.subscription?.plan
                                                        ?.name,
                                                }
                                            )}
                                            fontSize="md"
                                            placement="top"
                                            hasArrow
                                        >
                                            {cardContent}
                                        </Tooltip>
                                    ) : (
                                        cardContent
                                    )
                                })}
                            </SimpleGrid>
                            <Flex justify="space-between" mt={4}>
                                <IconButton
                                    aria-label={t(
                                        'subscription.subscribe.previousPage'
                                    )}
                                    onClick={handlePreviousPage}
                                    disabled={!pageInfo?.hasPreviousPage}
                                >
                                    <FaArrowCircleLeft />
                                </IconButton>
                                <IconButton
                                    aria-label={t(
                                        'subscription.subscribe.nextPage'
                                    )}
                                    onClick={handleNextPage}
                                    disabled={!pageInfo?.hasNextPage}
                                >
                                    <FaArrowCircleRight />
                                </IconButton>
                            </Flex>
                        </>
                    )}
                    {selectedCircle && (
                        <SimpleGrid columns={2}>
                            <Box mt={6} textAlign="center">
                                <Heading as="h3" size="lg">
                                    {t('subscription.subscribe.costSummary')}
                                </Heading>
                                <Flex
                                    justify="center"
                                    alignItems="center"
                                    mt={4}
                                    mb={4}
                                    gap={2}
                                >
                                    <Text fontSize="md" mr={2}>
                                        {t(
                                            'subscription.subscribe.numberOfUsersLabel'
                                        )}
                                    </Text>
                                    <NumberInputRoot
                                        defaultValue="10"
                                        width="200px"
                                        onValueChange={(e: {
                                            valueAsNumber: number
                                        }) => setNumUsers(e.valueAsNumber)}
                                    >
                                        <NumberInputField
                                            value={numUsers}
                                            min={0}
                                            width="100px"
                                        />
                                    </NumberInputRoot>
                                    <Text fontSize="md">
                                        {t(
                                            'subscription.subscribe.costPerUser',
                                            {
                                                costPerUser,
                                            }
                                        )}
                                    </Text>
                                </Flex>
                                <Text fontSize="xl" fontWeight="bold">
                                    {t('subscription.subscribe.totalCost', {
                                        totalCost,
                                    })}
                                </Text>
                            </Box>
                            <Alert status="info">
                                {t('subscription.subscribe.prorataRealInvoice')}
                            </Alert>
                        </SimpleGrid>
                    )}
                    <Button
                        size="lg"
                        mt={4}
                        colorPalette="yellow"
                        onClick={handleSubscribe}
                        disabled={!selectedCircle}
                    >
                        {t('subscription.subscribe.subscribeButton', {
                            circleName: selectedCircleData?.name ?? '. . .',
                        })}
                    </Button>
                    <Text mt={4} fontSize="sm" textAlign="center">
                        {t('subscription.subscribe.prorataRealInvoice')}
                        {t('subscription.subscribe.trialInfo')}
                    </Text>
                </VStack>
            </MotionBox>
            <SubscriptionCongratulationModal
                isOpen={isSubscriptionCongratsModalOpen}
                onClose={() => {
                    navigate(
                        generatePath(CommunitiesRoutes.Show, {
                            slug: selectedCircleData?.slug!,
                        })
                    )
                }}
            />

            <DialogRoot
                lazyMount
                isOpen={open}
                onClose={onClose}
                placement="center"
            >
                <DialogBackdrop />
                <DialogContent>
                    <DialogHeader>
                        {t('subscription.subscribe.createCircleModalTitle')}
                    </DialogHeader>
                    <DialogCloseTrigger />
                    <DialogBody pb={8}>
                        <CreateCircleForm
                            onCreate={handleCreateCircle}
                            loading={creatingCircle}
                        />
                    </DialogBody>
                </DialogContent>
            </DialogRoot>
        </Container>
    )
}
