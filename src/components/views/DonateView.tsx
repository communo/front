import { Container } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import * as React from 'react'
import { Helmet } from 'react-helmet'

export const DonateView = () => {
    const { t, i18n } = useTranslation()
    return (
        <>
            <Helmet>
                <title>{t('donate.title')}</title>
            </Helmet>
            <Container maxW="3xl">
                <iframe
                    id="haWidget"
                    title={t('donate.title')}
                    src={`https://www.helloasso.com/associations/getigne-collectif/formulaires/1/widget${
                        (i18n.language !== 'fr' && `/${i18n.language}`) || ''
                    }`}
                    style={{ width: '100%', height: '750px', border: 'none' }}
                />
            </Container>
        </>
    )
}
