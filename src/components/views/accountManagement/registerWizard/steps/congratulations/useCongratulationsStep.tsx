import {
    Box,
    ListIndicator,
    ListItem,
    ListRoot,
    Spinner,
    Stack,
    Text,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { GradientPlaceholder } from '@components/templates/gradient-placeholder'
import { Heading } from '@components/atoms/Heading'
import { InfoButton, RainbowButton } from '@components/atoms/Button'
import { LuCircleCheck, LuCircleDashed } from 'react-icons/lu'
import { PoolingRoutes, UserRoutes } from '@_/routes/_hooks/routes.enum'
import {
    useCirclesToJoinForEmailQuery,
    useHandleCommunityInvitationMutation,
} from '@_/graphql/api/generated'
import { Link } from 'react-router'
import { useState } from 'react'
import { motion } from 'framer-motion'
import { FaPlus, FaSearch } from 'react-icons/fa'
import { toast } from 'react-toastify'
import { useAuth } from '@_/auth/useAuth'

const META = () => {
    const { t } = useTranslation()

    return {
        title: t('register.wizard.congratulations.title'),
        description: t('register.wizard.congratulations.description'),
    }
}

export const useCongratulationsStep = () => {
    const { t } = useTranslation()
    const auth = useAuth()

    const [acceptedCommunities, setAcceptedCommunities] = useState<string[]>(
        auth.user?.circleMemberships?.edges?.map(
            (edge) => edge!.node!.circle.slug
        ) ?? []
    )

    const { data, loading } = useCirclesToJoinForEmailQuery({
        variables: { email: auth.user?.email },
    })

    const [acceptCommunityInvitation] = useHandleCommunityInvitationMutation()

    const handleAcceptCommunityInvitation = (
        slug: string,
        invitationId: string
    ) => {
        acceptCommunityInvitation({
            variables: {
                id: invitationId,
                decision: 'accept',
            },
        })
        setAcceptedCommunities((prev) => [...prev, slug])
    }

    console.log(data?.circles?.edges)

    return {
        ...META(),
        right: (
            <GradientPlaceholder color1="yellow.400" color2="orange.400">
                <Stack>
                    <Heading size="7xl" py="6" color="white">
                        {t('register.wizard.congratulations.welcome')}
                    </Heading>
                </Stack>
            </GradientPlaceholder>
        ),
        left: (
            <Box mt="8" minH={{ base: 'auto', md: '200px' }} gap={15}>
                {loading && <Spinner />}
                {data?.circles?.edges?.length &&
                    data.circles.edges.length > 0 && (
                        <Stack>
                            <Text
                                textStyle={{ base: 'sm', md: 'md' }}
                                fontWeight="medium"
                                color="colorPalette.fg"
                            >
                                {t(
                                    'register.wizard.congratulations.invitationsPrompt',
                                    {
                                        context: auth.user?.gender,
                                    }
                                )}
                            </Text>
                            <Heading>
                                {t(
                                    'register.wizard.congratulations.clickToJoinCommunities'
                                )}
                            </Heading>

                            <ListRoot gap="2" variant="plain" align="center">
                                {data.circles.edges.map((edge) => {
                                    const { slug, name } = edge.node!
                                    const isAccepted =
                                        acceptedCommunities.includes(slug)
                                    const invitation =
                                        edge.node!.invitations!.edges![0]!.node!

                                    return (
                                        <ListItem
                                            key={slug}
                                            onClick={
                                                isAccepted
                                                    ? undefined
                                                    : () =>
                                                          handleAcceptCommunityInvitation(
                                                              slug,
                                                              invitation.id
                                                          )
                                            }
                                            cursor={
                                                isAccepted
                                                    ? 'default'
                                                    : 'pointer'
                                            }
                                            color={
                                                isAccepted
                                                    ? 'gray.500'
                                                    : 'inherit'
                                            }
                                            display="flex"
                                            alignItems="center"
                                            gap="2"
                                        >
                                            <ListIndicator asChild>
                                                {isAccepted ? (
                                                    <LuCircleCheck color="yellow" />
                                                ) : (
                                                    <LuCircleDashed />
                                                )}
                                            </ListIndicator>
                                            {name}
                                            {isAccepted && (
                                                <motion.span
                                                    initial={{
                                                        opacity: 0,
                                                        x: 10,
                                                    }}
                                                    animate={{
                                                        opacity: 1,
                                                        x: 0,
                                                    }}
                                                    transition={{
                                                        duration: 0.3,
                                                    }}
                                                    style={{
                                                        marginLeft: '8px',
                                                    }}
                                                >
                                                    <Text color="green.500">
                                                        {t(
                                                            'register.wizard.congratulations.invitationAccepted'
                                                        )}
                                                    </Text>
                                                </motion.span>
                                            )}
                                        </ListItem>
                                    )
                                })}
                            </ListRoot>
                        </Stack>
                    )}
                <Stack direction={{ base: 'column', sm: 'row' }} gap="4" mt="8">
                    <InfoButton
                        key="searchMaterialBtn"
                        rounded="full"
                        size="lg"
                        asChild
                    >
                        <Link to={PoolingRoutes.Search}>
                            <FaSearch />
                            {t('home.hero.button.iSearch')}
                        </Link>
                    </InfoButton>
                    <RainbowButton
                        key="addMaterialBtn"
                        size="lg"
                        onClick={() => {
                            if (!auth.user) toast.info(t('global.please.login'))
                        }}
                        asChild
                    >
                        <Link
                            to={
                                (auth.user && PoolingRoutes.New) ||
                                UserRoutes.Login
                            }
                        >
                            <FaPlus />
                            {t('home.hero.button.iPropose')}
                        </Link>
                    </RainbowButton>
                </Stack>
            </Box>
        ),
    }
}
