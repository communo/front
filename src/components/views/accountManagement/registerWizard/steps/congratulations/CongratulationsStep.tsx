import React from 'react'
import { StepperDiptychLayout } from '@components/templates/StepperDiptychLayout'
import { useCongratulationsStep } from '@components/views/accountManagement/registerWizard/steps/congratulations/useCongratulationsStep'

export const CongratulationsStep = () => {
    const { title, description, right, left } = useCongratulationsStep()
    return (
        <StepperDiptychLayout
            title={title}
            description={description}
            leftElement={left}
            rightElement={right}
        />
    )
}
