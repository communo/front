// usePersonalInformationStep.tsx
import {
    Box,
    Flex,
    Input,
    SimpleGrid,
    Stack,
    Text,
    Textarea,
    VStack,
} from '@chakra-ui/react'
import { Field } from '@components/ui/field'
import { PhoneNumberInput } from '@components/atoms/Form/PhoneNumberInput'
import { useColorModeValue } from '@components/ui/color-mode'
import { useTranslation } from 'react-i18next'
import { RequiredLabel } from '@components/atoms/Form/RequiredLabel'
import { RegisterWizardPersonalInformationsFormData } from '@components/views/accountManagement/registerWizard/RegisterWizardHomeView.types'
import { useForm } from 'react-hook-form'
import { useStepWizard } from '@components/views/accountManagement/registerWizard/StepWizardContext'
import { GlobalState, useStateMachine } from 'little-state-machine'
import { ImageForm } from '@components/atoms/Form/ImageForm'
import { GenderForm } from '@components/atoms/Form/GenderForm'
import { ProfileUserCover } from '@components/molecules/User/Profile/ProfileUserCover'
import { Heading } from '@components/atoms/Heading'
import { FaBirthdayCake } from 'react-icons/fa'
import { PrevNextNav } from '@components/molecules/Pager/PrevNextNav'
import { fileToBase64 } from '@utils/image'
import { CommunicationMode } from '@_/graphql/api/generated'

const META = () => {
    const { t } = useTranslation()
    return {
        title: t('register.wizard.personalInformations.title'),
        description: t('register.wizard.personalInformations.description'),
    }
}

export const usePersonalInformationsStep = () => {
    const { t } = useTranslation()
    const { prev, next } = useStepWizard()
    const previewBg = useColorModeValue('gray.50', 'gray.600')
    const updatePersonalInformations = (
        state: GlobalState,
        payload: RegisterWizardPersonalInformationsFormData
    ) => {
        return {
            ...state,
            personalInformations: {
                ...state.personalInformations,
                ...payload,
            },
        }
    }

    const { actions, state } = useStateMachine({
        actions: { updatePersonalInformations },
    })
    const {
        register,
        handleSubmit,
        watch,
        setValue,
        formState: { errors },
    } = useForm<RegisterWizardPersonalInformationsFormData>({
        defaultValues: state.personalInformations,
    })

    const onSubmit = (data: RegisterWizardPersonalInformationsFormData) => {
        actions.updatePersonalInformations(data)
        next()
    }

    const handleAvatarChange = (file: File) => {
        fileToBase64(file).then((avatar) => {
            actions.updatePersonalInformations({
                ...state.personalInformations,
                avatar,
            })
            setValue('avatar', avatar)
        })
    }

    const handleAvatarDelete = () => {
        setValue('avatar', undefined)
        actions.updatePersonalInformations({
            ...state.personalInformations,
            avatar: undefined,
        })
    }

    const today = new Date()
    const minDate = new Date()
    minDate.setFullYear(today.getFullYear() - 13)
    const watchedData = watch()
    const avatarPreview = watchedData.avatar

    const left = (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Stack
                gap={{ base: '6', md: '8' }}
                bg={useColorModeValue('white', 'gray.800')}
                p="8"
                rounded="2xl"
                shadow="lg"
            >
                <Stack gap={{ base: '4', md: '6' }}>
                    <SimpleGrid columns={{ base: 1, md: 2 }} gap="4">
                        <Field
                            label={
                                <>
                                    {t(
                                        'register.wizard.personalInformations.fields.avatar'
                                    )}
                                </>
                            }
                            errorText={errors.avatar?.message}
                        >
                            <ImageForm
                                preview={avatarPreview?.base64}
                                avatarProps={{}}
                                onFileChange={handleAvatarChange}
                                onDelete={handleAvatarDelete}
                            />
                        </Field>
                        <Field
                            label={t(
                                'register.wizard.personalInformations.fields.gender'
                            )}
                        >
                            <GenderForm
                                required={false}
                                register={register('gender')}
                            />
                            {errors.lastname && (
                                <Text color="red.500" fontSize="sm">
                                    {errors.lastname.message}
                                </Text>
                            )}
                        </Field>
                        <Field
                            label={
                                <RequiredLabel>
                                    {t(
                                        'register.wizard.personalInformations.fields.firstname'
                                    )}
                                </RequiredLabel>
                            }
                        >
                            <Input
                                id="register-firstname-input"
                                size="lg"
                                {...register('firstname', {
                                    required: true,
                                })}
                            />
                            {errors.firstname && (
                                <Text color="red.500" fontSize="sm">
                                    {errors.firstname.message}
                                </Text>
                            )}
                        </Field>
                        <Field
                            label={
                                <RequiredLabel>
                                    {t(
                                        'register.wizard.personalInformations.fields.lastname'
                                    )}
                                </RequiredLabel>
                            }
                        >
                            <Input
                                size="lg"
                                {...register('lastname', {
                                    required: true,
                                })}
                            />
                            {errors.lastname && (
                                <Text color="red.500" fontSize="sm">
                                    {errors.lastname.message}
                                </Text>
                            )}
                        </Field>

                        <Field
                            label={
                                <RequiredLabel>
                                    {t(
                                        'register.wizard.personalInformations.fields.phoneNumber'
                                    )}
                                </RequiredLabel>
                            }
                        >
                            <PhoneNumberInput
                                name="phoneNumberObject"
                                countryCodeRegister={register('countryCode', {
                                    valueAsNumber: true,
                                })}
                                nationalNumberRegister={register(
                                    'nationalNumber'
                                )}
                                required={true}
                                /* @ts-ignore */
                                errors={errors.phoneNumberObject}
                            />
                        </Field>
                        <Field
                            label={
                                <RequiredLabel>
                                    <Flex gap="2">
                                        <FaBirthdayCake />
                                        {t(
                                            'register.wizard.personalInformations.fields.birthDate.label'
                                        )}
                                    </Flex>
                                </RequiredLabel>
                            }
                        >
                            <Input
                                type="date"
                                size="lg"
                                {...register('birthDate', {
                                    required: true,
                                    validate: (value) => {
                                        if (!value) return
                                        const selectedDate = new Date(value)
                                        // eslint-disable-next-line consistent-return
                                        return (
                                            selectedDate <= minDate ||
                                            t(
                                                'register.wizard.personalInformations.fields.birthDate.tooYoung'
                                            )
                                        )
                                    },
                                })}
                            />
                            {errors.birthDate && (
                                <Text color="red.500" fontSize="sm">
                                    {errors.birthDate.message}
                                </Text>
                            )}
                        </Field>
                    </SimpleGrid>
                    <Field
                        label="Bio"
                        helperText={t(
                            'register.wizard.personalInformations.fields.bio.helperText',
                            {
                                context: watchedData.gender,
                            }
                        )}
                    >
                        <Textarea
                            {...register('bio')}
                            placeholder={t(
                                'register.wizard.personalInformations.fields.bio.placeholder',
                                {
                                    context: watchedData.gender,
                                    firstname: watchedData.firstname,
                                }
                            )}
                            size="lg"
                            rows={6}
                            resize="none"
                        />
                    </Field>
                </Stack>
                <Flex justify={'center'}>
                    <PrevNextNav
                        prev={{
                            onClick: prev,
                            title: t('global.previous', { context: 'step' }),
                        }}
                        next={{
                            type: 'submit',
                            title: t('global.next', { context: 'step' }),
                        }}
                    />
                </Flex>
            </Stack>
        </form>
    )

    return {
        ...META(),
        left,
        right: (
            <Flex align="center" h="full" w="full">
                <VStack>
                    <Heading>{t('global.preview')}</Heading>
                    <Box bgColor={previewBg} p={10} rounded="xl" shadow={'sm'}>
                        <ProfileUserCover
                            user={{
                                fullname: `${watchedData.firstname || t('register.wizard.personalInformations.fields.firstname')} ${watchedData.lastname || t('register.wizard.personalInformations.fields.lastname')}`,
                                gender: watchedData.gender || undefined,
                                birthDate: watchedData.birthDate
                                    ? new Date(watchedData.birthDate)
                                          .toISOString()
                                          .split('T')[0]
                                    : '1986/07/07',
                                city: undefined,
                                country: undefined,
                                bio: watchedData.bio,
                                avatar: {
                                    contentUrl: avatarPreview?.base64,
                                },
                                phoneNumber: {
                                    nationalNumber:
                                        state.personalInformations
                                            .nationalNumber,
                                    countryCode:
                                        state.personalInformations.countryCode,
                                },
                                settings: {
                                    preferedCommunicationMode:
                                        state.preferences
                                            .preferedCommunicationMode ??
                                        CommunicationMode.Email,
                                },
                                slug: '',
                                roles: [],
                                email: state.credentials.email,
                            }}
                            w={'2xl'}
                        />
                    </Box>
                </VStack>
            </Flex>
        ),
    }
}
