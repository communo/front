import React from 'react'
import { StepperDiptychLayout } from '@components/templates/StepperDiptychLayout'
import { usePersonalInformationsStep } from '@components/views/accountManagement/registerWizard/steps/2personalInformations/usePersonalInformationsStep'

export const PersonalInformationsStep = () => {
    const { title, description, right, left } = usePersonalInformationsStep()
    return (
        <StepperDiptychLayout
            title={title}
            description={description}
            leftElement={left}
            rightElement={right}
        />
    )
}
