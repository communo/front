import React from 'react'
import { StepperDiptychLayout } from '@components/templates/StepperDiptychLayout'
import { useWelcomeStep } from '@components/views/accountManagement/registerWizard/steps/0welcome/useWelcomeStep'

export const WelcomeStep = () => {
    const { title, description, right, left } = useWelcomeStep()
    return (
        <StepperDiptychLayout
            title={title}
            description={description}
            leftElement={left}
            rightElement={right}
        />
    )
}
