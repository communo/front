import { Box, Button } from '@chakra-ui/react'
import { LuArrowRight } from 'react-icons/lu'
import { useStepWizard } from '@components/views/accountManagement/registerWizard/StepWizardContext'
import { ImagePlaceholder } from '@components/templates/image-placeholder'
import { useTranslation } from 'react-i18next'

export const useWelcomeStep = () => {
    const { t } = useTranslation()
    const { next } = useStepWizard()
    return {
        title: t('register.wizard.welcome.title'),
        description: t('register.wizard.welcome.description'),
        right: <ImagePlaceholder image={'/photos/community-blured.webp'} />,
        left: (
            <Box mt="8" minH={{ base: 'auto', md: '200px' }}>
                <Button
                    size="xl"
                    minW="8rem"
                    onClick={() => {
                        next()
                    }}
                >
                    {t('register.wizard.welcome.start')} <LuArrowRight />
                </Button>
            </Box>
        ),
    }
}
