import 'mapbox-gl/dist/mapbox-gl.css'
import { Flex } from '@chakra-ui/react'
import mapboxgl from 'mapbox-gl'
import { useEffect, useRef, useState } from 'react'
import { useDebounce } from 'react-use'
import './map.scss'
import { RegisterWizardLocationFormData } from '@components/views/accountManagement/registerWizard/RegisterWizardHomeView.types'
import { toast } from 'react-toastify'
import { useColorModeValue } from '@components/ui/color-mode'

mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_ACCESS_TOKEN!

interface SimpleMapProps {
    address: string
    onGeocode: (data: Partial<RegisterWizardLocationFormData>) => void
    enableMarkerDrag?: boolean
    onMarkerDragEnd?: (coordinates: { lat: number; lng: number }) => void
}

export const SimpleMap = ({
    address,
    onGeocode,
    enableMarkerDrag,
    onMarkerDragEnd,
}: SimpleMapProps) => {
    const mapContainerRef = useRef<HTMLDivElement>(null)
    const markerRef = useRef<mapboxgl.Marker | null>(null)
    const [map, setMap] = useState<mapboxgl.Map | null>(null)
    const [debouncedAddress, setDebouncedAddress] = useState<string>('')

    useDebounce(
        () => {
            setDebouncedAddress(address)
        },
        500,
        [address]
    )
    const mapStyle = useColorModeValue(
        process.env.REACT_APP_MAP_LIGHT_URL,
        process.env.REACT_APP_MAP_DARK_URL
    )

    useEffect(() => {
        if (!mapContainerRef.current) return

        const mapInstance = new mapboxgl.Map({
            container: mapContainerRef.current,
            style: mapStyle || 'mapbox://styles/mapbox/streets-v11',
            center: [2.3522, 48.8566],
            zoom: 12,
        })

        setMap(mapInstance)

        // eslint-disable-next-line consistent-return
        return () => {
            mapInstance.remove()
        }
    }, [])

    useEffect(() => {
        if (!debouncedAddress || !map) return

        const fetchCoordinates = async () => {
            try {
                const response = await fetch(
                    `https://api.mapbox.com/geocoding/v5/mapbox.places/${encodeURIComponent(
                        debouncedAddress
                    )}.json?access_token=${mapboxgl.accessToken}`
                )

                const data = await response.json()
                if (data.features && data.features.length > 0) {
                    const [lng, lat] = data.features[0].geometry.coordinates
                    const postalCode = data.features[0].context?.find(
                        (c: any) => c.id.startsWith('postcode')
                    )?.text
                    const city = data.features[0].context?.find((c: any) =>
                        c.id.startsWith('place')
                    )?.text
                    const country = data.features[0].context?.find((c: any) =>
                        c.id.startsWith('country')
                    )?.text

                    // Update global state via callback
                    onGeocode({
                        address: debouncedAddress,
                        postalCode: postalCode || '',
                        city: city || '',
                        country: country || '',
                        latitude: lat.toString(),
                        longitude: lng.toString(),
                    })

                    // Manage the marker
                    if (!markerRef.current) {
                        markerRef.current = new mapboxgl.Marker({
                            draggable: enableMarkerDrag,
                        })
                            .setLngLat([lng, lat])
                            .addTo(map)

                        if (enableMarkerDrag && onMarkerDragEnd) {
                            markerRef.current.on('dragend', () => {
                                const markerLngLat =
                                    markerRef.current!.getLngLat()
                                onMarkerDragEnd({
                                    lat: markerLngLat.lat,
                                    lng: markerLngLat.lng,
                                })
                            })
                        }
                    } else {
                        markerRef.current.setLngLat([lng, lat])
                    }

                    map.jumpTo({ center: [lng, lat], zoom: 14 })
                } else {
                    toast.error('No location found for the given address.')
                }
            } catch (error) {
                alert('Error fetching coordinates:')
            }
        }

        fetchCoordinates()
    }, [debouncedAddress, map, onGeocode])

    return (
        <Flex align="center" h="100%" w="100%">
            <div ref={mapContainerRef} className="map-container" />
        </Flex>
    )
}
