import React from 'react'
import { StepperDiptychLayout } from '@components/templates/StepperDiptychLayout'
import { useLocationStep } from '@components/views/accountManagement/registerWizard/steps/3location/useLocationStep'

export const LocationStep = () => {
    const { title, description, right, left } = useLocationStep()
    return (
        <StepperDiptychLayout
            title={title}
            description={description}
            leftElement={left}
            rightElement={right}
        />
    )
}
