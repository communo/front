import {
    defaultStore,
    useStepWizard,
} from '@components/views/accountManagement/registerWizard/StepWizardContext'
import { GlobalState, useStateMachine } from 'little-state-machine'
import { useTranslation } from 'react-i18next'
import { useColorMode } from '@components/ui/color-mode'
import { RegisterWizardPreferencesFormData } from '@components/views/accountManagement/registerWizard/RegisterWizardHomeView.types'
import { FormProvider, useForm } from 'react-hook-form'
import { useEffect } from 'react'
import { useRegisterWizardHomeViewMutation } from '@_/graphql/api/generated'
import { randomTrans } from '@utils/random'
import { toast } from 'react-toastify'
import { useAuth } from '@_/auth/useAuth'
import { base64ToFile } from '@utils/image'
import { PreferencesPreview } from '@components/views/accountManagement/registerWizard/steps/4preferences/preferencesPreview'
import { PreferencesForm } from '@components/views/accountManagement/registerWizard/steps/4preferences/PreferencesForm'
import { useSearchParams } from 'react-router'
import { getShortLanguage } from '@utils/language'

const META = () => {
    const { t } = useTranslation()

    return {
        title: t('register.wizard.preferences.title'),
        description: t('register.wizard.preferences.description'),
    }
}

export const usePreferencesStep = () => {
    const { next } = useStepWizard()
    const { colorMode, setColorMode } = useColorMode()
    const { t, i18n } = useTranslation()
    const auth = useAuth()
    const updatePreferences = (
        previousState: GlobalState,
        payload: RegisterWizardPreferencesFormData
    ) => {
        return {
            ...previousState,
            preferences: {
                ...previousState.preferences,
                ...payload,
            },
        }
    }
    const clearAction = (
        _previousState: GlobalState,
        _payload: RegisterWizardPreferencesFormData
    ) => {
        return defaultStore
    }
    const { actions, state } = useStateMachine({
        actions: { updatePreferences, clearAction },
    })
    const methods = useForm<RegisterWizardPreferencesFormData>({
        defaultValues: state.preferences,
    })
    const { watch, setError } = methods
    const watchedData = watch()
    useEffect(() => {
        if (colorMode === watchedData.themeMode) {
            return
        }
        setColorMode(watchedData.themeMode.toLowerCase())
    }, [colorMode, setColorMode, watchedData.themeMode])
    const [registerUser] = useRegisterWizardHomeViewMutation()
    const [searchParams] = useSearchParams()
    const token = searchParams.get('token') ?? undefined

    const onSubmit = (data: RegisterWizardPreferencesFormData) => {
        actions.updatePreferences(data)
        const { avatar, ...personalInformations } = state.personalInformations
        const { abbreviatedName, ...preferences } = state.preferences
        const { address: streetAddress, ...location } = state.location

        registerUser({
            variables: {
                input: {
                    ...state.credentials,
                    ...personalInformations,
                    ...location,
                    streetAddress,
                    settings: {
                        ...preferences,
                        useAbbreviatedName: abbreviatedName,
                        language: getShortLanguage(i18n.language),
                        currencySymbol: '€',
                    },
                    avatar: avatar ? base64ToFile(avatar) : undefined,
                    token,
                },
            },
            onCompleted: (r) => {
                if (r.createUser?.user) {
                    auth.signin(r.createUser.user, () => {
                        document.body.scrollTop = 0
                        document.documentElement.scrollTop = 0
                    })
                    toast.success(
                        randomTrans(t, 'register.toast.success', 4, {
                            user: r.createUser.user,
                        })
                    )
                    actions.clearAction()
                    next()
                }
            },
            onError: (error) => {
                const err = error.graphQLErrors as unknown as {
                    extensions?: {
                        status: number
                        violations: { path: 'string'; message: string }[]
                    }
                }[]
                err.forEach((e) => {
                    e.extensions?.violations?.forEach((violation) => {
                        // @ts-ignore TODO specify types for violation
                        setError(violation.path, {
                            type: 'manual',
                            message: violation.message,
                        })
                    })
                })
            },
        }).catch((error) => {
            console.log(error)
        })
    }

    return {
        ...META(),
        right: (
            <FormProvider {...methods}>
                <PreferencesPreview />
            </FormProvider>
        ),
        left: (
            <FormProvider {...methods}>
                <PreferencesForm onSubmit={onSubmit} />
            </FormProvider>
        ),
    }
}
