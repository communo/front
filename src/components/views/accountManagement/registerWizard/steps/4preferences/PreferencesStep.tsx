import React from 'react'
import { StepperDiptychLayout } from '@components/templates/StepperDiptychLayout'
import { usePreferencesStep } from '@components/views/accountManagement/registerWizard/steps/4preferences/usePreferencesStep'

export const PreferencesStep = () => {
    const { title, description, right, left } = usePreferencesStep()
    return (
        <StepperDiptychLayout
            title={title}
            description={description}
            leftElement={left}
            rightElement={right}
        />
    )
}
