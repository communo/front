import { Box, Flex, VStack } from '@chakra-ui/react'
import { Heading } from '@components/atoms/Heading'
import { ProfileUserCover } from '@components/molecules/User/Profile/ProfileUserCover'
import { useStateMachine } from 'little-state-machine'
import { useTranslation } from 'react-i18next'
import { useColorModeValue } from '@components/ui/color-mode'
import { RegisterWizardPreferencesFormData } from '@components/views/accountManagement/registerWizard/RegisterWizardHomeView.types'
import { useFormContext } from 'react-hook-form'
import { CommunicationMode } from '@_/graphql/api/generated'

export const PreferencesPreview = () => {
    const { t } = useTranslation()
    const { watch } = useFormContext<RegisterWizardPreferencesFormData>()
    const { abbreviatedName, preferedCommunicationMode } = watch()
    const { state } = useStateMachine()
    const avatarPreview = state.personalInformations.avatar

    return (
        <Flex align="center" h="full" w="full">
            <VStack>
                <Heading>{t('global.preview')}</Heading>
                <Box
                    bgColor={useColorModeValue('gray.50', 'gray.600')}
                    p={10}
                    rounded="xl"
                    shadow={'sm'}
                >
                    <ProfileUserCover
                        user={{
                            fullname: `${state.personalInformations.firstname || t('accountManagement.my.profile.form.firstname.label')} ${abbreviatedName ? `${state.personalInformations.lastname![0]}.` : state.personalInformations.lastname || t('accountManagement.my.profile.form.lastname.label')}`,
                            gender:
                                state.personalInformations.gender || undefined,
                            birthDate: state.personalInformations.birthDate
                                ? new Date(state.personalInformations.birthDate)
                                      .toISOString()
                                      .split('T')[0]
                                : '1986/07/07',
                            city: state.location.city,
                            country: state.location.country,
                            bio: state.personalInformations.bio,
                            avatar: {
                                contentUrl: avatarPreview?.base64,
                            },
                            phoneNumber: {
                                nationalNumber:
                                    state.personalInformations.nationalNumber,
                                countryCode:
                                    state.personalInformations.countryCode,
                            },
                            settings: {
                                preferedCommunicationMode:
                                    preferedCommunicationMode ??
                                    CommunicationMode.Email,
                            },
                            slug: '',
                            roles: [],
                            email: state.credentials.email,
                        }}
                        w={'2xl'}
                    />
                </Box>
            </VStack>
        </Flex>
    )
}
