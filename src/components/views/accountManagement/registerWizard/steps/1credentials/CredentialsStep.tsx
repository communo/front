import React from 'react'
import { StepperDiptychLayout } from '@components/templates/StepperDiptychLayout'
import { useCredentialsStep } from '@components/views/accountManagement/registerWizard/steps/1credentials/useCredentialsStep'

export const CredentialsStep = () => {
    const { title, description, right, left } = useCredentialsStep()
    return (
        <StepperDiptychLayout
            title={title}
            description={description}
            leftElement={left}
            rightElement={right}
        />
    )
}
