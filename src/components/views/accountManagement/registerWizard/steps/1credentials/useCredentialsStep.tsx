import { Flex, Input, SimpleGrid, Stack, Text } from '@chakra-ui/react'

import { useForm } from 'react-hook-form'
import { Field } from '@components/ui/field'
import { useColorModeValue } from '@components/ui/color-mode'
import { useTranslation } from 'react-i18next'
import { RequiredLabel } from '@components/atoms/Form/RequiredLabel'
import { PasswordInput } from '@components/ui/password-input'
import { RegisterWizardCredentialsFormData } from '@components/views/accountManagement/registerWizard/RegisterWizardHomeView.types'
import { useStepWizard } from '@components/views/accountManagement/registerWizard/StepWizardContext'
import { GlobalState, useStateMachine } from 'little-state-machine'
import { ImagePlaceholder } from '@components/templates/image-placeholder'
import { PrevNextNav } from '@components/molecules/Pager/PrevNextNav'
import { useRegisterWizardHomeView_FindUserByEmailLazyQuery } from '@_/graphql/api/generated'

const META = () => {
    const { t } = useTranslation()
    return {
        title: t('register.wizard.credentials.title'),
        description: t('register.wizard.credentials.description'),
        right: <ImagePlaceholder image={'/photos/community-blured.webp'} />,
    }
}

export const useCredentialsStep = () => {
    const { t } = useTranslation()
    const { next } = useStepWizard()
    const updateCredentials = (
        state: GlobalState,
        payload: RegisterWizardCredentialsFormData
    ) => {
        return {
            ...state,
            credentials: {
                ...state.credentials,
                ...payload,
            },
        }
    }

    const { state, actions } = useStateMachine({
        actions: { updateCredentials },
    })
    const {
        register,
        handleSubmit,
        setError,
        formState: { errors },
    } = useForm<RegisterWizardCredentialsFormData>({
        defaultValues: {
            email: state.credentials.email,
        },
    })
    const [findUserByEmail] =
        useRegisterWizardHomeView_FindUserByEmailLazyQuery()

    const onSubmit = (data: RegisterWizardCredentialsFormData) => {
        actions.updateCredentials(data)
        findUserByEmail({
            variables: { email: data.email },
            onCompleted: (r) => {
                if (r?.users?.totalCount === 0) {
                    next()
                } else {
                    setError('email', {
                        type: 'manual',
                        message: t(
                            'register.wizard.credentials.fields.email.alreadyExists'
                        ),
                    })
                }
            },
        })
    }

    // @ts-ignore
    const left = (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Stack
                gap={{ base: '6', md: '8' }}
                bg={useColorModeValue('white', 'gray.800')}
                p="8"
                rounded="2xl"
                shadow="lg"
            >
                <Stack gap={{ base: '4', md: '6' }}>
                    <SimpleGrid columns={{ base: 1, md: 2 }} gap="4">
                        <Field
                            label={
                                <RequiredLabel>
                                    {t(
                                        'register.wizard.credentials.fields.email.label'
                                    )}
                                </RequiredLabel>
                            }
                        >
                            <Input
                                size="lg"
                                {...register('email', {
                                    required: true,
                                })}
                            />
                            {errors.email && (
                                <Text color="red.500" fontSize="sm">
                                    {errors.email.message}
                                </Text>
                            )}
                        </Field>
                        <Field
                            label={
                                <RequiredLabel>
                                    {t(
                                        'register.wizard.credentials.fields.password.label'
                                    )}
                                </RequiredLabel>
                            }
                        >
                            <PasswordInput
                                size="lg"
                                {...register('password', {
                                    required: true,
                                })}
                                autoComplete="new-password"
                            />
                            {errors.password && (
                                <Text color="red.500" fontSize="sm">
                                    {errors.password.message}
                                </Text>
                            )}
                        </Field>
                    </SimpleGrid>
                </Stack>
                <Flex justify={'center'}>
                    <PrevNextNav
                        next={{
                            type: 'submit',
                            title: t('global.next', { context: 'step' }),
                        }}
                    />
                </Flex>
            </Stack>
        </form>
    )

    return {
        ...META(),
        left,
    }
}
