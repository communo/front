import { CommunicationMode, ThemeMode } from '@_/graphql/api/generated'
import { Base64File } from '@utils/image'

export type RegisterWizardCredentialsFormData = {
    email: string
    password: string
}

export type RegisterWizardPersonalInformationsFormData = {
    avatar?: Base64File
    birthDate?: string
    bio?: string
    countryCode: number
    gender?: 'male' | 'female' | 'nonBinary' | ''
    firstname: string
    lastname: string
    nationalNumber: string
}

export type RegisterWizardLocationFormData = {
    address: string
    postalCode: string
    city: string
    country: string
    latitude: string
    longitude: string
}

export type RegisterWizardPreferencesFormData = {
    themeMode: ThemeMode
    abbreviatedName: boolean
    preferedCommunicationMode: CommunicationMode | null
}
