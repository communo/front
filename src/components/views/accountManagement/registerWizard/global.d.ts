import 'little-state-machine'
import {
    RegisterWizardCredentialsFormData,
    RegisterWizardLocationFormData,
    RegisterWizardPersonalInformationsFormData,
} from '@components/views/accountManagement/registerWizard/RegisterWizardHomeView.types'

declare module 'little-state-machine' {
    interface GlobalState {
        credentials: RegisterWizardCredentialsFormData
        personalInformations: RegisterWizardPersonalInformationsFormData
        location: RegisterWizardLocationFormData
        preferences: RegisterWizardPreferencesFormData
    }
}
