import React, { useEffect, useState } from 'react'
import { createStore, GlobalState, useStateMachine } from 'little-state-machine'
import { withRouter } from '@components/views/withRouter'
import { generatePath, useSearchParams } from 'react-router'
import { UserRoutes } from '@_/routes/_hooks/routes.enum'
import { useRegisterWizardHomeView_InvitationLazyQuery } from '@_/graphql/api/generated'
import { Loader } from '@components/atoms/Loader/Loader'
import { WelcomeStep } from '@components/views/accountManagement/registerWizard/steps/0welcome/WelcomeStep'
import { CredentialsStep } from '@components/views/accountManagement/registerWizard/steps/1credentials/CredentialsStep'
import { PersonalInformationsStep } from '@components/views/accountManagement/registerWizard/steps/2personalInformations/PersonalInformationsStep'
import { LocationStep } from '@components/views/accountManagement/registerWizard/steps/3location/LocationStep'
import { PreferencesStep } from '@components/views/accountManagement/registerWizard/steps/4preferences/PreferencesStep'
import { CongratulationsStep } from '@components/views/accountManagement/registerWizard/steps/congratulations/CongratulationsStep'
import {
    defaultStore,
    StepWizardProvider,
    useStepWizard,
} from './StepWizardContext'

createStore(defaultStore)
const stepsComponents = [
    WelcomeStep,
    CredentialsStep,
    PersonalInformationsStep,
    LocationStep,
    PreferencesStep,
    CongratulationsStep,
]

export const RegisterWizardHomeInner = () => {
    const { currentStep, setTotalSteps } = useStepWizard()

    useEffect(() => {
        setTotalSteps(stepsComponents.length - 1)
    }, [stepsComponents.length, setTotalSteps])

    const ActiveStepComponent = stepsComponents[currentStep]

    return <ActiveStepComponent />
}

export const RegisterWizardHomeView = withRouter(() => {
    const [currentStep, setCurrentStep] = useState(0)
    const [searchParams] = useSearchParams()
    const invitationId = searchParams.get('i') ?? undefined

    const [invitationFetched, setInvitationFetched] = useState(!invitationId)

    const updateState = (
        state: GlobalState,
        payload: {
            email: string
            firstname: string
            lastname: string
        }
    ) => {
        return {
            ...state,
            credentials: {
                ...state.credentials,
                email: payload.email,
            },
            personalInformations: {
                ...state.personalInformations,
                firstname: payload.firstname,
                lastname: payload.lastname,
            },
        }
    }

    const { actions } = useStateMachine({
        actions: { updateState },
    })

    const [findInvitation, { loading }] =
        useRegisterWizardHomeView_InvitationLazyQuery()

    useEffect(() => {
        if (invitationId) {
            findInvitation({
                variables: {
                    id: generatePath(UserRoutes.InvitationsIRI, {
                        id: invitationId,
                    }),
                },
                onCompleted: (r) => {
                    if (r?.invitation) {
                        actions.updateState({
                            email: r.invitation.email,
                            firstname: r.invitation.firstname ?? '',
                            lastname: r.invitation.lastname ?? '',
                        })
                    }
                    setInvitationFetched(true)
                },
                onError: () => {
                    // Gérer l'erreur si besoin
                    setInvitationFetched(true)
                },
            })
        }
    }, [invitationId, findInvitation, actions])

    if (loading || !invitationFetched) {
        return <Loader />
    }

    return (
        <StepWizardProvider
            currentStep={currentStep}
            setCurrentStep={setCurrentStep}
        >
            <RegisterWizardHomeInner />
        </StepWizardProvider>
    )
})
