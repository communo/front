import { Card, Container } from '@chakra-ui/react'
import React from 'react'
import { Helmet } from 'react-helmet'
import { useTranslation } from 'react-i18next'
import { useApolloClient } from '@apollo/client'
import { toast } from 'react-toastify'
import { useAuth } from '@_/auth/useAuth'
import { Heading } from '@components/atoms/Heading'
import { Loader } from '@components/atoms/Loader/Loader'
import {
    inUserSpace,
    WithUserProps,
} from '@components/templates/AccountManagement/inUserSpace'
import {
    useUpdateUserMutation,
    useUserPreferencesQuery,
} from '@_/graphql/api/generated'
import { FormSection } from '@components/ui/form-section'
import { useColorModeValue } from '@components/ui/color-mode'
import { UserPrivacyForm } from '@components/molecules/Form/AccountManagement/User/Preferences/Privacy/UserPrivacyForm'
import { UserPreferencesFormValues } from '@components/views/accountManagement/preferences/MyPreferences.types'
import { UserAppearenceForm } from '@components/molecules/Form/AccountManagement/User/Preferences/Appearence/UserAppearenceForm'
import { EmphasisText } from '@components/atoms/Text/EmphasisText'

const MyPreferences = ({ user: { id } }: WithUserProps) => {
    const { t } = useTranslation()
    const cardBgColor = useColorModeValue('gray.50', 'gray.800')
    const client = useApolloClient()
    const { data, error } = useUserPreferencesQuery({
        variables: {
            id,
        },
    })
    const auth = useAuth()
    const user = data?.user
    const [updateUser] = useUpdateUserMutation({
        update() {
            client.resetStore()
        },
    })
    const onUpdateSubmit = (input: UserPreferencesFormValues) => {
        if (user) {
            updateUser({
                variables: {
                    input: {
                        id: user.id,
                        ...input,
                    },
                },
            }).then((r) => {
                if (r.data?.updateUser?.user) {
                    const returnedUser = r.data?.updateUser?.user
                    toast.success(
                        t(
                            'accountManagement.my.preferences.update.toast.success'
                        )
                    )
                    auth.signin(returnedUser)
                    return
                }
                toast.error(
                    t(
                        'accountManagement.my.preferences.updateStatus.toast.error'
                    )
                )
            })
        }
    }

    return (
        (error && <p>Error </p>) ||
        (user && (
            <>
                <Helmet>
                    <title>
                        {t(`accountManagement.my.preferences.title`)}{' '}
                        {'< Communo'}
                    </title>
                </Helmet>
                <Container>
                    <Heading size="3xl" py="6">
                        {t('accountManagement.my.preferences.title')}
                    </Heading>
                    <Card.Root
                        mb="20"
                        rounded="2xl"
                        p={'10'}
                        bgColor={cardBgColor}
                    >
                        <Card.Body gap={10}>
                            <FormSection
                                title={t(
                                    'accountManagement.my.preferences.privacy.title'
                                )}
                                description={t(
                                    'accountManagement.my.preferences.privacy.subTitle'
                                )}
                            >
                                <UserPrivacyForm
                                    user={user}
                                    onSubmit={onUpdateSubmit}
                                />
                            </FormSection>
                            <FormSection
                                title={t(
                                    'accountManagement.my.preferences.appearence.title'
                                )}
                                description={
                                    <EmphasisText color={'pink.400'}>
                                        {t(
                                            'accountManagement.my.preferences.appearence.subTitle'
                                        )}
                                    </EmphasisText>
                                }
                            >
                                <UserAppearenceForm
                                    user={user}
                                    onSubmit={onUpdateSubmit}
                                />
                            </FormSection>
                        </Card.Body>
                    </Card.Root>
                </Container>
            </>
        )) || <Loader />
    )
}

export const MyPreferencesView = inUserSpace(MyPreferences)
