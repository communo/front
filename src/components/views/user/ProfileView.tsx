import {
    Box,
    Container,
    Flex,
    Grid,
    Heading,
    Separator,
    Spacer,
    VStack,
} from '@chakra-ui/react'
import { useCallback, useEffect, useState } from 'react'
import { client } from '@_/apollo/main/client'
import {
    MaterialPageInfo,
    ProfileView_MaterialsDocument,
    ProfileView_MaterialsQuery,
    SearchMaterialFragment,
    useProfileView_UserQuery,
} from '@_/graphql/api/generated'
import { PaginatedMaterialList } from '@components/molecules/Material/PaginatedMaterialList'
import { Loader } from '@components/atoms/Loader/Loader'
import { Helmet } from 'react-helmet'
import { useTranslation } from 'react-i18next'
import {
    BreadcrumbCurrentLink,
    BreadcrumbLink,
    BreadcrumbRoot,
} from '@components/ui/breadcrumb'
import { generatePath, Link } from 'react-router'
import { FiHome } from 'react-icons/fi'
import { UserRoutes } from '@_/routes/_hooks/routes.enum'
import { ProfileUserCover } from '@components/molecules/User/Profile/ProfileUserCover'
import { useColorModeValue } from '@components/ui/color-mode'
import { ProfilePoolingHero } from './ProfilePoolingHero'
import { withRouter } from '../withRouter'

interface ProfileViewProps {
    params?: {
        slug: string
    }
}

export const ProfileView = withRouter<ProfileViewProps>(
    ({ params: { slug } }) => {
        const [materials, setMaterials] = useState<SearchMaterialFragment[]>()
        const [afterCursor, setAfterCursor] = useState<string>()
        const [beforeCursor, setBeforeCursor] = useState<string>()
        const [pageInfo, setPageInfo] = useState<MaterialPageInfo>()

        const [totalCount, setTotalCount] = useState<number>(0)
        const [materialLoading, setLoading] = useState<boolean>(false)
        const nbToFetch = 16 // 4 x 4 cards
        const { t } = useTranslation()

        const fetchUserMaterials = useCallback(() => {
            setLoading(true)
            client
                .query<ProfileView_MaterialsQuery>({
                    query: ProfileView_MaterialsDocument,
                    variables: {
                        owner: generatePath(UserRoutes.IRI, { slug }),
                        nbToFetch,
                        afterCursor,
                        beforeCursor,
                    },
                    errorPolicy: 'ignore',
                    fetchPolicy: 'no-cache',
                })
                .then((res) => {
                    setLoading(false)
                    if (!res.data?.ownerMaterials) {
                        return
                    }
                    setMaterials(
                        res.data?.ownerMaterials?.edges
                            ?.map((edge) => edge.node!)
                            .filter(Boolean) || []
                    )
                    setPageInfo(res.data?.ownerMaterials?.pageInfo || null)
                    setTotalCount(res.data?.ownerMaterials?.totalCount)
                })
        }, [nbToFetch])

        const { data } = useProfileView_UserQuery({
            variables: {
                id: generatePath(UserRoutes.IRI, { slug }),
            },
        })
        useEffect(() => {
            fetchUserMaterials()
        }, [fetchUserMaterials])

        if (!data?.user) {
            return <Loader />
        }

        return (
            <>
                <Container>
                    <Helmet>
                        <title>
                            {data.user?.fullname}
                            {t('meta.title.suffix')}
                        </title>
                    </Helmet>
                    <BreadcrumbRoot py={2} color="yellow.600">
                        <BreadcrumbLink asChild>
                            <Link to="/">
                                <FiHome />
                            </Link>
                        </BreadcrumbLink>
                        <BreadcrumbCurrentLink>
                            {data.user?.fullname}
                        </BreadcrumbCurrentLink>
                    </BreadcrumbRoot>
                    <ProfileUserCover user={data.user} />
                </Container>
                <Separator mt="5" />
                <Box boxShadow="md" bg={useColorModeValue('white', 'gray.800')}>
                    {(materialLoading && <Loader />) ||
                        (materials && materials.length > 0 && (
                            <VStack>
                                <ProfilePoolingHero user={data.user} />
                                <Box textAlign="center" fontSize="xl">
                                    <Grid p={4}>
                                        <VStack>
                                            <Heading as="h1">
                                                <Flex
                                                    direction={'row'}
                                                    alignItems="center"
                                                >
                                                    <Spacer />
                                                    {t('pooling.index.title')}
                                                    <Spacer />
                                                </Flex>
                                            </Heading>
                                            {
                                                <PaginatedMaterialList
                                                    materials={materials}
                                                    pageInfo={
                                                        pageInfo ||
                                                        ({} as MaterialPageInfo)
                                                    }
                                                    totalCount={totalCount}
                                                    loading={materialLoading}
                                                    setBeforeCursor={
                                                        setBeforeCursor
                                                    }
                                                    setAfterCursor={
                                                        setAfterCursor
                                                    }
                                                    searchTerms={''}
                                                    viewMode={'card'}
                                                />
                                            }
                                        </VStack>
                                    </Grid>
                                </Box>
                            </VStack>
                        )) || (
                            <Container py={{ base: '16', md: '24' }}>
                                <VStack gap="10" textAlign="center">
                                    <Heading as={'h2'} size={'2xl'}>
                                        {t(
                                            'accountManagement.profile.pooling.noMaterial',
                                            {
                                                firstname: data.user.firstname,
                                            }
                                        )}
                                    </Heading>
                                </VStack>
                            </Container>
                        )}
                </Box>
            </>
        )
    }
)
