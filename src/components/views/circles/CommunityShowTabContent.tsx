import React from 'react'
import { useTabSelected } from '@components/layout/TabNavigationLayout'
import { Box, Separator } from '@chakra-ui/react'
import { ShowCommunityTabs } from '@components/views/circles/useShowCommunityTabs'
import { MembershipList } from '@components/molecules/Circles/members/MembershipList'
import { CircleShowView_CircleQuery } from '@_/graphql/api/generated'

export const CommunityShowTabContent = ({
    community,
}: {
    community: CircleShowView_CircleQuery['circle']
}) => {
    const { selectedTab } = useTabSelected()
    if (!community) return <div>Error</div>
    console.log(selectedTab)

    return (
        <Box>
            {selectedTab === ShowCommunityTabs.MEMBERS && (
                <>
                    <Box pt={{ base: 6, md: 8 }}>
                        <MembershipList
                            title="communities.show.users.ok.title"
                            circleSlug={community.slug}
                            statuses={['ok']}
                            order={[
                                {
                                    permission: 'ASC',
                                    user_firstname: 'ASC',
                                    user_lastname: 'ASC',
                                },
                            ]}
                        />
                    </Box>
                </>
            )}
            {selectedTab === ShowCommunityTabs.PENDING_MEMBERS &&
                community.activeUserActions.includes('acceptUser') && (
                    <>
                        <Box p={{ base: 6, md: 8 }}>
                            <MembershipList
                                title="communities.show.users.waiting.title"
                                circleSlug={community.slug}
                                statuses={['pending', 'invited']}
                                order={[
                                    {
                                        createdAt: 'ASC',
                                    },
                                ]}
                            />
                        </Box>
                        <Separator />
                    </>
                )}
        </Box>
    )
}
