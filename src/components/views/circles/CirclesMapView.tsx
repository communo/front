import React, { useEffect, useRef, useState } from 'react'
import { Box } from '@chakra-ui/react'
import 'mapbox-gl/dist/mapbox-gl.css'
import { useTranslation } from 'react-i18next'
import mapboxgl, { GeolocateControl } from 'mapbox-gl'
import { MapFilter } from '@components/molecules/Map/MapFilter'
import { MapCircleModal } from '@components/molecules/Map/MapCircleModal'
import { Circle, useCirclesMapViewLazyQuery } from '@_/graphql/api/generated'
import { useColorModeValue } from '@components/ui/color-mode'
import { Helmet } from 'react-helmet'
import { stripTags } from '@utils/text'
import { system } from 'src/theme'
import { NewCircleDialog } from '@components/molecules/Form/Circles/CircleForm/NewCircleDialog'

export const CirclesMapView = () => {
    const [getCirclesQuery, { data, loading, error }] =
        useCirclesMapViewLazyQuery()
    const [totalResults, setTotalResults] = useState(0)
    const [selectedCircle, setSelectedCircle] = useState<Circle | null>(null)
    const mapContainer = useRef<HTMLDivElement>(null)
    const [searchTerm, setSearchTerm] = useState('')
    const searchTimeoutRef = useRef<NodeJS.Timeout>()
    const { t } = useTranslation()
    const nbToFetch = 100

    const handleCloseModal = () => {
        setSelectedCircle(null)
    }

    const handleClickCircle = (circle: Circle) => {
        setSelectedCircle(circle)
    }
    useEffect(() => {
        getCirclesQuery({
            variables: {
                nbToFetch,
            },
        })
    }, [getCirclesQuery])
    const mapStyle = useColorModeValue(
        process.env.REACT_APP_MAP_LIGHT_URL,
        process.env.REACT_APP_MAP_DARK_URL
    )

    useEffect(() => {
        const mapboxAccessToken = process.env.REACT_APP_MAPBOX_ACCESS_TOKEN
        if (mapContainer.current && mapboxAccessToken) {
            const map = new mapboxgl.Map({
                container: mapContainer.current,
                accessToken: mapboxAccessToken,
                style: mapStyle,
                center: [2.3522, 48.8566], // Centre par défaut sur Paris
                zoom: 10,
            })

            const geolocate = new GeolocateControl({
                positionOptions: { enableHighAccuracy: true },
                trackUserLocation: true,
                showUserLocation: true,
            })

            map.addControl(geolocate, 'bottom-right')

            if (data?.circles?.edges && data?.circles?.edges.length > 0) {
                let zoom = 10
                const firstCircle = data.circles.edges[0].node
                const firstCircleLngLat: mapboxgl.LngLatLike = [
                    parseFloat(firstCircle?.longitude ?? '0'),
                    parseFloat(firstCircle?.latitude ?? '0'),
                ]
                map.setCenter(firstCircleLngLat)
                if (data.circles.edges.length === 1) {
                    zoom = 14
                }
                map.flyTo({ center: firstCircleLngLat, zoom })

                map.on('load', () => {
                    map.addSource('circle-links', {
                        type: 'geojson',
                        data: {
                            type: 'FeatureCollection',
                            features: [],
                        },
                    })

                    map.addLayer({
                        id: 'circle-links-layer',
                        type: 'line',
                        source: 'circle-links',
                        layout: { 'line-cap': 'round', 'line-join': 'round' },
                        paint: {
                            'line-color': '#FF1493',
                            'line-width': 2,
                            'line-opacity': 0.8,
                        },
                    })

                    const features: any[] = []
                    data.circles?.edges?.forEach((edge) => {
                        const circle = edge.node as Circle
                        const circleLngLat = [
                            parseFloat(circle.longitude!),
                            parseFloat(circle.latitude!),
                        ] as mapboxgl.LngLatLike

                        const marker = new mapboxgl.Marker({
                            color: system.token.var('colors.pink.400'),
                        })
                            .setLngLat(circleLngLat)
                            .addTo(map)

                        marker.getElement().addEventListener('click', () => {
                            handleClickCircle(circle)
                            map.flyTo({ center: circleLngLat, zoom: 15 })
                        })

                        if (circle.parent) {
                            const parentCircle = data.circles?.edges?.find(
                                (e) => e.node!.id === circle.parent?.id
                            )?.node

                            if (parentCircle) {
                                const parentLngLat = [
                                    parseFloat(parentCircle.longitude!),
                                    parseFloat(parentCircle.latitude!),
                                ] as mapboxgl.LngLatLike

                                features.push({
                                    type: 'Feature',
                                    geometry: {
                                        type: 'LineString',
                                        coordinates: [
                                            circleLngLat,
                                            parentLngLat,
                                        ],
                                    },
                                    properties: {},
                                })
                            }
                        }
                    })

                    const source = map.getSource(
                        'circle-links'
                    ) as mapboxgl.GeoJSONSource
                    source.setData({
                        type: 'FeatureCollection',
                        features,
                    })
                })
            }

            setTotalResults(data?.circles?.totalCount || 0)
        }
    }, [data?.circles?.edges, data?.circles?.totalCount, mapStyle])

    const handleSearchTermChange = (
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        setSearchTerm(event.target.value)
        if (searchTimeoutRef.current) {
            clearTimeout(searchTimeoutRef.current)
        }
        searchTimeoutRef.current = setTimeout(() => {
            getCirclesQuery({
                variables: {
                    searchTerm: event.target.value,
                    nbToFetch,
                },
            })
        }, 300)
    }

    if (error) return <p>Error :(</p>

    return (
        <>
            <Helmet>
                <title>
                    {stripTags(t('communities.map.title'))}{' '}
                    {t('meta.title.suffix')}
                </title>
            </Helmet>
            <div
                id="map"
                ref={mapContainer}
                style={{
                    height: 'calc(100vh - 200px)',
                    width: '100%',
                }}
            />
            <Box position="absolute" top="100px" right={10}>
                <NewCircleDialog title={t('communities.map.actions.new')} />
            </Box>
            <MapFilter
                {...{
                    onChange: handleSearchTermChange,
                    value: searchTerm,
                    loading,
                    totalResults,
                }}
            />
            <MapCircleModal
                selectedCircle={selectedCircle}
                onCloseModal={handleCloseModal}
            />
        </>
    )
}
