import { useTranslation } from 'react-i18next'
import { TabItem } from '@components/layout/TabNavigationLayout'
import { HStack } from '@chakra-ui/react'
import { Tag } from '@components/ui/tag'

export const enum ShowCommunityTabs {
    MEMBERS = 'MEMBERS',
    PENDING_MEMBERS = 'PENDING_MEMBERS',
    MATERIALS = 'MATERIALS',
}

export const useShowCommunityTabs = (hasAdminRight = false): TabItem[] => {
    const { t } = useTranslation()

    const tabs: TabItem[] = [
        {
            key: ShowCommunityTabs.MEMBERS,
            name: t('communities.show.members.title'),
        },
    ]
    if (hasAdminRight) {
        tabs.push({
            key: ShowCommunityTabs.PENDING_MEMBERS,
            name: t('communities.show.waitingMembers.title'),
        })
    }
    tabs.push({
        key: ShowCommunityTabs.MATERIALS,
        name: (
            <HStack>
                <Tag colorPalette={'pink'}>{t('global.soon')}</Tag>
                {t('communities.show.materials.title', {
                    context: 'communities',
                })}
            </HStack>
        ),
        disabled: true,
    })

    return tabs
}
