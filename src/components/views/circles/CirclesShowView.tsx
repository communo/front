import * as React from 'react'
import { useState } from 'react'
import {
    Badge,
    Box,
    ButtonGroup,
    Container,
    Flex,
    HStack,
} from '@chakra-ui/react'
import { generatePath, Link, useNavigate, useParams } from 'react-router'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import {
    FaBox,
    FaChevronDown,
    FaEnvelope,
    FaFire,
    FaUserAstronaut,
} from 'react-icons/fa'
import { Loader } from '@components/atoms/Loader/Loader'
import { CircleLogo } from '@components/molecules/User/Circle/CircleLogo'
import { useAuth } from '@_/auth/useAuth'
import { JoinCircleButton } from '@components/molecules/Circles/JoinCircleButton/JoinCircleButton'
import {
    CircleShowView_CircleQuery,
    useCircleShowView_CircleQuery,
} from '@_/graphql/api/generated'
import { CommunitiesRoutes, UserRoutes } from '@_/routes/_hooks/routes.enum'
import { Button } from '@components/ui/button'
import { toast } from 'react-toastify'
import {
    MenuContent,
    MenuItem,
    MenuRoot,
    MenuTrigger,
} from '@components/ui/menu'
import { InvitationFormDialog } from '@components/molecules/Form/AccountManagement/User/InvitationForm/InvitationFormDialog'
import { PageHeaderWithActions } from '@components/molecules/PageHeader/PageHeaderWithActions'
import {
    ShowCommunityTabs,
    useShowCommunityTabs,
} from '@components/views/circles/useShowCommunityTabs'
import { TabNavigationLayout } from '@components/layout/TabNavigationLayout'
import { useColorModeValue } from '@components/ui/color-mode'
import { CommunityShowTabContent } from '@components/views/circles/CommunityShowTabContent'

const CircleShowViewInner = ({
    community,
}: {
    community: CircleShowView_CircleQuery['circle']
}) => {
    const tabs = useShowCommunityTabs(
        community?.activeUserActions.includes('acceptUser')
    )

    return (
        <Box
            bgGradient="to-b"
            gradientFrom={useColorModeValue('gray.200', 'gray.700')}
            gradientTo={useColorModeValue('gray.200', 'gray.700')}
            gradientVia={useColorModeValue('whiteAlpha.300', 'whiteAlpha.300')}
            pb={{ base: 6, md: 8 }}
        >
            <Container>
                <TabNavigationLayout
                    {...{
                        tabs,
                        activeTab: ShowCommunityTabs.MEMBERS,
                    }}
                >
                    {community && (
                        <CommunityShowTabContent community={community} />
                    )}
                </TabNavigationLayout>
            </Container>
        </Box>
    )
}

export const CirclesShowView = () => {
    const { t } = useTranslation()
    const params = useParams()
    if (!params.slug) {
        throw new Error('circle slug is required')
    }

    const [inviteModalIsOpen, setInviteModalOpen] = useState(false)
    const { error, data } = useCircleShowView_CircleQuery({
        variables: {
            id: generatePath(CommunitiesRoutes.IRI, { slug: params.slug }),
        },
    })
    const auth = useAuth()
    const navigate = useNavigate()
    const circle = data?.circle
    const currentUserMembership = circle?.memberships?.edges?.find(
        (edge) => edge.node!.user.id === auth.user?.id
    )

    return (
        (circle && (
            <>
                <Helmet>
                    <title>
                        {t('communities.show.meta.title', {
                            community: circle,
                        })}
                        {t('meta.title.suffix')}
                    </title>
                </Helmet>
                <PageHeaderWithActions
                    {...{
                        tagLine: circle.subscription?.plan && (
                            <>
                                <FaFire /> {circle.subscription.plan.name}
                            </>
                        ),
                        title: (
                            <Flex gap={3} alignItems="center" mb={5}>
                                <CircleLogo circle={circle} size="xl" />
                                {circle.name}
                            </Flex>
                        ),
                        badges: (
                            <HStack gap={2}>
                                {circle.indexable && (
                                    <Badge
                                        size="md"
                                        colorPalette="gray"
                                        fontWeight="medium"
                                    >
                                        Public
                                    </Badge>
                                )}
                            </HStack>
                        ),
                        metrics: (
                            <>
                                <HStack>
                                    <FaUserAstronaut />
                                    {t(
                                        'communities.card.members.numberOfMembers',
                                        {
                                            count: circle.activeUserActions
                                                .length,
                                        }
                                    )}
                                </HStack>
                                <HStack>
                                    <FaBox />
                                    {t(
                                        'communities.card.members.numberOfMaterials',
                                        {
                                            count: circle.activeUserActions
                                                .length,
                                        }
                                    )}
                                </HStack>
                            </>
                        ),
                        description: circle.description,
                        link: circle.website,

                        actions: (
                            <HStack gap="4">
                                {(circle.activeUserActions.length > 0 &&
                                    circle.activeUserActions.map(
                                        (action: string) =>
                                            (action === 'edit' && circle && (
                                                <ButtonGroup
                                                    size="sm"
                                                    attached
                                                    key={`edit-button-${circle.id}`}
                                                >
                                                    <Button asChild>
                                                        <Link
                                                            to={generatePath(
                                                                CommunitiesRoutes.Edit,
                                                                {
                                                                    slug: circle.slug,
                                                                }
                                                            )}
                                                            title={t(
                                                                'communities.my_communities.index.table.manage'
                                                            )}
                                                        >
                                                            {t(
                                                                'communities.my_communities.index.table.manage'
                                                            )}
                                                        </Link>
                                                    </Button>
                                                    <MenuRoot
                                                        positioning={{
                                                            placement:
                                                                'bottom-end',
                                                        }}
                                                    >
                                                        <MenuTrigger asChild>
                                                            <Button roundedLeft="none">
                                                                <FaChevronDown />
                                                            </Button>
                                                        </MenuTrigger>
                                                        <MenuContent>
                                                            <MenuItem
                                                                value="cut"
                                                                valueText="cut"
                                                            >
                                                                <Button
                                                                    variant="plain"
                                                                    onClick={() =>
                                                                        setInviteModalOpen(
                                                                            true
                                                                        )
                                                                    }
                                                                    asChild
                                                                >
                                                                    <Flex
                                                                        gap={2}
                                                                        alignItems="center"
                                                                    >
                                                                        <FaEnvelope />
                                                                        {t(
                                                                            'communities.my_communities.show.actions.inviteUser'
                                                                        )}
                                                                    </Flex>
                                                                </Button>
                                                            </MenuItem>
                                                        </MenuContent>
                                                    </MenuRoot>
                                                </ButtonGroup>
                                            )) ||
                                            (action === 'quit' &&
                                                currentUserMembership &&
                                                currentUserMembership.node
                                                    ?.status === 'pending' && (
                                                    <JoinCircleButton
                                                        key={`already-join-button-${circle.id}`}
                                                        circle={circle}
                                                        disabled={true}
                                                        title={
                                                            (currentUserMembership
                                                                ?.node
                                                                ?.status &&
                                                                t(
                                                                    `communities.my_communities.index.table.status.${currentUserMembership.node?.status}_tooltip`
                                                                )) ||
                                                            undefined
                                                        }
                                                    />
                                                ))
                                    )) ||
                                    (!auth.user && (
                                        <Button
                                            onClick={() => {
                                                toast.info(
                                                    t('global.please.login')
                                                )
                                                navigate(UserRoutes.Login, {
                                                    state: {
                                                        from: generatePath(
                                                            CommunitiesRoutes.Show,
                                                            {
                                                                slug: circle.slug,
                                                            }
                                                        ),
                                                    },
                                                })
                                            }}
                                        >
                                            {t(
                                                'communities.show.actions.askToJoin.button'
                                            )}
                                        </Button>
                                    )) || <JoinCircleButton circle={circle} />}
                            </HStack>
                        ),
                    }}
                />

                <CircleShowViewInner community={circle} />
                {inviteModalIsOpen && (
                    <InvitationFormDialog
                        {...{
                            inviteModalIsOpen,
                            setInviteModalOpen,
                            community: circle,
                        }}
                    />
                )}
            </>
        )) ||
        (error && <p>Error </p>) || <Loader />
    )
}
