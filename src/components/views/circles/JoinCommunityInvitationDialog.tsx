import {
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogHeader,
    DialogRoot,
} from '@components/ui/dialog'
import { CircleLogo } from '@components/molecules/User/Circle/CircleLogo'
import { Button } from '@components/ui/button'
import { CommunitiesRoutes } from '@_/routes/_hooks/routes.enum'
import { useState } from 'react'
import { generatePath, useSearchParams } from 'react-router'
import { AspectRatio, Box, Flex, Heading } from '@chakra-ui/react'
import { FaCheck } from 'react-icons/fa'
import {
    GetUserCirclesDocument,
    GetUserCirclesQueryVariables,
    JoinCommunityInvitation__CircleFragment,
    useJoinCommunityByInvitationLinkMutation,
    useJoinCommunityInvitationQuery,
} from '@_/graphql/api/generated'
import { useTranslation } from 'react-i18next'
import { toast } from 'react-toastify'

export const JoinCommunityInvitationDialog = ({
    invitationId,
    variables,
}: {
    invitationId: string
    variables: GetUserCirclesQueryVariables
}) => {
    const { t } = useTranslation()
    const [searchParams, setSearchParams] = useSearchParams()
    const [invitationDialogOpen, setInvitationDialogOpen] = useState(true)
    const invitationIRI = generatePath(CommunitiesRoutes.InvitationLinksIRI, {
        token: invitationId!,
    })
    const [communityFromInvitation, setCommunityFromInvitation] =
        useState<JoinCommunityInvitation__CircleFragment>()

    useJoinCommunityInvitationQuery({
        variables: {
            id: invitationIRI,
        },
        skip: !invitationDialogOpen,
        onCompleted: (r) => {
            if (r?.circleInvitationLink) {
                setCommunityFromInvitation(r.circleInvitationLink.circle)
            }
        },
    })
    const [joinCommunity] = useJoinCommunityByInvitationLinkMutation()

    const handleClose = () => {
        setInvitationDialogOpen(false)
        const params = new URLSearchParams(searchParams)
        params.delete('invitation')
        setSearchParams(params)
    }

    return (
        communityFromInvitation && (
            <DialogRoot
                open={invitationDialogOpen}
                onOpenChange={(e: { open: boolean }) => {
                    if (!e.open) {
                        handleClose()
                    }
                }}
                placement="center"
                lazyMount
            >
                <DialogContent>
                    <DialogCloseTrigger onClick={() => handleClose} />
                    <DialogHeader p={0}>
                        <AspectRatio ratio={16 / 9} w="full">
                            <Box
                                bgImage={`url(${communityFromInvitation.cover?.contentUrl ?? '/photos/community-blured.webp'})`}
                                bgSize="cover"
                                bgPos="center"
                                roundedTop={'2xl'}
                                opacity={0.2}
                                h={'full'}
                                w={'full'}
                            >
                                <DialogCloseTrigger
                                    onClick={() => handleClose}
                                />
                            </Box>
                        </AspectRatio>
                        <Flex
                            position="absolute"
                            marginTop={-220}
                            alignItems="center"
                            w="full"
                            direction="column"
                            gap={5}
                        >
                            <CircleLogo
                                circle={communityFromInvitation}
                                size={'2xl'}
                            />
                            <Heading size="4xl" textAlign="center">
                                {communityFromInvitation.name}
                            </Heading>
                        </Flex>
                    </DialogHeader>
                    <DialogBody>
                        <Flex direction="column" alignItems="center" mb="4">
                            <Box ml="4">
                                <Heading size="2xl" textAlign="center">
                                    {t('communities.joinCommunity.modal.title')}
                                </Heading>
                                {t(
                                    'communities.joinCommunity.modal.description'
                                )}
                            </Box>
                        </Flex>
                        <Flex gap={5} justifyContent="center">
                            <Button
                                onClick={() =>
                                    joinCommunity({
                                        variables: {
                                            invitationLinkId: invitationIRI,
                                        },
                                        refetchQueries: [
                                            {
                                                query: GetUserCirclesDocument,
                                                variables,
                                            },
                                        ],
                                        onCompleted: (res) => {
                                            if (
                                                res.joinCircleMembership
                                                    ?.circleMembership
                                            ) {
                                                handleClose()
                                                toast.success(
                                                    t(
                                                        'communities.joinCommunity.modal.toast_success'
                                                    )
                                                )
                                            }
                                        },
                                        onError: (error) => {
                                            toast.error(
                                                t(
                                                    'communities.joinCommunity.modal.toast_error'
                                                )
                                            )
                                            console.log(error.message)
                                        },
                                    })
                                }
                                colorPalette="green"
                            >
                                <FaCheck />
                                {t(
                                    'communities.my_communities.index.table.actions.acceptInvitation'
                                )}
                            </Button>
                        </Flex>
                    </DialogBody>
                </DialogContent>
            </DialogRoot>
        )
    )
}
