import { useTranslation } from 'react-i18next'
import * as React from 'react'
import { Helmet } from 'react-helmet'
import { useParams } from 'react-router'
import { ResetForgottenPasswordForm } from '@components/molecules/Form/AccountManagement/ForgotPassword/ResetForgottenPasswordForm'

export const ForgotPasswordResetView = () => {
    const { t } = useTranslation()
    const params = useParams()

    // useEffect(() => {
    //     fetch(url, {
    //         headers: {
    //             'Accept-Language': i18n.language,
    //             'Content-Type': 'application/json',
    //         },
    //     })
    //         .then((response) => response.json())
    //         .then((response) => {
    //             if (response.message) {
    //                 throw new Error(response.message)
    //             }
    //             if (response.error) {
    //                 throw new Error(response.error)
    //             }
    //         })
    // }, [i18n.language, url])
    return (
        <>
            <Helmet>
                <title>{t('forgotPassword.title')}</title>
            </Helmet>
            {(params.token && (
                <ResetForgottenPasswordForm token={params.token} />
            )) ||
                'Oups'}
        </>
    )
}
