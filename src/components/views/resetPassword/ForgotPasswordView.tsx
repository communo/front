import { useTranslation } from 'react-i18next'
import * as React from 'react'
import { Helmet } from 'react-helmet'
import { ForgotPasswordForm } from '@components/molecules/Form/AccountManagement/ForgotPassword/ForgotPasswordForm'

export const ForgotPasswordView = () => {
    const { t } = useTranslation()

    return (
        <>
            <Helmet>
                <title>{t('forgotPassword.title')}</title>
            </Helmet>
            <ForgotPasswordForm />
        </>
    )
}
