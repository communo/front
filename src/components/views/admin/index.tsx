import { Box } from '@chakra-ui/react'
import { useColorModeValue } from '@components/ui/color-mode'
import { useTranslation } from 'react-i18next'
import * as React from 'react'
import { Helmet } from 'react-helmet'
import { BasicStatistics } from '@components/organisms/admin/Statistics'

export const Admin = () => {
    const { t } = useTranslation()

    return (
        <>
            <Helmet>
                <title>{t('admin.title')}</title>
            </Helmet>
            <Box
                as="section"
                bg={useColorModeValue('gray.50', 'gray.800')}
                pt="5"
                pb="24"
            >
                <Box
                    maxW={{ base: 'xl', md: '7xl' }}
                    mx="auto"
                    px={{ base: '6', md: '8' }}
                >
                    <BasicStatistics title={t('admin.title')} />
                </Box>
            </Box>
        </>
    )
}
