import { Box, Card } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import React, { Suspense, useState } from 'react'
import { H1 } from '@components/atoms/Heading'
import { InvitationList } from '@components/organisms/admin/Invitation'
import {
    InvitationList__InvitationFragment,
    InvitationsQueryVariables,
    useInvitationsQuery,
} from '@_/graphql/api/generated'
import { Loader } from '@components/atoms/Loader/Loader'
import { InvitationsFilterForm } from '@components/organisms/admin/Invitation/InvitationsFilterForm'
import { Pager } from '@components/molecules/Pager/Pager'
import { usePaginatedQuery } from '@_/hooks/usePaginatedQuery'
import { useColorModeValue } from '@components/ui/color-mode'

export const AdminInvitationsView = () => {
    const { t } = useTranslation()
    const [afterCursor, setAfterCursor] = useState<string | undefined>()
    const [beforeCursor, setBeforeCursor] = useState<string | undefined>()
    const [variables, setVariables] = useState<InvitationsQueryVariables>({
        invited: false,
    })

    const {
        loading,
        nodes: invitations,
        pageInfo,
    } = usePaginatedQuery<
        InvitationList__InvitationFragment,
        InvitationsQueryVariables
    >(
        useInvitationsQuery,
        { path: ['invitations'] },
        {
            ...variables,
            afterCursor,
            beforeCursor,
            first: 50,
        }
    )

    return (
        <>
            <Helmet>
                <title>{t('admin.accountManagement.invitations.title')}</title>
            </Helmet>
            <Box
                as="section"
                bg={useColorModeValue('gray.50', 'gray.800')}
                pt="16"
                pb="24"
            >
                <Box maxW="100%" mx="auto" px={{ base: '6', md: '8' }}>
                    <H1
                        textAlign="center"
                        fontSize="4xl"
                        pb={10}
                        fontWeight="bold"
                    >
                        {t('admin.accountManagement.invitations.title')}
                    </H1>
                    <Card.Root
                        rounded="xl"
                        p={5}
                        bgColor={useColorModeValue('gray.50', 'gray.600')}
                    >
                        <Suspense fallback={<Loader />}>
                            <Card.Body>
                                <InvitationsFilterForm
                                    setVariables={setVariables}
                                />

                                {(invitations && (
                                    <>
                                        <InvitationList
                                            invitations={invitations}
                                        />
                                        {(pageInfo?.hasPreviousPage ||
                                            pageInfo?.hasNextPage) && (
                                            <Pager
                                                pageInfo={pageInfo}
                                                loading={loading}
                                                setBeforeCursor={
                                                    setBeforeCursor
                                                }
                                                setAfterCursor={setAfterCursor}
                                            />
                                        )}
                                    </>
                                )) ||
                                    t('global.noResult')}
                            </Card.Body>
                        </Suspense>
                    </Card.Root>
                </Box>
            </Box>
        </>
    )
}
