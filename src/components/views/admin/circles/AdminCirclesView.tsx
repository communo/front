import React, { Suspense, useState } from 'react'
import { Box, Card } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { H1 } from '@components/atoms/Heading'
import { CircleList } from '@components/organisms/admin/Circle'
import {
    CircleList__CircleFragment,
    CirclesQueryVariables,
    useCirclesQuery,
} from '@_/graphql/api/generated'
import { Loader } from '@components/atoms/Loader/Loader'
import { Pager } from '@components/molecules/Pager/Pager'
import { usePaginatedQuery } from '@_/hooks/usePaginatedQuery'
import { CirclesFilterForm } from '@components/organisms/admin/Circle/CirclesFilterForm'
import { useColorModeValue } from '@components/ui/color-mode'

export const AdminCirclesView = () => {
    const { t } = useTranslation()
    const [afterCursor, setAfterCursor] = useState<string | undefined>()
    const [beforeCursor, setBeforeCursor] = useState<string | undefined>()
    const [variables, setVariables] = useState<CirclesQueryVariables>({})

    const {
        loading,
        nodes: circles,
        pageInfo,
    } = usePaginatedQuery<CircleList__CircleFragment, CirclesQueryVariables>(
        useCirclesQuery,
        { path: ['circles'] },
        {
            ...variables,
            afterCursor,
            beforeCursor,
            first: 50,
        }
    )

    return (
        <>
            <Helmet>
                <title>{t('admin.communities.title')}</title>
            </Helmet>
            <Box
                as="section"
                bg={useColorModeValue('gray.50', 'gray.800')}
                pt="16"
                pb="24"
            >
                <Box maxW="100%" mx="auto" px={{ base: '6', md: '8' }}>
                    <H1
                        textAlign="center"
                        fontSize="4xl"
                        pb={10}
                        fontWeight="bold"
                    >
                        {t('admin.communities.title')}
                    </H1>
                    <Card.Root>
                        <Suspense fallback={<Loader />}>
                            <Card.Body
                                rounded="xl"
                                p={5}
                                bgColor={useColorModeValue(
                                    'gray.50',
                                    'gray.600'
                                )}
                            >
                                <CirclesFilterForm
                                    setVariables={setVariables}
                                />
                                {(circles && (
                                    <>
                                        <CircleList circles={circles} />
                                        {(pageInfo?.hasPreviousPage ||
                                            pageInfo?.hasNextPage) && (
                                            <Pager
                                                pageInfo={pageInfo}
                                                loading={loading}
                                                setBeforeCursor={
                                                    setBeforeCursor
                                                }
                                                setAfterCursor={setAfterCursor}
                                            />
                                        )}
                                    </>
                                )) ||
                                    t('global.noResult')}
                            </Card.Body>
                        </Suspense>
                    </Card.Root>
                </Box>
            </Box>
        </>
    )
}
