import React, { Suspense, useState } from 'react'
import { Box, Card } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { H1 } from '@components/atoms/Heading'
import { UserList } from '@components/organisms/admin/User'
import {
    UserList__UserFragment,
    UsersQueryVariables,
    useUsersQuery,
} from '@_/graphql/api/generated'
import { Loader } from '@components/atoms/Loader/Loader'
import { Pager } from '@components/molecules/Pager/Pager'
import { usePaginatedQuery } from '@_/hooks/usePaginatedQuery'
import { UsersFilterForm } from '@components/organisms/admin/User/UsersFilterForm'
import { useColorModeValue } from '@components/ui/color-mode'

export const AdminUsersView = () => {
    const { t } = useTranslation()
    const [afterCursor, setAfterCursor] = useState<string | undefined>()
    const [beforeCursor, setBeforeCursor] = useState<string | undefined>()
    const [variables, setVariables] = useState<UsersQueryVariables>({})

    const {
        loading,
        nodes: users,
        pageInfo,
    } = usePaginatedQuery<UserList__UserFragment, UsersQueryVariables>(
        useUsersQuery,
        { path: ['users'] },
        {
            ...variables,
            afterCursor,
            beforeCursor,
            first: 50,
        }
    )

    return (
        <>
            <Helmet>
                <title>{t('admin.accountManagement.users.title')}</title>
            </Helmet>
            <Box
                as="section"
                bg={useColorModeValue('gray.50', 'gray.800')}
                pt="16"
                pb="24"
            >
                <Box maxW="100%" mx="auto" px={{ base: '6', md: '8' }}>
                    <H1
                        textAlign="center"
                        fontSize="4xl"
                        pb={10}
                        fontWeight="bold"
                    >
                        {t('admin.accountManagement.users.title')}
                    </H1>
                    <Card.Root
                        rounded="xl"
                        p={5}
                        bgColor={useColorModeValue('gray.50', 'gray.600')}
                    >
                        <Suspense fallback={<Loader />}>
                            <Card.Body>
                                <UsersFilterForm setVariables={setVariables} />
                                {(users && (
                                    <>
                                        <UserList users={users} />
                                        {(pageInfo?.hasPreviousPage ||
                                            pageInfo?.hasNextPage) && (
                                            <Pager
                                                pageInfo={pageInfo}
                                                loading={loading}
                                                setBeforeCursor={
                                                    setBeforeCursor
                                                }
                                                setAfterCursor={setAfterCursor}
                                            />
                                        )}
                                    </>
                                )) ||
                                    t('global.noResult')}
                            </Card.Body>
                        </Suspense>
                    </Card.Root>
                </Box>
            </Box>
        </>
    )
}
