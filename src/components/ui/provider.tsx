'use client'

import { ChakraProvider } from '@chakra-ui/react'
import { PropsWithChildren } from 'react'
import { system } from 'src/theme'
import { ColorModeProvider } from './color-mode'

export const Provider = (props: PropsWithChildren) => (
    <ChakraProvider value={system}>
        <ColorModeProvider>{props.children}</ColorModeProvider>
    </ChakraProvider>
)
