import { Center, type CenterProps } from '@chakra-ui/react'

export const GradientPlaceholder = ({
    color1,
    color2,
    direction = 'to-r',
    ...centerProps
}: { color1: string; color2: string; direction?: string } & CenterProps) => (
    <Center
        w="full"
        h="full"
        bgGradient={direction}
        gradientFrom={color1}
        gradientTo={color2}
        color="fg.subtle"
        {...centerProps}
    />
)
