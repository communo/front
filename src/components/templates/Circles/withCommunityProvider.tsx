import React, { FunctionComponent } from 'react'
import { CommunityProvider } from '@_/contexts/Circles/CommunityContext'

/** @ts-ignore-next-line */
export const withCommunityProvider = (
    WrappedComponent: FunctionComponent<unknown>
) => {
    // eslint-disable-next-line func-names,react/display-name
    return function ({ ...props }) {
        return (
            <CommunityProvider>
                <WrappedComponent {...props} />
            </CommunityProvider>
        )
    }
}
