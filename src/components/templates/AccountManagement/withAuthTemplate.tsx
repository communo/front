import React, { ComponentType } from 'react'
import { Helmet } from 'react-helmet'
import {
    Box,
    Container,
    ContainerProps,
    Flex,
    Heading,
    Stack,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'

import { useColorModeValue } from '@components/ui/color-mode'
import { LoadingWrapper } from '../../layout/LoadingWrapper'

export const withAuthTemplate = (
    WrappedComponent: ComponentType<unknown>,
    mode: 'login' | 'register' | 'requestInvite',
    loadingElement?: React.ReactNode,
    maxWContainer: ContainerProps['maxW'] = 'md'
) => {
    // eslint-disable-next-line func-names,react/display-name
    return function ({ ...props }) {
        const { t } = useTranslation()
        const content = (
            <Container maxW={maxWContainer} my={{ base: 2, sm: '50px' }}>
                <Stack align="center">
                    <Heading fontSize="4xl" textAlign="center">
                        {t(`signin.${mode}`)}
                    </Heading>
                </Stack>
                <Stack
                    bg={useColorModeValue('white', 'gray.600')}
                    rounded="3xl"
                    p={{ base: 4, sm: 6, md: 8 }}
                    mt={{ base: 2, sm: 4 }}
                    gap={{ base: 8 }}
                >
                    <Flex>
                        <Box m="0 auto">
                            <WrappedComponent {...props} />
                        </Box>
                    </Flex>
                </Stack>
            </Container>
        )
        return (
            <>
                <Helmet>
                    <title>{`${t(`signin.${mode}`)} < Communo`}</title>
                </Helmet>
                {loadingElement ? (
                    <LoadingWrapper loadingElement={loadingElement} name="auth">
                        {content}
                    </LoadingWrapper>
                ) : (
                    content
                )}
            </>
        )
    }
}
