import React from 'react'
import {
    Center,
    Flex,
    Heading,
    Stack,
    Text,
    useBreakpointValue,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Avatar, AvatarGroup } from '@components/ui/avatar'

const avatars = [
    {
        name: 'Leny',
        url: '/photos/leny.jpg',
    },
    {
        name: 'Marion',
        url: '/photos/marion.jpg',
    },
    {
        name: 'Kent Dodds',
        url: 'https://bit.ly/kent-c-dodds',
    },
    {
        name: 'Prosper Otemuyiwa',
        url: 'https://bit.ly/prosper-baba',
    },
    {
        name: 'Christian Nwamba',
        url: 'https://bit.ly/code-beast',
    },
    {
        name: 'Cyril',
        url: '/photos/cyril.jpg',
    },
]

export const LoadingElement = () => {
    const { t } = useTranslation()
    return (
        <Flex
            flexDirection={{ base: 'column' }}
            maxW="3xl"
            alignItems="center"
            justifyContent="center"
            m="0 auto"
            style={{
                height: 'calc(100vh - 80px)',
            }}
        >
            <Stack gap={{ base: 2 }}>
                <Heading
                    lineHeight={1.1}
                    fontSize={{
                        base: '3xl',
                        sm: '4xl',
                        md: '3xl',
                        lg: '4xl',
                    }}
                    textAlign="center"
                >
                    <>
                        {t('signin.hero.title.part1')}
                        <br />
                        <Text
                            as="span"
                            bgGradient="to-r"
                            gradientFrom="red.400"
                            gradientTo="pink.400"
                            bgClip="text"
                        >
                            {t('signin.hero.title.part2')}
                        </Text>{' '}
                        {t('signin.hero.title.part3')}
                    </>
                </Heading>
                <Center>
                    <Stack direction="row" gap={4} align="center">
                        <AvatarGroup>
                            {avatars.map((avatar) => (
                                <Avatar
                                    key={avatar.name}
                                    name={avatar.name}
                                    src={avatar.url}
                                    size="md"
                                    position="relative"
                                    zIndex={2}
                                    _before={{
                                        content: '""',
                                        width: 'full',
                                        height: 'full',
                                        rounded: 'full',
                                        transform: 'scale(1.125)',
                                        bgGradient:
                                            'linear(to-bl, red.400,pink.400)',
                                        position: 'absolute',
                                        zIndex: -1,
                                        top: 0,
                                        left: 0,
                                    }}
                                />
                            ))}
                        </AvatarGroup>
                        <Text
                            fontFamily="heading"
                            fontSize={{
                                base: '4xl',
                                md: '6xl',
                            }}
                        >
                            +
                        </Text>
                        <Flex
                            align="center"
                            justify="center"
                            fontFamily="heading"
                            fontSize={{ base: 'sm', md: 'lg' }}
                            bg="gray.800"
                            color="white"
                            rounded="full"
                            width={useBreakpointValue({
                                base: '44px',
                                md: '60px',
                            })}
                            height={useBreakpointValue({
                                base: '44px',
                                md: '60px',
                            })}
                            position="relative"
                            _before={{
                                content: '""',
                                width: 'full',
                                height: 'full',
                                rounded: 'full',
                                transform: 'scale(1.125)',
                                bgGradient:
                                    'linear(to-bl, orange.400,yellow.400)',
                                position: 'absolute',
                                zIndex: -1,
                                top: 0,
                                left: 0,
                            }}
                        >
                            <Text>{t('register.you')}</Text>
                        </Flex>
                    </Stack>
                </Center>
            </Stack>
        </Flex>
    )
}
