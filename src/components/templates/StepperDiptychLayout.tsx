import {
    Box,
    Center,
    Container,
    Flex,
    Heading,
    HStack,
    Stack,
    Text,
} from '@chakra-ui/react'
import { ReactNode } from 'react'
import { EmphasisText } from '@components/atoms/Text/EmphasisText'
import { useStepWizard } from '@components/views/accountManagement/registerWizard/StepWizardContext'
import { useTranslation } from 'react-i18next'

interface ProgressIndicatorProps {
    totalSteps: number
    currentStep: number
}

const ProgressIndicator = (props: ProgressIndicatorProps) => {
    const { totalSteps, currentStep } = props
    const stepsArray = Array.from({ length: totalSteps }, (_, i) => i + 1)
    return (
        <HStack>
            {stepsArray.map((step) => (
                <Box
                    key={step}
                    bg={
                        currentStep === step
                            ? 'colorPalette.solid'
                            : 'colorPalette.muted'
                    }
                    w={currentStep === step ? '10' : '6'}
                    transition="all"
                    h="1.5"
                />
            ))}
        </HStack>
    )
}

interface StepperImageDiptychLayoutProps {
    title: string
    description: string
    leftElement: ReactNode
    rightElement: ReactNode
}

export const StepperDiptychLayout = (props: StepperImageDiptychLayoutProps) => {
    const { totalSteps, currentStep } = useStepWizard()
    const { t } = useTranslation()
    const { title, description, leftElement, rightElement } = props
    return (
        <Box minH="full">
            <Flex h="full" w="full">
                <Container
                    maxW="2xl"
                    display="flex"
                    flex="1"
                    flexDirection="column"
                >
                    <Stack gap="8" flex="1" py="20">
                        <Stack>
                            {currentStep > 0 && (
                                <Text
                                    fontWeight="medium"
                                    textStyle="xs"
                                    color="fg.muted"
                                >
                                    {t('stepper.step', {
                                        currentStep,
                                        totalSteps,
                                    })}
                                </Text>
                            )}
                            <Heading size="5xl">{title}</Heading>
                            <EmphasisText color="yellow.400">
                                {description}
                            </EmphasisText>
                        </Stack>
                        {leftElement}
                    </Stack>
                    {currentStep > 0 && (
                        <Center py="10">
                            <ProgressIndicator
                                totalSteps={totalSteps}
                                currentStep={currentStep}
                            />
                        </Center>
                    )}
                </Container>
                <Box flex="1" maxW="4xl" hideBelow="lg">
                    {rightElement}
                </Box>
            </Flex>
        </Box>
    )
}
