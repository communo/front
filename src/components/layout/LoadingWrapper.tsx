import React, { useEffect, useState } from 'react'
import { AnimatePresence, motion, MotionProps } from 'framer-motion'

interface LoadingWrapperProps {
    name: string
    loadingElement: React.ReactNode
    children: React.ReactNode
    delay?: number
    loaderProps?: MotionProps
    displayOnce?: boolean
}

const MotionBox = motion.div

export const LoadingWrapper: React.FC<LoadingWrapperProps> = ({
    name,
    loadingElement,
    children,
    delay = 3000,
    loaderProps = {
        initial: { opacity: 0, scale: 0.9, y: -10 },
        animate: { opacity: 1, scale: 1, y: 0 },
        exit: { opacity: 0 },
        transition: {
            duration: 2.5,
        },
    },
    displayOnce = true,
}) => {
    const [isLoading, setIsLoading] = useState(true)

    const hasAlreadyBeenLoadeded =
        localStorage.getItem(`loading_${name}`) === 'true'

    // eslint-disable-next-line consistent-return
    useEffect(() => {
        const timer = setTimeout(() => {
            setIsLoading(false)
            if (displayOnce) {
                localStorage.setItem(`loading_${name}`, 'true')
            }
        }, delay)

        return () => clearTimeout(timer)
    }, [name, delay, displayOnce])

    return (
        (hasAlreadyBeenLoadeded && displayOnce && <div>{children}</div>) || (
            <AnimatePresence exitBeforeEnter>
                {isLoading ? (
                    <MotionBox key="loading" {...loaderProps}>
                        {loadingElement}
                    </MotionBox>
                ) : (
                    <MotionBox
                        key="content"
                        initial={{ opacity: 0 }}
                        animate={{ opacity: 1 }}
                        exit={{ opacity: 0 }}
                        transition={{ duration: 0.5 }}
                    >
                        {children}
                    </MotionBox>
                )}
            </AnimatePresence>
        )
    )
}
