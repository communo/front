import React, { useEffect } from 'react'
import { Box, Flex } from '@chakra-ui/react'
import { Crisp } from 'crisp-sdk-web'
import { useTranslation } from 'react-i18next'
import { ToastContainer } from 'react-toastify'
import { Outlet } from 'react-router'
import { Helmet } from 'react-helmet'
import {
    CommunitiesRoutes,
    PoolingRoutes,
    StaticPagesRoutes,
} from '@_/routes/_hooks/routes.enum'
import { SearchProvider } from '@_/contexts/Circles/SearchContext'
import { useColorMode, useColorModeValue } from '@components/ui/color-mode'
import Footer from '@components/molecules/Footer/Footer'
import { HeadBar } from '@components/molecules/Menu/HeadBar'
import {
    FaEnvelope,
    FaHome,
    FaMap,
    FaSearchengin,
    FaUsers,
} from 'react-icons/fa'

export const useMainMenu = () => {
    const { t } = useTranslation()
    return [
        {
            name: t('menu.home'),
            icon: FaHome,
            to: StaticPagesRoutes.Home,
            display: { base: 'none', md: 'flex' },
        },
        {
            name: t('menu.pooling'),
            icon: FaSearchengin,
            to: PoolingRoutes.Search,
            display: { base: 'none', md: 'flex' },
        },
        {
            name: t('menu.community.findAndJoin'),
            icon: FaMap,
            to: CommunitiesRoutes.Map,
            display: { base: 'none', md: 'flex' },
        },
        {
            name: t('menu.contact'),
            icon: FaEnvelope,
            to: StaticPagesRoutes.Contact,
            display: { base: 'none', md: 'flex' },
        },
        {
            name: t('menu.team'),
            icon: FaUsers,
            to: StaticPagesRoutes.Team,
            display: { base: 'none', md: 'flex' },
        },
    ]
}

export const Layout = () => {
    useEffect(() => {
        if (process.env.REACT_APP_CRISP_WEBSITE_ID) {
            Crisp.configure(process.env.REACT_APP_CRISP_WEBSITE_ID)
        }
    }, [])

    const { colorMode } = useColorMode()

    return (
        <>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Communo</title>
                <link
                    rel="icon"
                    type="image/svg+xml"
                    href={useColorModeValue(
                        '/favicon-light.svg',
                        '/favicon.svg'
                    )}
                />

                <link
                    rel="apple-touch-icon"
                    sizes="180x180"
                    href="/favicon/apple-touch-icon.png"
                />
                <link
                    rel="icon"
                    type="image/png"
                    sizes="32x32"
                    href="/favicon/favicon-32x32.png"
                />
                <link
                    rel="icon"
                    type="image/png"
                    sizes="16x16"
                    href="/favicon/favicon-16x16.png"
                />
                <link rel="manifest" href="/favicon/site.webmanifest" />
                <link
                    rel="mask-icon"
                    href="/favicon/safari-pinned-tab.svg"
                    color="#5bbad5"
                />
                <meta name="msapplication-TileColor" content="#da532c" />
                <meta name="theme-color" content="#ffde59" />
            </Helmet>
            <SearchProvider>
                <Flex
                    direction="column"
                    flex="1"
                    bg={useColorModeValue('gray.200', 'gray.700')}
                >
                    <HeadBar links={useMainMenu()} />
                    <Flex
                        as="main"
                        role="main"
                        direction="column"
                        flex="1"
                        mt={{ base: 10, sm: 20 }}
                    >
                        <Box minH="lg">
                            <Outlet />
                        </Box>
                    </Flex>
                    <ToastContainer
                        theme={colorMode as 'dark' | 'light'}
                        draggablePercent={60}
                        role="alert"
                        newestOnTop
                        position="bottom-right"
                        style={{ marginBottom: 80 }}
                    />
                    <Footer />
                </Flex>
            </SearchProvider>
        </>
    )
}
