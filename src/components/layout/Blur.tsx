import React, { useEffect, useRef } from 'react'
import { Icon, IconProps, useBreakpointValue } from '@chakra-ui/react'

const useAnimateCircle = (
    initialRadius: number,
    elementRef: React.RefObject<SVGCircleElement>,
    delay: number
) => {
    useEffect(() => {
        const el = elementRef.current
        let animationFrameId: number

        const animate = () => {
            const currentTime = Date.now() / 1000
            const newRadius = initialRadius + Math.sin(currentTime) * 10
            if (el) {
                el.setAttribute('r', String(newRadius))
            }
            animationFrameId = requestAnimationFrame(animate)
        }

        const timeoutId = setTimeout(() => {
            animationFrameId = requestAnimationFrame(animate)
        }, delay)

        return () => {
            clearTimeout(timeoutId)
            cancelAnimationFrame(animationFrameId)
        }
    }, [initialRadius, elementRef, delay])
}

export const Blur: React.FC<IconProps> = ({
    position,
    bottom,
    style,
    left,
}) => {
    const circleRef1 = useRef<SVGCircleElement>(null)
    const circleRef2 = useRef<SVGCircleElement>(null)
    const circleRef3 = useRef<SVGCircleElement>(null)
    const circleRef4 = useRef<SVGCircleElement>(null)
    const circleRef5 = useRef<SVGCircleElement>(null)
    const circleRef6 = useRef<SVGCircleElement>(null)
    const circleRef7 = useRef<SVGCircleElement>(null)

    useAnimateCircle(111, circleRef1, 0)
    useAnimateCircle(139, circleRef2, 50)
    useAnimateCircle(139, circleRef3, 100)
    useAnimateCircle(101.5, circleRef4, 0)
    useAnimateCircle(101.5, circleRef5, 500)
    useAnimateCircle(100, circleRef6, 1000)
    useAnimateCircle(101.5, circleRef7, 0)

    return (
        <Icon
            width={useBreakpointValue({ base: '100%', md: '40vw', lg: '30vw' })}
            zIndex={useBreakpointValue({ base: -1, md: -1, lg: 0 })}
            height="860px"
            position={position}
            bottom={bottom}
            style={style}
            left={left}
        >
            <svg
                viewBox="0 0 528 860"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
            >
                <circle
                    ref={circleRef1}
                    cx="71"
                    cy="61"
                    r="111"
                    fill="#ffde59"
                />
                <circle
                    ref={circleRef2}
                    cx="244"
                    cy="106"
                    r="139"
                    fill="#ED64A6"
                />
                <circle ref={circleRef3} cy="291" r="139" fill="#ED64A6" />
                <circle
                    ref={circleRef4}
                    cx="80.5"
                    cy="189.5"
                    r="101.5"
                    fill="#ffde59"
                />
                <circle
                    ref={circleRef5}
                    cx="196.5"
                    cy="317.5"
                    r="101.5"
                    fill="#ECC94B"
                />
                <circle
                    ref={circleRef6}
                    cx="70.5"
                    cy="458.5"
                    r="101.5"
                    fill="#48BB78"
                />
                <circle
                    ref={circleRef7}
                    cx="426.5"
                    cy="-0.5"
                    r="101.5"
                    fill="#ffde59"
                />
            </svg>
        </Icon>
    )
}
