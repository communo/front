import React, { createContext, ReactNode, useContext, useState } from 'react'
import { Flex, HStack, Spacer, Tabs, TabsRootProps } from '@chakra-ui/react'
import { useNavigate } from 'react-router'

export type TabItem = {
    key: string
    name: string | React.ReactElement
    tag?: string
    icon?: React.ReactElement
    disabled?: boolean
    path?: string
} & TabsRootProps

export const TabSelectedContext = createContext<{
    selectedTab: string | undefined
    setSelectedTab: (value: string) => void
}>({
    selectedTab: undefined,
    setSelectedTab: () => {},
})

export const useTabSelected = () => useContext(TabSelectedContext)

export const TabNavigationLayout = ({
    tabs,
    activeTab,
    children,
    renderTabs,
    actions = [],
    variant = 'enclosed',
}: {
    tabs: TabItem[]
    children: React.ReactNode
    activeTab?: string
    actions?: { key: string; component: ReactNode }[]
    variant?: TabsRootProps['variant']
    renderTabs?: (tabsContent: ReactNode) => ReactNode
}) => {
    const navigate = useNavigate()
    const [selectedTab, setSelectedTab] = useState(activeTab)

    const TabsContent = (
        <Tabs.Root
            variant={variant}
            w="full"
            value={selectedTab}
            onValueChange={(newValue: { value: string }) => {
                const relatedTab = tabs.find(
                    (tab) => tab.key === newValue.value
                )
                if (relatedTab?.path && !relatedTab.disabled) {
                    navigate(relatedTab.path)
                } else {
                    setSelectedTab(newValue.value)
                }
            }}
        >
            <Tabs.List w="full" alignItems="center">
                {tabs.map((tab) => (
                    <Tabs.Trigger
                        key={tab.key}
                        value={tab.key}
                        disabled={tab.disabled}
                    >
                        {tab.icon} {tab.name}
                    </Tabs.Trigger>
                ))}
                <Spacer />
                <HStack pos="relative" gap="4">
                    {actions.map(
                        (action) =>
                            action && (
                                <div key={action.key}>{action.component}</div>
                            )
                    )}
                </HStack>
            </Tabs.List>
        </Tabs.Root>
    )

    return (
        <TabSelectedContext.Provider value={{ selectedTab, setSelectedTab }}>
            <Flex direction="column" w="full">
                {renderTabs ? renderTabs(TabsContent) : TabsContent}
                {children}
            </Flex>
        </TabSelectedContext.Provider>
    )
}
