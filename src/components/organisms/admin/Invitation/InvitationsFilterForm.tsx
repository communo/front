import { Flex, IconButton, Input } from '@chakra-ui/react'
import React, { Dispatch, SetStateAction } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { FaPlay } from 'react-icons/fa'
import { InvitationsQueryVariables } from '@_/graphql/api/generated'
import {
    NativeSelectField,
    NativeSelectRoot,
} from '@components/ui/native-select'
import { Field } from '@components/ui/field'

type InvitationsFilterFormData = {
    email?: string
    postalCode?: string
    invited?: string
}

type Props = {
    setVariables: Dispatch<SetStateAction<InvitationsQueryVariables>>
}

export const InvitationsFilterForm = ({ setVariables }: Props) => {
    const { t } = useTranslation()
    const { register, handleSubmit } = useForm<InvitationsFilterFormData>({
        values: {
            invited: '0',
            postalCode: '',
            email: '',
        },
    })

    const onSubmit = ({
        postalCode,
        email,
        invited,
    }: InvitationsFilterFormData) => {
        setVariables({
            postalCode: postalCode !== '' ? postalCode : undefined,
            email: email !== '' ? email : undefined,
            invited: invited === '' ? undefined : invited === '1',
        })
    }
    const filterFields = [
        {
            label: t('admin.accountManagement.invitations.table.email'),
            input: <Input {...register('email')} />,
        },
        {
            label: t('admin.accountManagement.invitations.table.postalCode'),
            input: <Input {...register('postalCode')} />,
        },
        {
            label: t('admin.accountManagement.invitations.table.status.label'),
            input: (
                <NativeSelectRoot {...register('invited')}>
                    <NativeSelectField>
                        {[
                            { label: '' },
                            {
                                label: t(
                                    'admin.accountManagement.invitations.table.status.choices.notInvited'
                                ),
                                value: 0,
                            },
                            {
                                label: t(
                                    'admin.accountManagement.invitations.table.status.choices.invited'
                                ),
                                value: 1,
                            },
                        ].map(({ label, value }) => (
                            <option value={value} key={label}>
                                {label}
                            </option>
                        ))}
                    </NativeSelectField>
                </NativeSelectRoot>
            ),
        },
    ]

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Flex gap={3} mb={5} alignItems="center">
                {filterFields.map(({ label, input }) => (
                    <Field key={label} label={label}>
                        {input}
                    </Field>
                ))}
                <IconButton aria-label="filter" type="submit">
                    <FaPlay />
                </IconButton>
            </Flex>
        </form>
    )
}
