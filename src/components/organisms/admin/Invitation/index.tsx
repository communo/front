import React from 'react'
import {
    Body,
    Cell,
    Header,
    HeaderCell,
    HeaderRow,
    Row,
    Table,
} from '@table-library/react-table-library/table'
import moment, { locale as setMomentLocale } from 'moment'
import { toast } from 'react-toastify'
import { Flex, Group, HStack, IconButton, Text } from '@chakra-ui/react'
import { FaEnvelope, FaTrash } from 'react-icons/fa'
import { useTranslation } from 'react-i18next'
import {
    InvitationList__InvitationFragment,
    useDeleteInvitationMutation,
    useSendInvitationMutation,
} from '@_/graphql/api/generated'
import { client } from '@_/apollo/main/client'
import { useTableTheme } from '@_/hooks/useTableTheme'
import { Tooltip } from '@components/ui/tooltip'
import { Avatar } from '@components/ui/avatar'
import { PrimaryButton } from '../../../atoms/Button'

type Props = {
    invitations: InvitationList__InvitationFragment[]
}

export const InvitationList = ({ invitations }: Props) => {
    const data = { nodes: invitations }
    const resize = { resizerHighlight: '#ffde59' }
    const { t, i18n } = useTranslation()
    setMomentLocale(i18n.language)
    const [sendInvitation] = useSendInvitationMutation({
        update: () => {
            client.resetStore()
        },
    })
    const [deleteInvitation] = useDeleteInvitationMutation()

    return (
        <Table
            {...{
                data,
                theme: useTableTheme(),
                layout: {
                    rounded: 'xl',
                    fixedHeader: true,
                    bgColor: 'yellow',
                },
            }}
        >
            {(tableList: InvitationList__InvitationFragment[]) => {
                return (
                    <>
                        <Header>
                            <HeaderRow>
                                <HeaderCell resize={resize}>
                                    {t(
                                        'admin.accountManagement.invitations.table.email'
                                    )}
                                </HeaderCell>
                                <HeaderCell resize={resize}>
                                    {t(
                                        'admin.accountManagement.invitations.table.postalCode'
                                    )}
                                </HeaderCell>
                                <HeaderCell resize={resize}>
                                    {t(
                                        'admin.accountManagement.invitations.table.status.label'
                                    )}
                                </HeaderCell>
                                <HeaderCell resize={resize} />
                            </HeaderRow>
                        </Header>

                        <Body>
                            {tableList.map((item) => {
                                const {
                                    id,
                                    email,
                                    postalCode,
                                    invited,
                                    user,
                                    updatedAt,
                                } = item
                                const status = (invited && (
                                    <Tooltip
                                        label={moment(updatedAt).format(
                                            'ddd ll LT'
                                        )}
                                    >
                                        {invited && (
                                            <Text color="green.400">
                                                <Flex
                                                    gap={3}
                                                    alignItems="center"
                                                >
                                                    <FaEnvelope />
                                                    {t(
                                                        'admin.accountManagement.invitations.table.status.sent'
                                                    )}
                                                </Flex>
                                            </Text>
                                        )}
                                    </Tooltip>
                                )) || (
                                    <Text color="gray.400">
                                        <FaEnvelope />
                                    </Text>
                                )

                                return (
                                    <Row key={id} item={item}>
                                        <Cell>{email}</Cell>
                                        <Cell>{postalCode}</Cell>
                                        <Cell>
                                            {(user && (
                                                <HStack>
                                                    <Avatar
                                                        size="xs"
                                                        src={`${user.avatar?.contentUrl}`}
                                                        name={`${user.firstname} ${user.lastname}`}
                                                        mb={4}
                                                        pos="relative"
                                                    />
                                                    {`${user.firstname} ${user.lastname}`}
                                                </HStack>
                                            )) ||
                                                status}
                                        </Cell>
                                        <Cell>
                                            {!user && (
                                                <Group>
                                                    <IconButton
                                                        colorPalette="red"
                                                        onClick={() => {
                                                            deleteInvitation({
                                                                variables: {
                                                                    id,
                                                                },
                                                            })
                                                            toast.success(
                                                                t(
                                                                    'admin.accountManagement.invitations.table.actions.delete.success'
                                                                )
                                                            )
                                                        }}
                                                        aria-label={t(
                                                            'admin.accountManagement.invitations.table.actions.delete.label',
                                                            {
                                                                email,
                                                            }
                                                        )}
                                                    >
                                                        <FaTrash />
                                                    </IconButton>
                                                    <PrimaryButton
                                                        onClick={() => {
                                                            sendInvitation({
                                                                variables: {
                                                                    id,
                                                                },
                                                            })
                                                            toast.success(
                                                                t(
                                                                    'admin.accountManagement.invitations.table.actions.sendInvitation.success'
                                                                )
                                                            )
                                                        }}
                                                    >
                                                        <FaEnvelope />
                                                        {invited
                                                            ? t(
                                                                  'admin.accountManagement.invitations.table.actions.resend'
                                                              )
                                                            : t(
                                                                  'admin.accountManagement.invitations.table.actions.send'
                                                              )}
                                                    </PrimaryButton>
                                                </Group>
                                            )}
                                        </Cell>
                                    </Row>
                                )
                            })}
                        </Body>
                    </>
                )
            }}
        </Table>
    )
}
