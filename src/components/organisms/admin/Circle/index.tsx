import React from 'react'
import {
    Body,
    Cell,
    Header,
    HeaderCell,
    HeaderRow,
    Row,
    Table,
} from '@table-library/react-table-library/table'
import moment, { locale as setMomentLocale } from 'moment'
import { Flex, Text } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { CircleList__CircleFragment } from '@_/graphql/api/generated'
import { useTableTheme } from '@_/hooks/useTableTheme'
import { CircleLogo } from '../../../molecules/User/Circle/CircleLogo'

type Props = {
    circles: CircleList__CircleFragment[]
}

export const CircleList = ({ circles }: Props) => {
    const data = { nodes: circles }
    const resize = { resizerHighlight: '#ffde59' }
    const { t, i18n } = useTranslation()
    setMomentLocale(i18n.language)

    return (
        <Table
            {...{
                data,
                theme: useTableTheme(),
                layout: {
                    rounded: 'xl',
                    fixedHeader: true,
                    bgColor: 'yellow',
                },
            }}
        >
            {(tableList: CircleList__CircleFragment[]) => {
                return (
                    <>
                        <Header>
                            <HeaderRow>
                                <HeaderCell resize={resize}>
                                    {t('admin.communities.table.logo')}
                                </HeaderCell>
                                <HeaderCell resize={resize}>
                                    {t('admin.communities.table.name')}
                                </HeaderCell>
                                <HeaderCell resize={resize}>
                                    {t(
                                        'admin.communities.table.explicitLocationLabel'
                                    )}
                                </HeaderCell>
                                <HeaderCell resize={resize}>
                                    {t('admin.communities.table.createdAt')}
                                </HeaderCell>
                                <HeaderCell resize={resize}>
                                    {t('admin.communities.table.status.label')}
                                </HeaderCell>
                                <HeaderCell resize={resize} />
                            </HeaderRow>
                        </Header>

                        <Body>
                            {tableList.map((item) => {
                                const {
                                    id,
                                    name,
                                    explicitLocationLabel,
                                    indexable,
                                    createdAt,
                                } = item
                                return (
                                    <Row key={id} item={item}>
                                        <Cell>
                                            <CircleLogo circle={item} />
                                        </Cell>
                                        <Cell>{`${name}`}</Cell>
                                        <Cell>{explicitLocationLabel}</Cell>
                                        <Cell>
                                            {moment(createdAt).format(
                                                'ddd ll LT'
                                            )}
                                        </Cell>
                                        <Cell>
                                            {(indexable && (
                                                <Text color="green.400">
                                                    <Flex
                                                        gap={3}
                                                        alignItems="center"
                                                    >
                                                        {t(
                                                            'admin.communities.table.status.choices.indexed'
                                                        )}
                                                    </Flex>
                                                </Text>
                                            )) || (
                                                    <Text color="red.400">
                                                        <Flex
                                                            gap={3}
                                                            alignItems="center"
                                                        >
                                                            {t(
                                                                'admin.communities.table.status.choices.notIndexed'
                                                            )}
                                                        </Flex>
                                                    </Text>
                                                ) || (
                                                    <Text color="gray.400">
                                                        {t(
                                                            'admin.communities.table.status.notConfirmed'
                                                        )}
                                                    </Text>
                                                )}
                                        </Cell>
                                        <Cell />
                                    </Row>
                                )
                            })}
                        </Body>
                    </>
                )
            }}
        </Table>
    )
}
