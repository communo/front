'use client'

import { Box, Flex, SimpleGrid } from '@chakra-ui/react'
import React, { ReactNode, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { BsPerson } from 'react-icons/bs'
import { FaHandshake } from 'react-icons/fa'
import { GiDrill, GiLetterBomb } from 'react-icons/gi'
import { GoLocation } from 'react-icons/go'
import { Link } from 'react-router'
import { StatisticsQuery, useStatisticsQuery } from '@_/graphql/api/generated'
import { useColorModeValue } from '@components/ui/color-mode'
import { StatLabel, StatRoot, StatValueText } from '@components/ui/stat'
import { AdminRoutes } from '@_/routes/_hooks/routes.enum'
import { H1 } from '../../atoms/Heading'

interface StatsCardProps {
    title: string
    stat: string
    icon: ReactNode
    link?: string
}

const StatsCard = (props: StatsCardProps) => {
    const { title, stat, icon, link } = props
    const activeStatProps = {
        borderColor: useColorModeValue('gray.800', 'gray.500'),
        _hover: {
            bgGradient: 'linear(to-l, purple.400, pink.400)',
            color: 'yellow',
            transform: 'translateY(2px)',
            boxShadow: 'lg',
        },
    }
    const disabledStatProps = {
        color: useColorModeValue('gray.100', 'gray.800'),
        borderColor: useColorModeValue('gray.100', 'gray.500'),
        bgColor: useColorModeValue('gray.400', 'gray.500'),
    }
    const statProps = link ? activeStatProps : disabledStatProps
    const content = (
        <StatRoot
            px={{ base: 2, md: 4 }}
            py="5"
            shadow="xl"
            border="1px solid"
            rounded="lg"
            {...statProps}
        >
            <Flex justifyContent="space-between">
                <Box pl={{ base: 2, md: 4 }}>
                    <StatLabel fontWeight="medium">{title}</StatLabel>
                    <StatValueText fontSize="2xl" fontWeight="medium">
                        {stat}
                    </StatValueText>
                </Box>
                <Box my="auto" alignContent="center">
                    {icon}
                </Box>
            </Flex>
        </StatRoot>
    )

    if (link) {
        return <Link to={link}>{content}</Link>
    }
    return content
}

export const BasicStatistics = ({ title }: { title: string }) => {
    const [statistics, setStatistics] = useState<StatisticsQuery>()
    const { t } = useTranslation()
    useStatisticsQuery({
        onCompleted: (res) => {
            setStatistics(res)
        },
    })

    return (
        <Box maxW="7xl" mx="auto" pt={5} px={{ base: 2, sm: 12, md: 17 }}>
            <H1 textAlign="center" fontSize="4xl" py={10} fontWeight="bold">
                {title}
            </H1>
            <SimpleGrid columns={{ base: 1, md: 3 }} gap={{ base: 5, lg: 8 }}>
                <StatsCard
                    title={t('admin.statistics.users')}
                    stat={statistics?.users?.totalCount.toString() ?? '---'}
                    icon={<BsPerson size="3em" />}
                    link={AdminRoutes.Users}
                />
                <StatsCard
                    title={t('admin.statistics.invitations')}
                    stat={
                        statistics?.invitations?.totalCount.toString() ?? '---'
                    }
                    icon={<GiLetterBomb size="3em" />}
                    link={AdminRoutes.Invitations}
                />
                <StatsCard
                    title={t('admin.statistics.communities')}
                    stat={statistics?.circles?.totalCount.toString() ?? '---'}
                    icon={<GoLocation size="3em" />}
                    link={AdminRoutes.Circles}
                />
                <StatsCard
                    title={t('admin.statistics.materials')}
                    stat={statistics?.materials?.totalCount.toString() ?? '---'}
                    icon={<GiDrill size="3em" />}
                />
                <StatsCard
                    title={t('admin.statistics.bookings')}
                    stat={
                        statistics?.materialBookings?.totalCount.toString() ??
                        '---'
                    }
                    icon={<FaHandshake size="3em" />}
                />
            </SimpleGrid>
        </Box>
    )
}
