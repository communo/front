import Lottie from 'react-lottie'
import animationData from './error.json'

export const Error = () => {
    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice',
        },
    }

    return (
        <div>
            <Lottie options={defaultOptions} height={400} width={400} />
        </div>
    )
}
