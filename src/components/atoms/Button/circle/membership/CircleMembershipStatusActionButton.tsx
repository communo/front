import { toast } from 'react-toastify'
import { useTranslation } from 'react-i18next'
import {
    CircleMembershipsTable__CircleMembershipFragment,
    useCircleMembershipsTable__ChangeStatusMutation,
} from '@_/graphql/api/generated'
import { client } from '@_/apollo/main/client'
import { PrimaryButton } from '../../index'

export const AcceptInvitationButton = ({
    circleMembership: { id, circle },
}: {
    circleMembership: CircleMembershipsTable__CircleMembershipFragment
}) => {
    const { t } = useTranslation()
    const [changeStatus] = useCircleMembershipsTable__ChangeStatusMutation()

    return (
        <PrimaryButton
            onClick={() => {
                changeStatus({
                    variables: {
                        id,
                        transition: 'acceptInvitation',
                    },
                    update: () => client.resetStore(),
                })
                toast.success(
                    t('communityMembership.toast.acceptInvitation', {
                        name: circle.name,
                    })
                )
            }}
        >
            {t(
                'communities.my_communities.index.table.actions.acceptInvitation'
            )}
        </PrimaryButton>
    )
}
