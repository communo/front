import React, { ReactNode } from 'react'
import { Button as CButton, ButtonProps } from '@components/ui/button'
import { Link } from 'react-router'

export type Props = {
    link?: string
    linkState?: { from: { pathname: string } }
    children: ReactNode
} & ButtonProps

export const Button = ({ children, link, ...otherProps }: Props) => {
    if (link) {
        return (
            <CButton asChild {...otherProps}>
                <Link to={link}>{children}</Link>
            </CButton>
        )
    }

    return <CButton {...otherProps}>{children}</CButton>
}

export const PrimaryButton = ({ children, ...otherProps }: Props) => (
    <Button colorPalette="yellow" {...otherProps}>
        {children}
    </Button>
)
export const InfoButton = ({ children, ...otherProps }: Props) => (
    <Button colorPalette="blue" {...otherProps}>
        {children}
    </Button>
)

export const SuccessButton = ({ children, ...otherProps }: Props) => (
    <Button colorPalette="green" {...otherProps}>
        {children}
    </Button>
)

export const DangerButton = ({ children, ...otherProps }: Props) => (
    <Button colorPalette="red" {...otherProps}>
        {children}
    </Button>
)

export const RainbowButton = ({
    bg = 'yellow.400',
    children,
    colorPalette = 'yellow',
    _hover = {
        bgGradient: 'to-l',
        gradientFrom: 'purple.400',
        gradientTo: 'pink.400',
        color: 'yellow',
        transform: 'translateY(2px)',
        boxShadow: 'lg',
    },
    ...otherProps
}: Props) => (
    <Button {...{ bg, colorPalette }} {...otherProps} _hover={_hover}>
        {children}
    </Button>
)

export const SubmitButton = ({
    bgGradient = 'to-r',
    gradientFrom = 'red.400',
    gradientTo = 'pink.400',
    children,
    color = 'white',
    _hover = {
        bgGradient: 'linear(to-r, red.400,pink.400)',
        boxShadow: 'xl',
    },
    type = 'submit',
    ...otherProps
}: Props) => (
    <Button
        {...{ bgGradient, gradientFrom, gradientTo, color, type }}
        {...otherProps}
        _hover={_hover}
    >
        {children}
    </Button>
)
