import * as React from 'react'
import { Switch, SwitchProps } from '@components/ui/switch'
import { useTranslation } from 'react-i18next'
import { useColorMode, useColorModeValue } from '@components/ui/color-mode'

export const ColorModeSwitcherButton: React.FC<SwitchProps> = (props) => {
    const { toggleColorMode } = useColorMode()
    const isChecked = useColorModeValue(false, true)
    const { t } = useTranslation()

    return (
        <Switch
            title={t(`switch.mode.${isChecked ? 'dark' : 'light'}`)}
            onChange={toggleColorMode}
            checked={isChecked}
            colorPalette={'yellow'}
            ml="2"
            {...props}
        >
            {t(`switch.mode.${isChecked ? 'dark' : 'light'}`)}
        </Switch>
    )
}
