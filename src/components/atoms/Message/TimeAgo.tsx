import { useTranslation } from 'react-i18next'
import TimeAgoLib from 'timeago-react'

export const TimeAgo = ({ datetime }: { datetime: string }) => {
    const { i18n } = useTranslation()
    const locale = i18n.language === 'en' ? 'en-US' : 'fr-FR'

    return <TimeAgoLib datetime={datetime} locale={locale} />
}
