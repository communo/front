import React from 'react'
import { Badge, Box } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { ReactMarkdown } from 'react-markdown/lib/react-markdown'
import rehypeRaw from 'rehype-raw'
import { useColorModeValue } from '@components/ui/color-mode'

export const AutomaticMessage = ({ content }: { content: string }) => {
    const { t } = useTranslation()

    return (
        <Box my="1" bg={useColorModeValue('gray.300', 'gray.700')} p="3">
            <Badge colorPalette="blue" mb="5">
                {t('discussion.message.automatic')}
            </Badge>
            <ReactMarkdown rehypePlugins={[rehypeRaw]}>{content}</ReactMarkdown>
        </Box>
    )
}
