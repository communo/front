import React, { useEffect, useState, useRef, ReactNode, FC } from 'react'
import { motion } from 'framer-motion'
import { Text } from '@chakra-ui/react'

const TextMotion = motion(Text)

const TextMotionComponent = ({
    children,
    color,
}: {
    children: ReactNode
    color: string
}) => {
    return (
        <TextMotion
            as="span"
            color={color}
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
            transition={{ duration: 1 }}
        >
            {children}
        </TextMotion>
    )
}

type RotatingContentProps = {
    items: ReactNode[]
    timeout: number
    color: string
}

export const RotatingContent: FC<RotatingContentProps> = ({
    items,
    timeout,
    color,
}) => {
    const [currentItem, setCurrentItem] = useState({
        text: items[0],
        key: Math.random().toString(),
    })
    const indexRef = useRef(0)

    useEffect(() => {
        const interval = setInterval(() => {
            indexRef.current = (indexRef.current + 1) % items.length
            setCurrentItem({
                text: items[indexRef.current],
                key: Math.random().toString(),
            })
        }, timeout)

        return () => clearInterval(interval)
    }, [items, timeout])

    return (
        <TextMotionComponent key={currentItem.key} color={color}>
            {currentItem.text}
        </TextMotionComponent>
    )
}
