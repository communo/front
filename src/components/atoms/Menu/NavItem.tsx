import { Link, useLocation } from 'react-router'
import { Box, Flex, Link as CLink } from '@chakra-ui/react'
import React from 'react'
import { isPathCurrent, isUrl } from '@utils/isUrl'
import { NavItemProps } from './NavItemProps'

export const NavItem = ({
    icon,
    to,
    display,
    children,
    ...rest
}: NavItemProps) => {
    const location = useLocation()
    const handle = (
        <Flex
            align="center"
            mx="4"
            p="6"
            as="span"
            gap="4"
            role="group"
            cursor="pointer"
            borderBottomWidth="5px"
            borderBottomColor={
                (to && isPathCurrent(to, location.pathname) && 'yellow.400') ||
                'transparent'
            }
            _hover={{
                bg: 'yellow.400',
                borderRadius: 'xl',
                color: 'black',
                borderBottomColor: 'transparent',
            }}
            {...rest}
        >
            <Box as="span" h="100%">
                {children}
            </Box>
        </Flex>
    )

    return (
        <CLink
            onClick={() => {
                document.body.scrollTop = 0
                document.documentElement.scrollTop = 0
            }}
            {...{
                to,
                as: isUrl(to) ? undefined : Link,
            }}
            display={display}
            zIndex={2}
            _hover={{
                textDecoration: 'none',
            }}
        >
            {handle}
        </CLink>
    )
}
