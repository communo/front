'use client'

import { Flex, FlexProps, IconButton } from '@chakra-ui/react'
import React, { ReactNode } from 'react'
import { FaEllipsis } from 'react-icons/fa6'
import {
    PopoverArrow,
    PopoverBody,
    PopoverContent,
    PopoverRoot,
    PopoverTrigger,
} from '@components/ui/popover'

export type ThreeDotsMenuItem = {
    key: string
    component: ReactNode
}

export const ThreeDotsMenu = ({
    ariaLabel,
    buttons,
    ...otherProps
}: {
    ariaLabel: string
    buttons: ThreeDotsMenuItem[]
} & FlexProps) => {
    return (
        <Flex {...otherProps}>
            <PopoverRoot placement="bottom-end" isLazy zIndex={10}>
                <PopoverTrigger>
                    <IconButton
                        aria-label={ariaLabel}
                        rounded="full"
                        variant="solid"
                        w="fit-content"
                        _hover={{ colorPalette: 'yellow' }}
                    >
                        <FaEllipsis />
                    </IconButton>
                </PopoverTrigger>
                <PopoverContent w="fit-content" _focus={{ boxShadow: 'none' }}>
                    <PopoverArrow />
                    <PopoverBody>
                        {buttons.map(({ key, component }, i) => (
                            <div key={key}>{component}</div>
                        ))}
                    </PopoverBody>
                </PopoverContent>
            </PopoverRoot>
        </Flex>
    )
}
