import { motion, Variants } from 'framer-motion'

export const FluidCard = ({
    children,
    isDisabled = false,
    cardVariants = {
        hidden: { opacity: 0, y: 20 },
        visible: { opacity: 1, y: 0 },
        hover: { scale: 1.05, boxShadow: '0px 4px 15px rgba(0, 0, 0, 0.2)' },
    },
}: {
    children: React.ReactNode
    cardVariants?: Variants
    isDisabled?: boolean
}) => {
    return (
        <motion.div
            initial="hidden"
            animate="visible"
            whileHover={isDisabled ? undefined : 'hover'}
            variants={cardVariants}
        >
            {children}
        </motion.div>
    )
}
