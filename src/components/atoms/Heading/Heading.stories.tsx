import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'

import { Box } from '@chakra-ui/react'
import { H1, H2, H3 } from '@components/atoms/Heading'
import { EmphasisText } from '@components/atoms/Text/EmphasisText'

const meta: Meta<typeof H1> = {
    title: 'atoms/Heading',
    component: H1,
    args: {},
    decorators: [
        (Story) => (
            <Box w={400}>
                <Story />
            </Box>
        ),
    ],
}

export default meta
type Story = StoryObj<typeof H1>

export const Default: Story = {
    args: {
        children: "Let's Communo",
    },
}

export const H1Template: StoryObj<typeof H1> = {}
export const H2Template: StoryObj<typeof H2> = {}
export const H3Template: StoryObj<typeof H3> = {}
export const EmphasisTemplate: StoryObj<typeof EmphasisText> = {
    args: {
        color: 'blue.400',
        children: 'I <em>Love</em> you',
    },
}
