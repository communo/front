import { Blaze__UserFragment } from '@_/graphql/api/generated'

export type Props = {
    permission?: string
} & Blaze__UserFragment
