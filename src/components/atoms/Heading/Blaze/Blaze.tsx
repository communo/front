import { Badge, Heading, Text } from '@chakra-ui/react'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Props } from './types'

export const Blaze = ({ roles, fullname, permission }: Props) => {
    const { t } = useTranslation()
    let content = <span>{fullname}</span>
    if (roles.includes('ROLE_MODERATOR')) {
        content = (
            <Text fontWeight="extrabold">
                {fullname}
                <br />
                <Badge ml="1" colorPalette="green">
                    {t('permission.superadmin')}
                </Badge>
            </Text>
        )
    } else if (permission && ['Owner', 'Administrator'].includes(permission)) {
        content = (
            <Text fontWeight="extrabold">
                {fullname}
                <br />
                <Badge ml="1" colorPalette="green">
                    {t('permission.communities', {
                        context: permission,
                    })}
                </Badge>
            </Text>
        )
    }
    return <Heading fontSize="lg">{content}</Heading>
}
