import React, { ReactNode } from 'react'
import { Box, BoxProps, Mark, Text } from '@chakra-ui/react'
import { Trans } from 'react-i18next'

type Props = {
    children: (string | ReactNode) & (string | string[] | undefined)
    color: string
    bg?: string
} & BoxProps

export const EmphasisText = ({
    children,
    color,
    bg = 'black',
    ...otherProps
}: Props) => {
    let ems = {}
    ;['yellow', 'pink', 'purple', 'blue', 'green', 'orange', 'red'].forEach(
        (item: string) => {
            ems = {
                ...ems,
                [item]: (
                    <Text
                        as="span"
                        fontWeight={'bold'}
                        color={`${item}.400`}
                        key={item}
                    >
                        {children}
                    </Text>
                ),
            }
        }
    )

    return (
        <Box {...otherProps}>
            <Trans
                i18nKey={children}
                components={{
                    ...ems,
                    u: <Box as="u" color={color} />,
                    mark: <Mark px={2} bg={bg} color={color} />,
                    em: <Text as="span" color={color} />,
                    xs: <Text as="span" fontSize="xs" />,
                    md: <Text as="span" fontSize="md" />,
                    lg: <Text as="span" fontSize="lg" />,
                    xl: <Text as="span" fontSize="xl" />,
                    small: <Text as="small" fontSize="md" />,
                    sub: <sub />,
                }}
            />
        </Box>
    )
}
