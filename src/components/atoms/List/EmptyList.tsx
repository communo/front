import React, { ReactNode } from 'react'
import { VStack } from '@chakra-ui/react'
import { useColorModeValue } from '@components/ui/color-mode'

export const EmptyList = ({ children }: { children: ReactNode }) => {
    return (
        <VStack
            gap={5}
            color={useColorModeValue('black', 'white')}
            bg={useColorModeValue('gray.200', 'gray.800')}
            borderRadius="2xl"
            p={5}
        >
            {children}
        </VStack>
    )
}
