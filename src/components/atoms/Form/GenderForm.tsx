import React, { useMemo } from 'react'
import { UseFormRegisterReturn } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import {
    NativeSelectField,
    NativeSelectRoot,
} from '@components/ui/native-select'

export const GenderForm = ({
    register,
    required = false,
}: {
    register: UseFormRegisterReturn
    required?: boolean
}) => {
    const { t } = useTranslation()
    const options = useMemo(
        () => [
            { label: '' },
            { label: t('form.gender.other'), value: '' },
            { label: t('form.gender.nonBinary'), value: 'nonBinary' },
            { label: t('form.gender.female'), value: 'female' },
            { label: t('form.gender.male'), value: 'male' },
        ],
        [t]
    )

    return (
        <NativeSelectRoot>
            <NativeSelectField required={required} {...register}>
                {options.map((option: { label: string; value?: string }) => (
                    <option key={option.label} value={option.value}>
                        {option.label}
                    </option>
                ))}
            </NativeSelectField>
        </NativeSelectRoot>
    )
}
