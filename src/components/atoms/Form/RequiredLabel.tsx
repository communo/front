import React, { ReactNode } from 'react'
import { RequiredAsterisk } from '@components/atoms/Form/RequiredAsterisk'
import { Flex } from '@chakra-ui/react'

export const RequiredLabel = ({ children }: { children: ReactNode }) => (
    <Flex gap="1" alignItems="center">
        {children}
        <RequiredAsterisk />
    </Flex>
)
