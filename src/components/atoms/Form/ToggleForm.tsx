'use client'

import { Box, Flex, Icon } from '@chakra-ui/react'
import { Switch } from '@components/ui/switch'
import { Field } from '@components/ui/field'

interface ToggleFormProps {
    icon?: React.ReactNode
    title: string
    description: string
    checked?: boolean
    onCheckedChange?: (value: boolean) => void
}

export const ToggleForm = (props: ToggleFormProps) => {
    const { icon, title, description, checked, onCheckedChange } = props
    return (
        <>
            {icon && (
                <Icon
                    alignSelf="flex-start"
                    pos="relative"
                    top="1"
                    size="lg"
                    color="fg.muted"
                >
                    {icon}
                </Icon>
            )}
            <Flex textStyle="sm" flex="1" gap="1" flexDirection={'row'}>
                <Field label={title}>
                    <Switch
                        size="lg"
                        defaultChecked={checked}
                        onCheckedChange={(e: { checked: boolean }) =>
                            onCheckedChange?.(e.checked)
                        }
                    />
                </Field>
            </Flex>
            <Box color="fg.muted" fontSize={'xs'} mt={3}>
                {description}
            </Box>
        </>
    )
}
