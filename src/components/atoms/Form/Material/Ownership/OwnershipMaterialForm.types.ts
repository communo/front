import { SelectRootProps } from '@chakra-ui/react'

export type OwnershipMaterialOption =
    | {
          value: string
          label: string
      }
    | {
          value: string
          label: string
          plan: string
      }
export type OwnershipMaterialFormProps = {
    inputName: 'mainOwnership' | 'ownerships'
} & SelectRootProps<OwnershipMaterialOption>
