import { UseFormRegisterReturn } from 'react-hook-form'
import { useMaterialCategorySelectQuery } from '@_/graphql/api/generated'
import {
    NativeSelectField,
    NativeSelectRoot,
} from '@components/ui/native-select'

type Props = {
    register: UseFormRegisterReturn
}

export const CategorySelect = ({ register }: Props) => {
    const { loading, error, data } = useMaterialCategorySelectQuery()
    if (error) return <p>Error :(</p>
    const categories = data?.materialCategories?.edges ?? []
    return (
        <NativeSelectRoot>
            <NativeSelectField
                required={false}
                disabled={loading}
                {...register}
            >
                {!loading &&
                    categories.map(({ node }) => (
                        <option key={node!.id} value={node!.id}>
                            {node!.name}
                        </option>
                    ))}
            </NativeSelectField>
        </NativeSelectRoot>
    )
}
