import React from 'react'
import { useTranslation } from 'react-i18next'
import { moneyFormat } from '@utils/money'
import { datePeriodText } from '@utils/datePeriodText'
import { priceText } from '@utils/priceText'
import { useAuth } from '@_/auth/useAuth'
import { StatLabel, StatRoot, StatValueText } from '@components/ui/stat'
import { Card } from '@chakra-ui/react'
import { useColorModeValue } from '@components/ui/color-mode'

export const BookingPriceStat = ({
    price,
    periods,
}: {
    price: number
    periods: {
        id: string
        endDate: string
        startDate: string
    }[]
}) => {
    const { t, i18n } = useTranslation()
    const bgAccent = useColorModeValue('gray.100', 'gray.800')
    const auth = useAuth()

    return (
        <Card.Root bg={bgAccent} borderRadius="2xl">
            <Card.Body>
                <StatRoot>
                    <StatLabel>
                        <ul>
                            {periods.map(({ endDate, startDate, id }) => (
                                <li key={`period-${id}`}>
                                    {datePeriodText(
                                        startDate,
                                        endDate,
                                        t,
                                        i18n.language
                                    )}
                                </li>
                            ))}
                        </ul>
                    </StatLabel>
                    <StatValueText>
                        {(price &&
                            price !== 0 &&
                            t('pooling.show.booking.summary.periodItem.price', {
                                price: moneyFormat(price, i18n.language),
                            })) ||
                            priceText(price, auth.user, t)}
                    </StatValueText>
                </StatRoot>
            </Card.Body>
        </Card.Root>
    )
}
