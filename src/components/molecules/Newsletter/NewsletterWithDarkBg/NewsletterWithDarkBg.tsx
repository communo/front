import {
    Badge,
    Box,
    Button,
    Flex,
    Heading,
    Input,
    Stack,
    useClipboard,
} from '@chakra-ui/react'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { HiShieldCheck } from 'react-icons/hi'
import { EmphasisText } from '@components/atoms/Text/EmphasisText'
import { SubmitHandler, useForm } from 'react-hook-form'
import { useSubscribeNewsletterMutation } from '@_/graphql/api/generated'
import { toast } from 'react-toastify'
import { useColorModeValue } from '@components/ui/color-mode'

const NewsletterWithDarkBg = () => {
    const { t } = useTranslation()
    const [subscribeNewsletter] = useSubscribeNewsletterMutation()
    type NewsletterFormData = {
        email: string
    }
    const { register, handleSubmit, reset } = useForm<NewsletterFormData>()
    const onSubmit: SubmitHandler<NewsletterFormData> = (data) => {
        subscribeNewsletter({
            variables: {
                input: data,
            },
            onCompleted: (res) => {
                if (res.subscribeNewsletterContact?.contact) {
                    reset()
                    toast.success(t('layout.newsletter.toast.success'))
                }
            },
        })
    }
    const { onCopy } = useClipboard(
        process.env.REACT_APP_CONTACT_EMAIL || 'hey@communo.app'
    )
    return (
        <Box
            as="section"
            bg={useColorModeValue('gray.100', 'gray.700')}
            py="12"
        >
            <Box
                textAlign="center"
                bg={useColorModeValue('white', 'gray.800')}
                shadow="lg"
                maxW={{ base: 'xl', md: '3xl' }}
                mx="auto"
                px={{ base: '6', md: '8' }}
                py="12"
                rounded="lg"
            >
                <Box maxW="md" mx="auto">
                    <Flex
                        color={useColorModeValue('green.600', 'green.400')}
                        fontWeight="bold"
                        fontSize="sm"
                        letterSpacing="wide"
                        gap={2}
                        justify={'center'}
                    >
                        <Badge
                            px="2"
                            variant="solid"
                            colorPalette="pink"
                            rounded="full"
                            textTransform="capitalize"
                        >
                            {t('global.new')}
                        </Badge>
                        {t('layout.newsletter.preTitle')}
                    </Flex>
                    <Heading mt="4" fontWeight="extrabold">
                        <EmphasisText color={'yellow.400'}>
                            {t('layout.newsletter.title')}
                        </EmphasisText>
                    </Heading>
                    <Box mt="6">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <Stack>
                                <Input
                                    aria-label={t(
                                        'layout.newsletter.input.ariaLabel'
                                    )}
                                    placeholder={t(
                                        'layout.newsletter.input.placeholder'
                                    )}
                                    {...register('email')}
                                    rounded="base"
                                />
                                <Button
                                    type="submit"
                                    w="full"
                                    colorPalette="blue"
                                    size="lg"
                                    textTransform="uppercase"
                                    fontSize="sm"
                                    fontWeight="bold"
                                >
                                    {t('layout.newsletter.cta')}
                                </Button>
                            </Stack>
                        </form>
                        <Box
                            color={useColorModeValue('gray.600', 'gray.400')}
                            fontSize="sm"
                            mt="5"
                        >
                            <Box
                                aria-hidden
                                as={HiShieldCheck}
                                display="inline-block"
                                marginEnd="2"
                                fontSize="lg"
                                color={useColorModeValue(
                                    'green.600',
                                    'green.400'
                                )}
                            />
                            <Box
                                onClick={() => {
                                    onCopy()
                                    toast.info(
                                        t('layout.newsletter.emailCopied')
                                    )
                                }}
                                cursor={'pointer'}
                            >
                                <EmphasisText color={'blue.400'}>
                                    {t('layout.newsletter.noSpam')}
                                </EmphasisText>
                            </Box>
                        </Box>
                    </Box>
                </Box>
            </Box>
        </Box>
    )
}

export default NewsletterWithDarkBg
