import * as React from 'react'
import { useColorModeValue } from '@components/ui/color-mode'
import { CircleLogoFragment } from '@_/graphql/api/generated'
import { Avatar, AvatarProps } from '@components/ui/avatar'
import { CommunityIcon } from '../../../atoms/Logo/CommunityIcon'

type Props = {
    circle: CircleLogoFragment
    alt?: string
} & AvatarProps

export const CircleLogo = ({ circle, ...otherProps }: Props) => {
    return (
        <Avatar
            src={`${circle.logo?.contentUrl}`}
            icon={<CommunityIcon width={30} height={30} />}
            border={`1px solid ${useColorModeValue('#eee', '#444')}`}
            boxShadow="lg"
            bg={useColorModeValue('white', 'gray.700')}
            name={circle.name}
            title={circle.name}
            {...otherProps}
        />
    )
}
