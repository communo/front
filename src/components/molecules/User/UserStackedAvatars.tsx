import { ReactNode } from 'react'
import { StackedAvatars } from '@components/molecules/Avatar/StackedAvatars'
import { HStack, Text } from '@chakra-ui/react'
import { useColorModeValue } from '@components/ui/color-mode'
import { useTranslation } from 'react-i18next'

export const UserStackedAvatars = ({
    users,
    totalCount,
    otherLabel,
}: {
    users?: {
        fullname: string
        roles: string[]
        avatar?: { contentUrl?: string | undefined }
    }[]
    totalCount?: number
    otherLabel?: ReactNode
}) => {
    const { t } = useTranslation()
    const avatars =
        users
            ?.map((user) => {
                return {
                    name: user.fullname,
                    url: user.avatar?.contentUrl,
                    color:
                        user.roles.includes('ROLE_ADMIN') ||
                        user.roles.includes('ROLE_MODERATOR')
                            ? 'yellow.400'
                            : undefined,
                }
            })
            .filter((avatar) => Boolean) || []
    const otherCount = (totalCount && totalCount - avatars.length) || 0
    const color = useColorModeValue('gray.500', 'gray.300')

    return (
        <HStack gap={2}>
            <StackedAvatars avatars={avatars} />
            {otherCount > 0 && (
                <Text color={color}>
                    {otherLabel ||
                        t('users.avatarStacked.andXOthers', {
                            count: otherCount,
                        })}
                </Text>
            )}
        </HStack>
    )
}
