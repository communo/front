import React from 'react'
import { Box, BoxProps, Flex, FlexProps } from '@chakra-ui/react'
import { FiBell } from 'react-icons/fi'
import { BsCalendar2Week } from 'react-icons/bs'
import { FaEllo, FaHammer, FaIdCard, FaUserShield } from 'react-icons/fa'
import { useNavigate } from 'react-router'
import { useTranslation } from 'react-i18next'
import {
    CommunitiesRoutes,
    PoolingRoutes,
    UserRoutes,
} from '@_/routes/_hooks/routes.enum'
import { useColorModeValue } from '@components/ui/color-mode'
import { Field } from '@components/ui/field'

import {
    SelectContent,
    SelectItem,
    SelectLabel,
    SelectRoot,
    SelectTrigger,
    SelectValueText,
} from '@components/ui/select'
import { FaGear } from 'react-icons/fa6'
import { NavItem } from '../../../atoms/Nav/NavItem'
import { NavItemProps } from '../../../atoms/Menu/NavItemProps'

type SidebarContextProps = {
    linkItems: NavItemProps[]
} & BoxProps

type MobileNavContextProps = {
    linkItems: NavItemProps[]
} & FlexProps

const SidebarContent = ({ linkItems, ...rest }: SidebarContextProps) => {
    return (
        <Box
            bg={useColorModeValue('white', 'gray.900')}
            borderRight="1px"
            borderRightColor={useColorModeValue('gray.200', 'gray.700')}
            w={{ base: 'full', md: 60 }}
            {...rest}
        >
            {linkItems.map((link) => (
                <NavItem key={link.name} {...link} />
            ))}
        </Box>
    )
}

const MobileNav = ({ linkItems, ...rest }: MobileNavContextProps) => {
    const navigate = useNavigate()
    const { t } = useTranslation()

    return (
        <Flex
            ml={{ base: 0, md: 60 }}
            px={{ base: 4, md: 24 }}
            height="20"
            alignItems="center"
            bg={useColorModeValue('white', 'gray.900')}
            borderBottomWidth="1px"
            position="sticky"
            top={70}
            left={0}
            right={0}
            borderBottomColor={useColorModeValue('gray.200', 'gray.700')}
            justifyContent="flex-start"
            order={{ base: 1, md: 2 }}
            {...rest}
        >
            <Field p={4}>
                <SelectRoot
                    onValueChange={(item: { value: string }) => {
                        if (
                            item &&
                            item.value !== null &&
                            item.value !== undefined
                        ) {
                            navigate(item.value[0])
                        }
                    }}
                    size="lg"
                >
                    <SelectLabel />
                    <SelectTrigger>
                        <SelectValueText
                            placeholder={t('pooling.booking.index.title')}
                        />
                    </SelectTrigger>
                    <SelectContent>
                        {linkItems.map((item) => {
                            return (
                                <SelectItem
                                    item={{ value: item.to }}
                                    key={item.name}
                                    disabled={item.disabled}
                                >
                                    {item.name}
                                </SelectItem>
                            )
                        })}
                    </SelectContent>
                </SelectRoot>
            </Field>
        </Flex>
    )
}

export const ProfileSidebar = () => {
    const { t } = useTranslation()
    const linkItems: Array<NavItemProps> = [
        {
            name: t('menu.my.profile'),
            icon: FaIdCard,
            to: UserRoutes.MyProfile,
        },
        {
            name: t('menu.my.preferences'),
            icon: FaGear,
            to: UserRoutes.MyPreferences,
        },
        {
            name: t('menu.my.materials'),
            icon: FaHammer,
            to: PoolingRoutes.MyMaterials,
        },
        {
            name: t('menu.my.bookings'),
            icon: BsCalendar2Week,
            to: PoolingRoutes.MyBookingAsOwner,
        },
        {
            name: t('menu.my.communities'),
            icon: FaEllo,
            to: CommunitiesRoutes.MyCommunities,
        },
        {
            name: t('menu.my.alerts'),
            icon: FiBell,
            to: PoolingRoutes.MyAlerts,
        },
        {
            name: t('menu.my.lists'),
            icon: FaUserShield,
            to: UserRoutes.MyLists,
        },
    ]

    return (
        <>
            <SidebarContent
                display={{ base: 'none', md: 'block' }}
                linkItems={linkItems}
            />
            <MobileNav hideFrom="md" linkItems={linkItems} />
        </>
    )
}
