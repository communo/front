import { Button, ButtonProps } from '@components/ui/button'
import { generatePath, Link } from 'react-router'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Material } from '@_/graphql/api/generated'

export const EditButton = ({
    material,
    ...otherProps
}: {
    material: Pick<Material, 'slug'>
} & ButtonProps) => {
    const { t } = useTranslation()
    return (
        <Button
            asChild
            flex={1}
            width="full"
            fontSize="sm"
            colorPalette="green"
            _hover={{
                transform: 'translateY(2px)',
                boxShadow: 'lg',
            }}
            {...otherProps}
        >
            <Link
                to={generatePath('/my/materials/:slug/edit', {
                    slug: material.slug,
                })}
            >
                {t('pooling.edit.button')}
            </Link>
        </Button>
    )
}
