import useEvent from 'react-use/lib/useEvent'
import React, { useCallback, useState } from 'react'
import {
    Box,
    Button,
    Center,
    HStack,
    IconButton,
    Image,
    Kbd,
    useDisclosure,
    VStack,
} from '@chakra-ui/react'
import { FaExpand, FaTimes } from 'react-icons/fa'
import { useTranslation } from 'react-i18next'
import {
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogRoot,
} from '@components/ui/dialog'
import { motion } from 'framer-motion'
import { useColorModeValue } from '@_/components/ui/color-mode'

const MotionImage = motion(Image)
const MotionBox = motion(Box)

export const Pictures = ({
    material,
    imageConnection,
    borderRadius = '10px',
}: {
    material: {
        name: string
    }
    imageConnection?: {
        edges?: {
            cursor: string
            node?: {
                id: string
                imageName: string
                size: number
                contentUrl?: string
            }
        }[]
    }
    borderRadius?: string
}) => {
    const [activeIndex, setActiveIndex] = useState<number>(0)
    const images = imageConnection?.edges?.map((edge) => edge.node!) || []

    const { t } = useTranslation()
    const { open, onOpen, onClose } = useDisclosure()

    const handleKeyDown = useCallback(
        // @ts-ignore
        (e) => {
            if (activeIndex === null) {
                return
            }

            if (e.key === 'ArrowLeft' || e.keyCode === 37) {
                setActiveIndex(Math.max(activeIndex - 1, 0))
            } else if (e.key === 'ArrowRight' || e.keyCode === 39) {
                setActiveIndex(Math.min(activeIndex + 1, images.length - 1))
            } else if (e.key === 'Escape' || e.keyCode === 27) {
                onClose()
            }
        },
        [activeIndex, images.length, onOpen]
    )

    useEvent('keydown', handleKeyDown, document)
    const dialogBg = useColorModeValue('yellow.100', 'gray.800')

    return (
        <Box
            w="100%"
            borderRadius={borderRadius}
            bgGradient="linear(to-r, gray.200, gray.300)"
        >
            {images.length > 0 && (
                <>
                    <MotionBox
                        position="relative"
                        width="100%"
                        display="inline-block"
                        whileHover={
                            images.length > 1
                                ? {
                                      y: -5,
                                      rotate: -1,
                                      transition: {
                                          duration: 0.5,
                                          ease: 'easeInOut',
                                      },
                                      boxShadow: '2xl',
                                  }
                                : undefined
                        }
                    >
                        <MotionImage
                            title={
                                images.length > 1
                                    ? t('pooling.show.pictures.seeOthers')
                                    : ''
                            }
                            cursor="pointer"
                            maxH={455}
                            w="100%"
                            objectFit="cover"
                            boxShadow="lg"
                            borderRadius={borderRadius}
                            src={`${images[0].contentUrl}`}
                            alt={material.name}
                            onClick={onOpen}
                        />
                        <HStack position="absolute" right={0} bottom="0" m="5">
                            <Button
                                size="xs"
                                px={2}
                                mt={8}
                                colorPalette="yellow"
                                onClick={onOpen}
                                variant="solid"
                            >
                                <FaExpand />
                            </Button>
                        </HStack>
                    </MotionBox>

                    <DialogRoot
                        lazyMount
                        onClose={onClose}
                        size="cover"
                        open={open}
                    >
                        <DialogContent bg={dialogBg}>
                            <DialogCloseTrigger>
                                <IconButton variant="ghost" onClick={onClose}>
                                    <FaTimes />
                                </IconButton>
                            </DialogCloseTrigger>
                            <DialogBody>
                                <VStack gap={2} mt={10}>
                                    {images[activeIndex] && (
                                        <Image
                                            boxShadow="lg"
                                            borderRadius={5}
                                            h={{
                                                base:
                                                    images.length === 1
                                                        ? '80vh'
                                                        : '60vh',
                                                sm:
                                                    images.length === 1
                                                        ? '80vh'
                                                        : '70vh',
                                            }}
                                            minH="500px"
                                            objectFit="contain"
                                            src={`${images[activeIndex]?.contentUrl}`}
                                            alt={material.name}
                                        />
                                    )}
                                    {images.length > 1 && (
                                        <Box p={5} w="full">
                                            <Center>
                                                <HStack>
                                                    {images.map((img, i) => (
                                                        <Image
                                                            cursor="pointer"
                                                            key={`${img.imageName}`}
                                                            src={`${img.contentUrl}`}
                                                            h="100px"
                                                            maxW="300px"
                                                            boxShadow="sm"
                                                            borderBottom="1px solid #ddd"
                                                            onClick={() =>
                                                                setActiveIndex(
                                                                    i
                                                                )
                                                            }
                                                        />
                                                    ))}
                                                </HStack>
                                            </Center>
                                        </Box>
                                    )}
                                    <Box
                                        position={{
                                            base: 'relative',
                                            md: 'fixed',
                                        }}
                                        right={{ base: 'initial', md: 10 }}
                                        bottom={{ base: 'initial', md: 10 }}
                                    >
                                        {images.length > 1 && (
                                            <>
                                                <Button
                                                    onClick={() =>
                                                        setActiveIndex(
                                                            (prevIndex) =>
                                                                (prevIndex -
                                                                    1 +
                                                                    images.length) %
                                                                images.length
                                                        )
                                                    }
                                                    variant="ghost"
                                                    colorPalette="yellow"
                                                >
                                                    <Kbd
                                                        display={{
                                                            base: 'none',
                                                            md: 'block',
                                                        }}
                                                        mr={2}
                                                    >
                                                        ←
                                                    </Kbd>
                                                    {t('global.previous')}
                                                </Button>
                                                <Button
                                                    onClick={() =>
                                                        setActiveIndex(
                                                            (prevIndex) =>
                                                                (prevIndex +
                                                                    1) %
                                                                images.length
                                                        )
                                                    }
                                                    variant="ghost"
                                                    colorPalette="yellow"
                                                >
                                                    <Kbd
                                                        display={{
                                                            base: 'none',
                                                            md: 'block',
                                                        }}
                                                        mr={2}
                                                    >
                                                        →
                                                    </Kbd>
                                                    {t('global.next')}
                                                </Button>
                                            </>
                                        )}
                                        <Button
                                            onClick={onClose}
                                            variant="ghost"
                                            colorPalette="yellow"
                                        >
                                            <Kbd
                                                display={{
                                                    base: 'none',
                                                    md: 'block',
                                                }}
                                                mr={2}
                                            >
                                                Esc
                                            </Kbd>
                                            {t('modal.close')}
                                        </Button>
                                    </Box>
                                </VStack>
                            </DialogBody>
                        </DialogContent>
                    </DialogRoot>
                </>
            )}
        </Box>
    )
}
