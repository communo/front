import { Button, IconButton } from '@chakra-ui/react'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { generatePath, useNavigate } from 'react-router'
import { toast } from 'react-toastify'
import {
    MaterialCard__MaterialFragment,
    useForkMaterialMutation,
} from '@_/graphql/api/generated'
import { PoolingRoutes } from '@_/routes/_hooks/routes.enum'
import { FaClone } from 'react-icons/fa6'
import {
    DialogActionTrigger,
    DialogBackdrop,
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogFooter,
    DialogHeader,
    DialogRoot,
    DialogTitle,
    DialogTrigger,
} from '@components/ui/dialog'
import { useAuth } from '@_/auth/useAuth'
import { useColorModeValue } from '@_/components/ui/color-mode'
import { MaterialCard } from '../Cards/MaterialCard/MaterialCard'

export const ForkButton = ({
    material,
}: {
    material: MaterialCard__MaterialFragment
}) => {
    const { t } = useTranslation()
    const [duplicateMaterial] = useForkMaterialMutation()
    const navigate = useNavigate()
    const auth = useAuth()

    return (
        <DialogRoot placement="center">
            <DialogBackdrop />
            <DialogTrigger asChild>
                <IconButton
                    fontSize="sm"
                    rounded="full"
                    colorPalette="yellow"
                    boxShadow="0px 1px 25px -5px rgb(66 153 225 / 48%), 0 10px 10px -5px rgb(66 153 225 / 43%)"
                    _focus={{
                        bg: 'yellow.300',
                    }}
                    _hover={{
                        bg: 'yellow.300',
                        transform: 'translateY(2px)',
                        boxShadow: 'lg',
                    }}
                    aria-label={t('pooling.show.button.duplicate.label')}
                >
                    <FaClone />
                </IconButton>
            </DialogTrigger>
            <DialogBackdrop />
            <DialogContent bg={useColorModeValue('blue.300', 'blue.800')}>
                <DialogHeader>
                    <DialogTitle>{t('pooling.duplicate.title')}</DialogTitle>
                </DialogHeader>
                <DialogBody>
                    {t('pooling.duplicate.description')}
                    {auth.user && (
                        <MaterialCard
                            disabled={true}
                            material={{
                                ...material,
                                ...{ mainOwnershipUser: auth.user },
                            }}
                        />
                    )}
                </DialogBody>
                <DialogFooter>
                    <DialogActionTrigger asChild>
                        <Button variant="outline">{t('global.cancel')}</Button>
                    </DialogActionTrigger>
                    <Button
                        onClick={() =>
                            duplicateMaterial({
                                variables: {
                                    input: { id: material.id },
                                },
                                onCompleted: (data) => {
                                    if (data.forkMaterial?.material) {
                                        toast.success(
                                            t('pooling.duplicate.toast.success')
                                        )
                                        const { slug } =
                                            data.forkMaterial.material
                                        navigate(
                                            generatePath(
                                                PoolingRoutes.EditMyMaterialStep,
                                                {
                                                    slug,
                                                    step: 'information',
                                                }
                                            )
                                        )
                                    }
                                },
                            })
                        }
                        colorPalette={'yellow'}
                    >
                        {t('pooling.duplicate.confirm')}
                    </Button>
                </DialogFooter>
                <DialogCloseTrigger />
            </DialogContent>
        </DialogRoot>
    )
}
