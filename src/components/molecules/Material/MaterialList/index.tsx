import { Box, Flex, SimpleGrid } from '@chakra-ui/react'
import * as React from 'react'
import { MaterialCard } from '../../Cards/MaterialCard/MaterialCard'
import { Props } from './types'

export const MaterialList = ({
    materials,
    columns = {
        base: 1,
        sm: 1,
        md: 2,
        lg: 3,
        xl: 4,
    },
}: Props) => {
    return (
        <Flex
            textAlign="center"
            justifyContent="center"
            direction="column"
            width="full"
        >
            <SimpleGrid columns={columns} gap={10} mt={5} mx="auto">
                {materials.map((material) => (
                    <Box key={material.slug}>
                        <MaterialCard material={material} />
                    </Box>
                ))}
            </SimpleGrid>
        </Flex>
    )
}
