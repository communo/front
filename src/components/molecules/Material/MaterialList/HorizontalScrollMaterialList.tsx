import { Box, Flex } from '@chakra-ui/react'
import * as React from 'react'
import { MaterialCard } from '../../Cards/MaterialCard/MaterialCard'
import { Props } from './types'

export const HorizontalScrollMaterialList = ({ materials }: Props) => {
    return (
        <Flex
            overflowX="scroll"
            gap={4}
            paddingLeft={{ base: 3, sm: '15%' }}
            scrollSnapType={'x mandatory'}
            css={{
                '&::-webkit-scrollbar': { display: 'none' }, // hide scrollbar
            }}
        >
            <Box
                minWidth={{ base: 3, sm: '15%' }}
                scrollSnapAlign={'start'}
                style={{ visibility: 'hidden' }}
            ></Box>

            {materials.map((material) => (
                <Box
                    key={material.slug}
                    minWidth="300px"
                    overflow={'hidden'}
                    scrollSnapAlign={'start'}
                >
                    <MaterialCard material={material} />
                </Box>
            ))}
        </Flex>
    )
}
