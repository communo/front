import { Button, ButtonProps } from '@components/ui/button'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { BookButton_MaterialFragment } from '@_/graphql/api/generated'

export const BookButton = ({
    material,
    onClick,
    label,
    ...buttonProps
}: {
    material: BookButton_MaterialFragment
    onClick: () => void
    label?: string
} & ButtonProps) => {
    const { t } = useTranslation()
    return (
        <Button
            flex={1}
            width="full"
            fontSize="sm"
            colorPalette="green"
            onClick={onClick}
            _hover={{
                transform: 'translateY(2px)',
                boxShadow: 'lg',
            }}
            {...buttonProps}
        >
            {label ??
                t('pooling.show.button.label', {
                    userName: material.mainOwnership?.ownerLabel,
                })}
        </Button>
    )
}
