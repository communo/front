import React, { Dispatch, SetStateAction } from 'react'
import { Flex, Spinner } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { FaChevronLeft, FaChevronRight } from 'react-icons/fa'
import { PageInfo } from '@_/types'
import { Button } from '@components/ui/button'

type PagerProps = {
    pageInfo: PageInfo
    loading: boolean
    setBeforeCursor: Dispatch<SetStateAction<string | undefined>>
    setAfterCursor: Dispatch<SetStateAction<string | undefined>>
}

export const Pager = ({
    pageInfo,
    loading,
    setBeforeCursor,
    setAfterCursor,
}: PagerProps) => {
    const { t } = useTranslation()

    return (
        <Flex justifyContent="center" gap={5} my={5}>
            <Button
                disabled={loading || !pageInfo.hasPreviousPage}
                onClick={async () => {
                    setBeforeCursor(pageInfo?.startCursor)
                    setAfterCursor(undefined)
                }}
            >
                {t('global.previous')}{' '}
                {(loading && <Spinner />) || <FaChevronLeft />}
            </Button>
            <Button
                disabled={loading || !pageInfo.hasNextPage}
                onClick={async () => {
                    setAfterCursor(pageInfo?.endCursor)
                    setBeforeCursor(undefined)
                }}
            >
                {t('global.next')}{' '}
                {(loading && <Spinner />) || <FaChevronRight />}
            </Button>
        </Flex>
    )
}
