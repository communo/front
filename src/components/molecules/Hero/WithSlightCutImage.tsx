import React, { ReactNode } from 'react'
import {
    Badge,
    Box,
    Heading,
    HStack,
    Image,
    Stack,
    Text,
} from '@chakra-ui/react'
import { HiArrowRight } from 'react-icons/hi'
import rehypeRaw from 'rehype-raw'
import ReactMarkdown from 'react-markdown'
import { useColorModeValue } from '@components/ui/color-mode'

type Props = {
    highlight?: {
        badge: { label: string; color: string }
        label: string
        link: string
    }
    image: { src: string; alt: string; credit?: ReactNode }
    heading: ReactNode
    content: string
    buttons: ReactNode[]
}

export const WithSlightCutImage = ({
    heading,
    content,
    image,
    buttons,
    highlight,
}: Props) => {
    const bg = useColorModeValue('gray.200', 'gray.700')

    return (
        <Box
            as="section"
            bg={useColorModeValue('gray.50', 'gray.800')}
            pb="24"
            pos="relative"
            px={{ base: '6', lg: '12' }}
        >
            <Box maxW="7xl" mx="auto">
                <Box
                    maxW={{ lg: 'md', xl: 'xl' }}
                    pt={{ base: '20', lg: '40' }}
                    pb={{ base: '16', lg: '24' }}
                >
                    {highlight && (
                        <HStack
                            className="group"
                            asChild
                            px="2"
                            py="1"
                            bg={bg}
                            rounded="full"
                            fontSize="sm"
                            mb="8"
                            display="inline-flex"
                            minW="18rem"
                        >
                            <Badge
                                px="2"
                                variant="solid"
                                colorPalette="green"
                                rounded="full"
                                textTransform="capitalize"
                            >
                                New
                            </Badge>
                            {heading}
                            <Box
                                aria-hidden
                                _groupHover={{ transform: 'translateX(2px)' }}
                                as={HiArrowRight}
                                display="inline-block"
                            />
                        </HStack>
                    )}
                    <Heading
                        as="h1"
                        size="6xl"
                        lineHeight="1"
                        fontWeight="extrabold"
                        letterSpacing="tight"
                        textAlign={{ base: 'center', md: 'left' }}
                    >
                        {heading}
                    </Heading>
                    <Text
                        as="section"
                        mt={4}
                        fontSize="xl"
                        fontWeight="medium"
                        color={useColorModeValue('gray.600', 'gray.400')}
                        textAlign={{ base: 'center', md: 'left' }}
                    >
                        <ReactMarkdown rehypePlugins={[rehypeRaw]}>
                            {content}
                        </ReactMarkdown>
                    </Text>
                    <Stack
                        direction={{ base: 'column', sm: 'row' }}
                        gap="4"
                        mt="8"
                    >
                        {buttons.map((button) => button)}
                    </Stack>
                </Box>
            </Box>
            <Box
                pos={{ lg: 'absolute' }}
                insetY={{ lg: '0' }}
                insetEnd={{ lg: '0' }}
                bg="gray.50"
                w={{ base: 'full', lg: '50%' }}
                height={{ base: '96', lg: 'full' }}
                css={{
                    clipPath: {
                        lg: 'polygon(8% 0%, 100% 0%, 100% 100%, 0% 100%)',
                    },
                }}
            >
                <Image
                    height="100%"
                    width="100%"
                    objectFit="cover"
                    src={image.src}
                    alt={image.alt}
                />
                {image.credit && (
                    <Box
                        w="100%"
                        bg="blue"
                        position={{ base: 'inherit', lg: 'absolute' }}
                        mt={{ base: 0, lg: '-20px' }}
                        bgColor={{
                            base: 'yellow.300',
                            lg: 'rgba(250, 240, 137, 0.7)',
                        }}
                        color="blue.700"
                        fontSize="small"
                        textAlign="right"
                        pr={5}
                        fontStyle="italic"
                    >
                        {image.credit}
                    </Box>
                )}
            </Box>
        </Box>
    )
}
