import React from 'react'
import { Box, Heading, Stack } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { useAuth } from '@_/auth/useAuth'
import { PoolingRoutes, UserRoutes } from '@_/routes/_hooks/routes.enum'
import { Button } from '@components/ui/button'
import { Link } from 'react-router'
import { Heart } from '../../atoms/Illustrations/Heart'
import { ArrowButton } from '../../atoms/Button/ArrowButton'
import { RotatingContent } from '../../atoms/RotatingContent/RotatingContent'

export const WithRotatingText = () => {
    const { t } = useTranslation()
    const lines = Array.from({ length: 9 }, (_, i) =>
        t(`home.rotatingText.line2.${i + 1}`)
    )

    const auth = useAuth()

    return (
        <Stack
            as={Box}
            textAlign="center"
            gap={{ base: 8, md: 14 }}
            py={{ base: 20, md: 36 }}
        >
            <Heading
                fontWeight={600}
                fontSize={{ base: '2xl', sm: '4xl', md: '6xl' }}
                lineHeight="110%"
            >
                {t('home.rotatingText.line1')}
                <br />
                <RotatingContent
                    items={lines}
                    timeout={3000}
                    color="green.400"
                />
            </Heading>

            <Heart
                height={{ sm: '24rem', lg: '28rem' }}
                mt={{ base: 12, sm: 16 }}
            />
            <Stack
                direction="column"
                gap={3}
                align="center"
                alignSelf="center"
                position="relative"
            >
                <ArrowButton
                    button={
                        <Button
                            colorPalette="green"
                            bg="green.400"
                            rounded="full"
                            px={6}
                            asChild
                            _hover={{
                                bg: 'green.500',
                            }}
                        >
                            <Link
                                to={
                                    auth.user
                                        ? PoolingRoutes.New
                                        : UserRoutes.Login
                                }
                            >
                                {t('home.rotatingText.getStarted')}
                            </Link>
                        </Button>
                    }
                    incentiveLabel={t('home.rotatingText.buttonIncentive')}
                />
            </Stack>
        </Stack>
    )
}
