import React, { ReactNode } from 'react'
import {
    Box,
    BoxProps,
    Flex,
    Heading,
    Image,
    Stack,
    Text,
} from '@chakra-ui/react'

type Props = BoxProps & {
    backgroundImage: string
    objectPosition: string
    heading: ReactNode
    content: ReactNode
    buttons: ReactNode[]
}

export const WithImageBackground = ({
    backgroundImage,
    heading,
    content,
    buttons,
    objectPosition = 'top bottom',
    ...rest
}: Props) => {
    return (
        <Box
            {...{
                bg: 'gray.800',
                as: 'section',
                minH: '140px',
                position: 'relative',
                ...rest,
            }}
        >
            <Box py="32" position="relative" zIndex={1}>
                <Box
                    maxW={{ base: 'xl', md: '7xl' }}
                    mx="auto"
                    px={{ base: '6', md: '8' }}
                    color="white"
                >
                    <Box maxW="xl">
                        <Heading
                            fontWeight={600}
                            fontSize={{ base: '3xl', sm: '4xl', md: '6xl' }}
                            lineHeight="110%"
                        >
                            {heading}
                        </Heading>
                        <Text fontSize={{ md: '2xl' }} mt="4" maxW="lg">
                            {content}
                        </Text>
                        <Stack
                            direction={{ base: 'column', md: 'row' }}
                            mt="10"
                            gap="4"
                        >
                            {buttons.map((button) => button)}
                        </Stack>
                    </Box>
                </Box>
            </Box>
            <Flex
                id="image-wrapper"
                position="absolute"
                insetX="0"
                insetY="0"
                w="full"
                h="full"
                overflow="hidden"
                align="center"
            >
                <Box position="relative" w="full" h="full">
                    <Image
                        src={backgroundImage}
                        w="full"
                        h="full"
                        objectFit="cover"
                        objectPosition={objectPosition}
                        position="absolute"
                    />
                    <Box
                        position="absolute"
                        w="full"
                        h="full"
                        bg="blackAlpha.600"
                    />
                </Box>
            </Flex>
        </Box>
    )
}
