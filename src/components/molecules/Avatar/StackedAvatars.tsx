import { useBreakpointValue } from '@chakra-ui/react'
import { Avatar, AvatarGroup } from '@components/ui/avatar'

type Props = {
    avatars: Array<{
        name: string
        url?: string
        color?: string
    }>
}
export const StackedAvatars = ({ avatars }: Props) => {
    const size = useBreakpointValue({ base: 'xs', md: 'sm' })

    return (
        <AvatarGroup>
            {avatars.map((avatar) => (
                <Avatar
                    key={avatar.name}
                    name={avatar.name}
                    src={avatar.url}
                    size={size}
                    position={'relative'}
                    zIndex={2}
                    _before={{
                        content: '""',
                        width: 'full',
                        height: 'full',
                        rounded: 'full',
                        transform: 'scale(1.125)',
                        bgColor: avatar.color,
                        position: 'absolute',
                        zIndex: -1,
                        top: 0,
                        left: 0,
                    }}
                />
            ))}
        </AvatarGroup>
    )
}
