import React, { ReactNode } from 'react'
import { Box, Float, IconButton, IconButtonProps } from '@chakra-ui/react'
import { Avatar, AvatarProps } from '@components/ui/avatar'

type AvatarWithCloseProps = {
    onClick: () => void
    icon?: ReactNode
    buttonProps?: IconButtonProps
} & AvatarProps

export const AvatarWithIcon: React.FC<AvatarWithCloseProps> = ({
    src,
    name,
    onClick,
    icon,
    buttonProps,
}) => {
    return (
        <Box position="relative" display="inline-block">
            <Avatar
                name={name}
                borderWidth="1px"
                borderColor="blackAlpha.50"
                src={src}
                css={{
                    '--avatar-size': 'sizes.32',
                    '--avatar-font-size': 'fontSizes.3xl',
                }}
            >
                <Float
                    offset="4"
                    placement="top-end"
                    boxSize="10"
                    layerStyle="fill.solid"
                    rounded="full"
                    borderWidth="4px"
                    borderColor="bg"
                >
                    <IconButton
                        onClick={onClick}
                        rounded="full"
                        {...buttonProps}
                    >
                        {icon}
                    </IconButton>
                </Float>
            </Avatar>
        </Box>
    )
}
