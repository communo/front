import React from 'react'
import { Container, Heading } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Helmet } from 'react-helmet'
import { Denied } from '../../atoms/Illustrations/Denied'

export const Unauthorized403 = () => {
    const { t } = useTranslation()
    return (
        <>
            <Helmet>
                <title>
                    {t('errors.unauthorized.title')} {'< Communo'}
                </title>
            </Helmet>
            <Container maxW="3xl" my={10} textAlign="center">
                <Heading as="h1" mb={20}>
                    {t('errors.unauthorized.heading')}
                </Heading>
                <Denied height="300px" />
            </Container>
        </>
    )
}
