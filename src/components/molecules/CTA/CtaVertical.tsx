import { Box, Heading, Stack, Text } from '@chakra-ui/react'
import React, { Fragment, ReactNode } from 'react'
import { EmphasisText } from '../../atoms/Text/EmphasisText'

interface Props {
    tagline?: string
    title: string
    description: string
    emphasisColor?: string
    buttons: { key: string; component: ReactNode }[]
}

export const CtaVertical = ({
    tagline,
    title,
    description,
    buttons,
    emphasisColor = 'yellow.400',
}: Props) => {
    return (
        <Box as="section">
            <Box
                maxW="3xl"
                mx="auto"
                px={{ base: '6', lg: '8' }}
                py={{ base: '16', sm: '20' }}
                textAlign="center"
            >
                <Stack gap={{ base: '3', md: '4' }}>
                    {tagline && (
                        <Text
                            textStyle={{ base: 'sm', md: 'md' }}
                            fontWeight="medium"
                            color="colorPalette.fg"
                        >
                            {tagline}
                        </Text>
                    )}
                    <Heading
                        my="4"
                        as="h2"
                        fontSize={{ base: '4xl', md: '6xl' }}
                        fontWeight="extrabold"
                        letterSpacing="tight"
                        lineHeight="1.2"
                    >
                        <EmphasisText color={emphasisColor}>
                            {title}
                        </EmphasisText>
                    </Heading>
                </Stack>
                <Box fontSize="lg" maxW="xl" mx="auto">
                    <EmphasisText color={emphasisColor}>
                        {description}
                    </EmphasisText>
                </Box>

                <Stack
                    direction={{ base: 'column', sm: 'row' }}
                    mt="10"
                    justify="center"
                    gap={{ base: '3', md: '5' }}
                    maxW="md"
                    mx="auto"
                >
                    {buttons.map(({ key, component }) => (
                        <Fragment key={key}>{component}</Fragment>
                    ))}
                </Stack>
            </Box>
        </Box>
    )
}
