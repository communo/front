import { Box, Heading, Stack, Text } from '@chakra-ui/react'
import React, { ReactElement } from 'react'
import { useColorModeValue } from '@components/ui/color-mode'
import { EmphasisText } from '../../atoms/Text/EmphasisText'

interface Props {
    title: string
    description: string
    buttons: ReactElement[]
}

export const CtaWithRightButtons = ({ title, description, buttons }: Props) => {
    return (
        <Box as="section" py="12">
            <Stack
                gap="6"
                direction={{ base: 'column', md: 'row' }}
                align={{ md: 'center' }}
                justify="space-between"
                maxW={{ base: 'xl', md: '7xl' }}
                mx="auto"
                px={{ base: '6', md: '8' }}
            >
                <Box>
                    <Text
                        mb="2"
                        fontSize="xl"
                        fontWeight="semibold"
                        color={useColorModeValue('gray.600', 'gray.400')}
                    >
                        <EmphasisText color="yellow.400">{title}</EmphasisText>
                    </Text>
                    <Heading size="xl" fontWeight="extrabold" maxW="20ch">
                        {description}
                    </Heading>
                </Box>
                <Stack
                    direction={{ base: 'column', sm: 'row' }}
                    gap={{ base: '2', sm: '4' }}
                >
                    {buttons.map((button) => button)}
                </Stack>
            </Stack>
        </Box>
    )
}
