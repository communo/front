import React from 'react'
import { Box, Center } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import ReactMarkdown from 'react-markdown'
import rehypeRaw from 'rehype-raw'
import { ConfirmModal__UserFragment } from '@_/graphql/api/generated'
import {
    DialogBackdrop,
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogHeader,
    DialogRoot,
} from '@components/ui/dialog'
import { PrimaryButton } from '../../../../atoms/Button'

type Props = {
    bookingLink: string
    material: {
        mainOwner?: ConfirmModal__UserFragment
    }
    onClose: () => void
    isOpen: boolean
}
export const ApplyConfirmedModal = ({
    bookingLink,
    material,
    onClose,
    isOpen,
}: Props) => {
    const { t } = useTranslation()
    return (
        <DialogRoot onClose={onClose} isOpen={isOpen} placement={'center'}>
            <DialogBackdrop />
            <DialogContent pb={5}>
                <DialogHeader>
                    {t('pooling.show.booking.validate.success.title')}
                </DialogHeader>
                <DialogCloseTrigger />
                <DialogBody>
                    <Box whiteSpace="pre-line">
                        <ReactMarkdown rehypePlugins={[rehypeRaw]}>
                            {t('pooling.show.booking.validate.success.body', {
                                user: material.mainOwner,
                                context: material.mainOwner?.gender,
                            })}
                        </ReactMarkdown>
                        <Center m={5}>
                            <PrimaryButton link={bookingLink}>
                                {t(
                                    'pooling.show.booking.validate.success.follow.label'
                                )}
                            </PrimaryButton>
                        </Center>
                    </Box>
                </DialogBody>
            </DialogContent>
        </DialogRoot>
    )
}
