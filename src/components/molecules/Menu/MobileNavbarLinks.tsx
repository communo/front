import { Link as CLink, Stack, type StackProps } from '@chakra-ui/react'
import {
    NavItemProps,
    NavSubItemProps,
} from '@components/atoms/Menu/NavItemProps'

type MobileNavbarLinksProps = {
    links: (NavSubItemProps | NavItemProps)[]
} & Omit<StackProps, 'separator'>
export const MobileNavbarLinks = ({
    links,
    ...stackProps
}: MobileNavbarLinksProps) => {
    return (
        <Stack
            direction={{ base: 'column', md: 'row' }}
            gap={{ base: '6', md: '8' }}
            {...stackProps}
        >
            {links.map((item) => (
                <CLink
                    key={item.name}
                    fontWeight="medium"
                    color="fg.muted"
                    _hover={{
                        _hover: {
                            color: 'colorPalette.fg',
                            textDecoration: 'none',
                        },
                    }}
                >
                    {item.name}
                </CLink>
            ))}
        </Stack>
    )
}
