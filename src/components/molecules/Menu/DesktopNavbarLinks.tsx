import { Stack, type StackProps, StackSeparator } from '@chakra-ui/react'
import { NavItemProps } from '@components/atoms/Menu/NavItemProps'
import { Link } from 'react-router'
import { Heading } from '@components/atoms/Heading'
import { useTranslation } from 'react-i18next'
import { Button } from '@components/ui/button'

type DesktopNavbarLinksProps = {
    links: NavItemProps[]
} & Omit<StackProps, 'separator'>
export const DesktopNavbarLinks = ({
    links,
    ...stackProps
}: DesktopNavbarLinksProps) => {
    const { t } = useTranslation()
    return (
        <>
            <Heading size="xl" color="fg.muted" textTransform="uppercase">
                {t('menu.title')}
            </Heading>
            <Stack
                as="nav"
                mt={10}
                separator={<StackSeparator />}
                gap={5}
                {...stackProps}
            >
                {links.map(({ icon: Icon, ...item }) => (
                    <Button
                        key={`desktop-nav-item-${item.name}`}
                        fontWeight="medium"
                        variant="ghost"
                        colorPalette="gray"
                        justifyContent="flex-start"
                        asChild
                    >
                        <Link to={item.to}>
                            {Icon && <Icon />} {item.name}
                        </Link>
                    </Button>
                ))}
            </Stack>
        </>
    )
}
