'use client'

import { Button, IconButton, PopoverContext, Stack } from '@chakra-ui/react'
import type { PropsWithChildren } from 'react'
import { LuAlignRight, LuX } from 'react-icons/lu'
import {
    PopoverContent,
    PopoverRoot,
    PopoverTrigger,
} from '@components/ui/popover'
import { UserRoutes } from '@_/routes/_hooks/routes.enum'
import { Link } from 'react-router'
import { useTranslation } from 'react-i18next'

export const MobileNavPopover = ({ children, ...props }: PropsWithChildren) => {
    const { t } = useTranslation()
    return (
        <PopoverRoot
            positioning={{
                placement: 'bottom',
                overflowPadding: 0,
                offset: { mainAxis: 12 },
            }}
        >
            <PopoverContext>
                {(context: any) => (
                    <PopoverTrigger asChild>
                        <IconButton
                            aria-label="Open Menu"
                            variant="ghost"
                            size="sm"
                            colorPalette="gray"
                            hideFrom="md"
                        >
                            {context.open ? <LuX /> : <LuAlignRight />}
                        </IconButton>
                    </PopoverTrigger>
                )}
            </PopoverContext>
            <PopoverContext>
                {({ store }: any) => (
                    <PopoverContent
                        textStyle="md"
                        boxShadow="none"
                        borderRadius="none"
                        maxW="unset"
                        px="4"
                        py="6"
                        width="var(--available-width)"
                        height="var(--available-height)"
                        {...props}
                    >
                        {children}
                        <Stack
                            gap="3"
                            direction={{ base: 'column', md: 'row' }}
                        >
                            <Button
                                variant="outline"
                                colorPalette="gray"
                                asChild
                                onClick={() => {
                                    store.setOpen(false)
                                }}
                            >
                                <Link to={UserRoutes.Login}>
                                    {t('menu.login')}
                                </Link>
                            </Button>

                            <Button
                                asChild
                                onClick={() => {
                                    store.setOpen(false)
                                }}
                            >
                                <Link to={UserRoutes.Register}>
                                    {t('menu.register')}
                                </Link>
                            </Button>
                        </Stack>
                    </PopoverContent>
                )}
            </PopoverContext>
        </PopoverRoot>
    )
}
