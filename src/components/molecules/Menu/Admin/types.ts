export interface AdminNavItem {
    label: string
    disabled?: boolean
    subLabel?: string
    children?: Array<AdminNavItem>
    href?: string
}
