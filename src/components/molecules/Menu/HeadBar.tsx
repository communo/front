import React from 'react'
import {
    Box,
    Container,
    Flex,
    FlexProps,
    HStack,
    Text,
    VStack,
} from '@chakra-ui/react'
import { useColorModeValue } from '@components/ui/color-mode'
import { Link } from 'react-router'
import { FiChevronDown } from 'react-icons/fi'
import { useTranslation } from 'react-i18next'
import { FaPlus } from 'react-icons/fa'
import { useAuth } from '@_/auth/useAuth'
import {
    AdminRoutes,
    CommunitiesRoutes,
    PoolingRoutes,
    StaticPagesRoutes,
    UserRoutes,
} from '@_/routes/_hooks/routes.enum'
import { SearchBar } from '@components/molecules/Menu/SearchBar'
import { Button } from '@components/ui/button'
import {
    MenuContent,
    MenuItem,
    MenuRoot,
    MenuTrigger,
} from '@components/ui/menu'
import { Avatar } from '@components/ui/avatar'
import { MobileNavbarLinks } from '@components/molecules/Menu/MobileNavbarLinks'
import { MobileNavPopover } from '@components/molecules/Menu/MobilePopover'
import { DesktopNavDrawer } from '@components/molecules/Menu/DesktopNavDrawer'
import { DesktopNavbarLinks } from '@components/molecules/Menu/DesktopNavbarLinks'
import { Logo } from '../../atoms/Logo/Logo'
import { NavItemProps } from '../../atoms/Menu/NavItemProps'

interface Props extends FlexProps {
    links: NavItemProps[]
}

export const HeadBar = ({ links, ...rest }: Props) => {
    const { t } = useTranslation()
    const auth = useAuth()
    const bgAccent = useColorModeValue('gray.100', 'gray.800')

    return (
        <>
            <Flex
                px={{ base: 4, md: 4 }}
                height="20"
                zIndex={1000}
                alignItems="center"
                bg={useColorModeValue('white', 'gray.900')}
                borderBottomWidth="1px"
                borderBottomColor={useColorModeValue('gray.200', 'gray.700')}
                justifyContent={{ base: 'space-between' }}
                position="fixed"
                top="0"
                width="100%"
                {...rest}
            >
                <Container maxW="container.xl">
                    <HStack
                        gap={{ base: '0', md: '6' }}
                        justifyContent="space-between"
                    >
                        <DesktopNavDrawer>
                            <DesktopNavbarLinks links={links} />
                        </DesktopNavDrawer>
                        <MobileNavPopover>
                            <MobileNavbarLinks links={links} />
                        </MobileNavPopover>
                        <Link to={StaticPagesRoutes.Home}>
                            <Logo
                                height="65px"
                                display={{ base: 'none', sm: 'block' }}
                            />
                        </Link>

                        <Flex grow={1} display={{ base: 'none', sm: 'block' }}>
                            <SearchBar />
                        </Flex>
                        <Flex alignItems="center">
                            {(auth.user && (
                                <Button
                                    asChild
                                    size="sm"
                                    mr={4}
                                    display={{ base: 'none', sm: 'flex' }}
                                    bgColor="yellow.400"
                                    color="black"
                                    flex={1}
                                    fontWeight={'bold'}
                                    hideBelow="md"
                                    _hover={{
                                        bgGradient: 'to-l',
                                        gradientFrom: 'purple.400',
                                        gradientTo: 'pink.400',
                                        color: 'yellow',
                                        transform: 'translateY(2px)',
                                        boxShadow: 'lg',
                                    }}
                                >
                                    <Link to={PoolingRoutes.New}>
                                        <FaPlus />

                                        {t('layout.head.cta.label')}
                                    </Link>
                                </Button>
                            )) || (
                                <Button
                                    asChild
                                    size="sm"
                                    mr={4}
                                    colorPalette="yellow"
                                    flex={1}
                                    fontSize="sm"
                                    boxShadow="0px 1px 25px -5px rgb(66 153 225 / 48%), 0 10px 10px -5px rgb(66 153 225 / 43%)"
                                >
                                    <Link to={UserRoutes.Login}>
                                        {t('menu.login')}
                                    </Link>
                                </Button>
                            )}
                            {(localStorage.getItem('token') && auth.user && (
                                <Flex alignItems="center">
                                    <MenuRoot variant={'subtle'}>
                                        <MenuTrigger asChild cursor={'pointer'}>
                                            <HStack>
                                                <Avatar
                                                    size="sm"
                                                    src={`${auth.user.avatar?.contentUrl}`}
                                                    aria-label={`avatar ${auth.user.fullname}`}
                                                />
                                                <VStack
                                                    display={{
                                                        base: 'none',
                                                        md: 'flex',
                                                    }}
                                                    alignItems="flex-start"
                                                    gap="1px"
                                                    ml="2"
                                                >
                                                    <Text fontSize="sm">
                                                        {auth.user.firstname}
                                                    </Text>
                                                </VStack>
                                                <Box
                                                    display={{
                                                        base: 'none',
                                                        md: 'flex',
                                                    }}
                                                >
                                                    <FiChevronDown />
                                                </Box>
                                            </HStack>
                                        </MenuTrigger>
                                        <MenuContent bg={bgAccent}>
                                            {auth.user?.roles?.includes(
                                                'ROLE_ADMIN'
                                            ) && (
                                                <MenuItem
                                                    px={5}
                                                    py={2}
                                                    cursor={'pointer'}
                                                    value={AdminRoutes.Home}
                                                    asChild
                                                >
                                                    <Link to={AdminRoutes.Home}>
                                                        {t('menu.admin')}
                                                    </Link>
                                                </MenuItem>
                                            )}
                                            <MenuItem
                                                px={5}
                                                py={2}
                                                cursor={'pointer'}
                                                value={UserRoutes.MyProfile}
                                                asChild
                                            >
                                                <Link to={UserRoutes.MyProfile}>
                                                    {t('menu.my.profile')}
                                                </Link>
                                            </MenuItem>
                                            <MenuItem
                                                px={5}
                                                py={2}
                                                cursor={'pointer'}
                                                value={UserRoutes.MyPreferences}
                                                asChild
                                            >
                                                <Link
                                                    to={
                                                        UserRoutes.MyPreferences
                                                    }
                                                >
                                                    {t('menu.my.preferences')}
                                                </Link>
                                            </MenuItem>
                                            <MenuItem
                                                px={5}
                                                py={2}
                                                cursor={'pointer'}
                                                value={
                                                    PoolingRoutes.MyMaterials
                                                }
                                                asChild
                                            >
                                                <Link
                                                    to={
                                                        PoolingRoutes.MyMaterials
                                                    }
                                                >
                                                    {t('menu.my.materials')}
                                                </Link>
                                            </MenuItem>
                                            <MenuItem
                                                px={5}
                                                py={2}
                                                cursor={'pointer'}
                                                value={
                                                    PoolingRoutes.MyBookingRequests
                                                }
                                                asChild
                                            >
                                                <Link
                                                    to={
                                                        PoolingRoutes.MyBookingRequests
                                                    }
                                                >
                                                    {t('menu.my.bookings')}
                                                </Link>
                                            </MenuItem>
                                            <MenuItem
                                                px={5}
                                                py={2}
                                                cursor={'pointer'}
                                                value={
                                                    CommunitiesRoutes.MyCommunities
                                                }
                                                asChild
                                            >
                                                <Link
                                                    to={
                                                        CommunitiesRoutes.MyCommunities
                                                    }
                                                >
                                                    {t('menu.my.communities')}
                                                </Link>
                                            </MenuItem>
                                            <MenuItem
                                                px={5}
                                                py={2}
                                                cursor={'pointer'}
                                                value={PoolingRoutes.MyAlerts}
                                                asChild
                                            >
                                                <Link
                                                    to={PoolingRoutes.MyAlerts}
                                                >
                                                    {t('menu.my.alerts')}
                                                </Link>
                                            </MenuItem>
                                            <MenuItem
                                                px={5}
                                                py={2}
                                                cursor={'pointer'}
                                                value={UserRoutes.MyLists}
                                                asChild
                                            >
                                                <Link to={UserRoutes.MyLists}>
                                                    {t('menu.my.lists')}
                                                </Link>
                                            </MenuItem>
                                            <MenuItem
                                                px={5}
                                                py={2}
                                                cursor={'pointer'}
                                                value={UserRoutes.Logout}
                                                asChild
                                            >
                                                <Link to={UserRoutes.Logout}>
                                                    {t('menu.logout')}
                                                </Link>
                                            </MenuItem>
                                        </MenuContent>
                                    </MenuRoot>
                                </Flex>
                            )) || (
                                <Box display={{ base: 'none', sm: 'flex' }}>
                                    <Link to={UserRoutes.Register}>
                                        {t('layout.head.register')}
                                    </Link>
                                </Box>
                            )}
                        </Flex>
                    </HStack>
                </Container>
            </Flex>

            <Box display={{ base: 'block', sm: 'none' }} mt={'40px'} px={5} />
        </>
    )
}
