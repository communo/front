import {
    Container,
    Flex,
    Heading,
    HStack,
    Link as CLink,
    Stack,
} from '@chakra-ui/react'
import { ReactNode } from 'react'
import { FaRightFromBracket } from 'react-icons/fa6'

export const PageHeaderWithActions = ({
    tagLine,
    title,
    badges,
    metrics,
    description,
    link,
    actions,
}: {
    tagLine: ReactNode
    title: ReactNode
    badges: ReactNode
    metrics: ReactNode
    description: ReactNode
    link?: string
    actions?: ReactNode
}) => {
    return (
        <Container maxW="6xl" py={{ base: '16', md: '24' }}>
            <Flex
                justify="space-between"
                align="flex-start"
                gap="8"
                direction={{ base: 'column', md: 'row' }}
            >
                <Stack gap="3">
                    <HStack fontWeight="medium" color="fg.muted">
                        {tagLine}
                    </HStack>

                    <HStack>
                        <Heading size="2xl" mr="4">
                            {title}
                        </Heading>
                        {badges}
                    </HStack>

                    <HStack
                        fontFamily="mono"
                        color="fg.muted"
                        textStyle="sm"
                        gap="5"
                        mb="2"
                    >
                        {metrics}
                    </HStack>

                    <HStack color="fg.muted" textStyle="sm" gap="5" mb="2">
                        {description}
                    </HStack>

                    {link && (
                        <CLink href={link} color="colorPalette.fg">
                            <FaRightFromBracket /> {link}
                        </CLink>
                    )}
                </Stack>

                {actions}
            </Flex>
        </Container>
    )
}
