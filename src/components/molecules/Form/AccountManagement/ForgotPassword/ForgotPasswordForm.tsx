import {
    Box,
    Flex,
    Heading,
    Input,
    Spinner,
    Stack,
    Text,
} from '@chakra-ui/react'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { HiShieldCheck } from 'react-icons/hi'
import ReactMarkdown from 'react-markdown'
import rehypeRaw from 'rehype-raw'
import { BiReset } from 'react-icons/bi'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import { randomTrans } from '@utils/random'
import { useColorModeValue } from '@components/ui/color-mode'
import { Field } from '@components/ui/field'
import { PrimaryButton } from '../../../../atoms/Button'

export type ForgotPasswordFormData = {
    email: string
}

export const ForgotPasswordForm = () => {
    const { t, i18n } = useTranslation()
    const {
        handleSubmit,
        register,
        formState: { errors, isSubmitting },
    } = useForm<ForgotPasswordFormData>()

    const onSubmit = (values: ForgotPasswordFormData) => {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Accept-Language': i18n.language,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(values, null, 2),
        }
        fetch(
            `${process.env.REACT_APP_API_URL}/forgot-password/`,
            requestOptions
        ).then(({ status }) => {
            if (status === 204) {
                toast.success('Yeah !')
                localStorage.setItem('email', values.email)
            }
        })
    }

    return (
        <Box
            as="section"
            bg={useColorModeValue('gray.100', 'gray.700')}
            py="12"
        >
            <Box
                textAlign="center"
                bg={useColorModeValue('white', 'gray.800')}
                shadow="lg"
                maxW={{ base: 'xl', md: '3xl' }}
                mx="auto"
                px={{ base: '6', md: '8' }}
                py="12"
                rounded="lg"
            >
                <Box maxW="md" mx="auto">
                    <Text
                        color={useColorModeValue('green.600', 'green.400')}
                        fontWeight="bold"
                        fontSize="sm"
                        letterSpacing="wide"
                    >
                        {randomTrans(t, 'forgotPassword.niceMessage', 3)}️️
                    </Text>
                    <Heading mt="4" fontWeight="extrabold">
                        {t('forgotPassword.title')}
                    </Heading>
                    <Box mt="6">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <Stack>
                                <Field
                                    id="email"
                                    invalid={errors.email !== undefined}
                                >
                                    <Input
                                        type="email"
                                        aria-label={t(
                                            'forgotPassword.email.label'
                                        )}
                                        title={t('forgotPassword.email.label')}
                                        placeholder={randomTrans(
                                            t,
                                            'forgotPassword.email.placeholder',
                                            2
                                        )}
                                        autoComplete="email"
                                        required
                                        {...register('email', {
                                            pattern: /^\S+@\S+$/i,
                                        })}
                                    />
                                </Field>
                                <PrimaryButton
                                    type="submit"
                                    w="full"
                                    disabled={isSubmitting}
                                >
                                    {t('forgotPassword.button')}
                                    {(isSubmitting && <Spinner />) || (
                                        <BiReset />
                                    )}
                                </PrimaryButton>
                            </Stack>
                        </form>
                        <Flex
                            color={useColorModeValue('gray.600', 'gray.400')}
                            fontSize="sm"
                            mt="5"
                        >
                            <Box
                                aria-hidden
                                as={HiShieldCheck}
                                display="inline-block"
                                marginEnd="2"
                                fontSize="lg"
                                color={useColorModeValue(
                                    'green.600',
                                    'green.400'
                                )}
                            />
                            <ReactMarkdown rehypePlugins={[rehypeRaw]}>
                                {t('forgotPassword.passwordManagerSuggestion')}
                            </ReactMarkdown>
                        </Flex>
                    </Box>
                </Box>
            </Box>
        </Box>
    )
}
