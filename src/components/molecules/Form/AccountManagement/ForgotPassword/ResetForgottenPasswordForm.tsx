import { Box, Heading, Spinner, Stack, Text } from '@chakra-ui/react'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router'
import { randomTrans } from '@utils/random'
import { useAuth } from '@_/auth/useAuth'
import { LoginUserPayload, useLoginMutation } from '@_/graphql/api/generated'
import { useColorModeValue } from '@components/ui/color-mode'
import { Field } from '@components/ui/field'
import { PasswordInput } from '@components/ui/password-input'
import { PrimaryButton } from '../../../../atoms/Button'

export type ResetForgottenPasswordFormData = {
    password: string
}

export const ResetForgottenPasswordForm = ({ token }: { token: string }) => {
    const { t, i18n } = useTranslation()
    const {
        handleSubmit,
        register,
        formState: { errors, isSubmitting },
    } = useForm<ResetForgottenPasswordFormData>()
    const navigate = useNavigate()
    const [login] = useLoginMutation()
    const auth = useAuth()

    const onSubmit = (values: ResetForgottenPasswordFormData) => {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Accept-Language': i18n.language,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(values, null, 2),
        }
        fetch(
            `${process.env.REACT_APP_API_URL}/forgot-password/${token}`,
            requestOptions
        )
            .then((response) => {
                if (!response.ok) {
                    throw new Error()
                }
                toast.success(t('forgotPassword.reset.success'))
                if (localStorage.getItem('email')) {
                    login({
                        variables: {
                            username: localStorage.getItem('email') as string,
                            ...values,
                        },
                        onCompleted: (data) => {
                            const { user } = data.loginUser as LoginUserPayload

                            toast.success(
                                randomTrans(t, 'login.toast.success', 4, {
                                    user,
                                })
                            )
                            if (user) {
                                auth.signin(user, () => {
                                    document.body.scrollTop = 0
                                    document.documentElement.scrollTop = 0
                                    navigate('/', { replace: true })
                                })
                            }
                        },
                        onError: () => {
                            toast.error(randomTrans(t, 'login.toast.error', 5))
                        },
                    })
                }
            })
            .catch(() => {
                toast.error('ouch')
            })
    }

    return (
        <Box
            as="section"
            bg={useColorModeValue('gray.100', 'gray.700')}
            py="12"
        >
            <Box
                textAlign="center"
                bg={useColorModeValue('white', 'gray.800')}
                shadow="lg"
                maxW={{ base: 'xl', md: '3xl' }}
                mx="auto"
                px={{ base: '6', md: '8' }}
                py="12"
                rounded="lg"
            >
                <Box maxW="md" mx="auto">
                    <Heading mt="4" fontWeight="extrabold">
                        {t('forgotPassword.reset.title')}
                    </Heading>
                    <Text
                        color={useColorModeValue('green.600', 'green.400')}
                        fontWeight="bold"
                        fontSize="sm"
                        letterSpacing="wide"
                    >
                        {t('forgotPassword.reset.password.suggestion')}️️
                    </Text>
                    <Box mt="6">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <Stack>
                                <Field
                                    id="email"
                                    invalid={errors.password !== undefined}
                                >
                                    <PasswordInput
                                        {...register('password')}
                                        aria-label={t(
                                            'forgotPassword.reset.password.label'
                                        )}
                                        title={t(
                                            'forgotPassword.reset.password.label'
                                        )}
                                        autoComplete="new-password"
                                        required
                                    />
                                </Field>
                                <PrimaryButton
                                    type="submit"
                                    w="full"
                                    disabled={isSubmitting}
                                >
                                    {t('global.save')}
                                    {isSubmitting && <Spinner />}
                                </PrimaryButton>
                            </Stack>
                        </form>
                    </Box>
                </Box>
            </Box>
        </Box>
    )
}
