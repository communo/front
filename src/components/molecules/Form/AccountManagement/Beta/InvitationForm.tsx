import { Flex, Input, Stack } from '@chakra-ui/react'
import * as React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { Field } from '@components/ui/field'
import { Button } from '@components/ui/button'
import { RequiredAsterisk } from '../../../../atoms/Form/RequiredAsterisk'

export type InvitationFormData = {
    email: string
    postalCode: string
}
type Props = {
    onSubmit: (data: InvitationFormData) => void
}

export const InvitationForm = ({ onSubmit }: Props) => {
    const { t } = useTranslation()
    const {
        register,
        handleSubmit,
        formState: { isSubmitting },
    } = useForm<InvitationFormData>()

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <Stack
                direction={{ base: 'column', md: 'row' }}
                align={{ md: 'flex-end' }}
            >
                <Flex direction="column" gap={5}>
                    {t('accountManagement.invitationRequest.content')}
                    <Field
                        label={
                            <>
                                {t(
                                    'accountManagement.invitationRequest.form.email.label'
                                )}
                                &nbsp;
                                <RequiredAsterisk />
                            </>
                        }
                        htmlFor="email"
                    >
                        <Input
                            id="email"
                            {...register('email')}
                            required
                            type="email"
                            size="lg"
                            fontSize="md"
                            placeholder={t(
                                'accountManagement.invitationRequest.form.email.placeholder'
                            )}
                        />
                    </Field>
                    <Field
                        label={
                            <>
                                {t(
                                    'accountManagement.invitationRequest.form.postalCode.label'
                                )}
                                &nbsp;
                                <RequiredAsterisk />
                            </>
                        }
                    >
                        <Input
                            id="postalCode"
                            {...register('postalCode')}
                            required
                            size="lg"
                            fontSize="md"
                            placeholder={t(
                                'accountManagement.invitationRequest.form.postalCode.placeholder'
                            )}
                        />
                    </Field>
                    <Button
                        type="submit"
                        disabled={isSubmitting}
                        size="lg"
                        colorPalette="yellow"
                        fontSize="md"
                        px="10"
                    >
                        {t(
                            'accountManagement.invitationRequest.form.button.label'
                        )}
                    </Button>
                </Flex>
            </Stack>
        </form>
    )
}
