import { useForm } from 'react-hook-form'
import { RequiredAsterisk } from '@components/atoms/Form/RequiredAsterisk'
import { PrimaryButton } from '@components/atoms/Button'
import { Flex, Input } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { AddUserToUserListInput } from '@_/graphql/api/generated'
import { Field } from '@components/ui/field'

type Props = {
    userListId: string
    onSubmit: (input: AddUserToUserListInput) => void
}

type AddUserToListByEmailFormValues = {
    email: string
}

export const AddUserToListByEmail = ({ userListId, onSubmit }: Props) => {
    const {
        handleSubmit,
        register,
        formState: { isSubmitting, errors },
    } = useForm<AddUserToListByEmailFormValues>({})
    const { t } = useTranslation()
    return (
        <form
            onSubmit={handleSubmit((data) => {
                onSubmit({
                    id: userListId,
                    userEmail: data.email,
                })
            })}
        >
            <Flex gap={2} direction={'column'}>
                <Field
                    id="email"
                    invalid={errors.email !== undefined}
                    label={
                        <>
                            {t('pooling.my_lists.form.newUser.email.label')}{' '}
                            <RequiredAsterisk />
                        </>
                    }
                    errorText={errors.email?.message}
                >
                    <Input
                        type="text"
                        w={300}
                        required
                        {...register('email', {
                            required: t('form.required').toString(),
                        })}
                    />
                </Field>
                <Field alignContent={'start'} textAlign="left">
                    <PrimaryButton
                        fontFamily="heading"
                        mt={8}
                        disabled={isSubmitting}
                        type="submit"
                        w={'full'}
                    >
                        {t('global.add')}
                    </PrimaryButton>
                </Field>
            </Flex>
        </form>
    )
}
