import React from 'react'
import { Box, Flex, IconButton, Input, useDisclosure } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { useForm } from 'react-hook-form'
import * as yup from 'yup'
import 'react-phone-number-input/style.css'
import {
    AddUserToUserListInput,
    MyListEditViewDocument,
    MyListEditViewQuery,
    MyListEditViewQueryVariables,
    useMyListEdit_AddFromListMutation,
    UserListAvatarsDocument,
} from '@_/graphql/api/generated'
import { yupResolver } from '@hookform/resolvers/yup'
import { FaPlus, FaUserPlus } from 'react-icons/fa'
import 'yup-phone'
import { AddUserToListByEmail } from '@components/molecules/Form/AccountManagement/UserList/AddUserToListByEmail'
import { toast } from 'react-toastify'
import { H2 } from '@components/atoms/Heading'
import { UserListAvatars } from '@components/molecules/Form/AccountManagement/UserList/UserListAvatars'
import { EmphasisText } from '@components/atoms/Text/EmphasisText'
import { motion } from 'framer-motion'
import {
    DialogBackdrop,
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogHeader,
    DialogRoot,
} from '@components/ui/dialog'
import { Field } from '@components/ui/field'
import { PrimaryButton } from '../../../../atoms/Button'
import { RequiredAsterisk } from '../../../../atoms/Form/RequiredAsterisk'

export type UserListFormValues = {
    name: string
}

export const schema = yup.object().shape({
    name: yup.string().required(),
})

export const EditUserListForm = ({
    onSubmit,
    userList,
    variables,
}: {
    onSubmit: (values: UserListFormValues) => void
    userList?: MyListEditViewQuery['userList']
    variables: MyListEditViewQueryVariables
}) => {
    const { t } = useTranslation()
    const {
        handleSubmit,
        register,
        formState: { isSubmitting, errors },
    } = useForm<UserListFormValues>({
        resolver: yupResolver(schema),
    })
    const { onOpen, onClose, open: isOpen } = useDisclosure()
    const [addUserToList] = useMyListEdit_AddFromListMutation()
    const onAdd = (input: AddUserToUserListInput) => {
        addUserToList({
            variables: {
                input,
            },
            refetchQueries: [
                {
                    query: MyListEditViewDocument,
                    variables,
                },
                {
                    query: UserListAvatarsDocument,
                    variables,
                },
            ],
            onCompleted: () => {
                onClose()
                toast.success(t('pooling.my_lists.form.newUser.toast.success'))
            },
            onError: (apolloError) => {
                apolloError.graphQLErrors.forEach((error) => {
                    if (!error?.extensions?.status) {
                        return
                    }
                    if (error.extensions.status === 404) {
                        toast.error(error.message)
                        return
                    }
                    toast.info(error.message)
                })
            },
        })
    }

    return (
        <Flex gap={10} direction={'column'} alignItems={'center'}>
            <Box>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <Box>
                        <Flex gap={5}>
                            <Field
                                id="name"
                                invalid={errors.name !== undefined}
                                label={
                                    <>
                                        {t('pooling.my_lists.form.name.label')}{' '}
                                        <RequiredAsterisk />
                                    </>
                                }
                                errorText={errors.name?.message}
                            >
                                <Input
                                    type="text"
                                    placeholder={t(
                                        'pooling.my_lists.form.name.placeholder'
                                    )}
                                    w={300}
                                    required
                                    {...register('name', {
                                        value: userList?.name ?? '',
                                        required: t('form.required').toString(),
                                    })}
                                />
                            </Field>
                            <Field alignContent={'start'} textAlign="left">
                                <PrimaryButton
                                    fontFamily="heading"
                                    mt={8}
                                    disabled={isSubmitting}
                                    type="submit"
                                >
                                    {(userList && t('global.save')) ||
                                        t('global.new')}
                                </PrimaryButton>
                            </Field>
                        </Flex>
                    </Box>
                </form>
            </Box>

            <Box>
                <H2 my={10}>
                    {t('pooling.my_lists.form.users.title', {
                        count: userList?.users?.totalCount,
                    })}
                </H2>
                {userList?.id && (
                    <>
                        {(userList?.users?.totalCount! > 0 && (
                            <>
                                <UserListAvatars id={userList?.id} />
                                <IconButton
                                    aria-label={t('global.add')}
                                    onClick={onOpen}
                                >
                                    <FaPlus />
                                </IconButton>
                            </>
                        )) || (
                            <motion.a
                                initial={{ opacity: 0, y: -20 }}
                                animate={{ opacity: 1, y: 0 }}
                                exit={{ opacity: 0, y: -20 }}
                                transition={{ duration: 0.5 }}
                            >
                                <Flex
                                    p={5}
                                    bgColor="blue.400"
                                    color="white"
                                    w={'500px'}
                                    m={'0 auto'}
                                    rounded={'xl'}
                                    boxShadow={'xl'}
                                    alignItems="center"
                                    justifyContent="center"
                                    gap={10}
                                    onClick={onOpen}
                                    cursor="pointer"
                                >
                                    <FaUserPlus size={'40'} />
                                    <EmphasisText color={'yellow.400'}>
                                        {t('pooling.my_lists.form.users.empty')}
                                    </EmphasisText>
                                </Flex>
                            </motion.a>
                        )}

                        <DialogRoot
                            lazyMount
                            open={isOpen}
                            onOpenChange={() => onClose()}
                            placement={'center'}
                        >
                            <DialogBackdrop />
                            <DialogContent>
                                <DialogHeader>
                                    {t(
                                        'pooling.my_lists.form.newUser.modal.title'
                                    )}
                                </DialogHeader>
                                <DialogCloseTrigger />
                                <DialogBody>
                                    <Flex>
                                        <AddUserToListByEmail
                                            userListId={userList.id}
                                            onSubmit={onAdd}
                                        />
                                    </Flex>
                                </DialogBody>
                            </DialogContent>
                        </DialogRoot>
                    </>
                )}
            </Box>
        </Flex>
    )
}
