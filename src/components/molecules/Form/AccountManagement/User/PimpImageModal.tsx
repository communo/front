import React, { useCallback, useEffect, useRef, useState } from 'react'
import Cropper from 'react-cropper'
import 'cropperjs/dist/cropper.css'
import { Button } from '@components/ui/button'
import { useTranslation } from 'react-i18next'
import { FaCropAlt } from 'react-icons/fa'
import { FaDroplet } from 'react-icons/fa6'
import {
    DialogBackdrop,
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogFooter,
    DialogHeader,
    DialogRoot,
} from '@components/ui/dialog'
import { IconButton } from '@chakra-ui/react'
import { useColorModeValue } from '@components/ui/color-mode'
import { BlurSelection } from '../../../../atoms/Image/BlurSelection'

interface ImageCropperModalProps {
    isOpen: boolean
    onUpload: (file: File, closeModal: () => void) => void
    onClose: () => void
    rawFile?: File
    preview?: string
    crop?: boolean
    blur?: boolean
}

export const PimpImageModal: React.FC<ImageCropperModalProps> = ({
    isOpen,
    onClose,
    onUpload,
    rawFile,
    preview,
    crop = true,
    blur = true,
}: ImageCropperModalProps) => {
    const [cropper, setCropper] = useState<Cropper>()
    const [imageSrc, setImageSrc] = useState('')
    const [mode, setMode] = useState<'crop' | 'blur'>(
        (crop && 'crop') || (blur && 'blur') || 'crop'
    )
    const blurCanvasRef = useRef<HTMLCanvasElement>(null)
    const { t } = useTranslation()

    useEffect(() => {
        if (rawFile) {
            const fileReader = new FileReader()
            fileReader.onload = (e) => {
                setImageSrc(e.target?.result as string)
            }
            fileReader.readAsDataURL(rawFile)
        } else if (preview) {
            const image = new Image()
            image.crossOrigin = 'anonymous'
            image.src = preview
            image.onload = () => {
                setImageSrc(image.src)
            }
        }
    }, [rawFile, preview])

    const onCrop = useCallback(() => {
        if (cropper) {
            cropper.getCroppedCanvas().toBlob((blob) => {
                if (blob) {
                    const mimeType = blob.type || 'image/jpeg'
                    const extension = mimeType.split('/')[1] || 'jpeg'
                    const imageFile = new File([blob], `image.${extension}`, {
                        type: mimeType,
                    })
                    onUpload(imageFile, onClose)
                }
            }, 'image/jpeg')
        }
    }, [cropper, onUpload, onClose])

    const onBlur = useCallback(() => {
        if (!blurCanvasRef.current) {
            throw new Error('Canvas ref is not defined')
        }
        blurCanvasRef.current.toBlob((blob) => {
            if (blob) {
                const mimeType = blob.type || 'image/jpeg'
                const extension = mimeType.split('/')[1] || 'jpeg'
                const imageFile = new File([blob], `image.${extension}`, {
                    type: mimeType,
                })
                onUpload(imageFile, onClose)
            }
        }, 'image/jpeg')
        const dataUrl = blurCanvasRef.current.toDataURL('image/jpeg')
        setImageSrc(dataUrl)
    }, [blurCanvasRef, onUpload, onClose])

    const onSave = () => {
        if (mode === 'crop') {
            onCrop()
        } else if (mode === 'blur') {
            onBlur()
        }
    }

    return (
        <DialogRoot
            lazyMount
            open={isOpen}
            onOpenChange={() => onClose()}
            placement={'center'}
        >
            <DialogBackdrop />
            <DialogContent bg={useColorModeValue('white', 'blue.800')}>
                <DialogHeader>
                    {t('media.image.actions.modal.title')}
                </DialogHeader>
                <DialogCloseTrigger />
                <DialogBody>
                    {mode === 'blur' ? (
                        <BlurSelection
                            ref={blurCanvasRef}
                            imageSrc={imageSrc}
                        />
                    ) : (
                        <Cropper
                            src={imageSrc}
                            onInitialized={(instance) => {
                                setCropper(instance)
                            }}
                            crossOrigin="anonymous"
                            checkCrossOrigin={false}
                            guides
                        />
                    )}
                </DialogBody>
                <DialogFooter>
                    {(crop || mode === 'crop') && (
                        <IconButton
                            variant={(mode === 'crop' && 'ghost') || 'solid'}
                            colorPalette="yellow"
                            mr={3}
                            onClick={() => setMode('crop')}
                            aria-label={t('media.image.actions.mode.crop')}
                            title={t('media.image.actions.mode.crop')}
                        >
                            <FaCropAlt />
                        </IconButton>
                    )}
                    {blur && (
                        <IconButton
                            variant={(mode === 'blur' && 'ghost') || 'solid'}
                            colorPalette="yellow"
                            mr={3}
                            onClick={() => setMode('blur')}
                            aria-label={t('media.image.actions.mode.blur')}
                            title={t('media.image.actions.mode.blur')}
                        >
                            <FaDroplet />
                        </IconButton>
                    )}
                    <Button variant="ghost" mr={3} onClick={onClose}>
                        {t('global.cancel')}
                    </Button>
                    <Button colorPalette="yellow" onClick={onSave}>
                        {t('global.save')}
                    </Button>
                </DialogFooter>
            </DialogContent>
        </DialogRoot>
    )
}
