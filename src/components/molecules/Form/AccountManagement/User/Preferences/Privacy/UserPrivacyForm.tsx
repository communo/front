import React from 'react'
import { Flex, HStack } from '@chakra-ui/react'
import { Controller, FormProvider, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { Button } from '@components/ui/button'
import 'react-phone-number-input/style.css'
import 'yup-phone'
import {
    CommunicationMode,
    UserPrivacyForm__UserFragment,
} from '@_/graphql/api/generated'
import { ToggleForm } from '@components/atoms/Form/ToggleForm'
import { UserPreferencesFormValues } from '@components/views/accountManagement/preferences/MyPreferences.types'
import {
    NativeSelectField,
    NativeSelectRoot,
} from '@components/ui/native-select'
import { Field } from '@components/ui/field'

export const UserPrivacyForm = ({
    onSubmit,
    user,
}: {
    onSubmit: (values: UserPreferencesFormValues) => void
    user?: UserPrivacyForm__UserFragment
}) => {
    const { t } = useTranslation()

    const methods = useForm<UserPreferencesFormValues>({
        defaultValues: {
            settings: {
                useAbbreviatedName: user?.settings?.useAbbreviatedName ?? false,
                preferedCommunicationMode:
                    user?.settings?.preferedCommunicationMode ??
                    CommunicationMode.Email,
            },
        },
    })
    const {
        control,
        handleSubmit,
        register,
        formState: { isSubmitting },
    } = methods

    return (
        <FormProvider {...methods}>
            <form onSubmit={handleSubmit(onSubmit)}>
                <Flex direction={'column'} gap={8}>
                    <input
                        type="hidden"
                        {...register('settings.id')}
                        value={user?.settings?.id}
                    />
                    <div>
                        <Controller
                            name="settings.useAbbreviatedName"
                            control={control}
                            render={({ field }) => (
                                <ToggleForm
                                    title={t(
                                        'accountManagement.my.preferences.privacy.form.useAbbreviatedName.label'
                                    )}
                                    description={t(
                                        'register.wizard.preferences.dataPrivacy.abbreviatedName.description'
                                    )}
                                    checked={user?.settings?.useAbbreviatedName}
                                    onCheckedChange={(checked: boolean) =>
                                        field.onChange(checked)
                                    }
                                />
                            )}
                        />
                    </div>

                    <Field
                        label={t(
                            'register.wizard.preferences.dataPrivacy.preferedCommunicationMode.title'
                        )}
                        helperText={t(
                            'register.wizard.preferences.dataPrivacy.preferedCommunicationMode.help'
                        )}
                    >
                        <NativeSelectRoot size="sm">
                            <NativeSelectField
                                {...register(
                                    'settings.preferedCommunicationMode'
                                )}
                                items={[
                                    {
                                        value: CommunicationMode.Email,
                                        label: 'Mail',
                                    },
                                    {
                                        value: CommunicationMode.Phone,
                                        label: 'Téléphone',
                                    },
                                ]}
                            />
                        </NativeSelectRoot>
                    </Field>
                </Flex>
                <HStack mt={10}>
                    <Button
                        fontFamily="heading"
                        mt={8}
                        loading={isSubmitting}
                        type="submit"
                    >
                        {user && t('global.save')}
                    </Button>
                </HStack>
            </form>
        </FormProvider>
    )
}
