export type InvitationFormValues = {
    email: string
    firstname: string
    lastname: string
    message: string
}
