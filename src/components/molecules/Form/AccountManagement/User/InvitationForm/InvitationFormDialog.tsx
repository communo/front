'use client'

import {
    DialogActionTrigger,
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogFooter,
    DialogHeader,
    DialogRoot,
    DialogTitle,
} from '@components/ui/dialog'

import { Button } from '@components/ui/button'
import {
    AlertIndicator,
    AlertRoot,
    AlertTitle,
    FieldLabel,
    FieldRoot,
    Flex,
    Heading,
    Input,
    Stack,
    Text,
    Textarea,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { ClipboardButton, ClipboardRoot } from '@components/ui/clipboard'
import { useForm, useWatch } from 'react-hook-form'
import { useEffect, useState } from 'react'
import { InvitationFormValues } from '@components/molecules/Form/AccountManagement/User/InvitationForm/InvitationFormDialog.types'
import { FaPaperPlane } from 'react-icons/fa'
import {
    FindExistingUserForEmailQuery,
    InvitationFormDialog_CircleFragment,
    InvitationFormDialog_ExistingUserFragment,
    useFindExistingUserForEmailLazyQuery,
    useSendCommunityInvitationMutation,
} from '@_/graphql/api/generated'
import { CommunitiesRoutes, UserRoutes } from '@_/routes/_hooks/routes.enum'
import { toast } from 'react-toastify'
import { useAuth } from '@_/auth/useAuth'
import { useDebounce } from 'react-use'
import { UserAvatar } from '@components/atoms/Image/UserAvatar'

type Props = {
    inviteModalIsOpen: boolean
    setInviteModalOpen: (open: boolean) => void
    community?: InvitationFormDialog_CircleFragment
}

export const InvitationFormDialog = ({
    inviteModalIsOpen,
    setInviteModalOpen,
    community,
}: Props) => {
    const { t } = useTranslation()
    const auth = useAuth()
    const {
        register,
        handleSubmit,
        control,
        setValue,
        formState: { dirtyFields },
    } = useForm<InvitationFormValues>()
    const token = community?.invitationLinks?.[0]?._id
    const [existingUser, setExistingUser] =
        useState<InvitationFormDialog_ExistingUserFragment>()
    const inviteLink = existingUser
        ? `${window.location.origin}${CommunitiesRoutes.MyCommunities}?invitation=${token}`
        : `${window.location.origin}${UserRoutes.Register}?token=${token}`
    const firstname = useWatch({ control, name: 'firstname' })
    const email = useWatch({ control, name: 'email' })

    const [sendCircleInvitation] = useSendCommunityInvitationMutation()
    const [findExistingUserForEmail] = useFindExistingUserForEmailLazyQuery()

    let context = community?.name && 'community'
    context += existingUser ? '' : 'NewUser'

    useEffect(() => {
        if (!dirtyFields.message) {
            setValue(
                'message',
                t('accountManagement.invite.form.message.defaultText', {
                    firstname: existingUser?.firstname || firstname || '',
                    community,
                    context,
                    inviteLink,
                    userFirstname: auth.user!.firstname,
                })
            )
        }
    }, [
        firstname,
        community,
        dirtyFields.message,
        setValue,
        t,
        inviteLink,
        existingUser,
        context,
    ])
    useDebounce(
        () => {
            if (email !== '') {
                findExistingUserForEmail({
                    variables: {
                        email,
                    },
                    onCompleted: (res: FindExistingUserForEmailQuery) => {
                        setExistingUser(res.users?.edges?.[0]?.node)
                    },
                })
            } else {
                setExistingUser(undefined)
            }
        },
        500,
        [email]
    )
    const onSubmit = (data: InvitationFormValues) => {
        sendCircleInvitation({
            variables: {
                input: {
                    ...data,
                    inviteLink,
                    circle: community?.id || undefined,
                },
            },
            onCompleted: () => {
                toast.success(t('accountManagement.invite.toast.success'))
            },
            onError: () => {
                toast.error(t('accountManagement.invite.toast.error'))
            },
        })
    }

    return (
        <DialogRoot
            lazyMount
            open={inviteModalIsOpen}
            onOpenChange={(e: { open: boolean }) => setInviteModalOpen(e.open)}
            placement="center"
        >
            <DialogContent minWidth="3xl">
                <Flex gap={5}>
                    <Stack gap={3} w={'2/3'}>
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <DialogHeader>
                                <DialogTitle>
                                    {t('accountManagement.invite.title')}
                                </DialogTitle>
                            </DialogHeader>
                            <DialogBody
                                px="6"
                                display="flex"
                                flexDirection="column"
                                gap={3}
                            >
                                <FieldRoot>
                                    <FieldLabel>
                                        {t(
                                            'accountManagement.invite.form.email.label'
                                        )}
                                    </FieldLabel>
                                    <Input {...register('email')} />
                                </FieldRoot>
                                {(existingUser === undefined && (
                                    <Flex gap={3}>
                                        <FieldRoot>
                                            <FieldLabel>
                                                {t(
                                                    'accountManagement.invite.form.firstname.label'
                                                )}
                                            </FieldLabel>
                                            <Input {...register('firstname')} />
                                        </FieldRoot>
                                        <FieldRoot>
                                            <FieldLabel>
                                                {t(
                                                    'accountManagement.invite.form.lastname.label'
                                                )}
                                            </FieldLabel>
                                            <Input {...register('lastname')} />
                                        </FieldRoot>
                                    </Flex>
                                )) ||
                                    (existingUser && (
                                        <AlertRoot status={'success'}>
                                            <AlertIndicator />
                                            <AlertTitle
                                                display="flex"
                                                gap={5}
                                                alignItems="center"
                                            >
                                                <UserAvatar
                                                    userId={
                                                        existingUser.id as UserRoutes.IRI
                                                    }
                                                />
                                                {t(
                                                    'accountManagement.invite.form.existingUser',
                                                    {
                                                        firstname:
                                                            existingUser.firstname,
                                                    }
                                                )}
                                            </AlertTitle>
                                        </AlertRoot>
                                    ))}

                                <FieldRoot>
                                    <FieldLabel>
                                        {t(
                                            'accountManagement.invite.form.message.label'
                                        )}
                                    </FieldLabel>
                                    <Textarea
                                        rows={12}
                                        {...register('message')}
                                    />
                                </FieldRoot>
                                <AlertRoot status="info" variant="surface">
                                    <AlertIndicator />
                                    <AlertTitle>
                                        {t(
                                            'accountManagement.invite.form.help'
                                        )}
                                    </AlertTitle>
                                </AlertRoot>
                            </DialogBody>
                            <DialogFooter>
                                <DialogActionTrigger asChild>
                                    <Button
                                        variant="outline"
                                        colorPalette="gray"
                                    >
                                        {t(
                                            'accountManagement.invite.form.cancel'
                                        )}
                                    </Button>
                                </DialogActionTrigger>
                                <Button type={'submit'}>
                                    <FaPaperPlane /> {t('global.send')}
                                </Button>
                            </DialogFooter>
                            <DialogCloseTrigger colorPalette="gray" />
                        </form>
                    </Stack>
                    <Stack
                        gap="2"
                        w={'1/3'}
                        bgGradient="to-tr"
                        gradientFrom="purple.400"
                        gradientTo="pink.400"
                        roundedRight={'4xl'}
                        pt={'120px'}
                        px={5}
                    >
                        <Heading size={'xl'}>
                            {t('accountManagement.invite.shareLink.title')}
                        </Heading>
                        <Text fontSize="sm" color="white">
                            {t('accountManagement.invite.shareLink.help')}
                        </Text>
                        <ClipboardRoot value={inviteLink} cursor={'copy'}>
                            <Flex gap="2">
                                <ClipboardButton
                                    w={'full'}
                                    variant={'solid'}
                                    colorPalette="yellow"
                                />
                            </Flex>
                        </ClipboardRoot>
                    </Stack>
                </Flex>
            </DialogContent>
        </DialogRoot>
    )
}
