import { OwnershipMaterialOption } from '@_/components/atoms/Form/Material/Ownership/OwnershipMaterialForm.types'
import {
    ImagesMaterialForm__MaterialFragment,
    InformationMaterialForm__MaterialFragment,
    MaterialConditionForm__MaterialFragment,
} from '@_/graphql/api/generated'

export type EditMaterialFormFormValues = InformationFormValues &
    MaterialConditionsFormValues & {
        id: string
    }

export type InformationFormValues = {
    name: string
    description?: string
    brand?: string
    model?: string
    mainOwnership: OwnershipMaterialOption
    ownerships?: OwnershipMaterialOption[]
}
export type LocationFormValues = {
    streeAddress: string
    location: {
        latitude: string
        longitude: string
    }
}

export type IRI<
    ResourceName extends string,
    Id extends string | number,
> = `/${ResourceName}/${Id}`

export type MaterialConditionsFormValues = {
    shareScope?: 'internal' | 'public'
    price: number
    userLists: {
        label: string
        value: string
    }[]
}

export enum MaterialFormSteps {
    Informations = 'informations',
    Conditions = 'conditions',
    Medias = 'medias',
}

export type UpdateMaterialFormProps = {
    material: InformationMaterialForm__MaterialFragment &
        ImagesMaterialForm__MaterialFragment &
        MaterialConditionForm__MaterialFragment
    step: MaterialFormSteps
}

export type InformationMaterialFormProps = {
    material?: InformationMaterialForm__MaterialFragment
}

export type ImageMaterialFormProps = {
    material?: ImagesMaterialForm__MaterialFragment
}
export type MaterialConditionFormProps = {
    material?: MaterialConditionForm__MaterialFragment
}
