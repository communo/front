import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import 'react-phone-number-input/style.css'
import { useFormContext } from 'react-hook-form'
import { Loader } from '@components/atoms/Loader/Loader'
import { H3 } from '@components/atoms/Heading'
import { EmphasisText } from '@components/atoms/Text/EmphasisText'
import { useAuth } from '@_/auth/useAuth'
import {
    IRI,
    MaterialConditionFormProps,
    MaterialConditionsFormValues,
} from '@components/molecules/Form/Pooling/Material/types'
import { FaCoins, FaLock, FaThumbsUp } from 'react-icons/fa'
import { AnimatePresence, motion } from 'framer-motion'
import {
    ConditionMaterialForm_UserListQuery,
    useConditionMaterialForm_UserListQuery,
} from '@_/graphql/api/generated'
import { UserListForm } from '@components/molecules/Form/Pooling/Material/ConditionsMaterialForm/UserListForm'
import { NewUserListModal } from '@components/views/pooling/my/lists/NewUserListModal'
import { randomTrans } from '@utils/random'
import { toast } from 'react-toastify'
import { Field } from '@components/ui/field'
import { NumberInputField, NumberInputRoot } from '@components/ui/number-input'
import {
    Box,
    Flex,
    Link as ChakraLink,
    Separator,
    VStack,
} from '@chakra-ui/react'
import { Skeleton } from '@components/ui/skeleton'
import { InputGroup } from '@components/ui/input-group'
import { generatePath, Link } from 'react-router'
import { UserRoutes } from '@_/routes/_hooks/routes.enum'

export const ConditionsMaterialForm = ({
    material,
}: MaterialConditionFormProps) => {
    const { t } = useTranslation()
    const { register, control, setValue, getValues } =
        useFormContext<MaterialConditionsFormValues>()
    const auth = useAuth()
    const [priceValue, setPriceValue] = useState<number>(material?.price!)
    const { data, loading } = useConditionMaterialForm_UserListQuery()
    let userLists: ConditionMaterialForm_UserListQuery['userLists']
    const userListValues = getValues('userLists')

    if (data) {
        userLists = data.userLists
    }

    return (
        (material && (
            <VStack gap={10}>
                <Flex direction={'column'} textAlign={'center'} gap={5}>
                    <H3 display={'flex'} justifyContent={'center'} gap={5}>
                        <FaCoins />
                        <EmphasisText color={'yellow.400'}>
                            {t('pooling.form.communityPricing.title')}
                        </EmphasisText>
                    </H3>
                    <Box>
                        <EmphasisText color="pink.400">
                            {t('pooling.form.communityPricing.description')}
                        </EmphasisText>
                    </Box>
                    <Field w={'fit-content'} m={'0 auto'}>
                        <NumberInputRoot
                            defaultValue="10"
                            width="200px"
                            min={0}
                            onValueChange={(val: { valueAsNumber: number }) => {
                                setPriceValue(val.valueAsNumber)
                            }}
                            value={priceValue}
                            {...register('price', {
                                value: priceValue,
                                valueAsNumber: true,
                                required: t('form.errors.required').toString(),
                            })}
                        >
                            <InputGroup
                                endElement={
                                    <Box marginRight={5}>
                                        {t(
                                            'pooling.form.communityPricing.amount.perDay',
                                            {
                                                currency:
                                                    material.mainOwner?.settings
                                                        ?.currencySymbol ?? '€',
                                            }
                                        )}
                                    </Box>
                                }
                            >
                                <NumberInputField w={200} rounded="sm" />
                            </InputGroup>
                        </NumberInputRoot>
                    </Field>
                    <AnimatePresence>
                        {priceValue === 0 && (
                            <motion.div
                                initial={{ opacity: 0, y: -20 }}
                                animate={{ opacity: 1, y: 0 }}
                                exit={{ opacity: 0, y: -20 }}
                                transition={{ duration: 0.5 }}
                            >
                                <Flex
                                    p={5}
                                    bgColor="green.500"
                                    color="white"
                                    w={'500px'}
                                    m={'0 auto'}
                                    rounded={'xl'}
                                    boxShadow={'xl'}
                                    alignItems="center"
                                    justifyContent="center"
                                    gap={10}
                                >
                                    <FaThumbsUp size={'40'} />
                                    <EmphasisText color={'yellow.400'}>
                                        {t(
                                            'pooling.form.price.freeYouAreAwesome',
                                            {
                                                context: auth.user!.gender,
                                            }
                                        )}
                                    </EmphasisText>
                                </Flex>
                            </motion.div>
                        )}
                    </AnimatePresence>
                </Flex>
                <Separator />
                <Flex direction={'column'} textAlign={'center'} gap={5}>
                    <H3 display={'flex'} justifyContent={'center'} gap={5}>
                        <FaLock />
                        <EmphasisText color={'yellow.400'}>
                            {t('pooling.form.userList.title')}
                        </EmphasisText>
                    </H3>
                    <Box>
                        <EmphasisText color="pink.400">
                            {t('pooling.form.userList.description')}
                        </EmphasisText>
                    </Box>
                    <Field w={'fit-content'} m={'0 auto'}></Field>
                    {loading && <Skeleton w="100" />}
                    {userLists?.edges && (
                        <UserListForm
                            control={control}
                            options={userLists.edges.map((edge) => {
                                const item = edge.node!
                                return {
                                    value: item.id as IRI<'user_lists', string>,
                                    label: item.name,
                                }
                            })}
                            isMulti
                            closeMenuOnSelect
                        />
                    )}
                    <Flex gap={5} justifyContent={'center'}>
                        <ChakraLink asChild fontSize="sm">
                            <Link to={generatePath(UserRoutes.MyLists)}>
                                {t('pooling.form.conditions.manageMyList')}
                            </Link>
                        </ChakraLink>
                        <NewUserListModal
                            afterSubmit={(selectedItem) => {
                                if (!selectedItem) {
                                    return
                                }
                                setValue('userLists', [
                                    ...userListValues,
                                    {
                                        value: selectedItem.id,
                                        label: selectedItem.name,
                                    },
                                ])
                                toast.success(
                                    randomTrans(
                                        t,
                                        'pooling.my_lists.form.new.toast.success',
                                        3
                                    )
                                )
                            }}
                        />
                    </Flex>
                </Flex>
            </VStack>
        )) || <Loader />
    )
}
