import { Control } from 'react-hook-form'
import {
    IRI,
    MaterialConditionsFormValues,
} from '@components/molecules/Form/Pooling/Material/types'
import { SelectRootProps } from '@chakra-ui/react'

type Option = {
    value: IRI<'user_lists', string>
    label: string
}
export type UserListFormProps = {
    control: Control<MaterialConditionsFormValues, string>
} & SelectRootProps<Option>
