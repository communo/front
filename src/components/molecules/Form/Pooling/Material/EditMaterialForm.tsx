import React from 'react'
import { Box, Flex } from '@chakra-ui/react'
import { FormProvider, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { generatePath, useNavigate } from 'react-router'
import { toast } from 'react-toastify'
import { FaImages, FaInfo, FaSave } from 'react-icons/fa'
import { TbCircles } from 'react-icons/tb'
import {
    UpdateMaterialInput,
    useUpdateMaterialMutation,
} from '@_/graphql/api/generated'
import { PoolingRoutes } from '@_/routes/_hooks/routes.enum'
import {
    StepsContent,
    StepsItem,
    StepsList,
    StepsRoot,
} from '@components/ui/steps'
import { useColorModeValue } from '@components/ui/color-mode'
import { InformationMaterialForm } from '@components/molecules/Form/Pooling/Material/InformationMaterialForm/InformationMaterialForm'
import { Button } from '@components/ui/button'
import { ImageMaterialForm } from '@components/molecules/Form/Pooling/Material/ImageMaterialForm/ImageMaterialForm'
import { ConditionsMaterialForm } from '@components/molecules/Form/Pooling/Material/ConditionsMaterialForm/ConditionsMaterialForm'
import {
    StepNextButton,
    StepPreviousButton,
} from '@components/atoms/Button/StepNavigationButton'
import {
    EditMaterialFormFormValues,
    IRI,
    MaterialFormSteps,
    UpdateMaterialFormProps,
} from './types'
import { prepareFormData } from './prepareFormData'

export const EditMaterialForm = ({
    material,
    step,
}: UpdateMaterialFormProps) => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const steps = [
        {
            name: MaterialFormSteps.Informations,
            label: t('pooling.form.informations.title'),
            icon: <FaInfo />,
        },
        {
            name: MaterialFormSteps.Medias,
            label: t('pooling.form.images.title'),
            icon: <FaImages />,
        },
        {
            name: MaterialFormSteps.Conditions,
            label: t('pooling.form.conditions.title'),
            icon: <TbCircles />,
        },
    ]
    const initialStep = steps.findIndex(({ name }) => name === step)

    const defaultValues = {
        ownerships: material.ownerships?.map((e) => ({
            value: (e?.ownerIRI as IRI<'users', string>) || undefined,
            label: e?.ownerLabel || undefined,
        })),
        mainOwnership:
            (material.mainOwnership && {
                value:
                    (material.mainOwnership.ownerIRI as IRI<'users', string>) ||
                    undefined,
                label: material.mainOwnership.ownerLabel,
            }) ||
            undefined,
        userLists:
            material.userLists?.edges?.map((edge) => {
                const node = edge?.node
                return {
                    value: node?.id || undefined,
                    label: node?.name || undefined,
                }
            }) || undefined,
    }

    const methods = useForm<EditMaterialFormFormValues>({
        defaultValues,
    })

    const {
        handleSubmit,
        formState: { isSubmitting },
        register,
    } = methods

    const [updateMaterial] = useUpdateMaterialMutation()

    const onSubmit = async (values: EditMaterialFormFormValues) => {
        const { mainOwnership, ownerships, userLists, ...otherValues } = values
        updateMaterial({
            variables: {
                input: {
                    ...prepareFormData<UpdateMaterialInput>(
                        otherValues,
                        mainOwnership,
                        ownerships,
                        userLists
                    ),
                },
            },
            onCompleted: (data) => {
                if (data?.updateMaterial?.material) {
                    const { slug } = data.updateMaterial.material
                    if (slug && slug !== material.slug) {
                        toast.success(
                            t('pooling.edit.toast.success_with_slug_change')
                        )
                        setTimeout(() => {
                            window.location.href = generatePath(
                                PoolingRoutes.EditMyMaterial,
                                {
                                    slug,
                                }
                            )
                        }, 5000)
                        return
                    }
                    toast.success(t('pooling.edit.toast.success'))
                    navigate(
                        generatePath(PoolingRoutes.Show, {
                            slug: material.slug,
                        })
                    )
                }
            },
        })
    }
    const stepContentBg = useColorModeValue('white', 'gray.700')

    return (
        <Box
            bg={useColorModeValue('white', 'gray.900')}
            p={10}
            mb={10}
            rounded="3xl"
            shadow="full"
            maxW="4xl"
        >
            <StepsRoot defaultStep={initialStep} count={steps.length}>
                <StepsList>
                    {steps.map((_step, index) => (
                        <StepsItem
                            key={`step-item-${index}`}
                            index={index}
                            title={_step.label}
                            icon={_step.icon}
                        />
                    ))}
                </StepsList>

                <FormProvider {...methods}>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <input
                            type="hidden"
                            {...register('id', {
                                value: material.id,
                            })}
                        />
                        {steps.map((_step, index) => (
                            <StepsContent
                                index={index}
                                key={`step-content-${index}`}
                            >
                                <Box
                                    p={8}
                                    bg={stepContentBg}
                                    my={8}
                                    rounded="2xl"
                                >
                                    {index === 0 && (
                                        <InformationMaterialForm
                                            material={material}
                                        />
                                    )}
                                    {index === 1 && (
                                        <ImageMaterialForm
                                            material={material}
                                        />
                                    )}
                                    {index === 2 && (
                                        <ConditionsMaterialForm
                                            material={material}
                                        />
                                    )}
                                </Box>
                                <Flex justifyContent="space-between">
                                    <StepPreviousButton />
                                    {index === steps.length - 1 ? (
                                        <Button
                                            type="submit"
                                            variant="solid"
                                            size="sm"
                                            colorPalette="yellow"
                                            loading={isSubmitting}
                                        >
                                            {t('global.save')}
                                            <FaSave />
                                        </Button>
                                    ) : (
                                        <StepNextButton />
                                    )}
                                </Flex>
                            </StepsContent>
                        ))}
                    </form>
                </FormProvider>
            </StepsRoot>
        </Box>
    )
}
