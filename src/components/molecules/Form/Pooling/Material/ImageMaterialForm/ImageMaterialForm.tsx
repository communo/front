import React, { useCallback } from 'react'
import {
    Box,
    Center,
    Flex,
    HStack,
    Image,
    SimpleGrid,
    useDisclosure,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { useFormContext } from 'react-hook-form'
import { ImageForm } from '@components/atoms/Form/ImageForm'
import { getImageDimensions } from '@utils/getImageDimensions'
import { EmptyList } from '@components/atoms/List/EmptyList'
import {
    UpdateMaterialInput,
    useCreateMaterialImageMutation,
    useDeleteMaterialImageMutation,
    useUpdateMaterialImageMutation,
} from '@_/graphql/api/generated'
import { H2 } from '@components/atoms/Heading'
import { client } from '@_/apollo/main/client'
import { ImageDropzoneForm } from '@components/atoms/Form/ImageDropzoneForm'
import Compressor from 'compressorjs'
import { ImageMaterialFormProps } from '../types'

export const ImageMaterialForm = ({ material }: ImageMaterialFormProps) => {
    const { register } = useFormContext<UpdateMaterialInput>()
    const { onClose } = useDisclosure()
    const { t } = useTranslation()

    const [deleteImage] = useDeleteMaterialImageMutation({
        update() {
            client.resetStore()
        },
    })
    const [createMaterialImage] = useCreateMaterialImageMutation({
        update() {
            client.resetStore()
        },
    })
    const [updateMaterialImage] = useUpdateMaterialImageMutation({
        update() {
            client.resetStore()
        },
    })

    const handleFileChange = useCallback(
        (file: File, imageId?: string, isUpdate: boolean = false) => {
            /* eslint-disable no-new */
            new Compressor(file, {
                quality: 0.8,
                maxWidth: 1024,
                maxHeight: 768,
                success(compressedFile) {
                    getImageDimensions(
                        URL.createObjectURL(compressedFile)
                    ).then((imageDimensions) => {
                        const variables = {
                            imageName: file.name,
                            dimensions: imageDimensions,
                            size: compressedFile.size,
                            file: compressedFile,
                        }
                        if (isUpdate && imageId) {
                            updateMaterialImage({
                                variables: {
                                    id: imageId,
                                    ...variables,
                                },
                            })
                        } else {
                            createMaterialImage({
                                variables: {
                                    material: material!.id,
                                    ...variables,
                                },
                            })
                        }
                        onClose()
                    })
                },
                error(err) {
                    console.error(
                        t('global.error.compression.title'),
                        err.message
                    )
                },
            })
        },
        [createMaterialImage, updateMaterialImage, material, onClose]
    )

    return (
        <Flex direction="column" gap={5}>
            {(!material || !material.images?.edges?.length) && (
                <EmptyList>
                    <>
                        <Box>{t('pooling.form.images.empty.description')}</Box>
                        <H2 colorPalette="blackAlpha">
                            {t('pooling.form.images.empty.title')}
                        </H2>
                        <Center>
                            <HStack wrap={'wrap'}>
                                {[
                                    'thumbnail1.jpg',
                                    'thumbnail2.jpg',
                                    'thumbnail3.jpg',
                                    'thumbnail4.jpg',
                                    'thumbnail5.jpg',
                                    'thumbnail6.jpg',
                                ].map((item) => (
                                    <Image
                                        key={item}
                                        opacity="50%"
                                        h={20}
                                        src={`/illustrations/pooling/material/${item}`}
                                    />
                                ))}
                            </HStack>
                        </Center>
                    </>
                </EmptyList>
            )}
            <SimpleGrid columns={5} gap={10}>
                {material?.images?.edges?.map((edge) => {
                    const { id, contentUrl } = edge.node!
                    return (
                        <div key={`material-image-${id}`}>
                            <ImageForm
                                preview={contentUrl}
                                register={register('images')}
                                onDelete={() =>
                                    deleteImage({ variables: { id } })
                                }
                                onFileChange={(file) =>
                                    handleFileChange(file, id, true)
                                }
                            />
                        </div>
                    )
                })}
            </SimpleGrid>
            <ImageDropzoneForm
                register={register('images')}
                onFileChange={(file) => {
                    handleFileChange(file)
                }}
                maxFiles={5}
                preview={false}
                maxFileSize={1024 * 1024 * 5} // 5Mo
            />
        </Flex>
    )
}
