import {
    Box,
    Flex,
    IconButton,
    Input,
    Textarea,
    VStack,
} from '@chakra-ui/react'
import React from 'react'
import { useTranslation } from 'react-i18next'
import 'react-phone-number-input/style.css'
import { FaInfo } from 'react-icons/fa'
import { useFormContext } from 'react-hook-form'
import { useCircle } from '@_/contexts/Circles/CommunityContext'
import { CircleInformationFormData } from '@components/molecules/Form/Circles/CircleForm/types'
import { Field } from '@components/ui/field'
import { Checkbox } from '@components/ui/checkbox'
import { Tooltip } from '@components/ui/tooltip'
import { RequiredAsterisk } from '../../../../atoms/Form/RequiredAsterisk'

export const InformationForm = () => {
    const { t } = useTranslation()
    const {
        setValue,
        watch,
        register,
        formState: { errors },
    } = useFormContext<CircleInformationFormData>()
    const { community } = useCircle()
    const watchedData = watch()

    return (
        <Flex direction={{ base: 'column', md: 'row' }} w="100%">
            <Box w="100%">
                <VStack pl={0} gap={3} alignItems="flex-start">
                    <Field
                        id="circle-name-formcontrol"
                        invalid={errors.name !== undefined}
                        label={
                            <>
                                {t('communities.form.name.label')}{' '}
                                <RequiredAsterisk />
                            </>
                        }
                        errorText={errors.name?.message}
                    >
                        <Input
                            type="text"
                            required
                            {...register('name', {
                                value: community?.name,
                                required: t('form.required').toString(),
                            })}
                        />
                    </Field>
                    <Field
                        id="circle-description-formcontrol"
                        label={<>{t('communities.form.description.label')} </>}
                        invalid={errors.description}
                        errorText={errors.description?.message}
                    >
                        <Textarea
                            required
                            {...register('description', {
                                value: community?.description,
                                required: t('form.required').toString(),
                            })}
                            rows={10}
                        />
                    </Field>
                    <Field
                        id="circle-website-formcontrol"
                        invalid={errors.website}
                        errotText={errors.website?.message}
                        label={t('communities.form.website.label')}
                    >
                        <Input
                            type="text"
                            {...register('website', {
                                value: community?.website,
                            })}
                        />
                    </Field>
                    {/* <Field */}
                    {/*     id="circle-explicitLocationLabel-formcontrol" */}
                    {/*     invalid={errors.explicitLocationLabel !== undefined} */}
                    {/*     label={t('communities.form.explicitLocationLabel.label')} */}
                    {/* > */}
                    {/*     <Input */}
                    {/*         type="text" */}
                    {/*         placeholder={t( */}
                    {/*             'communities.form.explicitLocationLabel.placeholder' */}
                    {/*         )} */}
                    {/*         {...register('explicitLocationLabel', { */}
                    {/*             value: circle?.explicitLocationLabel, */}
                    {/*         })} */}
                    {/*     /> */}
                    {/* </Field> */}
                    <Field
                        id="circle-indexable-formcontrol"
                        invalid={errors.indexable !== undefined}
                        errorText={errors.indexable?.message}
                    >
                        <Flex gap={5}>
                            <Checkbox
                                onCheckedChange={(e: any) =>
                                    setValue('indexable', !!e.checked)
                                }
                                checked={watchedData.indexable}
                            >
                                {t('communities.form.indexable.label')}
                            </Checkbox>
                            <Tooltip
                                hasArrow
                                showArrow
                                content={t('communities.form.indexable.help')}
                                openDelay={50}
                                closeDelay={100}
                            >
                                <IconButton
                                    size="sm"
                                    rounded="full"
                                    variant="ghost"
                                    color="gray.500"
                                    aria-label={t(
                                        'communities.form.indexable.help'
                                    )}
                                >
                                    <FaInfo />
                                </IconButton>
                            </Tooltip>
                        </Flex>
                    </Field>
                </VStack>
            </Box>
        </Flex>
    )
}
