import { Button } from '@components/ui/button'
import { CircleInformationFormData } from '@components/molecules/Form/Circles/CircleForm/types'
import { FormProvider, useForm } from 'react-hook-form'
import { FaSave } from 'react-icons/fa'
import { useTranslation } from 'react-i18next'
import { InformationForm } from '@components/molecules/Form/Circles/InformationForm/InformationForm'
import { useCircle } from '@_/contexts/Circles/CommunityContext'
import { useUpdateCircleMutation } from '@_/graphql/api/generated'
import { toast } from 'react-toastify'
import { generatePath, useNavigate } from 'react-router'
import { CommunitiesRoutes } from '@_/routes/_hooks/routes.enum'
import { Box } from '@chakra-ui/react'

export const InformationFormBlock = () => {
    const { t } = useTranslation()
    const navigate = useNavigate()
    const { community, setCommunity } = useCircle()
    const methods = useForm<CircleInformationFormData>({
        defaultValues: {
            indexable: community?.indexable,
        },
    })
    const [updateCircle] = useUpdateCircleMutation()
    const onSubmit = async (values: CircleInformationFormData) => {
        if (community) {
            updateCircle({
                variables: {
                    input: {
                        id: community.id,
                        ...values,
                    },
                },
            }).then((r) => {
                if (r.data?.updateCircle?.circle?.slug) {
                    toast.success(t('pooling.edit.toast.success'))
                    setCommunity(r.data?.updateCircle?.circle)
                    if (r.data?.updateCircle?.circle?.slug !== community.slug) {
                        navigate(
                            generatePath(CommunitiesRoutes.Edit, {
                                slug: r.data.updateCircle.circle.slug,
                            })
                        )
                    }
                }

                return

                toast.error(t('circles.edit.toast.error'))
            })
        }
    }

    return (
        <FormProvider {...methods}>
            <Box asChild w="full">
                <form onSubmit={methods.handleSubmit(onSubmit)}>
                    <InformationForm />
                    <Button type="submit" colorPalette="yellow">
                        <FaSave />
                        {t('global.save')}
                    </Button>
                </form>
            </Box>
        </FormProvider>
    )
}
