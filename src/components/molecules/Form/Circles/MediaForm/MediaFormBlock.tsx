import { Button } from '@components/ui/button'
import { CircleMediaFormData } from '@components/molecules/Form/Circles/CircleForm/types'
import { FormProvider, useForm } from 'react-hook-form'
import { FaSave } from 'react-icons/fa'
import { useTranslation } from 'react-i18next'
import { CircleMediaForm } from '@components/molecules/Form/Circles/MediaForm/CircleMediaForm'
import { useCircle } from '@_/contexts/Circles/CommunityContext'
import { useUpdateCircleMutation } from '@_/graphql/api/generated'
import { toast } from 'react-toastify'
import { Box, VStack } from '@chakra-ui/react'
import { base64ToFile } from '@utils/image'

export const MediaFormBlock = () => {
    const methods = useForm<CircleMediaFormData>()
    const { t } = useTranslation()
    const { community, setCommunity } = useCircle()
    const [updateCircle] = useUpdateCircleMutation()

    const onSubmit = async (values: CircleMediaFormData) => {
        const { logo, cover, ...input } = values
        if (community) {
            updateCircle({
                variables: {
                    input: {
                        id: community.id,
                        logo: logo ? base64ToFile(logo) : undefined,
                        cover: cover ? base64ToFile(cover) : undefined,
                        ...input,
                    },
                },
            }).then((r) => {
                if (r.data?.updateCircle?.circle?.slug) {
                    toast.success(t('pooling.edit.toast.success'))
                    setCommunity(r.data?.updateCircle?.circle)
                }

                return

                toast.error(t('circles.edit.toast.error'))
            })
        }
    }

    return (
        <FormProvider {...methods}>
            <Box asChild w="full">
                <form onSubmit={methods.handleSubmit(onSubmit)}>
                    <VStack gap={5} alignItems="flex-start">
                        <CircleMediaForm />
                        <Button type="submit" colorPalette="yellow">
                            <FaSave />
                            {t('global.save')}
                        </Button>
                    </VStack>
                </form>
            </Box>
        </FormProvider>
    )
}
