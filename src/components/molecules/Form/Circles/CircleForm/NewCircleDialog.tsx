import React, { useState } from 'react'
import {
    DialogActionTrigger,
    DialogBackdrop,
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogFooter,
    DialogRoot,
    DialogTrigger,
} from '@components/ui/dialog'
import { Button, Container, Input, Stack } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { RainbowButton } from '@components/atoms/Button'
import { PageHeader } from '@components/organisms/team/PageHeader'
import { useAuth } from '@_/auth/useAuth'
import { useNewCommunityMutation } from '@_/graphql/api/generated'
import { CommunitiesRoutes } from '@_/routes/_hooks/routes.enum'
import { toast } from 'react-toastify'
import { generatePath, Link, useNavigate } from 'react-router'

export const NewCircleDialog = ({ title }: { title?: string }) => {
    const { t } = useTranslation()
    const auth = useAuth()
    const [name, setName] = useState('')
    const [createCommunity, { loading }] = useNewCommunityMutation()
    const navigate = useNavigate()

    const handleCreateCircle = () => {
        createCommunity({
            variables: {
                input: {
                    name,
                    indexable: false,
                },
            },
        }).then((r) => {
            if (r.data?.createCircle?.circle) {
                toast.success(t('communities.new.toast.success'))
                toast.info(
                    <Stack gap={5}>
                        {t('communities.new.toast.pleaseComplete')}
                        <Button asChild variant={'ghost'}>
                            <Link
                                to={generatePath(CommunitiesRoutes.Show, {
                                    slug: r.data!.createCircle!.circle!.slug,
                                })}
                            >
                                {t('communities.new.toast.seeMyCommunity')}
                            </Link>
                        </Button>
                    </Stack>,
                    {
                        autoClose: false,
                    }
                )
                navigate(
                    generatePath(CommunitiesRoutes.Edit, {
                        slug: r.data!.createCircle!.circle!.slug,
                    })
                )
            }
        })
    }

    return (
        <DialogRoot placement="center">
            <DialogBackdrop />
            <DialogTrigger>
                <RainbowButton>
                    {title || t('communities.my_communities.index.actions.new')}
                </RainbowButton>
            </DialogTrigger>
            <DialogContent>
                <DialogBody>
                    <Container py={{ base: '16', md: '24' }}>
                        <PageHeader
                            tagline={t('communities.new.tagLine')}
                            headline={t('communities.new.headLine')}
                            description={t('communities.new.name.help', {
                                context: auth.user?.gender,
                            })}
                            align="center"
                            textAlign="center"
                        >
                            <Input
                                type="text"
                                placeholder={t(
                                    'communities.new.name.placeholder'
                                )}
                                size={{ base: 'lg', md: 'xl' }}
                                onChange={(e) => setName(e.target.value)}
                            />
                        </PageHeader>
                    </Container>
                </DialogBody>

                <DialogFooter>
                    <DialogActionTrigger asChild>
                        <Button variant="outline">{t('global.cancel')}</Button>
                    </DialogActionTrigger>

                    <Button
                        colorPalette="yellow"
                        disabled={name === '' || loading}
                        onClick={handleCreateCircle}
                    >
                        {t('communities.new.submit')}
                    </Button>
                </DialogFooter>
                <DialogCloseTrigger />
            </DialogContent>
        </DialogRoot>
    )
}
