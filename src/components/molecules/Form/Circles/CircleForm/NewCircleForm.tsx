import React, { useMemo } from 'react'
import 'react-phone-number-input/style.css'
import { Box, Flex, HStack, Spinner, useSteps } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { FormProvider, useForm } from 'react-hook-form'
import {
    FaExclamation,
    FaImages,
    FaInfo,
    FaMapMarkerAlt,
    FaSave,
} from 'react-icons/fa'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useCircle } from '@_/contexts/Circles/CommunityContext'
import {
    StepsContent,
    StepsItem,
    StepsList,
    StepsRoot,
} from '@components/ui/steps'
import { useColorModeValue } from '@components/ui/color-mode'
import { EmphasisText } from '@components/atoms/Text/EmphasisText'
import { H2 } from '@components/atoms/Heading'
import { InformationForm } from '../InformationForm/InformationForm'
import { CircleMediaForm } from '../MediaForm/CircleMediaForm'
import { CircleLocationForm } from '../LocationForm/LocationForm'
import { PrimaryButton } from '../../../../atoms/Button'
import { CircleFormData, CircleFormSteps, Props } from './types'
import {
    StepNextButton,
    StepPreviousButton,
} from '../../../../atoms/Button/StepNavigationButton'
import { DeleteCircleForm } from '../DeleteCircleForm'

export const NewCircleForm = ({
    step = CircleFormSteps.Information,
    onSubmit,
    onDelete,
}: Props) => {
    const { t } = useTranslation()
    const steps = useMemo(() => Object.values(CircleFormSteps), [])
    const { community } = useCircle()

    const { activeStep } = useSteps({
        initialStep: steps.indexOf(step),
    })
    const bg = useColorModeValue('white', 'gray.700')

    const schema = yup.object().shape({
        name: yup.string().required(),
        address: yup.string().required(),
    })
    const methods = useForm<CircleFormData>({
        resolver: yupResolver(schema),
    })
    const { handleSubmit, formState } = methods
    const { isSubmitting } = formState

    return (
        <HStack my="10" gap="30px" justify={{ base: 'center' }}>
            <Box
                bg={useColorModeValue('white', 'gray.900')}
                p={10}
                mb={10}
                rounded="xl"
                shadow="full"
                w="4xl"
            >
                <FormProvider {...methods}>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <StepsRoot
                            variant="solid"
                            colorPalette="yellow"
                            errorIcon={FaExclamation}
                            step={activeStep}
                            count={steps.length}
                        >
                            <StepsList>
                                <StepsItem
                                    title={t(
                                        'communities.form.informations.step'
                                    )}
                                    key="informations-form"
                                    icon={<FaInfo />}
                                    index={0}
                                />
                                <StepsItem
                                    title={t('communities.form.location.step')}
                                    key="location-form"
                                    icon={<FaMapMarkerAlt />}
                                    index={1}
                                />
                                <StepsItem
                                    title={t('communities.form.media.step')}
                                    key="media-form"
                                    icon={<FaImages />}
                                    index={2}
                                />
                            </StepsList>
                            <StepsContent index={0}>
                                <Box p={8} bg={bg} my={8} rounded="md">
                                    <InformationForm />
                                </Box>
                                <Flex justifyContent="space-between">
                                    <StepPreviousButton />
                                    <StepNextButton />
                                </Flex>
                            </StepsContent>
                            <StepsContent index={1}>
                                <Box p={8} bg={bg} my={8} rounded="md">
                                    <H2 textAlign="center" my={5}>
                                        <EmphasisText color="yellow.400">
                                            {t(
                                                'communities.form.location.title'
                                            )}
                                        </EmphasisText>{' '}
                                    </H2>
                                    <CircleLocationForm />
                                </Box>
                                <Flex justifyContent="space-between">
                                    <StepPreviousButton />
                                    <StepNextButton />
                                </Flex>
                            </StepsContent>
                            <StepsContent index={2}>
                                <Box p={8} bg={bg} my={8} rounded="md">
                                    <CircleMediaForm />
                                </Box>
                                <Flex
                                    justifyContent="space-between"
                                    alignItems="center"
                                >
                                    <StepPreviousButton />
                                    <Flex gap={5} alignItems="center">
                                        {onDelete && community && (
                                            <DeleteCircleForm
                                                id={community.id}
                                                name={community.name}
                                            />
                                        )}
                                        <PrimaryButton
                                            type="submit"
                                            disabled={isSubmitting}
                                            loading={isSubmitting}
                                        >
                                            {t('global.save')}
                                            {isSubmitting ? (
                                                <Spinner />
                                            ) : (
                                                <FaSave />
                                            )}
                                        </PrimaryButton>
                                    </Flex>
                                </Flex>
                            </StepsContent>
                        </StepsRoot>
                    </form>
                </FormProvider>
            </Box>
        </HStack>
    )
}
