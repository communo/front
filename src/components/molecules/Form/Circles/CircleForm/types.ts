import { CircleForm__CircleFragment } from '@_/graphql/api/generated'
import { Base64File } from '@utils/image'

export type CircleFormData = CircleInformationFormData &
    CircleMediaFormData &
    CircleLocationFormData

export type CircleInformationFormData = {
    name: string
    description?: string
    website?: string
    indexable: boolean
}
export type CircleMediaFormData = {
    logo?: Base64File | null
    removeLogo?: boolean
    cover?: Base64File | null
    removeCover?: boolean
}
export type CircleLocationFormData = {
    explicitLocationLabel: string
    address: string
    latitude: string
    longitude: string
}
export type CircleDeleteFormData = {
    id: string
}

export enum CircleFormSteps {
    Information = 'information',
    Location = 'location',
    Media = 'media',
}

export type Props = {
    step?: CircleFormSteps
    onSubmit: (values: CircleFormData) => Promise<void>
    onDelete?: (circle: CircleForm__CircleFragment) => void
    path?: string
}
