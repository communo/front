import { Box, HStack, Text } from '@chakra-ui/react'
import * as React from 'react'
import { useColorModeValue } from '@components/ui/color-mode'

interface FeatureProps {
    icon: React.ElementType
    title: string
    children: React.ReactNode
}
export const Feature = (props: FeatureProps) => {
    const { icon, title, children } = props
    return (
        <Box>
            <HStack gap="3" color={useColorModeValue('blue.500', 'blue.300')}>
                <Box as={icon} fontSize="xl" />
                <Text fontWeight="extrabold" fontSize="lg">
                    {title}
                </Text>
            </HStack>
            <Text mt="3">{children}</Text>
        </Box>
    )
}
