import React, { useEffect, useRef } from 'react'
import { Flex } from '@chakra-ui/react'
import { useApolloClient } from '@apollo/client'
import { FaTrashAlt } from 'react-icons/fa'
import { toast } from 'react-toastify'
import { useTranslation } from 'react-i18next'
import { useAuth } from '@_/auth/useAuth'
import {
    ChatBubble__MessageFragment,
    useDeleteMessageMutation,
} from '@_/graphql/api/generated'
import { Avatar } from '@components/ui/avatar'
import {
    ReceiverBubble,
    SenderBubble,
    SystemBubble,
} from '../../atoms/Message/ChatBubble'

const AlwaysScrollToBottom = () => {
    const elementRef = useRef<null | HTMLDivElement>(null)
    useEffect(() => elementRef.current?.scrollIntoView())
    return <div ref={elementRef} />
}

export const Messages = ({
    messages,
}: {
    messages: ChatBubble__MessageFragment[]
}) => {
    const { t } = useTranslation()
    const auth = useAuth()
    const client = useApolloClient()

    const [deleteMsg] = useDeleteMessageMutation({
        update() {
            client.resetStore()
        },
    })
    const onDelete = async (message: ChatBubble__MessageFragment) => {
        await deleteMsg({
            variables: {
                id: message.id,
            },
        }).then((r) => {
            if (r.data?.deleteMessage === null) {
                toast.error(t('discussion.message.delete.error'))
                return
            }
            toast.success(t('discussion.message.delete.success'))
        })
    }

    return (
        <Flex
            w="100%"
            maxH="500px"
            overflowY="auto"
            flexDirection="column"
            p="3"
            gap={10}
        >
            {messages.map((item) => {
                if (!item.author) {
                    return (
                        <SystemBubble
                            key={item.id}
                            {...{ item, type: 'system', actions: [] }}
                        />
                    )
                }
                if (item.author.slug === auth.user?.slug) {
                    return (
                        <SenderBubble
                            key={item.id}
                            {...{
                                item,
                                actions: [
                                    <FaTrashAlt
                                        key={`delete-${item.id}`}
                                        onClick={() => onDelete(item)}
                                    />,
                                ],
                            }}
                        />
                    )
                }
                return (
                    <Flex key={item.id}>
                        <Avatar
                            name={item.author.fullname}
                            src={item.author.avatar?.contentUrl}
                            bg="blue.300"
                        />
                        <ReceiverBubble {...{ item, actions: [] }} />
                    </Flex>
                )
            })}
            <AlwaysScrollToBottom />
        </Flex>
    )
}
