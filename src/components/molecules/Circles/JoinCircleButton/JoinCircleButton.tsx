import React, { useState } from 'react'
import { Textarea } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { toast } from 'react-toastify'
import { useApolloClient } from '@apollo/client'
import { FaEnvelope } from 'react-icons/fa'
import { useAuth } from '@_/auth/useAuth'
import {
    DialogActionTrigger,
    DialogBackdrop,
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogFooter,
    DialogHeader,
    DialogRoot,
    DialogTrigger,
} from '@components/ui/dialog'
import { Heading } from '@components/atoms/Heading'
import { useColorModeValue } from '@_/components/ui/color-mode'
import { useAskToJoinCircleButtonMutation } from '@_/graphql/api/generated'
import { PrimaryButton } from '../../../atoms/Button'
import { Props } from './types'

export const JoinCircleButton = ({ circle, ...otherProps }: Props) => {
    const { t } = useTranslation()
    const client = useApolloClient()
    const [askToJoinCircleMembership] = useAskToJoinCircleButtonMutation({
        update() {
            client.resetStore()
        },
    })
    const auth = useAuth()

    const [message, setMessage] = useState(
        t('communities.show.actions.askToJoin.modal.placeholder', {
            name: auth.user?.firstname,
            circle,
            context: auth.user?.gender,
        })
    )

    const join = () => {
        askToJoinCircleMembership({
            variables: {
                input: {
                    circle: circle.id,
                    message,
                },
            },
            onCompleted: () => {
                toast.success(
                    t('communities.show.actions.askToJoin.toast_success')
                )
            },
            onError: (error: Error) => {
                toast.error(
                    t('communities.show.actions.askToJoin.toast_error', {
                        error,
                    })
                )
            },
        })
    }

    return (
        <>
            <DialogRoot placement={'center'}>
                <DialogBackdrop />
                <DialogTrigger asChild>
                    <PrimaryButton {...otherProps}>
                        {t('communities.show.actions.askToJoin.button')}
                    </PrimaryButton>
                </DialogTrigger>
                <DialogContent
                    rounded={'2xl'}
                    bgColor={useColorModeValue('blue.50', 'blue.900')}
                >
                    <DialogHeader>
                        <Heading size={'lg'}>
                            {t(
                                'communities.show.actions.askToJoin.modal.title'
                            )}
                        </Heading>
                    </DialogHeader>
                    <DialogCloseTrigger />
                    <DialogBody>
                        <Textarea
                            rows={10}
                            value={message}
                            onChange={(e) => setMessage(e.target.value)}
                        />
                    </DialogBody>
                    <DialogFooter>
                        <DialogActionTrigger>
                            <PrimaryButton onClick={join}>
                                <FaEnvelope />
                                {t('global.send')}
                            </PrimaryButton>
                        </DialogActionTrigger>
                    </DialogFooter>
                </DialogContent>
            </DialogRoot>
        </>
    )
}
