import {
    Box,
    Flex,
    HStack,
    Link as CLink,
    Separator,
    SimpleGrid,
    Stack,
    Text,
} from '@chakra-ui/react'
import * as React from 'react'
import {
    useExtraFooterLinks,
    useLinks,
    useSocialLinks,
} from '@components/molecules/Footer/_data'
import { LogoBaselined } from '@components/atoms/Logo/LogoBaselined'
import { SiChakraui, SiGraphql, SiPostgresql } from 'react-icons/si'
import { GiHangingSpider } from 'react-icons/gi'
import { FaGitlab, FaSymfony } from 'react-icons/fa'
import { GrReactjs } from 'react-icons/gr'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router'
import { useColorModeValue } from '@components/ui/color-mode'
import { LinkButton } from '@components/ui/link-button'
import { useAuth } from '@_/auth/useAuth'
import { ColorModeAndLanguageFooterBar } from '@components/molecules/Footer/ColorModeAndLanguageFooterBar'

export const Footer = () => {
    const { t } = useTranslation()
    const bgGradient = useColorModeValue(
        {
            bgGradient: 'to-l',
            gradientFrom: 'gray.100',
            gradientTo: 'yellow.50',
        },
        {
            bgGradient: 'to-l',
            gradientFrom: 'gray.800',
            gradientTo: 'gray.900',
        }
    )
    const color = useColorModeValue('gray.800', 'white')
    const logoColor = useColorModeValue([255, 222, 89], [255, 255, 255])
    const auth = useAuth()

    return (
        <Box>
            <Box
                as="footer"
                color={color}
                pt="64px"
                pb="16px"
                boxShadow="dark-lg"
                zIndex={1}
                {...bgGradient}
            >
                <Box maxW="7xl" px="8" mx="auto">
                    <Flex
                        direction={{ base: 'column', lg: 'row' }}
                        justify="space-between"
                        pb="8"
                        align="flex-start"
                        id="top"
                    >
                        <Box paddingEnd="12" mb={{ base: '10', lg: 0 }}>
                            <LogoBaselined rgb={logoColor} h="4em" />
                            <HStack gap="4" mt="8" as="ul">
                                {useSocialLinks().map((link, idx) => (
                                    <LinkButton key={idx} href={link.href}>
                                        <Box srOnly>{link.label}</Box>
                                        {link.icon}
                                    </LinkButton>
                                ))}
                            </HStack>
                        </Box>
                        <SimpleGrid
                            w="full"
                            maxW={{ base: 'unset', lg: '2xl' }}
                            columns={{ base: 2, lg: 4 }}
                            gap={{ base: '8', md: '4' }}
                            fontSize="sm"
                        >
                            {useLinks().map((group, idx) => (
                                <Box key={idx}>
                                    <Text fontWeight="bold" mb="4">
                                        {group.title}
                                    </Text>
                                    <Stack as="ul" listStyleType="none">
                                        {group.links.map((link, index) => (
                                            <Box
                                                as="li"
                                                key={index}
                                                _hover={{
                                                    textDecor: 'underline',
                                                }}
                                            >
                                                <Link to={link.href}>
                                                    {link.label}
                                                    {link.badge && (
                                                        <Box
                                                            as="span"
                                                            marginStart="2"
                                                        >
                                                            {link.badge}
                                                        </Box>
                                                    )}
                                                </Link>
                                            </Box>
                                        ))}
                                    </Stack>
                                </Box>
                            ))}
                        </SimpleGrid>
                    </Flex>
                    <Separator my="10" borderColor="blue.300" />

                    <Box w={['auto', 'auto', 'auto']} my="5" fontStyle="italic">
                        <CLink href="https://gitlab.com/communo">
                            <FaGitlab
                                style={{
                                    display: 'inline',
                                    marginRight: 2,
                                }}
                            />
                            {t('layout.footer.license')}
                        </CLink>
                        {', '}
                        {t('layout.footer.librariesCredit.1')}
                        <CLink href="https://fr.reactjs.org">
                            <GrReactjs
                                style={{
                                    display: 'inline',
                                    marginRight: 2,
                                }}
                            />
                            React
                        </CLink>
                        {', '}
                        <CLink href="https://chakra-ui.com">
                            <SiChakraui
                                style={{
                                    display: 'inline',
                                    marginRight: 2,
                                }}
                            />
                            ChakraUI
                        </CLink>
                        {', '}
                        <CLink href="https://symfony.com">
                            <FaSymfony
                                style={{
                                    display: 'inline',
                                    marginRight: 2,
                                }}
                            />
                            Symfony
                        </CLink>
                        {', '}
                        <CLink href="https://api-platform.com">
                            <GiHangingSpider
                                style={{
                                    display: 'inline',
                                    marginRight: 2,
                                }}
                            />
                            Api-Plaform
                        </CLink>
                        {', '}
                        <CLink href="https://graphql.org">
                            <SiGraphql
                                style={{
                                    display: 'inline',
                                    marginRight: 2,
                                }}
                            />
                            GraphQL
                        </CLink>
                        {', '}
                        <CLink href="https://www.postgresql.org">
                            <SiPostgresql
                                style={{
                                    display: 'inline',
                                    marginRight: 2,
                                }}
                            />
                            Postgres
                        </CLink>{' '}
                        {t('layout.footer.librariesCredit.2')}
                    </Box>
                    <Flex
                        direction={{ base: 'column-reverse', lg: 'row' }}
                        align={{ base: 'flex-start', lg: 'center' }}
                        justify="space-between"
                        fontSize="sm"
                    >
                        <HStack
                            wrap="wrap"
                            id="bottom"
                            gap={{ base: '4', lg: '8' }}
                            mt={{ base: '4', lg: '0' }}
                        >
                            {useExtraFooterLinks().map((link, idx) => (
                                <CLink href={link.href} key={idx}>
                                    {link.label}
                                </CLink>
                            ))}
                        </HStack>
                    </Flex>
                </Box>
            </Box>
            {!auth.user && <ColorModeAndLanguageFooterBar />}
        </Box>
    )
}
export default Footer
