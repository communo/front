import React from 'react'
import { Box, Card, HStack, VStack } from '@chakra-ui/react'
import { generatePath, Link } from 'react-router'
import { CommunityCard_CommunityFragment } from '@_/graphql/api/generated'
import { useColorModeValue } from '@components/ui/color-mode'
import { CommunitiesRoutes } from '@_/routes/_hooks/routes.enum'
import { Heading } from '@_/components/atoms/Heading'
import { UserStackedAvatars } from '@components/molecules/User/UserStackedAvatars'
import { useTranslation } from 'react-i18next'
import { FaMapPin } from 'react-icons/fa'
import { CircleLogo } from '../../User/Circle/CircleLogo'

type CommunityCardProps = {
    circle: CommunityCard_CommunityFragment
    borderRadius?: number
}

export const CommunityCard = ({ circle, borderRadius }: CommunityCardProps) => {
    const { t } = useTranslation()
    const first3Users = circle.memberships?.edges?.map(
        (edge) => edge.node!.user
    )
    const totalCount = circle.memberships?.totalCount
    return (
        <Link
            to={generatePath(CommunitiesRoutes.Show, {
                slug: circle.slug,
            })}
        >
            <Card.Root
                borderRadius={borderRadius}
                p={0}
                rounded={'2xl'}
                bg={useColorModeValue('gray.50', 'gray.800')}
            >
                <Card.Header
                    bgImage={`url('${circle.cover?.contentUrl || '../photos/community-blured.webp'}')`}
                    roundedTop="2xl"
                    bgSize="cover"
                    p={0}
                >
                    <Box
                        bg={useColorModeValue(
                            'whiteAlpha.700',
                            'blackAlpha.700'
                        )}
                        w="100%"
                        roundedTop="2xl"
                        display="flex"
                        flexDirection="row"
                        alignItems="center"
                        gap={5}
                        p={5}
                    >
                        <CircleLogo circle={circle} size="xl" />
                        <Heading size="lg">{circle.name}</Heading>
                    </Box>
                </Card.Header>
                <Card.Body>
                    <VStack p="6" alignItems="flex-start" rounded={'2xl'}>
                        <UserStackedAvatars
                            users={first3Users}
                            totalCount={totalCount}
                            otherLabel={
                                totalCount &&
                                t(
                                    'communities.card.members.avatarStacked.andXOthers',
                                    {
                                        count: totalCount - 3,
                                    }
                                )
                            }
                        />
                        {circle.city && (
                            <HStack>
                                <FaMapPin />
                                {circle.city}
                            </HStack>
                        )}
                    </VStack>
                </Card.Body>
            </Card.Root>
        </Link>
    )
}
