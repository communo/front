import * as React from 'react'
import {
    Badge,
    Box,
    Card,
    Flex,
    HStack,
    Link as ChakraLink,
    Stack,
    Text,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router'
import { useColorModeValue } from '@components/ui/color-mode'
import { Avatar } from '@components/ui/avatar'
import {
    HoverCardArrow,
    HoverCardContent,
    HoverCardRoot,
    HoverCardTrigger,
} from '@components/ui/hover-card'
import { VerticalCardSkeleton } from '@components/ui/skeleton'
import { CircleLogo } from '../../User/Circle/CircleLogo'
import { Props } from './userCard.types'
import { Blaze } from '../../../atoms/Heading/Blaze/Blaze'
import { ContactUserModal } from '../../Modal/Pooling/Material/ContactUserModal'

export const UserCard = ({
    user,
    permission,
    actions,
    ...otherProps
}: Props) => {
    const { t } = useTranslation()
    const colorModeBG = useColorModeValue('gray.50', 'gray.800')
    const userCircleLogos = user?.circleMemberships?.edges?.map((edge) => {
        const { node: circleMembership } = edge!

        return (
            <Link
                key={circleMembership?.id}
                to={`/circle/${circleMembership!.circle.slug}`}
            >
                <CircleLogo
                    key={circleMembership!.circle.slug}
                    circle={circleMembership!.circle}
                />
            </Link>
        )
    })

    return (
        (user && (
            <Card.Root p={5} {...otherProps} bg={colorModeBG}>
                <Card.Body>
                    <Flex direction={'column'} alignItems={'center'} gap="3">
                        <Avatar
                            size="xl"
                            bgColor={'gray.400'}
                            src={`${user.avatar?.contentUrl}`}
                            name={`${user.firstname} ${user.lastname}`}
                            mb={4}
                            pos="relative"
                        />

                        <Box textAlign={'center'}>
                            <ChakraLink asChild cursor={'pointer'}>
                                <Link to={`/user/${user.slug}`}>
                                    <Blaze {...user} permission={permission} />
                                </Link>
                            </ChakraLink>

                            <Text fontWeight={600} color="gray.500">
                                {user.city}
                            </Text>
                        </Box>
                        <Stack
                            align="center"
                            justify="center"
                            direction="row"
                            mt={6}
                        >
                            <Badge
                                px={2}
                                py={1}
                                colorPalette={'yellow'}
                                fontWeight="400"
                            >
                                {t('user.card.sharedItemsNumber', {
                                    count: user.userOwnerships?.totalCount,
                                })}
                            </Badge>
                            <Badge
                                px={2}
                                py={1}
                                colorPalette={'green'}
                                fontWeight="400"
                            >
                                {user.circleMemberships?.edges &&
                                    user.circleMemberships?.edges?.length >
                                        0 && (
                                        <HoverCardRoot
                                            isLazy
                                            placement="bottom"
                                            openDelay={0}
                                            closeDelay={50}
                                        >
                                            <HoverCardTrigger>
                                                {t(
                                                    'user.card.communitiesNumber',
                                                    {
                                                        count: user
                                                            .circleMemberships
                                                            ?.totalCount,
                                                    }
                                                )}
                                            </HoverCardTrigger>
                                            <HoverCardContent>
                                                <HoverCardArrow />
                                                <HStack
                                                    gap="1rem"
                                                    justify="center"
                                                >
                                                    {userCircleLogos}
                                                </HStack>
                                            </HoverCardContent>
                                        </HoverCardRoot>
                                    )}
                            </Badge>
                        </Stack>

                        <Stack m={8} direction="row" gap={4}>
                            {actions || (
                                <>
                                    <ContactUserModal
                                        title={t('user.card.contact')}
                                        user={user}
                                    />
                                </>
                            )}
                        </Stack>
                    </Flex>
                </Card.Body>
            </Card.Root>
        )) || <VerticalCardSkeleton />
    )
}
