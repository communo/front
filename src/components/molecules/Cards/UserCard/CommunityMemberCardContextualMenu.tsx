import { ThreeDotsMenu } from '@components/atoms/Menu/ThreeDotsMenu'
import {
    CommunityMemberCardFragment,
    useChangeMembershipPermissionButtonMutation,
} from '@_/graphql/api/generated'
import { Group } from '@chakra-ui/react'
import { toast } from 'react-toastify'
import { useTranslation } from 'react-i18next'
import { Button } from '@components/ui/button'
import {
    DialogActionTrigger,
    DialogBody,
    DialogCloseTrigger,
    DialogContent,
    DialogFooter,
    DialogHeader,
    DialogRoot,
} from '@components/ui/dialog'
import { useState } from 'react'
import { useColorModeValue } from '@components/ui/color-mode'
import './CommunityMemberCardContextualMenu.css'
import { Heading } from '@components/atoms/Heading'

export const CommunityMemberCardContextualMenu = ({
    membership,
}: {
    membership: CommunityMemberCardFragment
}) => {
    const { t } = useTranslation()
    const [changePermission] = useChangeMembershipPermissionButtonMutation()
    const [action, setAction] = useState<string>()
    const [isOpen, setOpen] = useState(false)
    const onAction = (id: string, transition: string) => {
        const variables = { id, transition }
        changePermission({ variables }).then((r) => {
            const data =
                r.data?.changePermissionCircleMembership?.circleMembership
            if (data === null) {
                toast.success(
                    t(
                        `communities.show.users.updatePermission.toast_${transition}`,
                        {
                            context: 'error',
                        }
                    )
                )
                return
            }
            toast.success(
                t(
                    `communities.show.users.updatePermission.toast_${transition}`,
                    {
                        context: membership?.user.gender,
                        name: membership?.user.firstname,
                    }
                )
            )
            setOpen(false)
        })
    }
    const permissionActions =
        (!['pending', 'invited'].includes(membership.status) &&
            membership.permissionTransitionAvailables?.map(
                (transition: string) => {
                    const component = (
                        <Button
                            onClick={() => {
                                setAction(transition)
                                setOpen(true)
                            }}
                            variant="ghost"
                        >
                            {t('communities.show.users.actions', {
                                context: transition,
                            })}
                        </Button>
                    )
                    return {
                        key: `btn-permission-${transition}-${membership.id}`,
                        component,
                    }
                }
            )) ||
        []
    const buttons = [...permissionActions]
    const dialogContentProps = {
        bg: useColorModeValue('blue.50', 'blue.900'),
    }
    return (
        buttons.length > 0 && (
            <>
                <ThreeDotsMenu ariaLabel="Actions" buttons={buttons} />
                {action && (
                    <DialogRoot
                        lazyMount
                        open={isOpen}
                        onOpenChange={(e: { open: boolean }) => setOpen(e.open)}
                        placement="center"
                        closeOnEscape={true}
                        style={{ '--chakra-z-index-modal': 1600 }}
                    >
                        <DialogContent {...dialogContentProps}>
                            <DialogHeader>
                                <Heading size="lg" fontWeight="bold">
                                    {t(
                                        `communities.show.users.actionName_${action}`
                                    )}
                                </Heading>
                            </DialogHeader>
                            <DialogCloseTrigger />
                            <DialogBody>
                                {t(
                                    `communities.show.users.actionDescription_${action}`,
                                    {
                                        context: membership.user.gender,
                                        name: membership.user.firstname,
                                    }
                                )}
                            </DialogBody>
                            <DialogFooter>
                                <Group>
                                    <DialogActionTrigger asChild>
                                        <Button variant="outline">
                                            {t('global.cancel')}
                                        </Button>
                                    </DialogActionTrigger>
                                    <Button
                                        onClick={() =>
                                            onAction(membership.id, action)
                                        }
                                    >
                                        {t('global.continue')}
                                    </Button>
                                </Group>
                            </DialogFooter>
                        </DialogContent>
                    </DialogRoot>
                )}
            </>
        )
    )
}
