import * as React from 'react'
import {
    Badge,
    Box,
    Card,
    CardRootProps,
    Flex,
    Group,
    Link as ChakraLink,
    Stack,
    Text,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { generatePath, Link } from 'react-router'
import { useColorModeValue } from '@components/ui/color-mode'
import { Avatar } from '@components/ui/avatar'
import { HoverCardRoot, HoverCardTrigger } from '@components/ui/hover-card'
import { VerticalCardSkeleton } from '@components/ui/skeleton'
import { CommunityMemberCardFragment } from '@_/graphql/api/generated'
import { ChangeMembershipStatusButton } from '@components/molecules/Circles/members/ChangeMembershipStatusButton'
import { UserRoutes } from '@_/routes/_hooks/routes.enum'
import { CommunityMemberCardContextualMenu } from '@components/molecules/Cards/UserCard/CommunityMemberCardContextualMenu'
import { Blaze } from '../../../atoms/Heading/Blaze/Blaze'

type Props = {
    member: CommunityMemberCardFragment
    permission?: string
    borderRadius?: number
} & CardRootProps

export const CommunityMemberCard = ({
    member,
    permission,
    ...otherProps
}: Props) => {
    const { t } = useTranslation()
    const colorModeBG = useColorModeValue('gray.50', 'gray.800')
    const { user } = member

    return (
        (user && (
            <Card.Root p={5} {...otherProps} bg={colorModeBG}>
                <Card.Body>
                    <Box right={5} top={5} position="absolute">
                        <CommunityMemberCardContextualMenu
                            membership={member}
                        />
                    </Box>
                    <Flex direction={'column'} alignItems={'center'} gap="3">
                        <Avatar
                            size="xl"
                            bgColor={'gray.400'}
                            src={`${user.avatar?.contentUrl}`}
                            name={`${user.firstname} ${user.lastname}`}
                            mb={4}
                            pos="relative"
                        />

                        <Box textAlign={'center'}>
                            <ChakraLink asChild cursor={'pointer'}>
                                <Link
                                    to={generatePath(UserRoutes.Profile, {
                                        slug: user.slug,
                                    })}
                                >
                                    <Blaze
                                        {...user}
                                        permission={
                                            permission || member.permission
                                        }
                                    />
                                </Link>
                            </ChakraLink>
                            <Text fontWeight={600} color="gray.500">
                                {user.city}
                            </Text>
                        </Box>
                        <Stack
                            align="center"
                            justify="center"
                            direction="row"
                            mt={6}
                        >
                            <Badge
                                px={2}
                                py={1}
                                colorPalette={'yellow'}
                                fontWeight="400"
                            >
                                {t('user.card.sharedItemsNumber', {
                                    count: user.userOwnerships?.totalCount,
                                })}
                            </Badge>
                            <Badge
                                px={2}
                                py={1}
                                colorPalette={'green'}
                                fontWeight="400"
                            >
                                {user.circleMemberships?.edges &&
                                    user.circleMemberships?.edges?.length >
                                        0 && (
                                        <HoverCardRoot
                                            isLazy
                                            placement="bottom"
                                            openDelay={0}
                                            closeDelay={50}
                                        >
                                            <HoverCardTrigger>
                                                {t(
                                                    'user.card.communitiesNumber',
                                                    {
                                                        count: user
                                                            .circleMemberships
                                                            ?.totalCount,
                                                    }
                                                )}
                                            </HoverCardTrigger>
                                        </HoverCardRoot>
                                    )}
                            </Badge>
                        </Stack>

                        <Stack m={8} direction="row" gap={4}>
                            {member.statusTransitionAvailables && (
                                <Group alignSelf="center">
                                    {member.statusTransitionAvailables.map(
                                        (action: string) => (
                                            <ChangeMembershipStatusButton
                                                key={`change-membership-status-button${action}${member.id}`}
                                                circleMembership={member}
                                                action={action}
                                            />
                                        )
                                    )}
                                </Group>
                            )}
                        </Stack>
                    </Flex>
                </Card.Body>
            </Card.Root>
        )) || <VerticalCardSkeleton />
    )
}
