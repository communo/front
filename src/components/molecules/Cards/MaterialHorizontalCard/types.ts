import { MaterialHorizontalCard__MaterialFragment } from '@_/graphql/api/generated'

export type Material = MaterialHorizontalCard__MaterialFragment

export type MaterialHorizontalCardProps = {
    material: Material
}
