import React, { ReactNode } from 'react'
import {
    Badge,
    Box,
    Card,
    Flex,
    Heading,
    HStack,
    Spacer,
    Text,
} from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import { SearchAlertFragment } from '@_/graphql/api/generated'
import { useColorModeValue } from '@components/ui/color-mode'
import { LuMail } from 'react-icons/lu'

type SearchAlertCardProps = {
    alert: SearchAlertFragment
    button: ReactNode
}

export const SearchAlertCard = ({ alert, button }: SearchAlertCardProps) => {
    const { t } = useTranslation()
    const colorModeBG = useColorModeValue('gray.50', 'gray.800')

    return (
        <Card.Root color={'black'} bg={colorModeBG} p={5} rounded={'2xl'}>
            <Card.Body>
                <Flex align={'center'} gap={5}>
                    <Box textAlign={'left'}>
                        <Heading
                            color={useColorModeValue('gray.700', 'gray.200')}
                        >
                            <HStack gap={2}>
                                {t('pooling.searchAlerts.item.type.mail')}
                                <Badge
                                    size="sm"
                                    variant="solid"
                                    colorPalette="yellow"
                                >
                                    <LuMail />
                                </Badge>
                            </HStack>
                        </Heading>
                        <Text fontWeight={'bold'} color={'pink.400'}>
                            "{alert?.name}"
                        </Text>
                    </Box>
                    <Spacer />
                    {button}
                </Flex>
            </Card.Body>
        </Card.Root>
    )
}
