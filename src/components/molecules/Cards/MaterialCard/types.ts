import { CardRootProps } from '@chakra-ui/react'
import {
    MaterialCard__MaterialFragment,
    MaterialOwnerCard_CircleFragment,
    MaterialOwnerCard_UserFragment,
} from '@_/graphql/api/generated'

export type MaterialCardProps = {
    material: MaterialCard__MaterialFragment
    disabled?: boolean
} & CardRootProps

export type MaterialCardOwnerInfo = {
    name: string
    title: string
    city?: string
    picture?: string
}

// guard
export const isMainOwnerUser = (obj: {
    id: string
}): obj is MaterialOwnerCard_UserFragment => obj.id.startsWith('/users/')
export const isMainOwnerCircle = (obj: {
    id: string
}): obj is MaterialOwnerCard_CircleFragment => obj.id.startsWith('/circles/')
