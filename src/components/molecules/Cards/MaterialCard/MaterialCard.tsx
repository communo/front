import {
    Box,
    Card,
    Center,
    Heading,
    HStack,
    Image,
    Stack,
    Text,
} from '@chakra-ui/react'
import { generatePath, Link } from 'react-router'
import { useTranslation } from 'react-i18next'
import * as React from 'react'
import { litePriceText } from '@utils/priceText'
import { useAuth } from '@_/auth/useAuth'
import { materialmainPicture } from '@utils/image'
import { FluidCard } from '@components/atoms/Animation/FluidCard'
import { formatDistance } from '@utils/formatDistance'
import { useColorModeValue } from '@components/ui/color-mode'
import { Avatar } from '@components/ui/avatar'
import { Tag } from '@components/ui/tag'
import { PoolingRoutes } from '@_/routes/_hooks/routes.enum'
import { MaterialCardProps } from './types'

export const MaterialCard = ({
    material,
    disabled = false,
    ...otherProps
}: MaterialCardProps) => {
    const {
        name,
        brand,
        model,
        slug,
        mainOwnershipUser,
        mainOwnershipCircle,
        bestPriceForUser,
    } = material
    const { t } = useTranslation()
    const auth = useAuth()
    const cardBg = useColorModeValue('white.100', 'gray.800')

    const picture = materialmainPicture(material)
    const avatarBg = useColorModeValue('white', 'gray.700')

    let ownerInfo
    if (mainOwnershipUser) {
        ownerInfo = {
            name: `${mainOwnershipUser.fullname}`,
            title: `${mainOwnershipUser.firstname}`,
            picture: mainOwnershipUser.avatar?.contentUrl,
            city: mainOwnershipUser.city,
        }
    }
    if (mainOwnershipCircle) {
        ownerInfo = {
            name: `${mainOwnershipCircle.name}`,
            title: `${mainOwnershipCircle.name}`,
            picture: mainOwnershipCircle.logo?.contentUrl,
            city: mainOwnershipCircle.city,
        }
    }
    let distance
    if (material.distance) {
        distance = formatDistance(material.distance, { precision: 2 })
    }

    return (
        <Center py={12} key={`materialCard${slug}`}>
            <Link
                to={disabled ? '#' : generatePath(PoolingRoutes.Show, { slug })}
                title={t('pooling.index.see', { material })}
            >
                <FluidCard isDisabled={disabled}>
                    <Card.Root
                        {...otherProps}
                        cursor={disabled ? 'default' : 'pointer'}
                        role="group"
                        p={6}
                        maxW="330px"
                        bg={cardBg}
                        boxShadow="2xl"
                        zIndex={1}
                    >
                        <Card.Body
                            rounded="lg"
                            mt={-12}
                            pos="relative"
                            height="230px"
                            bg="white"
                            p={0}
                        >
                            <Image
                                rounded="lg"
                                height={230}
                                width={282}
                                objectFit="cover"
                                src={picture}
                                bgGradient="linear(to-b, yellow.400, yellow.200, yellow.400)"
                                boxShadow="lg"
                            />
                            {ownerInfo && (
                                <Box>
                                    <Avatar
                                        w={90}
                                        h={90}
                                        src={`${ownerInfo.picture}`}
                                        bgColor={avatarBg}
                                        title={`${ownerInfo.title}`}
                                        name={`${ownerInfo.name}`}
                                        pos="absolute"
                                        top="160px"
                                        ml="93px"
                                        _before={{
                                            content: '""',
                                            width: 'full',
                                            height: 'full',
                                            rounded: 'full',
                                            transform: 'scale(1.125)',
                                            bg: 'yellow.400',
                                            position: 'absolute',
                                            zIndex: -1,
                                            top: 0,
                                            right: 0,
                                        }}
                                    />
                                </Box>
                            )}
                        </Card.Body>
                        <Card.Footer>
                            <Stack pt={10} align="center">
                                <Heading
                                    fontSize="lg"
                                    fontFamily="body"
                                    fontWeight={500}
                                    lineClamp={2}
                                >
                                    <>
                                        <Text
                                            mr={3}
                                            fontSize="xs"
                                            fontStyle="italic"
                                        >
                                            {brand} {model}
                                        </Text>
                                        {name}
                                    </>
                                </Heading>
                                <HStack gap={4}>
                                    {bestPriceForUser !== undefined && (
                                        <Tag
                                            size="lg"
                                            variant="solid"
                                            bg={
                                                bestPriceForUser === 0
                                                    ? 'yellow.300'
                                                    : 'gray.100'
                                            }
                                            color="black"
                                        >
                                            {litePriceText(
                                                bestPriceForUser,
                                                auth.user,
                                                t
                                            )}
                                        </Tag>
                                    )}
                                    {ownerInfo && (
                                        <Tag
                                            size="lg"
                                            variant="solid"
                                            bg="teal.200"
                                            title={
                                                (distance &&
                                                    t(
                                                        'global.distance',
                                                        distance
                                                    )) ||
                                                undefined
                                            }
                                        >
                                            {ownerInfo.city}{' '}
                                        </Tag>
                                    )}
                                </HStack>
                            </Stack>
                        </Card.Footer>
                    </Card.Root>
                </FluidCard>
            </Link>
        </Center>
    )
}
