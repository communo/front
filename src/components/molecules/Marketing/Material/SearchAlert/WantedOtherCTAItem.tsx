import { Heading } from '@components/atoms/Heading'
import { Flex, LinkBox, LinkOverlay, Text } from '@chakra-ui/react'
import { motion, useAnimation } from 'framer-motion'
import { useInView } from 'react-intersection-observer'
import { MouseEvent, ReactNode, useEffect } from 'react'

const MotionLinkBox = motion(LinkBox)

export const WantedOtherCTAItem = ({
    title,
    description,
    link,
    onClick,
    button,
}: {
    title: string
    description: string
    link?: string
    onClick?: (e: MouseEvent) => void
    button?: ReactNode
}) => {
    const controls = useAnimation()
    const [ref, inView] = useInView({
        triggerOnce: true,
        threshold: 0.1,
    })

    useEffect(() => {
        if (inView) {
            controls.start('visible')
        }
    }, [controls, inView])

    const cardVariants = {
        hidden: { opacity: 0, y: 20 },
        visible: { opacity: 1, y: 0 },
        hover: { scale: 1.05, boxShadow: '0px 4px 15px rgba(0, 0, 0, 0.2)' },
    }

    return (
        <MotionLinkBox
            as="article"
            bg="yellow.400"
            shadow={{ sm: 'base' }}
            rounded={{ sm: 'md' }}
            overflow="hidden"
            transition={{ all: '0.2s' }}
            ref={ref}
            initial="hidden"
            animate={controls}
            whileHover="hover"
            variants={cardVariants}
            onClick={onClick}
            cursor="pointer"
            height="100%"
        >
            <Flex
                direction="column"
                align="center"
                justify="center"
                p="6"
                height="100%"
            >
                <Heading
                    as="h3"
                    size="lg"
                    mb="4"
                    lineHeight="base"
                    color="black"
                >
                    <LinkOverlay href={link}>{title}</LinkOverlay>
                </Heading>
                <Text
                    lineClamp={2}
                    mb="8"
                    fontSize="md"
                    color="black"
                    textAlign="center"
                >
                    {description}
                </Text>
                {button}
            </Flex>
        </MotionLinkBox>
    )
}
