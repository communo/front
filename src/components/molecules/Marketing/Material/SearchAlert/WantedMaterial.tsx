import { Heading } from '@components/atoms/Heading'
import { Box, Container, SimpleGrid } from '@chakra-ui/react'
import { MouseEvent } from 'react'
import { t } from 'i18next'
import { PrimaryButton } from '@components/atoms/Button'
import { EmphasisText } from '@components/atoms/Text/EmphasisText'
import { useAuth } from '@_/auth/useAuth'
import { toast } from 'react-toastify'
import { PoolingRoutes, UserRoutes } from '@_/routes/_hooks/routes.enum'
import { Link, useNavigate } from 'react-router'
import { WantedOtherCTAItem } from '@components/molecules/Marketing/Material/SearchAlert/WantedOtherCTAItem'
import { WantedItem } from '@components/molecules/Marketing/Material/SearchAlert/WantedItem'
import { useColorModeValue } from '@components/ui/color-mode'
import { Button } from '@components/ui/button'

export const WantedMaterial = () => {
    const auth = useAuth()
    const navigate = useNavigate()

    const handleNavigation = (e: MouseEvent, url: string) => {
        e.preventDefault()
        if (!auth.user) {
            toast.info(t('global.please.login'))
            navigate(UserRoutes.Login, {
                state: {
                    from: PoolingRoutes.New,
                },
            })
        } else if (e.ctrlKey || e.metaKey) {
            window.open(url, '_blank')
        } else {
            navigate(url)
        }
    }

    return (
        <Container maxW="7xl" textAlign={'center'} my="16">
            <Heading
                as={'h2'}
                size="4xl"
                mt="10"
                fontWeight="extrabold"
                letterSpacing="tight"
                lineHeight="normal"
            >
                {t('home.wanted.title')}
            </Heading>
            <Box
                fontSize="lg"
                mt="6"
                color={useColorModeValue('gray.600', 'gray.400')}
                my={10}
            >
                <EmphasisText color={'yellow.400'}>
                    {t('home.wanted.subtitle')}
                </EmphasisText>
            </Box>
            <SimpleGrid columns={{ base: 1, md: 3 }} gap="8">
                {[
                    'tools',
                    'gardening',
                    'transport',
                    'sportAndOutdoors',
                    'culture',
                    'travel',
                    'events',
                    'kitchenEquipment',
                ]
                    .map((category) => ({
                        title: t(`home.wanted.items.${category}.title`),
                        description: t(
                            `home.wanted.items.${category}.description`
                        ),
                        image: `/images/home/home-wanted-${category}.webp`,
                        onClick: (e: MouseEvent) =>
                            handleNavigation(e, PoolingRoutes.New),
                        button: (
                            <PrimaryButton
                                mb={5}
                                link={
                                    !auth.user
                                        ? UserRoutes.Login
                                        : PoolingRoutes.New
                                }
                                onClick={(e) =>
                                    handleNavigation(e, PoolingRoutes.New)
                                }
                            >
                                {t('home.wanted.cta')}
                            </PrimaryButton>
                        ),
                    }))
                    .map((item, index) => (
                        <WantedItem key={index} {...item} />
                    ))}
                <WantedOtherCTAItem
                    title={t('home.wanted.items.other.title')}
                    description={t('home.wanted.items.other.description')}
                    link={!auth.user ? UserRoutes.Login : PoolingRoutes.New}
                    onClick={(e: MouseEvent) =>
                        handleNavigation(e, PoolingRoutes.New)
                    }
                    button={
                        <Button mb={5} asChild colorPalette="purple">
                            <Link to={'#'}>{t('home.wanted.cta')}</Link>
                        </Button>
                    }
                />
            </SimpleGrid>
        </Container>
    )
}
