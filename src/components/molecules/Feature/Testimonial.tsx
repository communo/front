import { Box, Flex, Image, Text } from '@chakra-ui/react'
import * as React from 'react'
import { useColorModeValue } from '@components/ui/color-mode'

interface TestimonialProps {
    image: string
    name: string
    role: string
    children: React.ReactNode
}

export const Testimonial = (props: TestimonialProps) => {
    const { children, image, name, role } = props
    return (
        <Box
            as="blockquote"
            rounded="2xl"
            bg={useColorModeValue('white', 'gray.700')}
            color={useColorModeValue('gray.800', 'white')}
            shadow="lg"
            px="10"
            py="8"
        >
            <Flex mb="6">
                <Image
                    mt="-12"
                    bg={useColorModeValue('white', 'gray.700')}
                    objectFit="cover"
                    w="24"
                    h="24"
                    rounded="full"
                    color={useColorModeValue('white', 'gray.700')}
                    shadow="0 0 0 10px currentColor"
                    src={image}
                    alt={name}
                />
                <Box marginStart="5">
                    <Text
                        as="cite"
                        fontStyle="normal"
                        fontSize="md"
                        fontWeight="extrabold"
                    >
                        {name}
                    </Text>
                    <Text
                        mt="1"
                        color={useColorModeValue('gray.600', 'gray.400')}
                        textTransform="uppercase"
                        fontSize="xs"
                        fontWeight="semibold"
                        letterSpacing="wide"
                    >
                        {role}
                    </Text>
                </Box>
            </Flex>
            <Text color={useColorModeValue('gray.600', 'gray.400')}>
                {children}
            </Text>
        </Box>
    )
}
