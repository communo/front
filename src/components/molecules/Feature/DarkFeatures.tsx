import { Box, Heading, SimpleGrid, Stack } from '@chakra-ui/react'
import React, { ReactElement } from 'react'
import { Feature } from './Feature'

type Props = {
    button?: ReactElement
    features: {
        content: string
        icon: ReactElement
        title: string
    }[]
    title: string
}

export const DarkFeatures = ({ features, title, button }: Props) => {
    return (
        <Box bg="gray.800" color="white" pt="24" pb="12rem">
            <Box
                maxW={{ base: 'xl', md: '7xl' }}
                mx="auto"
                px={{ base: '6', md: '8' }}
            >
                <Stack
                    gap="10"
                    direction={{ base: 'column', lg: 'row' }}
                    align={{ base: 'flex-start', lg: 'center' }}
                    justify="space-between"
                >
                    <Heading
                        size="2xl"
                        lineHeight="short"
                        fontWeight="extrabold"
                        maxW={{ base: 'unset', lg: '800px' }}
                    >
                        {title}
                    </Heading>
                    {button}
                </Stack>
                <SimpleGrid
                    columns={{ base: 1, md: 2, lg: 4 }}
                    gap={{ base: '12', md: '8', lg: '2' }}
                    mt={{ base: '12', md: '20' }}
                >
                    {features.map(({ icon, title: featureTitle, content }) => (
                        <Feature
                            key={featureTitle}
                            icon={icon}
                            title={featureTitle}
                        >
                            {content}
                        </Feature>
                    ))}
                </SimpleGrid>
            </Box>
        </Box>
    )
}
