import { motion, useMotionValue, useTransform } from 'framer-motion'

const materials = [
    { image: { contentUrl: '/photos/pooling/future-tools.webp' } },
    { image: { contentUrl: '/photos/pooling/future-tools.webp' } },
    { image: { contentUrl: '/photos/pooling/future-tools.webp' } },
    { image: { contentUrl: '/photos/pooling/future-tools.webp' } },
    { image: { contentUrl: '/photos/pooling/future-tools.webp' } },
]
const height = 70
const padding = 10
const size = 150

function getHeight(items: { image: { contentUrl: string } }[]) {
    const totalHeight = items.length * height
    const totalPadding = (items.length - 1) * padding
    const totalScroll = totalHeight + totalPadding
    return totalScroll
}

export const ScrollProgress = () => {
    const scrollY = useMotionValue(0)

    const width = useTransform(
        scrollY,
        [0, -getHeight(materials) + size],
        ['calc(0% - 0px)', 'calc(100% - 40px)']
    )

    return (
        <>
            <motion.div
                style={{
                    width: 150,
                    height: 150,
                    borderRadius: 30,
                    overflow: 'hidden',
                    position: 'relative',
                    transform: 'translateZ(0)',
                }}
            >
                <motion.div
                    style={{
                        width: 150,
                        height: getHeight(materials),
                        y: scrollY,
                    }}
                    drag="y"
                    dragConstraints={{
                        top: -getHeight(materials) + size,
                        bottom: 0,
                    }}
                >
                    {materials.map((material, index) => {
                        return (
                            <motion.div
                                style={{
                                    width: 150,
                                    height,
                                    borderRadius: 20,
                                    background: `url(${material.image.contentUrl})`,
                                    backgroundPosition: 'contain',
                                    position: 'absolute',
                                    top: (height + padding) * (index + 1),
                                }}
                                key={index + 1}
                            />
                        )
                    })}
                </motion.div>
            </motion.div>
            <motion.div
                style={{
                    width,
                    height: 6,
                    borderRadius: 3,
                    backgroundColor: '#fff',
                    position: 'absolute',
                    bottom: 20,
                    left: 20,
                }}
            />
        </>
    )
}
