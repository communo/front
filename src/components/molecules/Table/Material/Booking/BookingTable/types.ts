import {
    BookingTable__MaterialBookingCursorConnectionFragment,
    BookingTable__MaterialBookingFragment,
} from '@_/graphql/api/generated'

export type BookingTableProps = {
    materialBookings: BookingTable__MaterialBookingCursorConnectionFragment
    side?: 'applicant' | 'owner'
}

export type PeriodListProps = { booking: BookingTable__MaterialBookingFragment }
