import React from 'react'
import { useTranslation } from 'react-i18next'
import moment from 'moment'
import { BookingTable__DatePeriodFragment } from '@_/graphql/api/generated'
import { PeriodListProps } from './types'

export const PeriodList = ({ booking }: PeriodListProps) => {
    const { t } = useTranslation()
    const bookingPeriods = booking?.periods
        ? booking.periods.reduce(
              (acc: BookingTable__DatePeriodFragment[], period) => {
                  if (period) {
                      acc.push(period)
                  }
                  return acc
              },
              []
          )
        : []
    return (
        <ul>
            {bookingPeriods.map((period) => (
                <li key={`periods-${booking.id}-${period.id}`}>
                    {(moment(period.startDate).format('L') !==
                        moment(period.endDate).format('L') &&
                        t(
                            'pooling.show.booking.summary.periodItem.range.label',
                            {
                                startDate: moment(period.startDate).format(
                                    'LL'
                                ),
                                endDate: moment(period.endDate).format('LL'),
                                price: period.price,
                            }
                        )) ||
                        t(
                            'pooling.show.booking.summary.periodItem.singleDay.label',
                            {
                                day: moment(period.startDate).format('LL'),
                            }
                        )}
                </li>
            ))}
        </ul>
    )
}
