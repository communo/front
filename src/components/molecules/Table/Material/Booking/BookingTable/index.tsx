import { Flex, Group, Image, Table } from '@chakra-ui/react'
import { FaChevronRight, FaSearch } from 'react-icons/fa'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { generatePath, Link } from 'react-router'
import { BookingStatusActionButton } from '@components/atoms/Button/material/booking/BookingStatusActionButton'
import { moneyFormat } from '@utils/money'
import { InfoButton, PrimaryButton } from '@components/atoms/Button'
import { materialmainPicture } from '@utils/image'
import { priceText } from '@utils/priceText'
import { useAuth } from '@_/auth/useAuth'
import { ThreeDotsMenu } from '@components/atoms/Menu/ThreeDotsMenu'
import { MaterialMainOwnerImage } from '@components/atoms/Image/MaterialMainOwnerImage'
import { IRI } from '@components/molecules/Form/Pooling/Material/types'
import { useColorModeValue } from '@components/ui/color-mode'
import { Avatar } from '@components/ui/avatar'
import { Button } from '@components/ui/button'
import { PoolingRoutes } from '@_/routes/_hooks/routes.enum'
import { BookingTableProps } from './types'
import { PeriodList } from './PeriodList'
import { CtaVertical } from '../../../../CTA/CtaVertical'

export const BookingTable = ({ materialBookings, side }: BookingTableProps) => {
    const { t, i18n } = useTranslation()
    const auth = useAuth()
    const tableBgColor = useColorModeValue('gray.50', 'gray.800')

    return (
        (materialBookings.edges && materialBookings.edges.length > 0 && (
            <Table.Root w="100%" my={25} borderRadius={'2xl'} striped size="sm">
                <Table.Header>
                    <Table.Row>
                        {[
                            'material',
                            'user',
                            'periods',
                            'pricing',
                            'status',
                        ].map((item) => (
                            <Table.ColumnHeader
                                key={item}
                                p={4}
                                bgColor={tableBgColor}
                            >
                                {t(`pooling.booking.index.table.${item}`)}
                            </Table.ColumnHeader>
                        ))}
                        <Table.ColumnHeader p={4} bgColor={tableBgColor} />
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {materialBookings.edges?.map((edge) => {
                        const booking = edge.node!
                        if (!booking?.material) {
                            return <div key={'nothing'} />
                        }

                        const picture = materialmainPicture(booking.material)
                        const UserPicture =
                            side === 'owner' ? (
                                <Avatar
                                    src={booking.user?.avatar?.contentUrl}
                                    title={`${booking.user?.firstname} ${booking.user?.lastname}`}
                                />
                            ) : (
                                <MaterialMainOwnerImage
                                    ownerIRI={
                                        booking.material.mainOwnership!
                                            .ownerIRI as IRI<
                                            'users' | 'circles',
                                            string
                                        >
                                    }
                                />
                            )

                        return (
                            <Table.Row key={`booking-${booking.id}`}>
                                <Table.Cell>
                                    <Flex alignItems="center" gap={3}>
                                        <Link
                                            to={`/pooling/${booking.material.slug}`}
                                        >
                                            <Image
                                                bgColor="#fff"
                                                borderRadius="3"
                                                boxSize="50px"
                                                objectFit="cover"
                                                src={picture}
                                                alt={booking.material.name}
                                                title={`${booking.material.name} - `}
                                            />
                                        </Link>
                                    </Flex>
                                </Table.Cell>
                                <Table.Cell>
                                    <Flex alignItems="center" gap={3}>
                                        {UserPicture}
                                    </Flex>
                                </Table.Cell>
                                <Table.Cell>
                                    <PeriodList booking={booking} />
                                </Table.Cell>
                                <Table.Cell>
                                    {(booking.price &&
                                        booking.price !== 0 &&
                                        t(
                                            'pooling.show.booking.summary.periodItem.price',
                                            {
                                                price: moneyFormat(
                                                    booking.price,
                                                    i18n.language
                                                ),
                                            }
                                        )) ||
                                        priceText(
                                            booking.price ?? 0,
                                            auth.user,
                                            t
                                        )}
                                </Table.Cell>
                                <Table.Cell>
                                    {t('pooling.booking.status', {
                                        context: booking.status,
                                    })}
                                </Table.Cell>
                                <Table.Cell>
                                    <Group>
                                        <BookingStatusActionButton
                                            id={booking.id}
                                        />
                                        <ThreeDotsMenu
                                            ariaLabel="Actions"
                                            buttons={[
                                                {
                                                    key: `booking-${booking.id}-see`,
                                                    component: (
                                                        <Button
                                                            variant="ghost"
                                                            colorPalette="red"
                                                            asChild
                                                        >
                                                            <Link
                                                                to={generatePath(
                                                                    PoolingRoutes.MyBookingShow,
                                                                    {
                                                                        slug: booking.slug,
                                                                    }
                                                                )}
                                                            >
                                                                {t(
                                                                    'global.see'
                                                                )}
                                                                <FaChevronRight />
                                                            </Link>
                                                        </Button>
                                                    ),
                                                },
                                            ]}
                                        />
                                    </Group>
                                </Table.Cell>
                            </Table.Row>
                        )
                    })}
                </Table.Body>
            </Table.Root>
        )) || (
            <CtaVertical
                title={t('pooling.booking.index.empty.title', {
                    context: auth.user?.gender,
                })}
                description={t('pooling.booking.index.empty.description')}
                emphasisColor="pink.400"
                buttons={[
                    {
                        key: 'btn-search',
                        component: (
                            <InfoButton asChild>
                                <Link to={PoolingRoutes.Search}>
                                    <>
                                        <FaSearch />
                                        {t(
                                            'pooling.booking.index.empty.actions.search'
                                        )}
                                    </>
                                </Link>
                            </InfoButton>
                        ),
                    },
                    {
                        key: 'btn-new',
                        component: (
                            <PrimaryButton asChild>
                                <Link to={PoolingRoutes.New}>
                                    {t(
                                        'pooling.booking.index.empty.actions.new'
                                    )}
                                </Link>
                            </PrimaryButton>
                        ),
                    },
                ]}
            />
        )
    )
}
