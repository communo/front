import { Box, Flex, Image, Table, VStack } from '@chakra-ui/react'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router'
import { FaChevronRight, FaWrench } from 'react-icons/fa'
import { materialmainPicture } from '@utils/image'
import { MaterialTable_MaterialFragment } from '@_/graphql/api/generated'
import { MaterialContextualButton } from '@components/views/pooling/show/MaterialContextualButton'
import { Button } from '@components/ui/button'
import { useColorModeValue } from '@components/ui/color-mode'

export const MaterialTable = ({
    materials,
    emptyState,
}: {
    materials: MaterialTable_MaterialFragment[]
    emptyState: React.ReactNode
}) => {
    const { t } = useTranslation()
    const tableBgColor = useColorModeValue('gray.50', 'gray.800')

    return (
        (materials.length > 0 && (
            <Table.Root w="100%" my={25} rounded={'2xl'} striped size="sm">
                <Table.Header>
                    <Table.Row>
                        {['name'].map((item) => (
                            <Table.ColumnHeader
                                key={item}
                                p={4}
                                bgColor={tableBgColor}
                            >
                                {t(
                                    `pooling.my_materials.index.table.${item}.label`
                                )}
                            </Table.ColumnHeader>
                        ))}
                        <Table.ColumnHeader p={4} bgColor={tableBgColor} />
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {materials.map((node) => {
                        const picture = materialmainPicture(node!)
                        const actions = [
                            {
                                key: `btn-${node.slug}-edit`,
                                component: (
                                    <Button
                                        variant="ghost"
                                        asChild
                                        title={t(
                                            'pooling.my_materials.index.table.actions.edit.label'
                                        )}
                                    >
                                        <Link
                                            to={`/my/materials/${node.slug}/edit`}
                                        >
                                            <FaWrench />
                                            {t(
                                                'pooling.my_materials.index.table.actions.edit.label'
                                            )}
                                        </Link>
                                    </Button>
                                ),
                            },
                            {
                                key: `btn-${node.slug}-see`,
                                component: (
                                    <Button
                                        variant="ghost"
                                        asChild
                                        title={t('global.see')}
                                    >
                                        <Link to={`/pooling/${node.slug}`}>
                                            <FaChevronRight />
                                            {t('global.see')}
                                        </Link>
                                    </Button>
                                ),
                            },
                            {
                                key: `delimiter-${node.slug}`,
                                component: (
                                    <Box my={3}>
                                        <hr />
                                    </Box>
                                ),
                            },
                        ]

                        return (
                            <Table.Row key={`material-${node.id}`}>
                                <Table.Cell>
                                    <Flex alignItems="center" gap={3}>
                                        <Image
                                            boxSize="30px"
                                            objectFit="cover"
                                            src={picture}
                                            alt={node.name}
                                        />
                                        <VStack>
                                            <Link to={`/pooling/${node.slug}`}>
                                                {node.name}
                                            </Link>
                                        </VStack>
                                    </Flex>
                                </Table.Cell>
                                <Table.Cell>
                                    <MaterialContextualButton
                                        material={node}
                                        additionalButtons={actions}
                                    />
                                </Table.Cell>
                            </Table.Row>
                        )
                    })}
                </Table.Body>
            </Table.Root>
        )) ||
        emptyState
    )
}
