import React from 'react'
import {
    ColumnDef,
    flexRender,
    getCoreRowModel,
    RowData,
    useReactTable,
} from '@tanstack/react-table'
import { TableOptions } from '@tanstack/table-core'
import { Flex, Table as ChakraTable } from '@chakra-ui/react'
import { FaCaretDown, FaCaretUp } from 'react-icons/fa'

type TableProps<T extends RowData> = {
    columns: ColumnDef<T>[]
    data: T[]
    expandedRowRenderFn?: (data: T, columnLength: number) => React.ReactNode
} & Omit<TableOptions<T>, 'data' | 'columns' | 'getCoreRowModel'>

export const Table = <T,>({
    columns,
    data,
    expandedRowRenderFn,
    ...options
}: TableProps<T>) => {
    const table = useReactTable<T>({
        data,
        columns,
        getCoreRowModel: getCoreRowModel(),
        ...options,
    })

    return (
        <ChakraTable.Root
            w="100%"
            my={25}
            borderRadius={'2xl'}
            striped
            rounded={'2xl'}
        >
            <ChakraTable.Header>
                {table.getHeaderGroups().map((headerGroup) => (
                    <ChakraTable.Row key={headerGroup.id}>
                        {headerGroup.headers.map((header) => {
                            let title: string | undefined

                            if (header.column.getCanSort()) {
                                title =
                                    header.column.getNextSortingOrder() ===
                                    'asc'
                                        ? 'Sort ascending'
                                        : 'Sort descending'
                            }
                            return (
                                <ChakraTable.ColumnHeader
                                    key={header.id}
                                    colSpan={header.colSpan}
                                >
                                    <Flex
                                        alignItems={'center'}
                                        gap={2}
                                        onClick={header.column.getToggleSortingHandler()}
                                        title={title}
                                    >
                                        {header.isPlaceholder
                                            ? null
                                            : flexRender(
                                                  header.column.columnDef
                                                      .header,
                                                  header.getContext()
                                              )}
                                        {{
                                            asc: (
                                                <a href={'#'}>
                                                    <FaCaretUp
                                                        className="h-5 w-5 text-gray-400"
                                                        aria-hidden="true"
                                                    />
                                                </a>
                                            ),
                                            desc: (
                                                <a href={'#'}>
                                                    <FaCaretDown
                                                        className="h-5 w-5 text-gray-400"
                                                        aria-hidden="true"
                                                    />
                                                </a>
                                            ),
                                        }[
                                            header.column.getIsSorted() as string
                                        ] ?? null}
                                    </Flex>
                                </ChakraTable.ColumnHeader>
                            )
                        })}
                    </ChakraTable.Row>
                ))}
            </ChakraTable.Header>
            <ChakraTable.Body>
                {table.getRowModel().rows.map((row) => (
                    <React.Fragment key={row.id}>
                        <ChakraTable.Row>
                            {row.getVisibleCells().map((cell) => (
                                <ChakraTable.Cell key={cell.id}>
                                    {flexRender(
                                        cell.column.columnDef.cell,
                                        cell.getContext()
                                    )}
                                </ChakraTable.Cell>
                            ))}
                        </ChakraTable.Row>
                        {row.getIsExpanded() &&
                            expandedRowRenderFn &&
                            expandedRowRenderFn(row.original, columns.length)}
                    </React.Fragment>
                ))}
            </ChakraTable.Body>
        </ChakraTable.Root>
    )
}
