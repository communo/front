import { RouteObject } from 'react-router'
import { Layout } from '@components/layout/Layout'
import { ProfileView } from '@components/views/user/ProfileView'
import { UserRoutes } from './routes.enum'

export const useSocialRouteConfig = (): RouteObject[] => {
    return [
        {
            path: '/user',
            element: <Layout />,
            children: [
                {
                    path: UserRoutes.Profile,
                    element: <ProfileView />,
                },
            ],
        },
    ]
}
