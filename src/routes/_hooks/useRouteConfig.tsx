import { RouteObject } from 'react-router'
import { useStaticPagesRouteConfig } from '@_/routes/_hooks/useStaticPagesRouteConfig'
import { useCirclesRouteConfig } from '@_/routes/_hooks/useCirclesRouteConfig'
import { usePoolingRouteConfig } from '@_/routes/_hooks/usePoolingRouteConfig'
import { useAdminRouteConfig } from '@_/routes/_hooks/useAdminRouteConfig'
import { useUserAccessRouteConfig } from '@_/routes/_hooks/useUserAccessRouteConfig'
import { useSubscriptionRouteConfig } from '@_/routes/_hooks/useSubscriptionRouteConfig'
import { useUserSpaceRouteConfig } from '@_/routes/_hooks/useUserSpaceRouteConfig'
import { useSocialRouteConfig } from './useSocialRouteConfig'

export const useRouteConfig = (): RouteObject[] => {
    return [
        ...useStaticPagesRouteConfig(),
        ...useCirclesRouteConfig(),
        ...usePoolingRouteConfig(),
        ...useAdminRouteConfig(),
        ...useSubscriptionRouteConfig(),
        ...useUserAccessRouteConfig(),
        ...useUserSpaceRouteConfig(),
        ...useSocialRouteConfig(),
        {
            path: '*',
            element: (
                <main
                    style={{
                        padding: '1rem',
                    }}
                >
                    <p>Nothing here!</p>
                </main>
            ),
        },
    ]
}
