import React from 'react'
import { Layout } from '@components/layout/Layout'
import { SubscriptionRoutes } from '@_/routes/_hooks/routes.enum'
import { SubscribeView } from '@components/views/subscription/SubscribeView'

export const useSubscriptionRouteConfig = () => {
    return [
        {
            path: '/',
            element: <Layout />,
            children: [
                {
                    path: SubscriptionRoutes.Subscribe,
                    element: <SubscribeView />,
                },
            ],
        },
    ]
}
