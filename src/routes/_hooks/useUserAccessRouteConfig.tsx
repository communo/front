import { RequestInviteView } from '@components/views/accountManagement/requestInvite/RequestInviteView'
import { LoginView } from '@components/views/accountManagement/login/LoginView'
import { LogoutView } from '@_/auth/LogoutView'
import { ForgotPasswordView } from '@components/views/resetPassword/ForgotPasswordView'
import { ForgotPasswordResetView } from '@components/views/resetPassword/ForgotPasswordResetView'
import { Layout } from '@components/layout/Layout'
import { RegisterWizardHomeView } from '@components/views/accountManagement/registerWizard/RegisterWizardHomeView'
import { UserRoutes } from '@_/routes/_hooks/routes.enum'

export const useUserAccessRouteConfig = () => {
    return [
        {
            path: '/',
            element: <Layout />,
            children: [
                {
                    path: UserRoutes.Register,
                    element: <RegisterWizardHomeView />,
                },
                {
                    path: UserRoutes.RequestInvite,
                    element: <RequestInviteView />,
                },
                {
                    path: UserRoutes.Login,
                    element: <LoginView />,
                },
                {
                    path: UserRoutes.Logout,
                    element: <LogoutView />,
                },
                {
                    path: UserRoutes.ForgotPassword,
                    element: <ForgotPasswordView />,
                },
                {
                    path: UserRoutes.ForgotPasswordReset,
                    element: <ForgotPasswordResetView />,
                },
            ],
        },
    ]
}
