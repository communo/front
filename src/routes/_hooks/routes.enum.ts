export const enum PoolingRoutes {
    IRI = `/materials/:slug`,
    Search = `/pooling`,
    Show = '/pooling/:slug',
    New = '/pooling/new',
    MyMaterials = '/my/materials',
    EditMyMaterial = '/my/materials/:slug/edit',
    EditMyMaterialStep = '/my/materials/:slug/edit/:step',
    Calendar = '/my/materials/:slug/calendar',
    MyBookingRequests = '/my/bookings/requests',
    MyBookingAsOwner = '/my/bookings/as-owner',
    MyBookingShow = '/my/bookings/:slug',
    MyAlerts = '/my/alerts',
}

export const enum StaticPagesRoutes {
    Home = '/',
    Contact = '/contact',
    Team = '/team',
    Contribute = '/contribute',
    Donate = '/donate',
    Pricing = '/pricing',
}

export const enum AdminRoutes {
    Home = '/admin',
    Invitations = '/admin/invitations',
    Users = '/admin/users',
    Circles = '/admin/circles',
}

export const enum CommunitiesRoutes {
    IRI = `/circles/:slug`,
    InvitationLinksIRI = `/circle_invitation_links/:token`,
    Map = `/map`,
    Show = '/communities/:slug',
    Edit = '/my/communities/:slug/edit',
    EditStep = '/my/communities/:slug/edit/:step',
    MyCommunities = '/my/communities',
}

export const enum UserRoutes {
    IRI = `/users/:slug`,
    InvitationsIRI = `/invitations/:id`,
    Register = '/register',
    RequestInvite = '/request-invite',
    Login = '/login',
    Logout = '/logout',
    ForgotPassword = '/forgot-password',
    ForgotPasswordReset = '/forgot-password/:token',
    MyProfile = '/my/profile',
    MyPreferences = '/my/preferences',
    MyLists = '/my/lists',
    EditMyList = '/my/lists/:slug',
    Profile = '/user/:slug',
}

export const enum SubscriptionRoutes {
    Subscribe = '/subscribe/:slug',
}

export const enum APIRoutes {
    IRI = '/:ns/:id',
}
