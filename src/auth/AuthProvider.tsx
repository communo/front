import * as Sentry from '@sentry/react'
import { User as SentryUser } from '@sentry/react'
import React, {
    createContext,
    Dispatch,
    ReactNode,
    useMemo,
    useState,
} from 'react'
import { useCookies } from 'react-cookie'
import { CookieSetOptions } from 'universal-cookie'
import { Crisp } from 'crisp-sdk-web'
import {
    AuthUser__UserFragment as User,
    useAuthUserQuery,
} from '@_/graphql/api/generated'

export interface AuthContextType {
    user: User | null
    token: string | null
    signin: (response: User, callback?: () => void) => void
    signout: (callback?: () => void) => void
}

export const AuthContext = createContext<AuthContextType>({
    user: null,
    token: null,
    signin: () => {},
    signout: () => {},
})

const computeAuthContextValue = (
    user: User | null,
    token: string | null,
    refreshToken: string | null,
    tokenExpiresAt: string | null,
    signout: (callback?: () => void) => void,
    signin: (response: User, callback?: () => void) => void
) => {
    return {
        user,
        token,
        refreshToken,
        tokenExpiresAt,
        signout,
        signin,
    }
}

const doSignout = (
    removeCookie: (
        name: string,
        options?: CookieSetOptions | undefined
    ) => void,
    setToken: Dispatch<string | null>,
    setTokenExpiresAt: Dispatch<string | null>,
    setRefreshToken: Dispatch<string | null>,
    setUser: Dispatch<User | null>
) => {
    return (callback?: () => void) => {
        removeCookie('user', {
            path: '/',
            sameSite: 'lax',
            domain: process.env.REACT_APP_COOKIE_DOMAIN,
            secure: true,
        })
        setToken(null)
        setTokenExpiresAt(null)
        setRefreshToken(null)
        setUser(null)
        Sentry.setUser(null)
        Crisp.session.reset()
        localStorage.removeItem('token')
        localStorage.removeItem('tokenExpiresAt')
        localStorage.removeItem('refreshToken')
        if (callback) {
            callback()
        }
    }
}

function doSignin(
    setCookie: (
        name: string,
        value: string | object | null | undefined,
        options?: CookieSetOptions | undefined
    ) => void,
    setToken: Dispatch<string | null>,
    setTokenExpiresAt: Dispatch<string | null>,
    setRefreshToken: Dispatch<string | null>,
    setUser: Dispatch<User | null>
) {
    return (response: User, callback?: () => void) => {
        setUser(response)
        if (response) {
            setToken(response.token ?? '')
            setTokenExpiresAt(response.tokenExpiresAt?.toString() ?? '')
            setRefreshToken(response.refreshToken ?? '')
            localStorage.setItem('token', response.token ?? '')
            localStorage.setItem(
                'tokenExpiresAt',
                (response.tokenExpiresAt ?? '').toString()
            )
            localStorage.setItem('refreshToken', response.refreshToken ?? '')
            setCookie(
                'user',
                { id: response.id, slug: response.slug },
                {
                    path: '/',
                    sameSite: 'lax',
                    domain: process.env.REACT_APP_COOKIE_DOMAIN,
                    secure: true,
                }
            )
            const {
                email,
                fullname,
                settings,
                phoneNumber,
                avatar,
                circleMemberships,
            } = response
            Crisp.user.setEmail(email)
            Crisp.user.setNickname(fullname)
            if (avatar?.contentUrl) {
                Crisp.user.setAvatar(avatar.contentUrl)
            }
            Crisp.user.setPhone(phoneNumber.nationalNumber)

            Crisp.session.setData({
                userId: response.id,
                city: response.city,
                country: response.country,
                gender: response.gender,
                language: settings?.language,
                circles: circleMemberships?.edges
                    ?.map((edge) => edge.node!.circle.name)
                    .join(', '),
            })

            const user: SentryUser = {
                email,
                id: response.id,
                country: response.country,
                createdAt: response.createdAt,
                fullname: response.fullname,
                gender: response.gender,
                language: settings?.language,
                circles: circleMemberships?.edges
                    ?.map((edge) => edge.node!.circle.name)
                    .join(', '),
                geo: {
                    city: response.city,
                    country_code: `${response.phoneNumber?.countryCode}`,
                },
            }
            Sentry.setUser(user)
            if (callback) {
                callback()
            }
        }
    }
}

export const AuthProvider = ({ children }: { children: ReactNode }) => {
    const [user, setUser] = useState<User | null>(null)
    const [token, setToken] = useState<string | null>(null)
    const [tokenExpiresAt, setTokenExpiresAt] = useState<string | null>(null)
    const [refreshToken, setRefreshToken] = useState<string | null>(null)
    const [cookies, setCookie, removeCookie] = useCookies()

    const cookieUser = cookies.user
    useAuthUserQuery({
        variables: {
            id: cookieUser?.id,
        },
        skip: !cookieUser?.id,
        onCompleted: (data) => {
            setUser(data.user ?? null)
        },
    })

    /**
     * signin is a way to log in thanks graphql mutation
     * @param response
     * @param callback
     */
    const signin = useMemo(
        () =>
            doSignin(
                setCookie,
                setToken,
                setTokenExpiresAt,
                setRefreshToken,
                setUser
            ),
        [setCookie, setToken, setTokenExpiresAt, setRefreshToken, setUser]
    )

    const signout = useMemo(
        () =>
            doSignout(
                removeCookie,
                setToken,
                setTokenExpiresAt,
                setRefreshToken,
                setUser
            ),
        [removeCookie, setToken, setTokenExpiresAt, setRefreshToken, setUser]
    )

    const value = useMemo(
        () =>
            computeAuthContextValue(
                user,
                token,
                refreshToken,
                tokenExpiresAt,
                signout,
                signin
            ),
        [user, token, refreshToken, tokenExpiresAt, signout, signin]
    )

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>
}
