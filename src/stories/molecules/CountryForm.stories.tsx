import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'

import { Box } from '@chakra-ui/react'
import { RequiredAsterisk } from '@components/atoms/Form/RequiredAsterisk'
import { CountryForm } from '@components/atoms/Form/CountryForm'
import { Field } from '@components/ui/field'

const meta: Meta<typeof CountryForm> = {
    title: 'molecules/Form/CountryForm',
    component: CountryForm,
    args: {},
    decorators: [
        (Story) => (
            <Box w={400}>
                <Field
                    label={
                        <>
                            Country <RequiredAsterisk />
                        </>
                    }
                >
                    <Story />
                </Field>
            </Box>
        ),
    ],
}

export default meta
type Story = StoryObj<typeof CountryForm>

export const Default: Story = {
    args: {},
}
