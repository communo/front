import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'

import { Box } from '@chakra-ui/react'
import { PhoneNumberInput } from '@components/atoms/Form/PhoneNumberInput'

const meta: Meta<typeof PhoneNumberInput> = {
    title: 'molecules/Form/PhoneNumberInput',
    component: PhoneNumberInput,
    args: {},
    decorators: [
        (Story) => (
            <Box maxW="600">
                <Story />
            </Box>
        ),
    ],
}

export default meta
type Story = StoryObj<typeof PhoneNumberInput>

export const Default: Story = {
    args: {},
}
