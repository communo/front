import React from 'react'
import type { Meta, StoryObj } from '@storybook/react'

import { Box } from '@chakra-ui/react'
import {
    DangerButton,
    InfoButton,
    PrimaryButton,
    RainbowButton,
    SuccessButton,
} from '@components/atoms/Button'
import { Button } from '@components/ui/button'
import { FaRainbow } from 'react-icons/fa'

const meta: Meta<typeof Button> = {
    title: 'atoms/Button',
    component: Button,
    args: {
        children: "Let's Communo",
        width: 'auto',
    },
    decorators: [
        (Story) => (
            <Box w={400}>
                <Story />
            </Box>
        ),
    ],
}

export default meta
type Story = StoryObj<typeof Button>

export const Default: Story = {
    args: {
        children: (
            <>
                Let's Communo
                <FaRainbow />
            </>
        ),
        width: 'auto',
    },
}

export const Ghost: Story = {
    args: {
        variant: 'ghost',
        colorPalette: 'orange',
    },
}
export const Outline: Story = {
    args: {
        variant: 'outline',
        colorPalette: 'red',
    },
}
export const Primary: StoryObj<typeof PrimaryButton> = {
    args: {},
}
export const BorderLite: StoryObj<typeof PrimaryButton> = {
    args: {
        borderRadius: 'lg',
    },
}
export const Success: StoryObj<typeof SuccessButton> = {}
export const Info: StoryObj<typeof InfoButton> = {}
export const Danger: StoryObj<typeof DangerButton> = {}
export const Rainbow: StoryObj<typeof RainbowButton> = {}
