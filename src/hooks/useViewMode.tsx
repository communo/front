import { useEffect, useState } from 'react'

const VIEW_MODE_KEY = 'viewMode'

export enum ViewMode {
    Table = 'table',
    Card = 'card',
    Grid = 'grid',
    Map = 'map',
}

const getInitialViewMode = <T extends string>(
    name: string,
    defaultValue: T
): T => {
    return (localStorage.getItem(name) as T) ?? defaultValue
}

export const useViewMode = <T extends string>(
    defaultMode: T,
    modeName = VIEW_MODE_KEY
) => {
    const [viewMode, setViewMode] = useState<T>(
        getInitialViewMode<T>(modeName, defaultMode)
    )

    useEffect(() => {
        localStorage.setItem(modeName, viewMode)
    }, [modeName, viewMode])

    return [viewMode, setViewMode] as const
}
