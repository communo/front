import { useMemo } from 'react'
import LinkifyIt from 'linkify-it'
import { FaLink } from 'react-icons/fa'
import { Button, Link } from '@chakra-ui/react'

const linkify = new LinkifyIt()

export const useLinkify = (text: string) => {
    return useMemo(() => {
        if (!text) return text

        const matches = linkify.match(text)
        if (!matches) return text

        const elements = []
        let lastIndex = 0

        matches.forEach((match, index) => {
            if (match.index > lastIndex) {
                elements.push(text.slice(lastIndex, match.index))
            }
            elements.push(
                <Button
                    key={index}
                    variant={'plain'}
                    textDecoration="underline"
                    asChild
                    rel="noopener noreferrer"
                >
                    <Link href={match.url}>
                        <FaLink />
                        {match.text}
                    </Link>
                </Button>
            )
            lastIndex = match.lastIndex
        })

        if (lastIndex < text.length) {
            elements.push(text.slice(lastIndex))
        }

        return elements
    }, [text])
}
