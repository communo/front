import { useApolloClient } from '@apollo/client'
import {
    useChangeDateMutation,
    useDeleteMaterialBookingMutation,
    useUnavailableMaterialBookingMutation,
} from '@_/graphql/api/generated'

export const useCalendarOperations = () => {
    const client = useApolloClient()
    const [changeDate] = useChangeDateMutation({
        update() {
            client.resetStore()
        },
    })
    const [deleteUnavailable] = useDeleteMaterialBookingMutation({
        update() {
            client.resetStore()
        },
    })
    const [unavailable] = useUnavailableMaterialBookingMutation({
        update() {
            client.resetStore()
        },
    })

    return { changeDate, deleteUnavailable, unavailable }
}
