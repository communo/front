import { TFunction } from 'i18next'

export const randomBetween = (min: number, max: number) =>
    Math.floor(Math.random() * (max - min + 1)) + min

export const randomTrans = (
    t: TFunction,
    translationString: string,
    itemCount: number,
    params: object = {}
) => t(`${translationString}.${randomBetween(1, itemCount)}`, params)

export const randomPicture = (path: string, unit: string, itemCount: number) =>
    `${path}${randomBetween(1, itemCount)}.${unit}`
