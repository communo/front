import { TFunction } from 'i18next'
import moment, { locale as setMomentLocale } from 'moment'

export const datePeriodText = (
    startDate: string,
    endDate: string,
    t: TFunction<'translation', undefined, 'translation'>,
    locale: string
) => {
    setMomentLocale(locale)
    if (moment(startDate).format('L') !== moment(endDate).format('L')) {
        let params = {
            startDate: moment(startDate).format('ll'),
            endDate: moment(endDate).format('LL'),
        }
        if (moment(startDate).format('Y') === moment(endDate).format('Y')) {
            params = {
                ...params,
                startDate: moment(startDate).format('Do MMMM'),
            }
            if (moment(startDate).format('M') === moment(endDate).format('M')) {
                params = {
                    ...params,
                    startDate: moment(startDate).format('Do'),
                }
            }
        }
        return t('pooling.show.booking.summary.periodItem.range.label', params)
    }
    return t('pooling.show.booking.summary.periodItem.singleDay.label', {
        day: moment(startDate).format('LL'),
    })
}
