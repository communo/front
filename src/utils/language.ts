export const getShortLanguage = (lang: string): string => {
    return lang.split('-')[0]
}
