export type Dimensions = {
    width: number
    height: number
}

type Props = {
    initialDimensions: Dimensions
    maxWidth: number
    maxHeight: number
    allowScaleUp?: boolean
}

/**
 * Scales down an image while maintaining its aspect ratio, given the initial
 * dimension, and the maximum width and height. If the dimensions of the
 * image are smaller than the maximum dimensions and scale up is not allowed,
 * the function returns the original dimensions of the image.
 *
 * @param {number, number} initialDimensions - The initial dimension of the image.
 * @param {number} maxWidth - The maximum width of the image.
 * @param {number} maxHeight - The maximum height of the image.
 * @param {boolean} allowScaleUp - Whether to allow scaling up of the image if
 *     its dimensions are smaller than the maximum dimensions. Defaults to false.
 * @return {{width: number, height: number}} The new width and height of the image
 *     after scaling down, while maintaining its aspect ratio.
 */
export const scaleDownImage = ({
    initialDimensions,
    maxWidth,
    maxHeight,
    allowScaleUp = false,
}: Props): Dimensions => {
    let { width: initialWidth, height: initialHeight } = initialDimensions
    if (
        !allowScaleUp &&
        initialWidth <= maxWidth &&
        initialHeight <= maxHeight
    ) {
        return initialDimensions
    }

    const ratio = initialWidth / initialHeight

    if (initialWidth > maxWidth) {
        initialWidth = maxWidth
        initialHeight = initialWidth / ratio
    }
    if (initialHeight > maxHeight) {
        initialHeight = maxHeight
        initialWidth = initialHeight * ratio
    }

    return { width: initialWidth, height: initialHeight }
}

export const fileCookiePrefix = (type: string, id?: string | null) =>
    `${id ?? 'new'}_${type}_`

export const materialmainPicture = (material: {
    images?: {
        edges?: {
            node?: {
                contentUrl?: string
            }
        }[]
    }
}) => {
    return material.images?.edges && material.images.edges[0]?.node?.contentUrl
}

export type Base64File = {
    base64: string
    name: string
    type: string
}

export const fileToBase64 = (file: File): Promise<Base64File> => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = () => {
            resolve({
                base64: reader.result as string,
                name: file.name,
                type: file.type,
            })
        }
        reader.onerror = (error) => reject(error)
    })
}

export const base64ToFile = (base64File: Base64File): File => {
    const arr = base64File.base64.split(',')
    const byteString = atob(arr[1])
    const ab = new ArrayBuffer(byteString.length)
    const ia = new Uint8Array(ab)
    for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i)
    }
    return new File([ab], base64File.name, { type: base64File.type })
}
