export const isUrl = (link: string) => {
    const expression =
        /(https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&//=]*))/gi
    const regex = new RegExp(expression)
    return link.match(regex)
}

export const isPathCurrent = (pathname: string, currentPathname: string) => {
    const expression = pathname
    const regex = new RegExp(expression)
    return currentPathname.match(regex)
}
