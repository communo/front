import { setContext } from '@apollo/client/link/context'
import i18n from 'i18next'

export function needRefreshToken() {
    const tokenExpiresAt = localStorage.getItem('tokenExpiresAt')
        ? localStorage.getItem('tokenExpiresAt')
        : 0
    const token = localStorage.getItem('token') ?? null
    return !token || !tokenExpiresAt || parseFloat(tokenExpiresAt) < Date.now()
}
const refreshAuthToken = async () => {
    if (localStorage.getItem('refreshToken')) {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Accept-Language': i18n.language,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(
                { refresh_token: localStorage.getItem('refreshToken') },
                null,
                2
            ),
        }
        fetch(`${process.env.REACT_APP_API_URL}/token/refresh`, requestOptions)
            .then((response) => response.json())
            .then((response) => {
                if (response.message) {
                    throw new Error(response.message)
                }
                if (response.error) {
                    throw new Error(response.error)
                }
                localStorage.setItem('token', response.token)
                localStorage.setItem(
                    'tokenExpiresAt',
                    (Date.now() + 3600000) /* 1 hour milliseconds */
                        .toString()
                )

                return response.token
            })
    }
}
export const ApolloRefreshTokenAuthMiddleware = setContext(
    async (request, { headers }) => {
        // set token as refreshToken for refreshing token request
        if (request.operationName === 'refreshAuthToken') {
            const refreshToken = localStorage.getItem('refreshToken')
            if (refreshToken) {
                return {
                    headers: {
                        ...headers,
                        Authorization: `Bearer ${refreshToken}`,
                    },
                }
            }
            return { headers }
        }
        let token: string | null | void = localStorage.getItem('token')
        if (needRefreshToken()) {
            const refreshPromise = refreshAuthToken()
            token = await refreshPromise
        }

        if (token) {
            return {
                headers: {
                    ...headers,
                    Authorization: `Bearer ${token}`,
                    'accept-language': i18n.language,
                },
            }
        }
        return { headers }
    }
)
