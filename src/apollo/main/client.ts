import createUploadLink from 'apollo-upload-client/createUploadLink.mjs'
import { ApolloClient, ApolloLink, InMemoryCache } from '@apollo/client'

import i18n from '@_/translations'
import { ApolloRefreshTokenAuthMiddleware } from './auth/ApolloRefreshTokenAuthMiddleware'

const link = createUploadLink({
    uri: `${process.env.REACT_APP_API_URL}/graphql`,
})
export const client = new ApolloClient({
    connectToDevTools: process.env.NODE_ENV !== 'production',
    cache: new InMemoryCache(),
    // @ts-ignore
    link: ApolloLink.from([ApolloRefreshTokenAuthMiddleware, link]),
    defaultContext: {
        headers: {
            'Accept-Language': i18n.language,
        },
    },
})
