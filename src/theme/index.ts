import { createSystem, defaultConfig } from '@chakra-ui/react'
import { slotRecipes } from './slot-recipes'
import { globalCss } from './global-css'
import { recipes } from './recipes'
import { semanticTokens } from './semantic-tokens'
import { tokens } from './tokens'

export const system = createSystem(defaultConfig, {
    globalCss,
    theme: {
        tokens,
        semanticTokens,
        recipes,
        slotRecipes,
    },
})
