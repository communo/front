import { defineSlotRecipe } from '@chakra-ui/react'

export const alertSlotRecipe = defineSlotRecipe({
    slots: ['root'],
    base: {
        root: {
            borderRadius: 'l2',
        },
    },
})
