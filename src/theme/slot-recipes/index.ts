import { dialogSlotRecipe } from './dialog'
import { alertSlotRecipe } from './alert'

export const slotRecipes = {
    dialog: dialogSlotRecipe,
    alert: alertSlotRecipe,
}
