import { defineTokens } from '@chakra-ui/react'

export const fonts = defineTokens.fonts({
    body: {
        value: 'Tahoma, Roboto, sans-serif;',
    },
    heading: {
        value: 'Bricolage Grotesque, Tahoma, Roboto, sans-serif;',
    },
    mono: {
        value: 'SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace',
    },
})
