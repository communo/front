import { defineGlobalStyles } from '@chakra-ui/react'

export const globalCss = defineGlobalStyles({
    html: {
        color: 'fg',
        bg: 'bg',
        lineHeight: '1.5',
        colorPalette: 'yellow',
    },
})
