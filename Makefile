include .env
DOCKER_COMPOSE_FILE =
DOCKER_COMP = docker compose $(DOCKER_COMPOSE_FILE)
DOCKER_RUN = $(DOCKER_COMP) run --rm --no-deps node
DOCKER_OPTS =

CROWDIN_CONT = $(DOCKER_COMP) run --rm crowdin
CROWDIN = $(CROWDIN_CONT) crowdin
YARN = $(DOCKER_RUN) yarn
NPX = $(DOCKER_RUN) npx

.PHONY = codegen

up: stop
	@$(DOCKER_COMP) up --detach --remove-orphans ${DOCKER_OPTS}
restart:
	@$(DOCKER_COMP) restart ${DOCKER_OPTS}
stop:
	@$(DOCKER_COMP) stop ${DOCKER_OPTS}
ps:
	@$(DOCKER_COMP) ps -a
logs:
	@$(DOCKER_COMP) logs -f -n 100
BUILD_OPT=--pull
docker_build:
	$(DOCKER_COMP) build ${BUILD_OPT}

yarn:
	@$(YARN) $(c)
run:
	@$(DOCKER_RUN) $(c)
yarn-build:
	@$(YARN) build
lint:
	@$(YARN) lint
yarn-install:
	$(YARN) install

yarn-add-dev: opt=-D
yarn-add-dev: yarn-add
yarn-add:
	@$(YARN) add $(opt) $(pkg)


trans_pull: DOCKER_COMPOSE_FILE = -f .docker/crowdin/docker-compose.yml
trans_push: DOCKER_COMPOSE_FILE = -f .docker/crowdin/docker-compose.yml
trans_push_translations: DOCKER_COMPOSE_FILE = -f .docker/crowdin/docker-compose.yml
trans_pull:
	@$(CROWDIN) pull -T $(CROWDIN_API_TOKEN)
	@$(CROWDIN) pull sources -T ${CROWDIN_API_TOKEN}

trans_push_source:
	@$(CROWDIN) push sources -T ${CROWDIN_API_TOKEN}

trans_push_translations:
	$(CROWDIN) push translations -T $(CROWDIN_API_TOKEN)

trans_push: trans_push_source trans_push_translations

generate-certificate:
	openssl genrsa -out build/key.pem 2048
	openssl req -new -key build/key.pem -out build/csr.pem -subj "/CN=localhost"
	openssl x509 -req -days 365 -in build/csr.pem -signkey build/key.pem -out build/cert.pem

serve:
	serve -s build --ssl-cert build/cert.pem --ssl-key build/key.pem

check_api:
	@echo "\033[1m\033[33m✭ Communo ✭\033[0m\033[0m API check : ${REACT_APP_DOCKER_API_ALIAS}..."
	@i=1; \
	while [ $$i -le 10 ]; do \
		${DOCKER_RUN} curl -I ${REACT_APP_DOCKER_API_ALIAS} 2>/dev/null | grep "200 OK" > /dev/null && echo "API is operational." && exit 0; \
		printf "\rAttempt $$i failed. Retrying in 1 second..."; \
		sleep 1; \
		printf "\r%*s\r" $(shell tput cols) ""; \
		i=$$(($$i + 1)); \
	done; \
	echo "\033[1m\033[33mCommuno\033[0m\033[0m API is not operational even after 10 attempts. communo/api needs to be ran before front. Exiting..."; \
	exit 1

graphql_codegen_watch: check_api
	@$(YARN) graphql-codegen --config codegen-api.ts --watch

codegen:
	@$(YARN) codegen

playwright:
	yarn playwright $c

playwright_test: c=test --ui
playwright_test: playwright
playwright_generate: c=codegen --ignore-https-errors http://localhost:3000
playwright_generate: playwright
playwright_update:
	yarn add -D @playwright/test@latest


#######################
## STORYBOOK COMMANDS #
#######################
STORYBOOK_PORT?=6006
storybook: export DOCKER_COMPOSE_FILE=-f .docker/storybook/docker-compose.yaml
storybook: docker_build stop up storybook_open logs ## run storybook

storybook_stop: export DOCKER_COMPOSE_FILE=-f .docker/storybook/docker-compose.yaml
storybook_stop: stop ## stop storybook
storybook_logs: export DOCKER_COMPOSE_FILE=-f .docker/storybook/docker-compose.yaml
storybook_logs: logs ## stop storybook
storybook_open:
	open http://localhost:$(STORYBOOK_PORT)
